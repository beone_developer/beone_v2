/*
select bsd.*
from beone_export_detail bed,
     beone_sales_detail bsd,
     beone_export_header beh
where bed.sales_detail_id = bsd.sales_detail_id
  and bed.export_header_id = beh.export_header_id
  and beh.jenis_bc = 30
  and coalesce(bsd.amount, 0) = 0;

update beone_export_detail as bed
set amount=bsd.amount,
    amount_sys=bsd.amount_sys
from beone_sales_detail bsd,
     beone_export_header beh
where bed.sales_detail_id = bsd.sales_detail_id
  and bed.export_header_id = beh.export_header_id
  and beh.jenis_bc = 30;
*/

SET session_replication_role = replica;

alter table beone_sales_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_sales_detail
    add if not exists amount_sys double precision not null default 0;

update beone_sales_detail
set price      = coalesce(price, 0),
    qty= coalesce(qty, 0),
    amount=coalesce(amount, 0),
    amount_sys = coalesce(price, 0) * coalesce(qty, 0)
where amount_sys = 0;

update beone_sales_detail
set amount = amount_sys
where coalesce(amount, 0) = 0;

update beone_sales_header as h
set grandtotal_sys = d.grandtotal_sys,
    grandtotal     = d.grandtotal
from (select sum(d.amount_sys) as grandtotal_sys, sum(d.amount) as grandtotal, d.sales_header_id
      from beone_sales_detail d
      group by d.sales_header_id) d
where h.sales_header_id = d.sales_header_id;

alter table beone_sales_detail
    alter column amount set not null;

alter table beone_sales_detail
    alter column price set not null;

alter table beone_sales_detail
    alter column qty set not null;

/*
update beone_export_detail as bed
set amount=bsd.amount,
    amount_sys=bsd.amount_sys
from beone_sales_detail bsd,
     beone_export_header beh
where bed.sales_detail_id = bsd.sales_detail_id
  and bed.export_header_id = beh.export_header_id
  and beh.jenis_bc = 30;
*/

alter table beone_export_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_export_detail
    add if not exists amount_sys double precision not null default 0;

update beone_export_detail
set price      = coalesce(price, 0),
    qty= coalesce(qty, 0),
    amount=coalesce(amount, 0),
    amount_sys = coalesce(price, 0) * coalesce(qty, 0)
where amount_sys = 0;

update beone_export_detail
set amount = amount_sys
where coalesce(amount, 0) = 0;

update beone_export_header as h
set sisabayar      = d.grandtotal,
    grandtotal_sys = d.grandtotal_sys,
    grandtotal     = d.grandtotal
from (select sum(d.amount_sys) as grandtotal_sys, sum(d.amount) as grandtotal, d.export_header_id
      from beone_export_detail d
      group by d.export_header_id) d
where h.export_header_id = d.export_header_id
  and jenis_bc = 30;

alter table beone_export_detail
    alter column amount set not null;

alter table beone_export_detail
    alter column price set not null;

alter table beone_export_detail
    alter column qty set not null;

-- NYALAKAN TRIGGER
SET session_replication_role = DEFAULT;


-- select * from beone_export_header
