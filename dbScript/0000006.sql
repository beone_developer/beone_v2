ALTER TABLE beone_konversi_stok_detail
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_inventory
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_export_detail
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_konversi_stok_header
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_import_detail
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_inventory
    ALTER COLUMN intvent_trans_no SET NOT NULL;
ALTER TABLE beone_inventory
    ALTER COLUMN trans_date SET NOT NULL;
ALTER TABLE beone_inventory
    ALTER COLUMN keterangan SET NOT NULL;

ALTER TABLE beone_export_detail
    DROP COLUMN IF EXISTS gudang__id;
ALTER TABLE beone_export_detail
    DROP COLUMN IF EXISTS gudang_2_id;
ALTER TABLE beone_received_import
    DROP COLUMN IF EXISTS gudang_id;

DROP FUNCTION IF EXISTS get_stok(integer, integer, date, varchar);

CREATE OR REPLACE FUNCTION get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date,
                                    p_intvent_trans_no varchar(200))
    RETURNS RECORD AS
$$
DECLARE
    res RECORD;
BEGIN
    select sum(x.stok_akhir) as stok, avg(hpp) hpp
    into res
    from (
             select 'saldo_awal' as type, coalesce(sum(it.saldo_qty), 0) stok_akhir, coalesce(sum(it.saldo_idr), 0) hpp
             from beone_item it
             where it.item_id = p_item_id
               and p_gudang_id = 1 --PATEN 1
             union
             select 'stok' as type, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
               and i.gudang_id = p_gudang_id
--                and i.trans_date <= p_trans_date
             union
             select 'transaksi' as type, coalesce(sum(i.qty_out) - sum(i.qty_in), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
--                and i.gudang_id = p_gudang_id
               and i.intvent_trans_no = p_intvent_trans_no) x;
    RETURN res;
END;
$$ LANGUAGE plpgsql;

DROP PROCEDURE IF EXISTS SET_STOK(integer, integer, date, varchar, varchar, double precision, double precision);

-- CREATE PROCEDURE SET STOK
CREATE OR REPLACE PROCEDURE set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date,
                                     p_intvent_trans_no varchar(200), p_keterangan varchar(200), p_qty_in float,
                                     p_qty_out float, price float)
    LANGUAGE plpgsql AS
$$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp float;
BEGIN
    --     SELECT get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) INTO v_stok_akhir;
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in    = p_qty_in,
                qty_out   = p_qty_out,
                gudang_id = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, trans_date, keterangan, qty_in, qty_out, sa_unit_price)
        SELECT p_intvent_trans_no, p_item_id, p_gudang_id, p_trans_date, p_keterangan, p_qty_in, p_qty_out, v_hpp;
    END IF;
END
$$;

-- CREATE TRIGGER beone_import_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_import_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_import_detail on beone_import_detail;

CREATE TRIGGER trg_create_beone_import_detail
    BEFORE INSERT OR UPDATE
    ON beone_import_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_import_detail();

-- CREATE TRIGGER beone_import_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_import_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = OLD.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_import_detail on beone_import_detail;

CREATE TRIGGER trg_delete_beone_import_detail
    BEFORE DELETE
    ON beone_import_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_import_detail();

-- CREATE TRIGGER beone_export_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_export_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
--
DROP TRIGGER IF EXISTS trg_create_beone_export_detail on beone_export_detail;

CREATE TRIGGER trg_create_beone_export_detail
    BEFORE INSERT OR UPDATE
    ON beone_export_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_export_detail();

-- CREATE TRIGGER beone_export_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_export_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = OLD.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_export_detail on beone_export_detail;

CREATE TRIGGER trg_delete_beone_export_detail
    BEFORE DELETE
    ON beone_export_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_export_detail();

-- CREATE TRIGGER beone_export_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_export_header() RETURNS trigger AS
$$
BEGIN
    update beone_export_detail set qty = qty where export_header_id = NEW.export_header_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
--
DROP TRIGGER IF EXISTS trg_create_beone_export_header on beone_export_header;

CREATE TRIGGER trg_create_beone_export_header
    BEFORE INSERT OR UPDATE
    ON beone_export_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_export_header();

-- CREATE TRIGGER beone_import_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_import_header() RETURNS trigger AS
$$
BEGIN
    update beone_import_detail set qty = qty where import_header_id = NEW.import_header_id;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
--
DROP TRIGGER IF EXISTS trg_create_beone_import_header on beone_import_header;

CREATE TRIGGER trg_create_beone_import_header
    BEFORE INSERT OR UPDATE
    ON beone_import_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_import_header();

-- CREATE TRIGGER beone_konversi_stok_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_konversi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty_real, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_konversi_stok_detail on beone_konversi_stok_detail;

CREATE TRIGGER trg_create_beone_konversi_stok_detail
    BEFORE INSERT OR UPDATE
    ON beone_konversi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_konversi_stok_detail();

-- CREATE TRIGGER beone_konversi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_konversi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_konversi_stok_detail on beone_konversi_stok_detail;

CREATE TRIGGER trg_delete_beone_konversi_stok_detail
    BEFORE DELETE
    ON beone_konversi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_konversi_stok_detail();

-- CREATE TRIGGER beone_konversi_stok_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_konversi_stok_header on beone_konversi_stok_header;

CREATE TRIGGER trg_create_beone_konversi_stok_header
    BEFORE INSERT OR UPDATE
    ON beone_konversi_stok_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_konversi_stok_header();

-- CREATE TRIGGER beone_konversi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = OLD.konversi_stok_header_id;
    v_trans_no = OLD.konversi_stok_no;
    v_trans_date = OLD.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_konversi_stok_header on beone_konversi_stok_header;

CREATE TRIGGER trg_delete_beone_konversi_stok_header
    BEFORE DELETE
    ON beone_konversi_stok_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_konversi_stok_header();
