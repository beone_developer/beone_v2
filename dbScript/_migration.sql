insert into beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan,
                                  coa_id_cash_bank, coa_no, update_by, update_date, deleted_at,
                                  created_at, updated_at)
SELECT voucher_header_id,
       voucher_number,
       voucher_date,
       tipe,
       keterangan,
       coa_id_cash_bank,
       coa_no,
       update_by,
       update_date,
       deleted_at,
       created_at,
       updated_at
FROM dblink('dbname=dbname user=postgres password=root',
            'select voucher_header_id, voucher_number, voucher_date, tipe, keterangan,
                    coa_id_cash_bank, coa_no, update_by, update_date, deleted_at,
                    created_at, updated_at FROM beone_voucher_header')
         AS t1(voucher_header_id int, voucher_number varchar(50),
               voucher_date date,
               tipe integer,
               keterangan varchar(200),
               coa_id_cash_bank integer,
               coa_no varchar(20),
               update_by integer,
               update_date date,
               deleted_at timestamp with time zone,
               created_at timestamp with time zone,
               updated_at timestamp with time zone);

insert into beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan,
                                  jumlah_valas, kurs, jumlah_idr, keterangan_detail, currency_id)
SELECT voucher_detail_id,
       voucher_header_id,
       coa_id_lawan,
       coa_no_lawan,
       jumlah_valas,
       kurs,
       jumlah_idr,
       keterangan_detail,
       currency_id
FROM dblink('dbname=dbname user=postgres password=root',
            'select voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan,
                    jumlah_valas, kurs, jumlah_idr, keterangan_detail, currency_id FROM beone_voucher_detail')
         AS t1(voucher_detail_id integer, voucher_header_id integer,
               coa_id_lawan integer,
               coa_no_lawan varchar(20),
               jumlah_valas double precision,
               kurs double precision,
               jumlah_idr double precision,
               keterangan_detail varchar(200),
               currency_id integer);

insert into beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk,
                       tipe_transaksi)
SELECT coa_id,
       nama,
       nomor,
       tipe_akun,
       debet_valas,
       debet_idr,
       kredit_valas,
       kredit_idr,
       dk,
       tipe_transaksi
FROM dblink('dbname=beone_byi user=postgres password=sur4b4y4123',
            'select coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi FROM beone_coa')
         AS t1(coa_id int,
               nama varchar(50),
               nomor varchar(50),
               tipe_akun integer,
               debet_valas double precision,
               debet_idr double precision,
               kredit_valas double precision,
               kredit_idr double precision,
               dk varchar,
               tipe_transaksi integer)
where not exists(select x.coa_id from beone_coa x where x.nomor = t1.nomor);
