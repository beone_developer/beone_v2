create or replace function trg_create_beone_transfer_stock_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (lower(NEW.tipe_transfer_stock) = 'bb') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
        elsif (lower(NEW.tipe_transfer_stock) = 'wip') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;
