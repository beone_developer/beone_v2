drop procedure if exists set_stok(integer, integer, date, varchar, varchar, double precision, double precision, double precision);

create or replace procedure set_stok(p_item_id integer, p_gudang_id integer,
                                     p_intvent_trans_detail_id integer, p_trans_date date,
                                     p_intvent_trans_no character varying, p_keterangan character varying,
                                     p_qty_in double precision, p_qty_out double precision, price double precision)
    language plpgsql
as
$$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp        float;
BEGIN
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
      and i.item_id = p_item_id
      and i.gudang_id = p_gudang_id
--       and i.invent_trans_detail_id = p_intvent_trans_detail_id
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in        = p_qty_in,
                qty_out       = p_qty_out,
                gudang_id     = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id
              and invent_trans_detail_id = p_intvent_trans_detail_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, invent_trans_detail_id, trans_date,
                                     keterangan, qty_in, qty_out,
                                     price_hpp)
        SELECT p_intvent_trans_no,
               p_item_id,
               p_gudang_id,
               p_intvent_trans_detail_id,
               p_trans_date,
               p_keterangan,
               p_qty_in,
               p_qty_out,
               v_hpp;
    END IF;
END
$$;

-- CREATE TRIGGER beone_sub_kontraktor_out_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_sub_kontraktor_out_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_out_header_id, trans_no, trans_date, 'SUBKONTRAKTOR OUT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_out_header
    where sub_kontraktor_out_header_id = NEW.sub_kontraktor_out_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_out_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_sub_kontraktor_out_detail on beone_sub_kontraktor_out_detail;

CREATE TRIGGER trg_create_beone_sub_kontraktor_out_detail
    BEFORE INSERT OR UPDATE
    ON beone_sub_kontraktor_out_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_sub_kontraktor_out_detail();

-- CREATE TRIGGER beone_sub_kontraktor_out_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_sub_kontraktor_out_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_out_header_id, trans_no, trans_date, 'SUBKONTRAKTOR OUT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_out_header
    where sub_kontraktor_out_header_id = OLD.sub_kontraktor_out_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, NEW.sub_kontraktor_out_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_sub_kontraktor_out_detail on beone_sub_kontraktor_out_detail;

CREATE TRIGGER trg_delete_beone_sub_kontraktor_out_detail
    BEFORE DELETE
    ON beone_sub_kontraktor_out_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_sub_kontraktor_out_detail();

-- CREATE TRIGGER beone_sub_kontraktor_out_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_sub_kontraktor_out_header() RETURNS trigger AS
$$
BEGIN
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
--
DROP TRIGGER IF EXISTS trg_create_beone_sub_kontraktor_out_header on beone_sub_kontraktor_out_header;

CREATE TRIGGER trg_create_beone_sub_kontraktor_out_header
    BEFORE INSERT OR UPDATE
    ON beone_sub_kontraktor_out_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_sub_kontraktor_out_header();

-- CREATE TRIGGER beone_sub_kontraktor_in_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_sub_kontraktor_in_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_in_header_id, trans_no, trans_date, 'SUBKONTRAKTOR IN' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_in_header
    where sub_kontraktor_in_header_id = NEW.sub_kontraktor_in_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_in_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_sub_kontraktor_in_detail on beone_sub_kontraktor_in_detail;

CREATE TRIGGER trg_create_beone_sub_kontraktor_in_detail
    BEFORE INSERT OR UPDATE
    ON beone_sub_kontraktor_in_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_sub_kontraktor_in_detail();

-- CREATE TRIGGER beone_sub_kontraktor_in_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_sub_kontraktor_in_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_in_header_id, trans_no, trans_date, 'SUBKONTRAKTOR IN' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_in_header
    where sub_kontraktor_in_header_id = OLD.sub_kontraktor_in_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, NEW.sub_kontraktor_in_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_sub_kontraktor_in_detail on beone_sub_kontraktor_in_detail;

CREATE TRIGGER trg_delete_beone_sub_kontraktor_in_detail
    BEFORE DELETE
    ON beone_sub_kontraktor_in_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_sub_kontraktor_in_detail();

-- CREATE TRIGGER beone_sub_kontraktor_in_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_sub_kontraktor_in_header() RETURNS trigger AS
$$
BEGIN
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
--
DROP TRIGGER IF EXISTS trg_create_beone_sub_kontraktor_in_header on beone_sub_kontraktor_in_header;

CREATE TRIGGER trg_create_beone_sub_kontraktor_in_header
    BEFORE INSERT OR UPDATE
    ON beone_sub_kontraktor_in_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_sub_kontraktor_in_header();

CREATE OR REPLACE function trg_create_beone_export_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.export_detail_id, v_trans_date, v_trans_no, v_keterangan, 0,
                      NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$;

CREATE OR REPLACE function trg_create_beone_export_header() returns trigger
    language plpgsql
as
$$
BEGIN
    update beone_export_detail set qty = qty where export_header_id = NEW.export_header_id;
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;

CREATE OR REPLACE function trg_create_beone_import_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEw.import_detail_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty,
                      0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;

CREATE OR REPLACE function trg_create_beone_import_header() returns trigger
    language plpgsql
as
$$
BEGIN
    update beone_import_detail set qty = qty where import_header_id = NEW.import_header_id;
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_konversi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.konversi_stok_detail_id, v_trans_date, v_trans_no, v_keterangan,
                      0, NEW.qty_real, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_konversi_stok_header() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0,
                      0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_mutasi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = NEW.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id_asal, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty, 0);
        call set_stok(NEW.item_id, NEW.gudang_id_tujuan, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty, 0, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_transfer_stock_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (lower(NEW.tipe_transfer_stock) = 'bb') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, 0, NEW.qty, 0);
        elsif (lower(NEW.tipe_transfer_stock) = 'wip') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, NEW.qty, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_delete_beone_export_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = OLD.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.export_detail_id, v_trans_date, v_trans_no, v_keterangan, 0, 0,
                      0);
    end if;

    RETURN OLD;
END;
$$;

create or replace function trg_delete_beone_import_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = OLD.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.import_detail_id, v_trans_date, v_trans_no, v_keterangan, 0, 0,
                      0);
    end if;

    RETURN OLD;
END;
$$;

create or replace function trg_delete_beone_konversi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.konversi_stok_detail_id, v_trans_date, v_trans_no, v_keterangan,
                      0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;

create or replace function trg_delete_beone_konversi_stok_header() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = OLD.konversi_stok_header_id;
    v_trans_no = OLD.konversi_stok_no;
    v_trans_date = OLD.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;

create or replace function trg_delete_beone_mutasi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = OLD.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id_tujuan, OLD.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, OLD.qty, 0);
        call set_stok(OLD.item_id, OLD.gudang_id_asal, OLD.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, OLD.qty, 0, 0);
    end if;

    RETURN OLD;
END;
$$;

create or replace function trg_delete_beone_transfer_stock_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = OLD.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, cast(OLD.transfer_stock_detail_id as int), v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;

