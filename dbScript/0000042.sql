create or replace function trg_create_beone_export_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.export_detail_id, v_trans_date, v_trans_no, v_keterangan, 0,
                      NEW.qty*NEW.rasio, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_import_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEw.import_detail_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty*NEW.rasio,
                      0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_konversi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.konversi_stok_detail_id, v_trans_date, v_trans_no, v_keterangan,
                      0, NEW.qty_real*NEW.rasio, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_konversi_stok_header() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real*NEW.rasio, 0, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_mutasi_stok_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = NEW.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id_asal, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty*NEW.rasio, 0);
        call set_stok(NEW.item_id, NEW.gudang_id_tujuan, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty*NEW.rasio, 0, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_sub_kontraktor_in_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_in_header_id, trans_no, trans_date, 'SUBKONTRAKTOR IN' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_in_header
    where sub_kontraktor_in_header_id = NEW.sub_kontraktor_in_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_in_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty*NEW.rasio, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_sub_kontraktor_out_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_out_header_id, trans_no, trans_date, 'SUBKONTRAKTOR OUT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_out_header
    where sub_kontraktor_out_header_id = NEW.sub_kontraktor_out_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_out_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty*NEW.rasio, 0);
    end if;

    RETURN NEW;
END;
$$;

create or replace function trg_create_beone_transfer_stock_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (lower(NEW.tipe_transfer_stock) = 'bb') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, 0, NEW.qty*NEW.rasio, 0);
        elsif (lower(NEW.tipe_transfer_stock) = 'wip') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, NEW.qty*NEW.rasio, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;

--triggere delete usage
create or replace function trg_delete_beone_usage_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select usage_header_id, trans_no, trans_date, 'USAGE' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_usage_header
    where usage_header_id = OLD.usage_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.usage_detail_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;

-------------------
DROP TRIGGER IF EXISTS trg_delete_beone_usage_detail on beone_usage_detail;

CREATE TRIGGER trg_delete_beone_usage_detail
    BEFORE DELETE
    ON beone_usage_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_usage_detail();

---------------

--trigger create usage detail
create or replace function trg_create_beone_usage_detail() returns trigger
    language plpgsql
as
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select usage_header_id, trans_no, trans_date, 'USAGE' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_usage_header
    where usage_header_id = NEW.usage_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.usage_detail_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_in*NEW.rasio,
                      NEW.qty_out*NEW.rasio, 0);
    end if;

    RETURN NEW;
END;
$$;

--------------------

DROP TRIGGER IF EXISTS trg_create_beone_usage_detail on beone_usage_detail;

CREATE TRIGGER trg_create_beone_usage_detail
    BEFORE INSERT OR UPDATE
    ON beone_usage_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_usage_detail();
