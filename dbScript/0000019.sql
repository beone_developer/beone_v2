ALTER TABLE public.beone_item
    ADD COLUMN IF NOT EXISTS level_item integer NOT NULL DEFAULT 3;
--keterangan :
-- level 0 = bahan baku
-- level 1 = bahan ?
-- level 2 = bahan setengah jadi
-- level 3 = bahan jadi