alter table beone_item
    add if not exists note varchar(500);

alter table beone_customer
    add if not exists note varchar(500);

alter table beone_supplier
    add if not exists note varchar(500);

alter table beone_gudang
    add if not exists note varchar(500);

alter table beone_coa
    add if not exists note varchar(500);

alter table beone_po_import_header
    add if not exists note varchar(500);

alter table beone_sales_header
    add if not exists note varchar(500);

alter table beone_import_detail
    drop if exists item_type_id;

alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_purchase_detail_purchase_detail_id_fk;
