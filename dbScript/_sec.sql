BEGIN;
CREATE OR REPLACE FUNCTION reset_sequence(_table_schema text, _tablename text, _columnname text, _sequence_name text)
    RETURNS VOID AS
$BODY$
DECLARE
BEGIN
    PERFORM 1
    FROM information_schema.sequences
    WHERE sequence_schema = _table_schema
      AND sequence_name = _sequence_name;
    IF FOUND THEN
        EXECUTE 'SELECT setval( ''' || _table_schema || '.' || _sequence_name || ''', ' || '(SELECT MAX(' ||
                _columnname || ') FROM ' || _table_schema || '.' || _tablename || ')' || '+1)';
    ELSE
        RAISE WARNING 'SEQUENCE NOT UPDATED ON %.%', _tablename, _columnname;
    END IF;
END;
$BODY$
    LANGUAGE 'plpgsql';

SELECT reset_sequence(table_schema, table_name, column_name, table_name || '_' || column_name || '_seq')
FROM information_schema.columns
WHERE column_default LIKE 'nextval%';

DROP FUNCTION reset_sequence(_table_schema text, _tablename text, _columnname text, _sequence_name text);
COMMIT;
