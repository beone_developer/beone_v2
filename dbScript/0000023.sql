ALTER TABLE beone_konfigurasi_perusahaan
    ADD IF NOT EXISTS cabang VARCHAR(8) not null default '00001/01';

alter table beone_voucher_header
    alter column voucher_number type varchar(50) using voucher_number::varchar(50);

create unique index if not exists beone_item_item_code_uindex
    on beone_item (item_code);

create unique index if not exists beone_satuan_item_item_id_rasio_uindex
    on beone_satuan_item (item_id, rasio);

alter table beone_export_header
    add column if not exists currency_id integer not null default 1;

alter table beone_user
    drop if exists password_v2;

alter table beone_user
    add if not exists password_old varchar(1000) not null default ('');

update beone_user
set password_old=password,
    password    = '$2y$10$dpGQpU726QkaKzMb8mwyzuIqMUJfDZD6xz4SauED6ypXeWsGNhrlS'
where coalesce(password_old, '') = '';

alter table beone_export_detail
    add if not exists sales_detail_id int;

ALTER TABLE beone_user
    alter column password set not null;

create unique index if not exists beone_user_username_uindex
    on beone_user (username);

ALTER TABLE beone_user
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_gudang
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_item
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_coa
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_custsup
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_item_type
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_role
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_po_import_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_export_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_konversi_stok_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_opname_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_pemakaian_bahan_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_pm_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_produksi_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_purchase_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_sales_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_subkon_out_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_voucher_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_transfer_stock
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_import_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_voucher_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

ALTER TABLE beone_mutasi_stok_header
    alter column deleted_at type timestamp with time zone using deleted_at::timestamp with time zone;

alter table beone_import_header
    add column if not exists bc_header_id integer;

alter table beone_import_detail
    add column if not exists bc_barang_id integer;
