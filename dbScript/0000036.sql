alter table beone_modul
    drop constraint if exists beone_modul_url_key;

alter table beone_modul
    add if not exists parent_id integer;

alter table beone_modul
    add if not exists is_hidden integer not null default 0;

alter table beone_modul
    add if not exists icon_class varchar(100);

alter table beone_roles_modul
    add if not exists parent_id int;

alter table beone_roles_modul
    add if not exists is_hidden integer not null default 0;

alter table beone_roles_user
    drop if exists modul_id;

create unique index if not exists beone_roles_modul_role_id_modul_id_uindex
    on beone_roles_modul (role_id, modul_id);
