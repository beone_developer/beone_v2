ALTER TABLE public.beone_sales_header
    ADD COLUMN IF NOT EXISTS realisasi integer NOT NULL DEFAULT 0;
	
ALTER TABLE public.beone_export_detail
    ADD COLUMN IF NOT EXISTS sales_header_id integer;

ALTER TABLE public.beone_purchase_header
    ADD COLUMN IF NOT EXISTS realisasi integer NOT NULL DEFAULT 0;