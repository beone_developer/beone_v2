CREATE TABLE IF NOT EXISTS beone_stok_opname_header
(
    stok_opname_header_id SERIAL PRIMARY KEY,
    stok_opname_no        VARCHAR(50) not null,
    trans_date            date        not null,
    keterangan            VARCHAR(255),
    approved_by           timestamp with time zone,
    approved_at           timestamp with time zone,
    deleted_at            timestamp with time zone,
    created_at            timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at            timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_stok_opname_detail
(
    stok_opname_detail_id SERIAL PRIMARY KEY,
    stok_opname_header_id int              not null,
    item_id               int              not null,
    qty                   double precision not null,
    gudang_id             int              not null
);