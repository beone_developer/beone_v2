CREATE TABLE IF NOT EXISTS beone_jurnal_header
(
    jurnal_header_id SERIAL PRIMARY KEY,
    jurnal_no        VARCHAR(50) not null,
    keterangan       VARCHAR(255),
    trans_date       date        not null,
    deleted_at       timestamp with time zone,
    created_at       timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at       timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_jurnal_detail
(
    jurnal_detail_id  SERIAL PRIMARY KEY,
    jurnal_header_id  INTEGER          not null,
    coa_id            INTEGER          not null,
    amount            DOUBLE PRECISION not null DEFAULT 0,
    kurs              DOUBLE PRECISION not null DEFAULT 1,
    amount_idr        DOUBLE PRECISION not null DEFAULT 0,
    keterangan_detail VARCHAR(255),
    status            VARCHAR(1)       not null,
    deleted_at        timestamp with time zone,
    created_at        timestamp with time zone  DEFAULT CURRENT_TIMESTAMP,
    updated_at        timestamp with time zone  DEFAULT CURRENT_TIMESTAMP
);
alter table beone_sales_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_sales_detail
    add if not exists amount_sys double precision not null default 0;

update beone_sales_detail
set amount_sys = price * qty
where amount_sys = 0;

update beone_sales_header as h
set grandtotal_sys = d.grandtotal_sys,
    grandtotal=d.grandtotal
from (select sum(d.amount_sys) as grandtotal_sys, sum(d.amount) as grandtotal, d.sales_header_id
      from beone_sales_detail d
      group by d.sales_header_id) d
where h.sales_header_id = d.sales_header_id;

alter table beone_sales_header
    add column if not exists currency_id integer not null default 1;

alter table beone_po_import_header
    add column if not exists currency_id integer not null default 1;

alter table beone_po_import_header
    add column if not exists kurs integer not null default 1;

alter table beone_sales_header
    add column if not exists kurs integer not null default 1;

alter table beone_export_header
    add column if not exists kurs integer not null default 1;

alter table beone_inventory
    add if not exists price_hpp double precision not null default 0;

alter table beone_export_detail
    add if not exists price_hpp double precision not null default 0;

alter table beone_konversi_stok_detail
    add if not exists price_hpp double precision not null default 0;

alter table beone_konversi_stok_header
    add if not exists price_hpp double precision not null default 0;

alter table beone_transfer_stock_detail
    add if not exists price_hpp double precision not null default 0;

alter table beone_jurnal_detail
    add column if not exists currency_id integer not null default 1;

alter table beone_voucher_detail
    add column if not exists currency_id integer not null default 1;

