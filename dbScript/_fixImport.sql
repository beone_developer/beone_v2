-- MATIKAN TRIGGER
SET session_replication_role = replica;

alter table beone_po_import_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_po_import_detail
    add if not exists amount_sys double precision not null default 0;

update beone_po_import_detail
set price      = coalesce(price, 0),
    qty= coalesce(qty, 0),
    amount=coalesce(amount, 0),
    amount_sys = coalesce(price, 0) * coalesce(qty, 0)
where coalesce(amount_sys,0) = 0;

update beone_po_import_detail
set amount = amount_sys
where coalesce(amount,0) = 0;

update beone_po_import_header as h
set grandtotal_sys = d.grandtotal_sys,
    grandtotal     = d.grandtotal
from (select sum(d.amount_sys) as grandtotal_sys, sum(d.amount) as grandtotal, d.purchase_header_id
      from beone_po_import_detail d
      group by d.purchase_header_id) d
where h.purchase_header_id = d.purchase_header_id;

alter table beone_po_import_detail
    alter column amount set not null;

alter table beone_po_import_detail
    alter column price set not null;

alter table beone_po_import_detail
    alter column qty set not null;

alter table beone_import_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_import_header
    add if not exists dpp_sys double precision not null default 0;

alter table beone_import_header
    add if not exists ppn_sys double precision not null default 0;

alter table beone_import_detail
    add if not exists amount double precision not null default 0;

alter table beone_import_detail
    add if not exists amount_sys double precision not null default 0;

alter table beone_import_detail
    add if not exists amount_ppn double precision not null default 0;

alter table beone_import_detail
    add if not exists amount_ppn_sys double precision not null default 0;

update beone_import_detail
set price          = coalesce(price, 0),
    qty= coalesce(qty, 0),
    amount_sys     = coalesce(price, 0) * coalesce(qty, 0),
    amount_ppn_sys = (coalesce(price, 0) * coalesce(qty, 0)) * case when (isppn = 1) then 1.1 else 1 end
where amount_sys = 0;

update beone_import_detail
set amount     = amount_sys,
    amount_ppn = amount_sys * case when (isppn = 1) then 1.1 else 1 end
where amount = 0;

update beone_import_header as h
set grandtotal_sys = (d.dpp_sys - coalesce(discount, 0)) + d.ppn_sys,
    grandtotal     = (d.dpp - coalesce(discount, 0)) + d.ppn,
    sisabayar      = (d.dpp - coalesce(discount, 0)) + d.ppn,
    dpp=d.dpp,
    dpp_sys=d.dpp_sys,
    ppn=d.ppn,
    ppn_sys=d.ppn_sys
from (select sum(d.amount_sys)                    as dpp_sys,
             sum(d.amount)                        as dpp,
             sum(d.amount_ppn_sys)                as subtotal_sys,
             sum(d.amount_ppn)                    as subtotal,
             sum(d.amount_ppn - d.amount)         as ppn,
             sum(d.amount_ppn_sys - d.amount_sys) as ppn_sys,
             d.import_header_id
      from beone_import_detail d
      group by d.import_header_id) d
where h.import_header_id = d.import_header_id;

alter table beone_import_detail
    alter column amount set not null;

alter table beone_import_detail
    alter column price set not null;

alter table beone_import_detail
    alter column qty set not null;

-- NYALAKAN TRIGGER
SET session_replication_role = DEFAULT;

