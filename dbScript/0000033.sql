CREATE TABLE IF NOT EXISTS beone_modul
(
    modul_id   INT PRIMARY KEY,
    modul      VARCHAR(45) UNIQUE,
    url        VARCHAR(1000)
);

CREATE TABLE IF NOT EXISTS beone_roles_modul
(
    role_modul_id SERIAL PRIMARY KEY,
    role_id       INT,
    modul_id      INT
);
