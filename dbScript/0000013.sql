ALTER TABLE beone_satuan_item
    ADD IF NOT EXISTS item_id INTEGER;

ALTER TABLE beone_satuan_item
    ADD IF NOT EXISTS rasio DOUBLE PRECISION DEFAULT 1;
