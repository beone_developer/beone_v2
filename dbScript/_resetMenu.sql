truncate beone_modul restart identity cascade;
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (1, 'Sistem', '#', 'fa fa-cog', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (2, 'Master', '#', 'fa fa-server', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (3, 'Import', '#', 'fa fa-shopping-cart', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (4, 'Export', '#', 'fa fa-truck-moving', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (5, 'Sub Kontraktor', '#', 'fa fa-file-contract', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (6, 'Inventory', '#', 'fa fa-boxes', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (7, 'Produksi', '#', 'fa fa-industry', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (8, 'Account Receivable', '#', 'fa fa-money-bill', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (9, 'Account Payable', '#', 'fa fa-money-bill', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (10, 'Keuangan', '#', 'fa fa-money-bill', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (11, 'Laporan', '#', 'fa fa-chart-bar', 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (12, 'Purchase Order', '#', null, 0, 3);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (13, 'Kedatangan Barang', '#', null, 0, 3);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (14, 'Sales Order', '#', null, 0, 4);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (15, 'Pengiriman Barang', '#', null, 0, 4);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (16, 'Subkontraktor In', '#', null, 0, 5);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (17, 'Subkontraktor Out', '#', null, 0, 5);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (18, 'Mutasi Stok', '#', null, 0, 6);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (19, 'Opname Stok', '#', null, 0, 6);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (20, 'Bill Of Material', '#', null, 0, 7);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (21, 'Pelunasan Piutang', '#', null, 0, 9);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (0, 'Home', 'home', null, 0, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (22, 'Pelunasan Hutang', '#', null, 0, 8);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (23, 'Kas & Bank', '#', null, 0, 10);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (24, 'Jurnal Umum', '#', null, 0, 10);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (25, 'Lap. Keuangan', '#', null, 0, 11);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (26, 'Lap. Inventory', '#', null, 0, 11);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (27, 'Lap. Beacukai', '#', null, 0, 11);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (29, 'Rekal Stok', 'rekal.stok', null, 0, 1);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (30, 'Rekal Gl', 'rekal.gl', null, 0, 1);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (31, 'User Detail', 'user.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (32, 'User List', 'user.list', null, 0, 1);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (33, 'Role Detail', 'role.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (34, 'Role List', 'role.list', null, 0, 1);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (35, 'Role User Detail', 'role_user.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (36, 'Role User List', 'role_user.list', null, 0, 1);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (37, 'Item Detail', 'item.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (38, 'Item List', 'item.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (39, 'Jenis Item Detail', 'jenis_item.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (40, 'Jenis Item List', 'jenis_item.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (41, 'Satuan Item Detail', 'satuan_item.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (42, 'Satuan Item List', 'satuan_item.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (43, 'Customer Detail', 'customer.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (44, 'Customer List', 'customer.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (45, 'Supplier Detail', 'supplier.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (46, 'Supplier List', 'supplier.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (47, 'Gudang Detail', 'gudang.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (48, 'Gudang List', 'gudang.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (49, 'Coa Detail', 'coa.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (50, 'Coa List', 'coa.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (51, 'Komposisi Detail', 'komposisi.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (52, 'Komposisi List', 'komposisi.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (53, 'Po Import Detail', 'po_import.detail', null, 0, 12);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (54, 'Po Import List', 'po_import.list', null, 0, 12);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (55, 'Receive Import Outstanding', 'receive_import.outstanding', null, 0, 13);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (56, 'Receive Import Terima', 'receive_import.terima', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (57, 'Receive Import Detail', 'receive_import.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (58, 'Receive Import List', 'receive_import.list', null, 0, 13);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (59, 'So Export Detail', 'so_export.detail', null, 0, 14);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (60, 'So Export List', 'so_export.list', null, 0, 14);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (61, 'Export Detail', 'export.detail', null, 0, 15);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (62, 'Export List', 'export.list', null, 0, 15);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (63, 'Mutasi Barang Detail', 'mutasi_barang.detail', null, 0, 18);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (64, 'Mutasi Barang List', 'mutasi_barang.list', null, 0, 18);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (65, 'Stok Opname Detail', 'stok_opname.detail', null, 0, 19);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (66, 'Stok Opname List', 'stok_opname.list', null, 0, 19);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (68, 'Konversi Stok Detail', 'konversi_stok.detail', null, 0, 95);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (69, 'Konversi Stok List', 'konversi_stok.list', null, 0, 95);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (70, 'Bom Detail', 'bom.detail', null, 0, 20);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (71, 'Bom List', 'bom.list', null, 0, 20);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (72, 'Sub Kontraktor Out Outstanding', 'sub_kontraktor.out.outstanding', null, 0, 17);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (73, 'Sub Kontraktor Out Kirim', 'sub_kontraktor.out.kirim', null, 0, 17);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (74, 'Sub Kontraktor In Outstanding', 'sub_kontraktor.in.outstanding', null, 0, 16);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (75, 'Sub Kontraktor In Terima', 'sub_kontraktor.in.terima', null, 0, 16);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (77, 'Pelunasan Piutang Detail', 'pelunasan_piutang.detail', null, 0, 21);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (78, 'Pelunasan Piutang List', 'pelunasan_piutang.list', null, 0, 21);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (80, 'Pelunasan Hutang Detail', 'pelunasan_hutang.detail', null, 0, 22);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (81, 'Pelunasan Hutang List', 'pelunasan_hutang.list', null, 0, 22);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (82, 'Kasbank Detail', 'kasbank.detail', null, 0, 23);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (83, 'Kasbank List', 'kasbank.list', null, 0, 23);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (84, 'Jurnal Umum Detail', 'jurnal_umum.detail', null, 0, 24);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (85, 'Jurnal Umum List', 'jurnal_umum.list', null, 0, 24);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (86, 'Report Buku Besar', 'report.buku_besar', null, 0, 25);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (87, 'Report Kartu Stok', 'report.kartu_stok', null, 0, 26);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (88, 'Report Pb Pemasukan', 'report.pb_pemasukan', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (89, 'Report Pb Pengeluaran', 'report.pb_pengeluaran', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (90, 'Report Pb Mutasi Wip', 'report.pb_mutasi_wip', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (91, 'Report Pb Mutasi Bahan Baku', 'report.pb_mutasi_bahan_baku', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (92, 'Report Pb Mutasi Scrap', 'report.pb_mutasi_scrap', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (93, 'Report Pb Mutasi Barang Jadi', 'report.pb_mutasi_barang_jadi', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (94, 'Report Pb Mutasi Mesin', 'report.pb_mutasi_mesin', null, 0, 27);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (95, 'Konversi Stok', '#', null, 0, 7);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (96, 'Usage', '#', null, 0, 6);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (97, 'Usage Detail', 'usage.detail', null, 0, 96);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (98, 'Usage List', 'usage.list', null, 0, 96);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (99, 'Subkontraktor Out List', 'sub_kontraktor.out.list', null, 0, 17);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (100, 'Subkontraktor In List', 'sub_kontraktor.in.list', null, 0, 16);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (101, 'Kategori Item Detail', 'kategori_item.detail', null, 1, null);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (102, 'Kategori Item List', 'kategori_item.list', null, 0, 2);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (103, 'BC 25', 'export.outstanding_25', null, 0, 15);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (104, 'BC 27', 'export.outstanding_27', null, 0, 15);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (105, 'BC 41', 'export.outstanding_41', null, 0, 15);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (106, 'Report Neraca Mutasi', 'report.neraca_mutasi', null, 0, 25);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (107, 'Lap. Customer', '#', null, 0, 11);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (108, 'Lap. Supplier', '#', null, 0, 11);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (109, 'Report Piutang', 'report.customer_piutang', null, 0, 107);
INSERT INTO beone_modul (modul_id, modul, url, icon_class, is_hidden, parent_id)
VALUES (110, 'Report Hutang', 'report.supplier_hutang', null, 0, 108);
-- commit

truncate beone_roles_modul restart identity;
insert into beone_roles_modul (role_id, modul_id, parent_id)
select 1, modul_id, parent_id
from beone_modul m
where not exists(select * from beone_roles_modul r where m.modul_id = r.modul_id and r.role_id = 1);

insert into beone_roles_modul (role_id, modul_id, parent_id)
select 2, modul_id, parent_id
from beone_modul m
where not exists(select * from beone_roles_modul r where m.modul_id = r.modul_id and r.role_id = 2)
  and (m.modul ilike '%report pb%' or m.modul ilike '%lap. beacukai%' or m.modul ilike '%laporan%');
