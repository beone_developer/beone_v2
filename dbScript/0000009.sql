create table if not exists beone_item_category
(
    item_category_id int primary key,
    nama             varchar(100),
    keterangan       varchar(200),
    flag             int
);

insert into beone_item_category (item_category_id, nama, keterangan, flag)
select 1, 'UMUM', 'UMUM', 1
where not exists(select * from beone_item_category where beone_item_category.item_category_id = 1);

alter table beone_item
    add column if not exists item_category_id int not null default 1;
