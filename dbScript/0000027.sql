drop table if exists temp_satuan;
select s.satuan_code as satuan_code,s.satuan_code as keterangan,1 as flag,i.item_id as item_id, 1 as rasio
into temp_satuan
from beone_item i
join beone_satuan_item s on i.satuan_id = s.satuan_id;

insert into beone_satuan_item(satuan_code, keterangan, flag, item_id,rasio)
select * from temp_satuan i where not exists(select * from beone_satuan_item s where i.item_id =s.item_id and i.rasio = s.rasio)
