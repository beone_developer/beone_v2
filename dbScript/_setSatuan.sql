SET session_replication_role = replica;

drop table if exists temp_satuan_item;
drop table if exists temp_satuan_final;

select i.item_id, s.satuan_code
into temporary temp_satuan_item
from beone_item i,
     beone_satuan_item s
where i.satuan_id = s.satuan_id
union
select i.item_id, s.satuan_code
from beone_item i,
     beone_satuan_item s
where i.item_id = s.item_id
  and s.rasio = 1;

select item_id, max(satuan_code) as satuan_code
into temp_satuan_final
from temp_satuan_item
group by item_id;

delete from beone_satuan_item;
ALTER SEQUENCE beone_satuan_item_satuan_id_seq RESTART WITH 1;

insert into beone_satuan_item (satuan_code, keterangan, flag, item_id)
select satuan_code, '', 1, item_id
from temp_satuan_final;

-- PO
alter table beone_po_import_detail
    add if not exists satuan_id int;

alter table beone_po_import_detail
    add if not exists rasio double precision default 1 not null;

update beone_po_import_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_po_import_detail
    drop constraint if exists beone_po_import_detail_beone_satuan_item_satuan_id_fk;

alter table beone_po_import_detail
    alter column satuan_id set not null;

alter table beone_po_import_detail
    add constraint beone_po_import_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- IMPORT
alter table beone_import_detail
    add if not exists satuan_id int;

alter table beone_import_detail
    add if not exists rasio double precision default 1 not null;

update beone_import_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_satuan_item_satuan_id_fk;

alter table beone_import_detail
    alter column satuan_id set not null;

alter table beone_import_detail
    add constraint beone_import_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- SALES ORDER
alter table beone_sales_detail
    add if not exists satuan_id int;

alter table beone_sales_detail
    add if not exists rasio double precision default 1 not null;

update beone_sales_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_sales_detail
    drop constraint if exists beone_sales_detail_beone_satuan_item_satuan_id_fk;

alter table beone_sales_detail
    alter column satuan_id set not null;

alter table beone_sales_detail
    add constraint beone_sales_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- EXPORT
alter table beone_export_detail
    add if not exists satuan_id int;

alter table beone_export_detail
    add if not exists rasio double precision default 1 not null;

update beone_export_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk;

alter table beone_export_detail
    alter column satuan_id set not null;

alter table beone_export_detail
    add constraint beone_export_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- MUTASI
alter table beone_mutasi_stok_detail
    add if not exists satuan_id int;

alter table beone_mutasi_stok_detail
    add if not exists rasio double precision default 1 not null;

update beone_mutasi_stok_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_satuan_item_satuan_id_fk;

alter table beone_mutasi_stok_detail
    alter column satuan_id set not null;

alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- KONVERSI HEADER
alter table beone_konversi_stok_header
    add if not exists satuan_id int;

alter table beone_konversi_stok_header
    add if not exists rasio double precision default 1 not null;

update beone_konversi_stok_header as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_satuan_item_satuan_id_fk;

alter table beone_konversi_stok_header
    alter column satuan_id set not null;

alter table beone_konversi_stok_header
    add constraint beone_konversi_stok_header_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- KONVERSI DETAIL
alter table beone_konversi_stok_detail
    add if not exists satuan_id int;

alter table beone_konversi_stok_detail
    add if not exists rasio double precision default 1 not null;

update beone_konversi_stok_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_konversi_stok_detail
    drop constraint if exists beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk;

alter table beone_konversi_stok_detail
    alter column satuan_id set not null;

alter table beone_konversi_stok_detail
    add constraint beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

-- BOM
alter table beone_transfer_stock_detail
    add if not exists satuan_id int;

alter table beone_transfer_stock_detail
    add if not exists rasio double precision default 1 not null;

update beone_transfer_stock_detail as d
set satuan_id = s.satuan_id
from beone_satuan_item s
where d.item_id = s.item_id
  and s.rasio = 1;

alter table beone_transfer_stock_detail
    drop constraint if exists beone_transfer_stock_detail_beone_satuan_item_satuan_id_fk;

alter table beone_transfer_stock_detail
    alter column satuan_id set not null;

alter table beone_transfer_stock_detail
    add constraint beone_transfer_stock_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

SET session_replication_role = DEFAULT;
