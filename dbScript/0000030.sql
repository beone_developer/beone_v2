alter table beone_voucher_header
    add if not exists grandtotal double precision not null default 0;

update beone_voucher_detail
set jumlah_valas = 0,
    kurs         = 1
where coalesce(jumlah_valas, 0) = 0;

update beone_voucher_detail
set jumlah_idr = 0
where coalesce(jumlah_idr, 0) = 0;

update beone_voucher_detail
set jumlah_valas = 0,
    kurs         = 1
where coalesce(jumlah_valas, 0) = 0;

update beone_voucher_detail
set jumlah_idr = 0
where coalesce(jumlah_idr, 0) = 0;

update beone_voucher_detail
set jumlah_idr = jumlah_valas*kurs;

update beone_voucher_header as h
set grandtotal = d.grandtotal
from (select sum(d.jumlah_idr) as grandtotal, d.voucher_header_id
      from beone_voucher_detail d
      group by d.voucher_header_id) d
where h.voucher_header_id = d.voucher_header_id;

alter table beone_jurnal_header
    add if not exists grandtotal double precision not null default 0;

update beone_jurnal_header as h
set grandtotal = d.grandtotal
from (select sum(d.amount_idr) as grandtotal, d.jurnal_header_id
      from beone_jurnal_detail d
      where status = 'D'
      group by d.jurnal_header_id) d
where h.jurnal_header_id = d.jurnal_header_id;
