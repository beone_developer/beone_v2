alter table beone_gudang
    add if not exists gudang_code varchar(50) not null default '';

update beone_gudang set gudang_code = gudang_id;

create unique index if not exists beone_gudang_gudang_code_uindex
    on beone_gudang (gudang_code);

create table if not exists beone_general_ledger
(
    general_ledger_id serial primary key,
    coa_id            integer not null,
    trans_no          varchar(100),
    trans_date        date,
    keterangan        varchar(500),
    currency_id       integer,
    kurs              double precision,
    debet             double precision,
    kredit            double precision,
    debet_idr         double precision,
    kredit_idr        double precision
);
