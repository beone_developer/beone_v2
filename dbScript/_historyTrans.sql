create or replace function get_stok_history(p_trans_date_awal date, p_trans_date_akhir date)
    returns TABLE
            (
                r_trans_no   character varying,
                r_trans_date date,
                r_keterangan character varying,
                r_item_id    int,
                r_qty        double precision,
                r_rasio      double precision,
                r_satuan_id  int
            )
    language plpgsql
as
$$
BEGIN
    DROP TABLE IF EXISTS temp_trans;

    CREATE TEMP TABLE temp_trans AS
--         IMPORT
    SELECT beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           bed.item_id          as item_id,
           bed.qty              as qty,
           bed.rasio            as rasio,
           bed.satuan_id        as satuan_id,
           beh.deleted_at
    FROM beone_import_header beh
             join beone_import_detail bed on beh.import_header_id = bed.import_header_id
    WHERE coalesce(beh.receive_no, '') <> ''
      AND beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK HEADER
    SELECT beh.konversi_stok_date      as trans_date,
           beh.konversi_stok_header_id as trans_id,
           beh.konversi_stok_no        as trans_no,
           beh.konversi_stok_no        as keterangan,
           beh.item_id                 as item_id,
           beh.qty_real                as qty,
           beh.rasio                   as rasio,
           beh.satuan_id               as satuan_id,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
    WHERE beh.qty_real > 0
    union all
--         KONVERSI STOK DETAIL
    SELECT beh.konversi_stok_date      as trans_date,
           beh.konversi_stok_header_id as trans_id,
           beh.konversi_stok_no        as trans_no,
           beh.konversi_stok_no        as keterangan,
           bed.item_id                 as item_id,
           bed.qty_real                as qty,
           bed.rasio                   as rasio,
           bed.satuan_id               as satuan_id,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
             JOIN beone_konversi_stok_detail bed
                  on beh.konversi_stok_header_id = bed.konversi_stok_header_id
    WHERE bed.qty_real > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         BOM
    SELECT beh.transfer_date                                                                 as trans_date,
           beh.transfer_stock_id                                                             as trans_id,
           beh.transfer_no                                                                   as trans_no,
           beh.transfer_no                                                                   as keterangan,
           bed.item_id                                                                       as item_id,
           case lower(bed.tipe_transfer_stock) when 'wip' then bed.qty else bed.qty * -1 end as qty,
           bed.rasio                                                                         as rasio,
           bed.satuan_id                                                                     as satuan_id,
           beh.deleted_at
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE beh.transfer_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         EXPORT
    SELECT beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           bed.item_id          as item_id,
           bed.qty              as qty,
           bed.rasio            as rasio,
           bed.satuan_id        as satuan_id,
           beh.deleted_at
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.export_header_id = bed.export_header_id
    where beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         MUTASI STOK
    SELECT beh.trans_date            as trans_date,
           beh.mutasi_stok_header_id as trans_id,
           beh.mutasi_stok_no        as trans_no,
           beh.keterangan            as keterangan,
           bed.item_id               as item_id,
           bed.qty                   as qty,
           bed.rasio                 as rasio,
           bed.satuan_id             as satuan_id,
           beh.deleted_at
    FROM beone_mutasi_stok_header beh
             JOIN beone_mutasi_stok_detail bed
                  on beh.mutasi_stok_header_id = bed.mutasi_stok_header_id
    where beh.trans_date between p_trans_date_awal AND p_trans_date_akhir;

    RETURN QUERY SELECT cast(t.trans_no as varchar(50))    as trans_no,
                        t.trans_date,
                        cast(t.keterangan as varchar(100)) as keterangan,
                        t.item_id,
                        t.qty,
                        t.rasio,
                        t.satuan_id
                 FROM temp_trans t
                 where deleted_at is null;
END;
$$;
