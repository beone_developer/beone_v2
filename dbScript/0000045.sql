alter table beone_sub_kontraktor_out_header
    add if not exists keterangan varchar(500);

alter table beone_sub_kontraktor_in_header
    add if not exists keterangan varchar(500);

alter table beone_jurnal_detail
    drop constraint if exists beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk;

alter table beone_jurnal_detail
    add constraint beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk
        foreign key (jurnal_header_id) references beone_jurnal_header;

