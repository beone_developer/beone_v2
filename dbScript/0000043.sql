alter table beone_item_category
    add if not exists item_category_code varchar(50);

update beone_item_category
set item_category_code = item_category_id
where coalesce(item_category_code, '') = '';

alter table beone_item_category
    alter column item_category_code set not null;

create unique index if not exists beone_item_category_item_category_code_uindex
    on beone_item_category (item_category_code);
-------
create sequence if not exists beone_item_category_item_category_id_seq;

alter table beone_item_category
    alter column item_category_id set default nextval('beone_item_category_item_category_id_seq');

alter sequence beone_item_category_item_category_id_seq owned by beone_item_category.item_category_id;

alter table beone_item_category
    add if not exists deleted_at TIMESTAMP WITH TIME ZONE,
    add if not exists created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    add if not exists updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP;

alter table beone_sub_kontraktor_out_header
    add if not exists bc_nomor_jaminan varchar(500) not null;

alter table beone_sub_kontraktor_out_header
    add if not exists bc_nomor_bpj varchar(500) not null;

alter table beone_sub_kontraktor_in_header
    add if not exists grandtotal_sys double precision not null default 0;

alter table beone_sub_kontraktor_in_header
    add if not exists dpp_sys double precision not null default 0;

alter table beone_sub_kontraktor_in_header
    add if not exists ppn_sys double precision not null default 0;

alter table beone_sub_kontraktor_in_detail
    add if not exists amount_sys double precision not null default 0;

alter table beone_sub_kontraktor_in_detail
    add if not exists amount_ppn_sys double precision not null default 0;

alter table beone_sub_kontraktor_in_header
    add if not exists discount double precision not null default 0;

alter table beone_sub_kontraktor_in_header
    alter column supplier_id set not null;

alter table beone_sub_kontraktor_out_header
    alter column supplier_id set not null;

alter table beone_import_header
    alter column supplier_id set not null;

alter table beone_import_header
    alter column import_no set not null;

alter table beone_import_header
    alter column trans_date set not null;

alter table beone_export_header
    alter column invoice_no set not null;

alter table beone_export_header
    alter column invoice_date set not null;

alter table beone_export_header
    alter column receiver_id set not null;

create table if not exists beone_print_out
(
    print_out_id serial primary key,
    nama         varchar(50),
    design       text,
    design_def   text,
    created_at   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
