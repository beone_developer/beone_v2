
DROP PROCEDURE IF EXISTS SET_STOK(integer, integer, date, varchar, varchar, double precision, double precision);

-- CREATE PROCEDURE SET STOK
CREATE OR REPLACE PROCEDURE set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date,
                                     p_intvent_trans_no varchar(200), p_keterangan varchar(200), p_qty_in float,
                                     p_qty_out float, price float)
    LANGUAGE plpgsql AS
$$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp float;
BEGIN
    --     SELECT get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) INTO v_stok_akhir;
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
    and i.item_id = p_item_id
    and i.gudang_id = p_gudang_id
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in    = p_qty_in,
                qty_out   = p_qty_out,
                gudang_id = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, trans_date, keterangan, qty_in, qty_out, sa_unit_price)
        SELECT p_intvent_trans_no, p_item_id, p_gudang_id, p_trans_date, p_keterangan, p_qty_in, p_qty_out, v_hpp;
    END IF;
END
$$;

-- CREATE TRIGGER beone_mutasi_stok_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_mutasi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = NEW.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id_asal, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
        call set_stok(NEW.item_id, NEW.gudang_id_tujuan, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_mutasi_stok_detail on beone_mutasi_stok_detail;

CREATE TRIGGER trg_create_beone_mutasi_stok_detail
    BEFORE INSERT OR UPDATE
    ON beone_mutasi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_mutasi_stok_detail();

-- CREATE TRIGGER beone_mutasi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_mutasi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = OLD.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id_tujuan, v_trans_date, v_trans_no, v_keterangan, 0, OLD.qty, 0);
        call set_stok(OLD.item_id, OLD.gudang_id_asal, v_trans_date, v_trans_no, v_keterangan, OLD.qty, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_mutasi_stok_detail on beone_mutasi_stok_detail;

CREATE TRIGGER trg_delete_beone_mutasi_stok_detail
    BEFORE DELETE
    ON beone_mutasi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_mutasi_stok_detail();
