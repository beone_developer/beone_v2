drop function if exists general_ledger(p_trans_date_awal date, p_trans_date_akhir date);
create or replace function general_ledger(p_trans_date_awal date, p_trans_date_akhir date)
    returns TABLE
            (
                r_coa_id      integer,
                r_trans_no    character varying,
                r_trans_date  date,
                r_keterangan  character varying,
                r_currency_id integer,
                r_kurs        double precision,
                r_debet       double precision,
                r_kredit      double precision,
                r_debet_idr   double precision,
                r_kredit_idr  double precision
            )
    language plpgsql
as
$$
DECLARE
    coa_id_persediaan_bb  int;
    coa_id_hutang_usaha   int;
    coa_id_pemakaian_bb   int;
    coa_id_hpp_produksi   int;
    coa_id_persediaan_bj  int;
    coa_id_persediaan_wip int;
    coa_id_hpp_penjualan  int;
    coa_id_piutang_usaha  int;
    coa_id_penjualan      int;
BEGIN
--     coa_id_persediaan_bb = (select coa_id from beone_coa where nomor = '116-01');
--     coa_id_persediaan_wip = (select coa_id from beone_coa where nomor = '116-07');
--     coa_id_persediaan_bj = (select coa_id from beone_coa where nomor = '116-08');
--
--     coa_id_pemakaian_bb = (select coa_id from beone_coa where nomor = '511-01');
--
--     coa_id_hpp_produksi = (select coa_id from beone_coa where nomor = '510-00');
--     coa_id_hpp_penjualan = (select coa_id from beone_coa where nomor = '500-00');
--
--     coa_id_hutang_usaha = (select coa_id from beone_coa where nomor = '211-01');
--     coa_id_piutang_usaha = (select coa_id from beone_coa where nomor = '115-01');
--     coa_id_penjualan = (select coa_id from beone_coa where nomor = '411-01');

    coa_id_persediaan_bb = (select coa_id from beone_coa where nomor = '116.01.01');
    coa_id_persediaan_wip = (select coa_id from beone_coa where nomor = '116.05.01');
    coa_id_persediaan_bj = (select coa_id from beone_coa where nomor = '116.06.01');

    coa_id_pemakaian_bb = (select coa_id from beone_coa where nomor = '510.02.01');

    coa_id_hpp_produksi = (select coa_id from beone_coa where nomor = '511.01.01');
    coa_id_hpp_penjualan = (select coa_id from beone_coa where nomor = '500.01.01');

    coa_id_hutang_usaha = (select coa_id from beone_coa where nomor = '211.02.01');
    coa_id_piutang_usaha = (select coa_id from beone_coa where nomor = '115.01.01');
    coa_id_penjualan = (select coa_id from beone_coa where nomor = '411.01.01');

    DROP TABLE IF EXISTS temp_trans;

    CREATE TEMP TABLE temp_trans AS
--         IMPORT
    SELECT coa_id_persediaan_bb as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           beh.grandtotal       as debet,
           0                    as kredit,
           beh.deleted_at
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
      AND beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_hutang_usaha  as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           beh.grandtotal       as kredit,
           beh.deleted_at
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
      AND beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK WIP
    SELECT coa_id_pemakaian_bb          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_persediaan_bb         as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK WIP 2
    SELECT coa_id_persediaan_wip        as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_hpp_produksi          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit,
           beh.deleted_at
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit,
           beh.deleted_at
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'wip'
      AND beh.transfer_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_wip   as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit,
           beh.deleted_at
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.
                         transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'bb'
      AND beh.transfer_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         EXPORT HPP
    SELECT coa_id_hpp_penjualan    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit,
           beh.deleted_at
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit,
           beh.deleted_at
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         EXPORT USAHA
    SELECT coa_id_piutang_usaha as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           bed.qty * bsd.price  as debet,
           0                    as kredit,
           beh.deleted_at
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_penjualan     as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           bed.qty * bsd.price  as kredit,
           beh.deleted_at

    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT bed.coa_id                                                 as coa_id,
           beh.trans_date                                             as trans_date,
           beh.jurnal_header_id                                       as trans_id,
           beh.jurnal_no                                              as trans_no,
           bed.keterangan_detail                                      as keterangan,
           bed.kurs                                                   as kurs,
           bed.currency_id                                            as currency_id,
           case lower(bed.status) when 'd' then bed.amount else 0 end as debet,
           case lower(bed.status) when 'k' then bed.amount else 0 end as kredit,
           beh.deleted_at
    FROM beone_jurnal_header beh
             JOIN beone_jurnal_detail bed
                  on beh.jurnal_header_id = bed.jurnal_header_id
    where beh.trans_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT bed.coa_id_lawan                                                   as coa_id,
           beh.voucher_date                                                   as trans_date,
           beh.voucher_header_id                                              as trans_id,
           beh.voucher_number                                                 as trans_no,
           bed.keterangan_detail                                              as keterangan,
           bed.kurs                                                           as kurs,
           bed.currency_id                                                    as currency_id,
           case coalesce(beh.tipe, 1) when 1 then bed.jumlah_valas else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then bed.jumlah_valas else 0 end as kredit,
           beh.deleted_at
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    where beh.voucher_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT beh.coa_id_cash_bank                                                               as coa_id,
           beh.voucher_date                                                                   as trans_date,
           beh.voucher_header_id                                                              as trans_id,
           beh.voucher_number                                                                 as trans_no,
           beh.keterangan                                                                     as keterangan,
           1                                                                                  as kurs,
           1                                                                                  as currency_id,
           case coalesce(beh.tipe, 1) when 0 then sum(bed.jumlah_valas * bed.kurs) else 0 end as debet,
           case coalesce(beh.tipe, 1) when 1 then sum(bed.jumlah_valas * bed.kurs) else 0 end as kredit,
           beh.deleted_at
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    where beh.voucher_date between p_trans_date_awal AND p_trans_date_akhir
    group by beh.coa_id_cash_bank, beh.voucher_date, beh.voucher_header_id, beh.voucher_number, beh.keterangan,
             beh.tipe;

    RETURN QUERY SELECT t.coa_id,
                        cast(t.trans_no as varchar(50))    as trans_no,
                        t.trans_date,
                        cast(t.keterangan as varchar(100)) as keterangan,
                        t.currency_id,
                        t.kurs,
                        t.debet,
                        t.kredit,
                        (t.kurs * t.debet)                 as debet_idr,
                        (t.kurs * t.kredit)                as kredit_idr
                 FROM temp_trans t
                 where deleted_at is null;
END;
$$;
