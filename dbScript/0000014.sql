CREATE TABLE IF NOT EXISTS beone_customer(
    customer_id SERIAL PRIMARY KEY,
    customer_code VARCHAR (50) UNIQUE NOT NULL,
    nama VARCHAR (50),
    alamat VARCHAR (355),
    ppn INTEGER,
    deleted_at TIMESTAMP,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_supplier(
    supplier_id SERIAL PRIMARY KEY,
    supplier_code VARCHAR (50) UNIQUE NOT NULL,
    nama VARCHAR (50),
    alamat VARCHAR (355),
    ppn INTEGER,
	pph21 INTEGER,
	pph23 INTEGER,
    deleted_at TIMESTAMP,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

/*
insert into beone_supplier (supplier_id, supplier_code, nama, alamat)
select bc.custsup_id as supplier_id, bc.custsup_id as supplier_code, bc.nama as nama, bc.alamat as alamat
from beone_custsup bc
where not exists(select * from beone_supplier s where s.supplier_id = bc.custsup_id)
and bc.tipe_custsup = 1
order by bc.custsup_id;

insert into beone_customer (customer_id, customer_code, nama, alamat)
select bc.custsup_id as supplier_id, bc.custsup_id as customer_code, bc.nama as nama, bc.alamat as alamat
from beone_custsup bc
where not exists(select * from beone_customer s where s.customer_id = bc.custsup_id)
and bc.tipe_custsup = 2
order by bc.custsup_id;
*/

insert into beone_supplier (supplier_id, supplier_code, nama, alamat)
select bc.custsup_id as supplier_id, bc.custsup_id as supplier_code, max(bc.nama) as nama, max(bc.alamat) as alamat
from beone_custsup bc
where not exists(select * from beone_supplier s where s.supplier_id = bc.custsup_id)
  and bc.tipe_custsup = 1
group by bc.custsup_id;

insert into beone_customer (customer_id, customer_code, nama, alamat)
select bc.custsup_id as supplier_id, bc.custsup_id as customer_code, max(bc.nama) as nama, max(bc.alamat) as alamat
from beone_custsup bc
where not exists(select * from beone_customer s where s.customer_id = bc.custsup_id)
  and bc.tipe_custsup = 2
group by bc.custsup_id;
