CREATE TABLE IF NOT EXISTS beone_roles_user
(
    role_id SERIAL PRIMARY KEY,
    nama_role VARCHAR(50),
    keterangan VARCHAR(255),
    modul_id INTEGER,
    deleted_at timestamp without time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_mutasi_stok_header
(
    mutasi_stok_header_id SERIAL PRIMARY KEY,
    mutasi_stok_no        VARCHAR(50) not null,
    trans_date            date        not null,
    keterangan            VARCHAR(255),
    deleted_at            timestamp without time zone,
    created_at            timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at            timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_mutasi_stok_detail
(
    mutasi_stok_detail_id SERIAL PRIMARY KEY,
    mutasi_stok_header_id int              not null,
    item_id               int              not null,
    qty                   double precision not null,
    gudang_id_asal        int              not null,
    gudang_id_tujuan      int              not null
);

ALTER TABLE beone_mutasi_stok_header
    ADD IF NOT EXISTS flag INTEGER DEFAULT 1;
