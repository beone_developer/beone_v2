alter table beone_import_header add if not exists import_no varchar(200);
alter table beone_import_header add if not exists trans_date date;
alter table beone_import_detail add if not exists purchase_detail_id int;
