ALTER TABLE public.beone_po_import_detail
    ADD COLUMN IF NOT EXISTS deleted_at timestamp without time zone;

ALTER TABLE public.beone_po_import_detail
    ADD COLUMN IF NOT EXISTS created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE public.beone_po_import_detail
    ADD COLUMN IF NOT EXISTS updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP;