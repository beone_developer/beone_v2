PGDMP     4                    x            beone_marufuji_v2    12.1    12.1 �   c           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            d           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            e           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            f           1262    18741    beone_marufuji_v2    DATABASE     �   CREATE DATABASE beone_marufuji_v2 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 !   DROP DATABASE beone_marufuji_v2;
                postgres    false            a           1255    20529    general_ledger(date, date)    FUNCTION     @6  CREATE FUNCTION public.general_ledger(p_trans_date_awal date, p_trans_date_akhir date) RETURNS TABLE(r_coa_id integer, r_trans_no character varying, r_trans_date date, r_keterangan character varying, r_currency_id integer, r_kurs double precision, r_debet double precision, r_kredit double precision, r_debet_idr double precision, r_kredit_idr double precision)
    LANGUAGE plpgsql
    AS $$
DECLARE
    coa_id_persediaan_bb  int;
    coa_id_hutang_usaha   int;
    coa_id_pemakaian_bb   int;
    coa_id_hpp_produksi   int;
    coa_id_persediaan_bj  int;
    coa_id_persediaan_wip int;
    coa_id_hpp_penjualan  int;
    coa_id_piutang_usaha  int;
    coa_id_penjualan      int;
BEGIN
    coa_id_persediaan_bb = (select coa_id from beone_coa where nomor = '116.01.01');
    coa_id_persediaan_wip = (select coa_id from beone_coa where nomor = '116.05.01');
    coa_id_persediaan_bj = (select coa_id from beone_coa where nomor = '116.06.01');

    coa_id_pemakaian_bb = (select coa_id from beone_coa where nomor = '510.02.01');

    coa_id_hpp_produksi = (select coa_id from beone_coa where nomor = '511.01.01');
    coa_id_hpp_penjualan = (select coa_id from beone_coa where nomor = '500.01.01');

    coa_id_hutang_usaha = (select coa_id from beone_coa where nomor = '211.02.01');
    coa_id_piutang_usaha = (select coa_id from beone_coa where nomor = '115.01.01');
    coa_id_penjualan = (select coa_id from beone_coa where nomor = '411.01.01');

    DROP TABLE IF EXISTS temp_trans;

    CREATE TEMP TABLE temp_trans AS
--         IMPORT
    SELECT coa_id_persediaan_bb as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           beh.grandtotal       as debet,
           0                    as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
      AND beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_hutang_usaha  as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           beh.grandtotal       as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
      AND beh.invoice_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK WIP
    SELECT coa_id_pemakaian_bb          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_persediaan_bb         as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK WIP 2
    SELECT coa_id_persediaan_wip        as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_hpp_produksi          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'wip'
      AND beh.transfer_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_wip   as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.
                         transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'bb'
      AND beh.transfer_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         EXPORT HPP
    SELECT coa_id_hpp_penjualan    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
--         EXPORT USAHA
    SELECT coa_id_piutang_usaha as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           bed.qty * bsd.price  as debet,
           0                    as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT coa_id_penjualan     as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           bed.qty * bsd.price  as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    where beh.kontrak_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT bed.coa_id                                                 as coa_id,
           beh.trans_date                                             as trans_date,
           beh.jurnal_header_id                                       as trans_id,
           beh.jurnal_no                                              as trans_no,
           bed.keterangan_detail                                      as keterangan,
           bed.kurs                                                   as kurs,
           bed.currency_id                                            as currency_id,
           case lower(bed.status) when 'd' then bed.amount else 0 end as debet,
           case lower(bed.status) when 'k' then bed.amount else 0 end as kredit
    FROM beone_jurnal_header beh
             JOIN beone_jurnal_detail bed
                  on beh.jurnal_header_id = bed.jurnal_header_id
    where beh.trans_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT bed.coa_id_lawan                                                              as coa_id,
           beh.voucher_date                                                              as trans_date,
           beh.voucher_header_id                                                         as trans_id,
           beh.voucher_number                                                            as trans_no,
           bed.keterangan_detail                                                         as keterangan,
           bed.kurs                                                                      as kurs,
           bed.currency_id                                                               as currency_id,
           case coalesce(beh.tipe, 1) when 1 then bed.jumlah_valas * bed.kurs else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then bed.jumlah_valas * bed.kurs else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    where beh.voucher_date between p_trans_date_awal AND p_trans_date_akhir
    union all
    SELECT beh.coa_id_cash_bank                                                               as coa_id,
           beh.voucher_date                                                                   as trans_date,
           beh.voucher_header_id                                                              as trans_id,
           beh.voucher_number                                                                 as trans_no,
           beh.keterangan                                                                     as keterangan,
           1                                                                                  as kurs,
           1                                                                                  as currency_id,
           case coalesce(beh.tipe, 1) when 0 then sum(bed.jumlah_valas * bed.kurs) else 0 end as debet,
           case coalesce(beh.tipe, 1) when 1 then sum(bed.jumlah_valas * bed.kurs) else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    where beh.voucher_date between p_trans_date_awal AND p_trans_date_akhir
    group by beh.coa_id_cash_bank, beh.voucher_date, beh.voucher_header_id, beh.voucher_number, beh.keterangan,
             beh.tipe;

    RETURN QUERY SELECT t.coa_id,
           cast(t.trans_no as varchar(50))    as trans_no,
           t.trans_date,
           cast(t.keterangan as varchar(100)) as keterangan,
           t.currency_id,
           t.kurs,
           t.debet,
           t.kredit,
           (t.kurs * t.debet)                   as debet_idr,
           (t.kurs * t.kredit)                  as kredit_idr
    FROM temp_trans t;
END;
$$;
 V   DROP FUNCTION public.general_ledger(p_trans_date_awal date, p_trans_date_akhir date);
       public          postgres    false            u           1255    19483 3   get_stok(integer, integer, date, character varying)    FUNCTION     �  CREATE FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
    res RECORD;
BEGIN
    select sum(x.stok_akhir) as stok, avg(hpp) hpp
    into res
    from (
             select 'saldo_awal' as type, coalesce(sum(it.saldo_qty), 0) stok_akhir, coalesce(sum(it.saldo_idr), 0) hpp
             from beone_item it
             where it.item_id = p_item_id
               and p_gudang_id = 1 --PATEN 1
             union
             select 'stok' as type, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
               and i.gudang_id = p_gudang_id
--                and i.trans_date <= p_trans_date
             union
             select 'transaksi' as type, coalesce(sum(i.qty_out) - sum(i.qty_in), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
--                and i.gudang_id = p_gudang_id
               and i.intvent_trans_no = p_intvent_trans_no) x;
    RETURN res;
END;
$$;
 �   DROP FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying);
       public          postgres    false            y           1255    19906    recalculate_gl(date, date) 	   PROCEDURE     91  CREATE PROCEDURE public.recalculate_gl(p_trans_date_awal date, p_trans_date_akhir date)
    LANGUAGE plpgsql
    AS $$
DECLARE
    coa_id_persediaan_bb  int;
    coa_id_hutang_usaha   int;
    coa_id_pemakaian_bb   int;
    coa_id_hpp_produksi   int;
    coa_id_persediaan_bj  int;
    coa_id_persediaan_wip int;
    coa_id_hpp_penjualan  int;
    coa_id_piutang_usaha  int;
    coa_id_penjualan      int;
BEGIN
    coa_id_persediaan_bb = (select coa_id from beone_coa where nomor = '116.01.01');
    coa_id_persediaan_wip = (select coa_id from beone_coa where nomor = '116.05.01');
    coa_id_persediaan_bj = (select coa_id from beone_coa where nomor = '116.06.01');

    coa_id_pemakaian_bb = (select coa_id from beone_coa where nomor = '510.02.01');

    coa_id_hpp_produksi = (select coa_id from beone_coa where nomor = '511.01.01');
    coa_id_hpp_penjualan = (select coa_id from beone_coa where nomor = '500.01.01');

    coa_id_hutang_usaha = (select coa_id from beone_coa where nomor = '211.02.01');
    coa_id_piutang_usaha = (select coa_id from beone_coa where nomor = '115.01.01');
    coa_id_penjualan = (select coa_id from beone_coa where nomor = '411.01.01');

    DROP TABLE IF EXISTS temp_trans;
    DROP TABLE IF EXISTS temp_trans_for_cursor;

    CREATE TEMP TABLE temp_trans AS
--         IMPORT
    SELECT coa_id_persediaan_bb as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           beh.grandtotal       as debet,
           0                    as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
    union all
    SELECT coa_id_hutang_usaha  as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           beh.grandtotal       as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
    union all
--         KONVERSI STOK WIP
    SELECT coa_id_pemakaian_bb          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
    SELECT coa_id_persediaan_bb         as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
--         KONVERSI STOK WIP 2
    SELECT coa_id_persediaan_wip        as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
    SELECT coa_id_hpp_produksi          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'wip'
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_wip   as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.
                         transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'bb'
    union all
--         EXPORT HPP
    SELECT coa_id_hpp_penjualan    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    union all
--         EXPORT USAHA
    SELECT coa_id_piutang_usaha as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           bed.qty * bsd.price  as debet,
           0                    as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT coa_id_penjualan     as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           bed.qty * bsd.price  as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT bed.coa_id                                                 as coa_id,
           beh.trans_date                                             as trans_date,
           beh.jurnal_header_id                                       as trans_id,
           beh.jurnal_no                                              as trans_no,
           bed.keterangan_detail                                      as keterangan,
           bed.kurs                                                   as kurs,
           bed.currency_id                                            as currency_id,
           case lower(bed.status) when 'd' then bed.amount else 0 end as debet,
           case lower(bed.status) when 'k' then bed.amount else 0 end as kredit
    FROM beone_jurnal_header beh
             JOIN beone_jurnal_detail bed
                  on beh.jurnal_header_id = bed.jurnal_header_id
    union all
    SELECT bed.coa_id_lawan                                                              as coa_id,
           beh.voucher_date                                                              as trans_date,
           beh.voucher_header_id                                                         as trans_id,
           beh.voucher_number                                                            as trans_no,
           bed.keterangan_detail                                                         as keterangan,
           bed.kurs                                                                      as kurs,
           bed.currency_id                                                               as currency_id,
           case coalesce(beh.tipe, 1) when 1 then bed.jumlah_valas * bed.kurs else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then bed.jumlah_valas * bed.kurs else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    union all
    SELECT beh.coa_id_cash_bank                                                               as coa_id,
           beh.voucher_date                                                                   as trans_date,
           beh.voucher_header_id                                                              as trans_id,
           beh.voucher_number                                                                 as trans_no,
           beh.keterangan                                                                     as keterangan,
           1                                                                                  as kurs,
           1                                                                                  as currency_id,
           case coalesce(beh.tipe, 1) when 1 then sum(bed.jumlah_valas * bed.kurs) else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then sum(bed.jumlah_valas * bed.kurs) else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    group by beh.coa_id_cash_bank, beh.voucher_date, beh.voucher_header_id, beh.voucher_number, beh.keterangan,
             beh.tipe;

    DELETE FROM beone_general_ledger WHERE coalesce(keterangan, '') <> 'SALDO AWAL';
    INSERT INTO beone_general_ledger (coa_id, trans_no, trans_date, keterangan, currency_id, kurs, debet, kredit,
                                      debet_idr, kredit_idr)
    SELECT coa_id,
           trans_no,
           trans_date,
           keterangan,
           currency_id,
           kurs,
           debet,
           kredit,
           (kurs * debet)  as debet_idr,
           (kurs * kredit) as kredit_idr
    FROM temp_trans;
END
$$;
 W   DROP PROCEDURE public.recalculate_gl(p_trans_date_awal date, p_trans_date_akhir date);
       public          postgres    false            x           1255    19904     recalculate_stok_hpp(date, date) 	   PROCEDURE     �"  CREATE PROCEDURE public.recalculate_stok_hpp(p_trans_date_awal date, p_trans_date_akhir date)
    LANGUAGE plpgsql
    AS $$
BEGIN
    DROP TABLE IF EXISTS temp_trans;
    DROP TABLE IF EXISTS temp_trans_for_cursor;

    CREATE TEMP TABLE temp_trans AS
    SELECT 1                       as priority,
           beh.receive_date        as trans_date,
           beh.import_header_id    as trans_id,
           bed.import_detail_id    as trans_detail_id,
           beh.receive_no          as trans_no,
           'IMPORT'                as keterangan,
           bed.gudang_id           as gudang_id,
           bed.item_id             as item_id,
           bed.qty                 as qty,
           bed.price * beh.bc_kurs as price
    FROM beone_import_header beh
             INNER JOIN beone_import_detail bed
                        on beh.
                               import_header_id = bed.import_header_id
                            AND beh.trans_date between p_trans_date_awal and p_trans_date_akhir
    WHERE coalesce(beh.receive_no, '') <> ''
    UNION ALL
    SELECT 2                           as priority,
           beh.konversi_stok_date,
           beh.konversi_stok_header_id,
           beh.konversi_stok_header_id as trans_detail_id,
           beh.konversi_stok_no,
           'KONVERSI STOK IN'          as keterangan,
           beh.gudang_id,
           beh.item_id,
           beh.qty_real,
           0                           as price
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 4                    as priority,
           beh.delivery_date    as trans_date,
           beh.export_header_id as trans_id,
           bed.export_detail_id as trans_detail_id,
           beh.delivery_no      as trans_no,
           'EXPORT'             as keterangan,
           bed.gudang_id        as gudang_id,
           bed.item_id          as item_id,
           bed.qty * -1         as qty,
           0                    as price
    FROM beone_export_header beh
             INNER JOIN beone_export_detail bed
                        on beh.export_header_id = bed.export_header_id
    WHERE coalesce(beh.delivery_no, '') <> ''
      AND beh.delivery_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 3                           as priority,
           beh.konversi_stok_date,
           beh.konversi_stok_header_id,
           bed.konversi_stok_detail_id as trans_detail_id,
           beh.konversi_stok_no,
           'KONVERSI STOK OUT'         as keterangan,
           bed.gudang_id,
           bed.item_id,
           bed.qty_real * -1,
           0                           as price
    FROM beone_konversi_stok_header beh
             INNER JOIN beone_konversi_stok_detail bed
                        on beh.konversi_stok_header_id = bed.konversi_stok_header_id
    WHERE coalesce(bed.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 5                            as priority,
           beh.transfer_date,
           beh.transfer_stock_id,
           bed.transfer_stock_detail_id as trans_detail_id,
           beh.transfer_no,
           'TRANSFER STOCK'             as keterangan,
           bed.gudang_id,
           bed.item_id,
           case (lower(bed.tipe_transfer_stock))
               when 'bb' then bed.qty * -1
               when 'wip'
                   then bed.qty end     as qty,
           0                            as price
    FROM beone_transfer_stock beh
             INNER JOIN beone_transfer_stock_detail bed
                        on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE beh.transfer_date between p_trans_date_awal and p_trans_date_akhir;

    DELETE
    FROM beone_inventory
    WHERE keterangan <> 'SALDO AWAL'
      AND trans_date between p_trans_date_awal and p_trans_date_akhir;

    INSERT INTO beone_inventory (intvent_trans_no, item_id, trans_date, keterangan, qty_in,
                                 qty_out, flag, gudang_id, invent_trans_detail_id)
    SELECT trans_no,
           item_id,
           trans_date,
           keterangan,
           case when (coalesce(qty, 0) > 0) then abs(coalesce(qty, 0)) else 0 end as qty_in,
           case when (coalesce(qty, 0) < 0) then abs(coalesce(qty, 0)) else 0 end as qty_out,
           1                                                                      as flag,
           gudang_id,
           trans_detail_id
    FROM temp_trans;

    CREATE TEMP TABLE temp_trans_for_cursor AS
    SELECT iv.intvent_trans_id, iv.invent_trans_detail_id, te.*
    FROM beone_inventory iv
             JOIN temp_trans te
                  ON iv.invent_trans_detail_id = te.trans_detail_id
                      AND iv.item_id = te.item_id
                      AND iv.intvent_trans_no = te.trans_no;
    DECLARE
        temp_trans_cursor CURSOR
            FOR
            SELECT t.*
            FROM temp_trans_for_cursor t
            ORDER BY t.trans_date, t.priority asc;
    BEGIN
        FOR rec IN temp_trans_cursor
            LOOP
                DROP TABLE IF EXISTS prev_rec;

                CREATE TEMP TABLE prev_rec AS
                SELECT i.*
                FROM beone_inventory i
                WHERE i.item_id = rec.item_id
                  AND i.qty_in > 0
                  AND i.trans_date <= rec.trans_date
                  AND i.intvent_trans_id < rec.intvent_trans_id
                ORDER BY i.trans_date, i.intvent_trans_id asc
                LIMIT 1;

                IF (select count(*) from prev_rec) THEN
                    UPDATE beone_inventory i
                    SET price_hpp     = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2),
                        update_by     = '2',
                        value_in      = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2) *
                                        i.qty_in,
                        value_out     = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2) *
                                        i.qty_out,
--                             sa_amount     = (i.value_in - i.value_out) + (pr.value_in - pr.value_out),
                        sa_unit_price = rec.price
                    FROM prev_rec pr
                    WHERE i.intvent_trans_no = rec.trans_no
                      AND i.item_id = rec.item_id;
                ELSE
                    UPDATE beone_inventory i
                    SET price_hpp     = rec.price,
                        update_by     = '1',
                        value_in      = rec.price * rec.qty,
                        value_out     = 0,
--                             sa_amount     = rec.price * rec.qty,
                        sa_unit_price = rec.price
                    WHERE i.intvent_trans_no = rec.trans_no
                      AND i.item_id = rec.item_id;
                END IF;
            END LOOP;
    END;

    UPDATE beone_konversi_stok_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_konversi_stok_header h,
         beone_inventory iv
    WHERE d.konversi_stok_detail_id = iv.invent_trans_detail_id
      AND d.konversi_stok_header_id = h.konversi_stok_header_id
      AND iv.intvent_trans_no = h.konversi_stok_no
      AND h.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_konversi_stok_header h
    SET price_hpp = x.price_hpp
    FROM (
             SELECT d.konversi_stok_header_id, avg(d.price_hpp) as price_hpp
             FROM beone_konversi_stok_detail d
             GROUP BY d.konversi_stok_header_id
         ) x
    WHERE x.konversi_stok_header_id = h.konversi_stok_header_id
      AND h.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_export_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_export_header h,
         beone_inventory iv
    WHERE d.export_detail_id = iv.invent_trans_detail_id
      AND d.export_header_id = h.export_header_id
      AND iv.intvent_trans_no = h.delivery_no
      AND h.delivery_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_transfer_stock_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_transfer_stock h,
         beone_inventory iv
    WHERE d.transfer_stock_detail_id = iv.invent_trans_detail_id
      AND d.transfer_stock_header_id = h.transfer_stock_id
      AND iv.intvent_trans_no = h.transfer_no
      AND iv.trans_date between p_trans_date_awal and p_trans_date_akhir;
END
$$;
 ]   DROP PROCEDURE public.recalculate_stok_hpp(p_trans_date_awal date, p_trans_date_akhir date);
       public          postgres    false            ~           1255    20531 �   set_stok(integer, integer, integer, date, character varying, character varying, double precision, double precision, double precision) 	   PROCEDURE     �  CREATE PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_intvent_trans_detail_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision)
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp        float;
BEGIN
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
      and i.item_id = p_item_id
      and i.gudang_id = p_gudang_id
--       and i.invent_trans_detail_id = p_intvent_trans_detail_id
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in        = p_qty_in,
                qty_out       = p_qty_out,
                gudang_id     = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id
              and invent_trans_detail_id = p_intvent_trans_detail_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, invent_trans_detail_id, trans_date,
                                     keterangan, qty_in, qty_out,
                                     price_hpp)
        SELECT p_intvent_trans_no,
               p_item_id,
               p_gudang_id,
               p_intvent_trans_detail_id,
               p_trans_date,
               p_keterangan,
               p_qty_in,
               p_qty_out,
               v_hpp;
    END IF;
END
$$;
   DROP PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_intvent_trans_detail_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision);
       public          postgres    false            p           1255    18744     trg_create_beone_export_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.export_detail_id, v_trans_date, v_trans_no, v_keterangan, 0,
                      NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_export_detail();
       public          postgres    false            r           1255    18745     trg_create_beone_export_header()    FUNCTION       CREATE FUNCTION public.trg_create_beone_export_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_export_detail set qty = qty where export_header_id = NEW.export_header_id;
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_export_header();
       public          postgres    false            s           1255    18746     trg_create_beone_import_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEw.import_detail_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty,
                      0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_import_detail();
       public          postgres    false            t           1255    18747     trg_create_beone_import_header()    FUNCTION       CREATE FUNCTION public.trg_create_beone_import_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_import_detail set qty = qty where import_header_id = NEW.import_header_id;
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_import_header();
       public          postgres    false            z           1255    18748 '   trg_create_beone_konversi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.konversi_stok_detail_id, v_trans_date, v_trans_no, v_keterangan,
                      0, NEW.qty_real, 0);
    end if;

    RETURN NEW;
END;
$$;
 >   DROP FUNCTION public.trg_create_beone_konversi_stok_detail();
       public          postgres    false            o           1255    18749 '   trg_create_beone_konversi_stok_header()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0,
                      0);
    end if;

    RETURN NEW;
END;
$$;
 >   DROP FUNCTION public.trg_create_beone_konversi_stok_header();
       public          postgres    false            |           1255    18750 %   trg_create_beone_mutasi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_mutasi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = NEW.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id_asal, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty, 0);
        call set_stok(NEW.item_id, NEW.gudang_id_tujuan, NEW.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty, 0, 0);
    end if;

    RETURN NEW;
END;
$$;
 <   DROP FUNCTION public.trg_create_beone_mutasi_stok_detail();
       public          postgres    false            �           1255    20538 +   trg_create_beone_sub_kontraktor_in_detail()    FUNCTION       CREATE FUNCTION public.trg_create_beone_sub_kontraktor_in_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_in_header_id, trans_no, trans_date, 'SUBKONTRAKTOR IN' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_in_header
    where sub_kontraktor_in_header_id = NEW.sub_kontraktor_in_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_in_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, NEW.qty, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;
 B   DROP FUNCTION public.trg_create_beone_sub_kontraktor_in_detail();
       public          postgres    false            �           1255    20542 +   trg_create_beone_sub_kontraktor_in_header()    FUNCTION     �   CREATE FUNCTION public.trg_create_beone_sub_kontraktor_in_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;
 B   DROP FUNCTION public.trg_create_beone_sub_kontraktor_in_header();
       public          postgres    false                       1255    20532 ,   trg_create_beone_sub_kontraktor_out_detail()    FUNCTION       CREATE FUNCTION public.trg_create_beone_sub_kontraktor_out_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_out_header_id, trans_no, trans_date, 'SUBKONTRAKTOR OUT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_out_header
    where sub_kontraktor_out_header_id = NEW.sub_kontraktor_out_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, NEW.sub_kontraktor_out_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$;
 C   DROP FUNCTION public.trg_create_beone_sub_kontraktor_out_detail();
       public          postgres    false            �           1255    20536 ,   trg_create_beone_sub_kontraktor_out_header()    FUNCTION     �   CREATE FUNCTION public.trg_create_beone_sub_kontraktor_out_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.sisabayar = NEW.grandtotal - NEW.terbayar;
    RETURN NEW;
END;
$$;
 C   DROP FUNCTION public.trg_create_beone_sub_kontraktor_out_header();
       public          postgres    false            {           1255    18751 (   trg_create_beone_transfer_stock_detail()    FUNCTION     c  CREATE FUNCTION public.trg_create_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (lower(NEW.tipe_transfer_stock) = 'bb') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, 0, NEW.qty, 0);
        elsif (lower(NEW.tipe_transfer_stock) = 'wip') then
            call set_stok(NEW.item_id, NEW.gudang_id, cast(NEW.transfer_stock_detail_id as int), v_trans_date,
                          v_trans_no,
                          v_keterangan, NEW.qty, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;
 ?   DROP FUNCTION public.trg_create_beone_transfer_stock_detail();
       public          postgres    false            v           1255    18752     trg_delete_beone_export_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = OLD.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.export_detail_id, v_trans_date, v_trans_no, v_keterangan, 0, 0,
                      0);
    end if;

    RETURN OLD;
END;
$$;
 7   DROP FUNCTION public.trg_delete_beone_export_detail();
       public          postgres    false            b           1255    18753     trg_delete_beone_import_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = OLD.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.import_detail_id, v_trans_date, v_trans_no, v_keterangan, 0, 0,
                      0);
    end if;

    RETURN OLD;
END;
$$;
 7   DROP FUNCTION public.trg_delete_beone_import_detail();
       public          postgres    false            w           1255    18754 '   trg_delete_beone_konversi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, OLD.konversi_stok_detail_id, v_trans_date, v_trans_no, v_keterangan,
                      0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 >   DROP FUNCTION public.trg_delete_beone_konversi_stok_detail();
       public          postgres    false            �           1255    18755 '   trg_delete_beone_konversi_stok_header()    FUNCTION     d  CREATE FUNCTION public.trg_delete_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = OLD.konversi_stok_header_id;
    v_trans_no = OLD.konversi_stok_no;
    v_trans_date = OLD.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 >   DROP FUNCTION public.trg_delete_beone_konversi_stok_header();
       public          postgres    false            }           1255    18756 %   trg_delete_beone_mutasi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_mutasi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select mutasi_stok_header_id, mutasi_stok_no, trans_date, 'MUTASI STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_mutasi_stok_header
    where mutasi_stok_header_id = OLD.mutasi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id_tujuan, OLD.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, OLD.qty, 0);
        call set_stok(OLD.item_id, OLD.gudang_id_asal, OLD.mutasi_stok_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, OLD.qty, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 <   DROP FUNCTION public.trg_delete_beone_mutasi_stok_detail();
       public          postgres    false            �           1255    20540 +   trg_delete_beone_sub_kontraktor_in_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_sub_kontraktor_in_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_in_header_id, trans_no, trans_date, 'SUBKONTRAKTOR IN' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_in_header
    where sub_kontraktor_in_header_id = OLD.sub_kontraktor_in_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, NEW.sub_kontraktor_in_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 B   DROP FUNCTION public.trg_delete_beone_sub_kontraktor_in_detail();
       public          postgres    false            �           1255    20534 ,   trg_delete_beone_sub_kontraktor_out_detail()    FUNCTION        CREATE FUNCTION public.trg_delete_beone_sub_kontraktor_out_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select sub_kontraktor_out_header_id, trans_no, trans_date, 'SUBKONTRAKTOR OUT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_sub_kontraktor_out_header
    where sub_kontraktor_out_header_id = OLD.sub_kontraktor_out_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, NEW.sub_kontraktor_out_detail_id, v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 C   DROP FUNCTION public.trg_delete_beone_sub_kontraktor_out_detail();
       public          postgres    false            q           1255    18757 (   trg_delete_beone_transfer_stock_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = OLD.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, cast(OLD.transfer_stock_detail_id as int), v_trans_date, v_trans_no,
                      v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 ?   DROP FUNCTION public.trg_delete_beone_transfer_stock_detail();
       public          postgres    false            �            1259    18758    beone_adjustment    TABLE     =  CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);
 $   DROP TABLE public.beone_adjustment;
       public         heap    postgres    false            �            1259    18761 "   beone_adjustment_adjustment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_adjustment_adjustment_id_seq;
       public          postgres    false    202            g           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;
          public          postgres    false    203            �            1259    18763 	   beone_coa    TABLE       CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.beone_coa;
       public         heap    postgres    false            �            1259    18771    beone_coa_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_coa_coa_id_seq;
       public          postgres    false    204            h           0    0    beone_coa_coa_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;
          public          postgres    false    205            �            1259    18773    beone_coa_jurnal    TABLE     �   CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);
 $   DROP TABLE public.beone_coa_jurnal;
       public         heap    postgres    false            �            1259    18779 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq;
       public          postgres    false    206            i           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;
          public          postgres    false    207            �            1259    18781 
   beone_country    TABLE     �   CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);
 !   DROP TABLE public.beone_country;
       public         heap    postgres    false            �            1259    18784    beone_country_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_country_country_id_seq;
       public          postgres    false    208            j           0    0    beone_country_country_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;
          public          postgres    false    209            >           1259    19832    beone_currency    TABLE     �   CREATE TABLE public.beone_currency (
    currency_id integer NOT NULL,
    currency_code character varying(3) NOT NULL,
    keterangan character varying(200) NOT NULL
);
 "   DROP TABLE public.beone_currency;
       public         heap    postgres    false            =           1259    19830    beone_currency_currency_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_currency_currency_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_currency_currency_id_seq;
       public          postgres    false    318            k           0    0    beone_currency_currency_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_currency_currency_id_seq OWNED BY public.beone_currency.currency_id;
          public          postgres    false    317            :           1259    19500    beone_customer    TABLE     �  CREATE TABLE public.beone_customer (
    customer_id integer NOT NULL,
    customer_code character varying(50) NOT NULL,
    nama character varying(50),
    alamat character varying(355),
    ppn integer,
    deleted_at timestamp without time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 "   DROP TABLE public.beone_customer;
       public         heap    postgres    false            9           1259    19498    beone_customer_customer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_customer_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_customer_customer_id_seq;
       public          postgres    false    314            l           0    0    beone_customer_customer_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_customer_customer_id_seq OWNED BY public.beone_customer.customer_id;
          public          postgres    false    313            �            1259    18786 
   beone_custsup    TABLE       CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 !   DROP TABLE public.beone_custsup;
       public         heap    postgres    false            �            1259    18791    beone_custsup_custsup_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_custsup_custsup_id_seq;
       public          postgres    false    210            m           0    0    beone_custsup_custsup_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;
          public          postgres    false    211            �            1259    18793    beone_export_detail    TABLE       CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer NOT NULL,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer,
    sales_header_id integer NOT NULL,
    gudang_id integer DEFAULT 1 NOT NULL,
    sales_detail_id integer NOT NULL,
    price_hpp double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL,
    amount double precision DEFAULT 0 NOT NULL,
    amount_sys double precision DEFAULT 0 NOT NULL
);
 '   DROP TABLE public.beone_export_detail;
       public         heap    postgres    false            �            1259    18797 (   beone_export_detail_export_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_detail_export_detail_id_seq;
       public          postgres    false    212            n           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;
          public          postgres    false    213            �            1259    18799    beone_export_header    TABLE     �  CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25),
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    sisabayar double precision DEFAULT 0 NOT NULL,
    terbayar double precision DEFAULT 0 NOT NULL,
    currency_id integer DEFAULT 1 NOT NULL,
    kurs integer DEFAULT 1 NOT NULL
);
 '   DROP TABLE public.beone_export_header;
       public         heap    postgres    false            �            1259    18807 (   beone_export_header_export_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_header_export_header_id_seq;
       public          postgres    false    214            o           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;
          public          postgres    false    215            D           1259    19888    beone_general_ledger    TABLE     �  CREATE TABLE public.beone_general_ledger (
    general_ledger_id integer NOT NULL,
    coa_id integer NOT NULL,
    trans_no character varying(100),
    trans_date date,
    keterangan character varying(500),
    currency_id integer,
    kurs double precision,
    debet double precision,
    kredit double precision,
    debet_idr double precision,
    kredit_idr double precision
);
 (   DROP TABLE public.beone_general_ledger;
       public         heap    postgres    false            C           1259    19886 *   beone_general_ledger_general_ledger_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_general_ledger_general_ledger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_general_ledger_general_ledger_id_seq;
       public          postgres    false    324            p           0    0 *   beone_general_ledger_general_ledger_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_general_ledger_general_ledger_id_seq OWNED BY public.beone_general_ledger.general_ledger_id;
          public          postgres    false    323            �            1259    18809    beone_gl    TABLE     �  CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);
    DROP TABLE public.beone_gl;
       public         heap    postgres    false            �            1259    18812    beone_gl_gl_id_seq    SEQUENCE     {   CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.beone_gl_gl_id_seq;
       public          postgres    false    216            q           0    0    beone_gl_gl_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;
          public          postgres    false    217            �            1259    18814    beone_gudang    TABLE     �  CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    gudang_code character varying(50) DEFAULT ''::character varying NOT NULL
);
     DROP TABLE public.beone_gudang;
       public         heap    postgres    false            �            1259    18819    beone_gudang_detail    TABLE     �  CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer NOT NULL,
    trans_date date,
    item_id integer NOT NULL,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);
 '   DROP TABLE public.beone_gudang_detail;
       public         heap    postgres    false            �            1259    18822 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq;
       public          postgres    false    219            r           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;
          public          postgres    false    220            �            1259    18824    beone_gudang_gudang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.beone_gudang_gudang_id_seq;
       public          postgres    false    218            s           0    0    beone_gudang_gudang_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;
          public          postgres    false    221            �            1259    18826    beone_pemakaian_bahan_header    TABLE       CREATE TABLE public.beone_pemakaian_bahan_header (
    pemakaian_header_id bigint NOT NULL,
    nomor_pemakaian character varying(50),
    keterangan character varying(250),
    tgl_pemakaian date,
    update_by integer,
    update_date date,
    nomor_pm character varying(50),
    amount_pemakaian double precision,
    status integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 0   DROP TABLE public.beone_pemakaian_bahan_header;
       public         heap    postgres    false            �            1259    18831 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq;
       public          postgres    false    222            t           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNED BY public.beone_pemakaian_bahan_header.pemakaian_header_id;
          public          postgres    false    223            T           1259    20409    beone_hutang_detail    TABLE     �   CREATE TABLE public.beone_hutang_detail (
    hutang_detail_id integer NOT NULL,
    hutang_header_id integer NOT NULL,
    import_header_id integer NOT NULL,
    amount double precision DEFAULT 0 NOT NULL
);
 '   DROP TABLE public.beone_hutang_detail;
       public         heap    postgres    false            S           1259    20407 (   beone_hutang_detail_hutang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_detail_hutang_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_hutang_detail_hutang_detail_id_seq;
       public          postgres    false    340            u           0    0 (   beone_hutang_detail_hutang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_hutang_detail_hutang_detail_id_seq OWNED BY public.beone_hutang_detail.hutang_detail_id;
          public          postgres    false    339            R           1259    20398    beone_hutang_header    TABLE     �  CREATE TABLE public.beone_hutang_header (
    hutang_header_id integer NOT NULL,
    trans_no character varying(50) NOT NULL,
    trans_date date NOT NULL,
    supplier_id integer NOT NULL,
    grandtotal double precision DEFAULT 0 NOT NULL,
    keterangan character varying(255),
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 '   DROP TABLE public.beone_hutang_header;
       public         heap    postgres    false            Q           1259    20396 (   beone_hutang_header_hutang_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_header_hutang_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_hutang_header_hutang_header_id_seq;
       public          postgres    false    338            v           0    0 (   beone_hutang_header_hutang_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_hutang_header_hutang_header_id_seq OWNED BY public.beone_hutang_header.hutang_header_id;
          public          postgres    false    337            �            1259    18833    beone_hutang_piutang    TABLE     �  CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);
 (   DROP TABLE public.beone_hutang_piutang;
       public         heap    postgres    false            �            1259    18836 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq;
       public          postgres    false    224            w           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;
          public          postgres    false    225            �            1259    18838    beone_import_detail    TABLE     �  CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer NOT NULL,
    import_header_id integer NOT NULL,
    purchase_detail_id integer NOT NULL,
    gudang_id integer DEFAULT 0 NOT NULL,
    isppn integer DEFAULT 0 NOT NULL,
    bc_barang_id integer,
    amount double precision DEFAULT 0 NOT NULL,
    amount_sys double precision DEFAULT 0 NOT NULL,
    amount_ppn double precision DEFAULT 0 NOT NULL,
    amount_ppn_sys double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL,
    bc_barang_uraian character varying(100)
);
 '   DROP TABLE public.beone_import_detail;
       public         heap    postgres    false            �            1259    18842 (   beone_import_detail_import_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_detail_import_detail_id_seq;
       public          postgres    false    226            x           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;
          public          postgres    false    227            �            1259    18844    beone_import_header    TABLE     ^  CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50),
    import_no character varying(200),
    trans_date date,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    sisabayar double precision DEFAULT 0 NOT NULL,
    terbayar double precision DEFAULT 0 NOT NULL,
    dpp double precision DEFAULT 0 NOT NULL,
    ppn double precision DEFAULT 0 NOT NULL,
    grandtotal double precision DEFAULT 0 NOT NULL,
    bc_no_aju character varying(500),
    currency_id integer DEFAULT 1 NOT NULL,
    bc_nama_vendor character varying(500),
    bc_nama_pemilik character varying(500),
    bc_nama_penerima_barang character varying(500),
    bc_nama_pengangkut character varying(500),
    bc_nama_pengirim character varying(500),
    purchase_header_id integer,
    bc_kurs double precision DEFAULT 1 NOT NULL,
    bc_header_id integer,
    grandtotal_sys double precision DEFAULT 0 NOT NULL,
    dpp_sys double precision DEFAULT 0 NOT NULL,
    ppn_sys double precision DEFAULT 0 NOT NULL
);
 '   DROP TABLE public.beone_import_header;
       public         heap    postgres    false            �            1259    18850 (   beone_import_header_import_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_header_import_header_id_seq;
       public          postgres    false    228            y           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;
          public          postgres    false    229            �            1259    18852    beone_inventory    TABLE     �  CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50) NOT NULL,
    item_id integer NOT NULL,
    trans_date date NOT NULL,
    keterangan character varying(100) NOT NULL,
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date,
    gudang_id integer DEFAULT 1 NOT NULL,
    invent_trans_detail_id integer DEFAULT 0 NOT NULL,
    price_hpp double precision DEFAULT 0 NOT NULL
);
 #   DROP TABLE public.beone_inventory;
       public         heap    postgres    false            �            1259    18856 $   beone_inventory_intvent_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.beone_inventory_intvent_trans_id_seq;
       public          postgres    false    230            z           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;
          public          postgres    false    231            �            1259    18858 
   beone_item    TABLE     q  CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(500),
    item_code character varying(200) NOT NULL,
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(500),
    flag integer,
    item_type_id integer NOT NULL,
    hscode character varying(50),
    satuan_id integer,
    item_category_id integer DEFAULT 1 NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    level_item integer DEFAULT 3 NOT NULL
);
    DROP TABLE public.beone_item;
       public         heap    postgres    false            �            1259    18867    beone_item_category    TABLE     �   CREATE TABLE public.beone_item_category (
    item_category_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 '   DROP TABLE public.beone_item_category;
       public         heap    postgres    false            �            1259    18870    beone_item_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_item_item_id_seq;
       public          postgres    false    232            {           0    0    beone_item_item_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;
          public          postgres    false    234            �            1259    18872    beone_item_type    TABLE     T  CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 #   DROP TABLE public.beone_item_type;
       public         heap    postgres    false            �            1259    18877     beone_item_type_item_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_item_type_item_type_id_seq;
       public          postgres    false    235            |           0    0     beone_item_type_item_type_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;
          public          postgres    false    236            B           1259    19859    beone_jurnal_detail    TABLE     \  CREATE TABLE public.beone_jurnal_detail (
    jurnal_detail_id integer NOT NULL,
    jurnal_header_id integer NOT NULL,
    coa_id integer NOT NULL,
    amount double precision DEFAULT 0 NOT NULL,
    kurs double precision DEFAULT 1 NOT NULL,
    amount_idr double precision DEFAULT 0 NOT NULL,
    keterangan_detail character varying(255),
    status character varying(1) NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    currency_id integer DEFAULT 1 NOT NULL
);
 '   DROP TABLE public.beone_jurnal_detail;
       public         heap    postgres    false            A           1259    19857 (   beone_jurnal_detail_jurnal_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_jurnal_detail_jurnal_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_jurnal_detail_jurnal_detail_id_seq;
       public          postgres    false    322            }           0    0 (   beone_jurnal_detail_jurnal_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_jurnal_detail_jurnal_detail_id_seq OWNED BY public.beone_jurnal_detail.jurnal_detail_id;
          public          postgres    false    321            @           1259    19849    beone_jurnal_header    TABLE     u  CREATE TABLE public.beone_jurnal_header (
    jurnal_header_id integer NOT NULL,
    jurnal_no character varying(50) NOT NULL,
    keterangan character varying(255),
    trans_date date NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 '   DROP TABLE public.beone_jurnal_header;
       public         heap    postgres    false            ?           1259    19847 (   beone_jurnal_header_jurnal_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_jurnal_header_jurnal_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_jurnal_header_jurnal_header_id_seq;
       public          postgres    false    320            ~           0    0 (   beone_jurnal_header_jurnal_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_jurnal_header_jurnal_header_id_seq OWNED BY public.beone_jurnal_header.jurnal_header_id;
          public          postgres    false    319            �            1259    18879    beone_kode_trans    TABLE     �   CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);
 $   DROP TABLE public.beone_kode_trans;
       public         heap    postgres    false            �            1259    18885 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_kode_trans_kode_trans_id_seq;
       public          postgres    false    237                       0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;
          public          postgres    false    238            �            1259    18887    beone_komposisi    TABLE     �   CREATE TABLE public.beone_komposisi (
    komposisi_id integer NOT NULL,
    item_jadi_id integer,
    item_baku_id integer,
    qty_item_baku double precision,
    flag integer
);
 #   DROP TABLE public.beone_komposisi;
       public         heap    postgres    false            �            1259    18890     beone_komposisi_komposisi_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_komposisi_komposisi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_komposisi_komposisi_id_seq;
       public          postgres    false    239            �           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_komposisi_komposisi_id_seq OWNED BY public.beone_komposisi.komposisi_id;
          public          postgres    false    240            �            1259    18892    beone_konfigurasi_perusahaan    TABLE     �  CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000),
    cabang character varying(8) DEFAULT '00001/01'::character varying NOT NULL
);
 0   DROP TABLE public.beone_konfigurasi_perusahaan;
       public         heap    postgres    false            �            1259    18898    beone_konversi_stok_detail    TABLE     �  CREATE TABLE public.beone_konversi_stok_detail (
    konversi_stok_detail_id integer NOT NULL,
    konversi_stok_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision,
    qty_real double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 1 NOT NULL,
    qty_allocation integer DEFAULT 0 NOT NULL,
    price_hpp double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL
);
 .   DROP TABLE public.beone_konversi_stok_detail;
       public         heap    postgres    false            �            1259    18903 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq;
       public          postgres    false    242            �           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq OWNED BY public.beone_konversi_stok_detail.konversi_stok_detail_id;
          public          postgres    false    243            �            1259    18905    beone_konversi_stok_header    TABLE     B  CREATE TABLE public.beone_konversi_stok_header (
    konversi_stok_header_id integer NOT NULL,
    item_id integer NOT NULL,
    konversi_stok_no character varying(255),
    konversi_stok_date date,
    flag integer,
    qty double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 1 NOT NULL,
    qty_real integer DEFAULT 0 NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    price_hpp double precision DEFAULT 0 NOT NULL
);
 .   DROP TABLE public.beone_konversi_stok_header;
       public         heap    postgres    false            �            1259    18912 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq;
       public          postgres    false    244            �           0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq OWNED BY public.beone_konversi_stok_header.konversi_stok_header_id;
          public          postgres    false    245            �            1259    18914 	   beone_log    TABLE     �   CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);
    DROP TABLE public.beone_log;
       public         heap    postgres    false            �            1259    18917    beone_log_log_id_seq    SEQUENCE     }   CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_log_log_id_seq;
       public          postgres    false    246            �           0    0    beone_log_log_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;
          public          postgres    false    247            J           1259    19936    beone_modul    TABLE     �   CREATE TABLE public.beone_modul (
    modul_id integer NOT NULL,
    modul character varying(45),
    url character varying(1000),
    parent_id integer,
    is_hidden integer DEFAULT 0 NOT NULL,
    icon_class character varying(100)
);
    DROP TABLE public.beone_modul;
       public         heap    postgres    false            �            1259    18919    beone_mutasi_stok_detail    TABLE       CREATE TABLE public.beone_mutasi_stok_detail (
    mutasi_stok_detail_id integer NOT NULL,
    mutasi_stok_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision NOT NULL,
    gudang_id_asal integer NOT NULL,
    gudang_id_tujuan integer NOT NULL
);
 ,   DROP TABLE public.beone_mutasi_stok_detail;
       public         heap    postgres    false            �            1259    18922 2   beone_mutasi_stok_detail_mutasi_stok_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_mutasi_stok_detail_mutasi_stok_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.beone_mutasi_stok_detail_mutasi_stok_detail_id_seq;
       public          postgres    false    248            �           0    0 2   beone_mutasi_stok_detail_mutasi_stok_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_mutasi_stok_detail_mutasi_stok_detail_id_seq OWNED BY public.beone_mutasi_stok_detail.mutasi_stok_detail_id;
          public          postgres    false    249            �            1259    18924    beone_mutasi_stok_header    TABLE     �  CREATE TABLE public.beone_mutasi_stok_header (
    mutasi_stok_header_id integer NOT NULL,
    mutasi_stok_no character varying(50) NOT NULL,
    trans_date date NOT NULL,
    keterangan character varying(255),
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    flag integer DEFAULT 1
);
 ,   DROP TABLE public.beone_mutasi_stok_header;
       public         heap    postgres    false            �            1259    18930 2   beone_mutasi_stok_header_mutasi_stok_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_mutasi_stok_header_mutasi_stok_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.beone_mutasi_stok_header_mutasi_stok_header_id_seq;
       public          postgres    false    250            �           0    0 2   beone_mutasi_stok_header_mutasi_stok_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_mutasi_stok_header_mutasi_stok_header_id_seq OWNED BY public.beone_mutasi_stok_header.mutasi_stok_header_id;
          public          postgres    false    251            �            1259    18932    beone_opname_detail    TABLE       CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);
 '   DROP TABLE public.beone_opname_detail;
       public         heap    postgres    false            �            1259    18935 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_detail_opname_detail_id_seq;
       public          postgres    false    252            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;
          public          postgres    false    253            �            1259    18937    beone_opname_header    TABLE     0  CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 '   DROP TABLE public.beone_opname_header;
       public         heap    postgres    false            �            1259    18942 (   beone_opname_header_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_header_opname_header_id_seq;
       public          postgres    false    254            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;
          public          postgres    false    255                        1259    18944    beone_peleburan    TABLE     !  CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);
 #   DROP TABLE public.beone_peleburan;
       public         heap    postgres    false                       1259    18947     beone_peleburan_peleburan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_peleburan_peleburan_id_seq;
       public          postgres    false    256            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;
          public          postgres    false    257                       1259    18949    beone_pemakaian_bahan_detail    TABLE     �   CREATE TABLE public.beone_pemakaian_bahan_detail (
    pemakaian_detail_id bigint NOT NULL,
    pemakaian_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision
);
 0   DROP TABLE public.beone_pemakaian_bahan_detail;
       public         heap    postgres    false                       1259    18952 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq;
       public          postgres    false    258            �           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNED BY public.beone_pemakaian_bahan_detail.pemakaian_detail_id;
          public          postgres    false    259            P           1259    20389    beone_piutang_detail    TABLE     �   CREATE TABLE public.beone_piutang_detail (
    piutang_detail_id integer NOT NULL,
    piutang_header_id integer NOT NULL,
    export_header_id integer NOT NULL,
    amount double precision DEFAULT 0 NOT NULL
);
 (   DROP TABLE public.beone_piutang_detail;
       public         heap    postgres    false            O           1259    20387 *   beone_piutang_detail_piutang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_piutang_detail_piutang_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_piutang_detail_piutang_detail_id_seq;
       public          postgres    false    336            �           0    0 *   beone_piutang_detail_piutang_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_piutang_detail_piutang_detail_id_seq OWNED BY public.beone_piutang_detail.piutang_detail_id;
          public          postgres    false    335            N           1259    20378    beone_piutang_header    TABLE     �  CREATE TABLE public.beone_piutang_header (
    piutang_header_id integer NOT NULL,
    trans_no character varying(50) NOT NULL,
    trans_date date NOT NULL,
    customer_id integer NOT NULL,
    grandtotal double precision DEFAULT 0 NOT NULL,
    keterangan character varying(255),
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 (   DROP TABLE public.beone_piutang_header;
       public         heap    postgres    false            M           1259    20376 *   beone_piutang_header_piutang_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_piutang_header_piutang_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_piutang_header_piutang_header_id_seq;
       public          postgres    false    334            �           0    0 *   beone_piutang_header_piutang_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_piutang_header_piutang_header_id_seq OWNED BY public.beone_piutang_header.piutang_header_id;
          public          postgres    false    333                       1259    18954    beone_pm_detail    TABLE     �   CREATE TABLE public.beone_pm_detail (
    pm_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_id integer,
    pm_header_id integer
);
 #   DROP TABLE public.beone_pm_detail;
       public         heap    postgres    false                       1259    18957     beone_pm_detail_pm_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_detail_pm_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_detail_pm_detail_id_seq;
       public          postgres    false    260            �           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_detail_pm_detail_id_seq OWNED BY public.beone_pm_detail.pm_detail_id;
          public          postgres    false    261                       1259    18959    beone_pm_header    TABLE     �  CREATE TABLE public.beone_pm_header (
    pm_header_id bigint NOT NULL,
    no_pm character varying(50),
    tgl_pm date,
    customer_id integer,
    qty double precision,
    keterangan_artikel character varying,
    status integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 #   DROP TABLE public.beone_pm_header;
       public         heap    postgres    false                       1259    18967     beone_pm_header_pm_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_header_pm_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_header_pm_header_id_seq;
       public          postgres    false    262            �           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_header_pm_header_id_seq OWNED BY public.beone_pm_header.pm_header_id;
          public          postgres    false    263                       1259    18969    beone_po_import_detail    TABLE     8  CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer,
    deleted_at timestamp without time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    amount_sys double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL
);
 *   DROP TABLE public.beone_po_import_detail;
       public         heap    postgres    false            	           1259    18972 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq;
       public          postgres    false    264            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;
          public          postgres    false    265            
           1259    18974    beone_po_import_header    TABLE     �  CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer NOT NULL,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    total_hitung double precision,
    currency_id integer DEFAULT 1 NOT NULL,
    kurs integer DEFAULT 1 NOT NULL,
    grandtotal_sys double precision DEFAULT 0 NOT NULL
);
 *   DROP TABLE public.beone_po_import_header;
       public         heap    postgres    false                       1259    18979 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_header_purchase_header_id_seq;
       public          postgres    false    266            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;
          public          postgres    false    267                       1259    18981    beone_produksi_detail    TABLE     �   CREATE TABLE public.beone_produksi_detail (
    produksi_detail_id bigint NOT NULL,
    produksi_header_id integer,
    pemakaian_header_id integer
);
 )   DROP TABLE public.beone_produksi_detail;
       public         heap    postgres    false            
           1259    18984 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq;
       public          postgres    false    268            �           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq OWNED BY public.beone_produksi_detail.produksi_detail_id;
          public          postgres    false    269                       1259    18986    beone_produksi_header    TABLE     '  CREATE TABLE public.beone_produksi_header (
    produksi_header_id bigint NOT NULL,
    nomor_produksi character varying(20),
    tgl_produksi date,
    tipe_produksi integer,
    item_produksi integer,
    qty_hasil_produksi double precision,
    keterangan character varying(250),
    flag integer,
    updated_by integer,
    udpated_date date,
    pm_header_id integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 )   DROP TABLE public.beone_produksi_header;
       public         heap    postgres    false                       1259    18991 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_header_produksi_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_header_produksi_header_id_seq;
       public          postgres    false    270            �           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_header_produksi_header_id_seq OWNED BY public.beone_produksi_header.produksi_header_id;
          public          postgres    false    271                       1259    18993    beone_purchase_detail    TABLE     �   CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 )   DROP TABLE public.beone_purchase_detail;
       public         heap    postgres    false                       1259    18996 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq;
       public          postgres    false    272            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;
          public          postgres    false    273                       1259    18998    beone_purchase_header    TABLE     d  CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer NOT NULL,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    realisasi integer DEFAULT 0 NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 )   DROP TABLE public.beone_purchase_header;
       public         heap    postgres    false                       1259    19004 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_header_purchase_header_id_seq;
       public          postgres    false    274            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;
          public          postgres    false    275                       1259    19006    beone_received_import    TABLE     �  CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);
 )   DROP TABLE public.beone_received_import;
       public         heap    postgres    false                       1259    19009 %   beone_received_import_received_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.beone_received_import_received_id_seq;
       public          postgres    false    276            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;
          public          postgres    false    277                       1259    19011 
   beone_role    TABLE     �  CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.beone_role;
       public         heap    postgres    false                       1259    19016    beone_role_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_role_role_id_seq;
       public          postgres    false    278            �           0    0    beone_role_role_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;
          public          postgres    false    279                       1259    19018    beone_role_user    TABLE     �  CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);
 #   DROP TABLE public.beone_role_user;
       public         heap    postgres    false                       1259    19021    beone_role_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.beone_role_user_role_id_seq;
       public          postgres    false    280            �           0    0    beone_role_user_role_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;
          public          postgres    false    281            L           1259    19948    beone_roles_modul    TABLE     �   CREATE TABLE public.beone_roles_modul (
    role_modul_id integer NOT NULL,
    role_id integer,
    modul_id integer NOT NULL,
    parent_id integer,
    is_hidden integer DEFAULT 0 NOT NULL
);
 %   DROP TABLE public.beone_roles_modul;
       public         heap    postgres    false            K           1259    19946 #   beone_roles_modul_role_modul_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_roles_modul_role_modul_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.beone_roles_modul_role_modul_id_seq;
       public          postgres    false    332            �           0    0 #   beone_roles_modul_role_modul_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.beone_roles_modul_role_modul_id_seq OWNED BY public.beone_roles_modul.role_modul_id;
          public          postgres    false    331                       1259    19023    beone_roles_user    TABLE     E  CREATE TABLE public.beone_roles_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(255),
    deleted_at timestamp without time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 $   DROP TABLE public.beone_roles_user;
       public         heap    postgres    false                       1259    19028    beone_roles_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_roles_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_roles_user_role_id_seq;
       public          postgres    false    282            �           0    0    beone_roles_user_role_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_roles_user_role_id_seq OWNED BY public.beone_roles_user.role_id;
          public          postgres    false    283                       1259    19030    beone_sales_detail    TABLE     |  CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer,
    amount_sys double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL
);
 &   DROP TABLE public.beone_sales_detail;
       public         heap    postgres    false                       1259    19033 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_detail_sales_detail_id_seq;
       public          postgres    false    284            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;
          public          postgres    false    285                       1259    19035    beone_sales_header    TABLE       CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer NOT NULL,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer,
    biaya double precision,
    status integer,
    realisasi integer DEFAULT 0 NOT NULL,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    grandtotal_sys double precision DEFAULT 0 NOT NULL,
    currency_id integer DEFAULT 1 NOT NULL,
    kurs integer DEFAULT 1 NOT NULL
);
 &   DROP TABLE public.beone_sales_header;
       public         heap    postgres    false                       1259    19041 &   beone_sales_header_sales_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_header_sales_header_id_seq;
       public          postgres    false    286            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;
          public          postgres    false    287                        1259    19043    beone_satuan_item    TABLE     �   CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer,
    item_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL
);
 %   DROP TABLE public.beone_satuan_item;
       public         heap    postgres    false            !           1259    19047    beone_satuan_item_satuan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.beone_satuan_item_satuan_id_seq;
       public          postgres    false    288            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;
          public          postgres    false    289            I           1259    19921    beone_stok_opname_detail    TABLE     �   CREATE TABLE public.beone_stok_opname_detail (
    stok_opname_detail_id integer NOT NULL,
    stok_opname_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision NOT NULL,
    gudang_id integer NOT NULL
);
 ,   DROP TABLE public.beone_stok_opname_detail;
       public         heap    postgres    false            H           1259    19919 2   beone_stok_opname_detail_stok_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_stok_opname_detail_stok_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.beone_stok_opname_detail_stok_opname_detail_id_seq;
       public          postgres    false    329            �           0    0 2   beone_stok_opname_detail_stok_opname_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_stok_opname_detail_stok_opname_detail_id_seq OWNED BY public.beone_stok_opname_detail.stok_opname_detail_id;
          public          postgres    false    328            G           1259    19911    beone_stok_opname_header    TABLE     �  CREATE TABLE public.beone_stok_opname_header (
    stok_opname_header_id integer NOT NULL,
    stok_opname_no character varying(50) NOT NULL,
    trans_date date NOT NULL,
    keterangan character varying(255),
    approved_by timestamp with time zone,
    approved_at timestamp with time zone,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 ,   DROP TABLE public.beone_stok_opname_header;
       public         heap    postgres    false            F           1259    19909 2   beone_stok_opname_header_stok_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_stok_opname_header_stok_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.beone_stok_opname_header_stok_opname_header_id_seq;
       public          postgres    false    327            �           0    0 2   beone_stok_opname_header_stok_opname_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_stok_opname_header_stok_opname_header_id_seq OWNED BY public.beone_stok_opname_header.stok_opname_header_id;
          public          postgres    false    326            X           1259    20451    beone_sub_kontraktor_in_detail    TABLE     �  CREATE TABLE public.beone_sub_kontraktor_in_detail (
    sub_kontraktor_in_detail_id integer NOT NULL,
    sub_kontraktor_in_header_id integer,
    item_id integer,
    qty double precision DEFAULT 0 NOT NULL,
    price double precision DEFAULT 0 NOT NULL,
    gudang_id integer DEFAULT 0 NOT NULL,
    bc_barang_id integer,
    bc_barang_uraian character varying(1000),
    isppn integer DEFAULT 0 NOT NULL,
    amount double precision DEFAULT 0 NOT NULL,
    amount_ppn double precision DEFAULT 0 NOT NULL
);
 2   DROP TABLE public.beone_sub_kontraktor_in_detail;
       public         heap    postgres    false            W           1259    20449 >   beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 U   DROP SEQUENCE public.beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq;
       public          postgres    false    344            �           0    0 >   beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq OWNED BY public.beone_sub_kontraktor_in_detail.sub_kontraktor_in_detail_id;
          public          postgres    false    343            V           1259    20425    beone_sub_kontraktor_in_header    TABLE     �  CREATE TABLE public.beone_sub_kontraktor_in_header (
    sub_kontraktor_in_header_id integer NOT NULL,
    trans_no character varying(200) NOT NULL,
    trans_date date NOT NULL,
    supplier_id integer,
    currency_id integer DEFAULT 1 NOT NULL,
    sub_kontraktor_out_header_id integer,
    biaya double precision DEFAULT 0 NOT NULL,
    dpp double precision DEFAULT 0 NOT NULL,
    ppn double precision DEFAULT 0 NOT NULL,
    grandtotal double precision DEFAULT 0 NOT NULL,
    sisabayar double precision DEFAULT 0 NOT NULL,
    terbayar double precision DEFAULT 0 NOT NULL,
    jenis_bc integer,
    bc_no character varying(50),
    bc_date date,
    bc_no_aju character varying(500),
    bc_nama_vendor character varying(500),
    bc_nama_pemilik character varying(500),
    bc_nama_penerima_barang character varying(500),
    bc_nama_pengangkut character varying(500),
    bc_nama_pengirim character varying(500),
    bc_kurs double precision DEFAULT 1 NOT NULL,
    bc_header_id integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 2   DROP TABLE public.beone_sub_kontraktor_in_header;
       public         heap    postgres    false            U           1259    20423 >   beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 U   DROP SEQUENCE public.beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq;
       public          postgres    false    342            �           0    0 >   beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq OWNED BY public.beone_sub_kontraktor_in_header.sub_kontraktor_in_header_id;
          public          postgres    false    341            \           1259    20503    beone_sub_kontraktor_out_detail    TABLE     5  CREATE TABLE public.beone_sub_kontraktor_out_detail (
    sub_kontraktor_out_detail_id integer NOT NULL,
    sub_kontraktor_out_header_id integer,
    item_id integer,
    qty double precision DEFAULT 0 NOT NULL,
    price double precision DEFAULT 0 NOT NULL,
    gudang_id integer DEFAULT 0 NOT NULL,
    bc_barang_id integer,
    bc_barang_uraian character varying(1000),
    isppn integer DEFAULT 0 NOT NULL,
    amount double precision DEFAULT 0 NOT NULL,
    amount_ppn double precision DEFAULT 0 NOT NULL,
    price_hpp double precision DEFAULT 0 NOT NULL
);
 3   DROP TABLE public.beone_sub_kontraktor_out_detail;
       public         heap    postgres    false            [           1259    20501 ?   beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 V   DROP SEQUENCE public.beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq;
       public          postgres    false    348            �           0    0 ?   beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq OWNED BY public.beone_sub_kontraktor_out_detail.sub_kontraktor_out_detail_id;
          public          postgres    false    347            Z           1259    20478    beone_sub_kontraktor_out_header    TABLE     ?  CREATE TABLE public.beone_sub_kontraktor_out_header (
    sub_kontraktor_out_header_id integer NOT NULL,
    trans_no character varying(200) NOT NULL,
    trans_date date NOT NULL,
    supplier_id integer,
    currency_id integer DEFAULT 1 NOT NULL,
    dpp double precision DEFAULT 0 NOT NULL,
    ppn double precision DEFAULT 0 NOT NULL,
    grandtotal double precision DEFAULT 0 NOT NULL,
    sisabayar double precision DEFAULT 0 NOT NULL,
    terbayar double precision DEFAULT 0 NOT NULL,
    jenis_bc integer,
    bc_no character varying(50),
    bc_date date,
    bc_no_aju character varying(500),
    bc_nama_vendor character varying(500),
    bc_nama_pemilik character varying(500),
    bc_nama_penerima_barang character varying(500),
    bc_nama_pengangkut character varying(500),
    bc_nama_pengirim character varying(500),
    bc_kurs double precision DEFAULT 1 NOT NULL,
    bc_header_id integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 3   DROP TABLE public.beone_sub_kontraktor_out_header;
       public         heap    postgres    false            Y           1259    20476 ?   beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 V   DROP SEQUENCE public.beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq;
       public          postgres    false    346            �           0    0 ?   beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq OWNED BY public.beone_sub_kontraktor_out_header.sub_kontraktor_out_header_id;
          public          postgres    false    345            "           1259    19049    beone_subkon_in_detail    TABLE     �   CREATE TABLE public.beone_subkon_in_detail (
    subkon_in_detail_id bigint NOT NULL,
    subkon_in_header_id integer,
    item_id integer,
    qty double precision,
    flag integer
);
 *   DROP TABLE public.beone_subkon_in_detail;
       public         heap    postgres    false            #           1259    19052 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq;
       public          postgres    false    290            �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNED BY public.beone_subkon_in_detail.subkon_in_detail_id;
          public          postgres    false    291            $           1259    19054    beone_subkon_in_header    TABLE     ~  CREATE TABLE public.beone_subkon_in_header (
    subkon_in_id bigint NOT NULL,
    no_subkon_in character varying(50),
    no_subkon_out character varying(50),
    trans_date date,
    keterangan character varying(250),
    nomor_aju character varying(50),
    biaya_subkon double precision,
    supplier_id integer,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_subkon_in_header;
       public         heap    postgres    false            %           1259    19057 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq;
       public          postgres    false    292            �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq OWNED BY public.beone_subkon_in_header.subkon_in_id;
          public          postgres    false    293            &           1259    19059    beone_subkon_out_detail    TABLE     �   CREATE TABLE public.beone_subkon_out_detail (
    subkon_out_detail_id bigint NOT NULL,
    subkon_out_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision,
    flag integer
);
 +   DROP TABLE public.beone_subkon_out_detail;
       public         heap    postgres    false            '           1259    19062 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 G   DROP SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq;
       public          postgres    false    294            �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNED BY public.beone_subkon_out_detail.subkon_out_detail_id;
          public          postgres    false    295            (           1259    19064    beone_subkon_out_header    TABLE     �  CREATE TABLE public.beone_subkon_out_header (
    subkon_out_id bigint NOT NULL,
    no_subkon_out character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    nomor_aju character varying(50),
    flag integer,
    update_by integer,
    update_date date,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 +   DROP TABLE public.beone_subkon_out_header;
       public         heap    postgres    false            )           1259    19069 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq;
       public          postgres    false    296            �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq OWNED BY public.beone_subkon_out_header.subkon_out_id;
          public          postgres    false    297            <           1259    19512    beone_supplier    TABLE     �  CREATE TABLE public.beone_supplier (
    supplier_id integer NOT NULL,
    supplier_code character varying(50) NOT NULL,
    nama character varying(50),
    alamat character varying(355),
    ppn integer,
    pph21 integer,
    pph23 integer,
    deleted_at timestamp without time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 "   DROP TABLE public.beone_supplier;
       public         heap    postgres    false            ;           1259    19510    beone_supplier_supplier_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_supplier_supplier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_supplier_supplier_id_seq;
       public          postgres    false    316            �           0    0    beone_supplier_supplier_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_supplier_supplier_id_seq OWNED BY public.beone_supplier.supplier_id;
          public          postgres    false    315            *           1259    19071    beone_tipe_coa    TABLE     i   CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);
 "   DROP TABLE public.beone_tipe_coa;
       public         heap    postgres    false            +           1259    19074    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq;
       public          postgres    false    298            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;
          public          postgres    false    299            ,           1259    19076    beone_tipe_coa_transaksi    TABLE     �   CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);
 ,   DROP TABLE public.beone_tipe_coa_transaksi;
       public         heap    postgres    false            -           1259    19079 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq;
       public          postgres    false    300            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;
          public          postgres    false    301            .           1259    19081    beone_transfer_stock    TABLE     �  CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 (   DROP TABLE public.beone_transfer_stock;
       public         heap    postgres    false            /           1259    19086    beone_transfer_stock_detail    TABLE       CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer NOT NULL,
    tipe_transfer_stock character varying(10),
    item_id integer NOT NULL,
    qty double precision,
    gudang_id integer NOT NULL,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer,
    price_hpp double precision DEFAULT 0 NOT NULL,
    satuan_id integer NOT NULL,
    rasio double precision DEFAULT 1 NOT NULL
);
 /   DROP TABLE public.beone_transfer_stock_detail;
       public         heap    postgres    false            0           1259    19089 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 O   DROP SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq;
       public          postgres    false    303            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;
          public          postgres    false    304            1           1259    19091 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq;
       public          postgres    false    302            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;
          public          postgres    false    305            `           1259    20557    beone_usage_detail    TABLE     �   CREATE TABLE public.beone_usage_detail (
    usage_detail_id integer NOT NULL,
    usage_header_id integer NOT NULL,
    item_id integer NOT NULL,
    qty double precision NOT NULL,
    gudang_id integer NOT NULL
);
 &   DROP TABLE public.beone_usage_detail;
       public         heap    postgres    false            _           1259    20555 &   beone_usage_detail_usage_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_usage_detail_usage_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_usage_detail_usage_detail_id_seq;
       public          postgres    false    352            �           0    0 &   beone_usage_detail_usage_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_usage_detail_usage_detail_id_seq OWNED BY public.beone_usage_detail.usage_detail_id;
          public          postgres    false    351            ^           1259    20547    beone_usage_header    TABLE     r  CREATE TABLE public.beone_usage_header (
    usage_header_id integer NOT NULL,
    trans_no character varying(50) NOT NULL,
    trans_date date NOT NULL,
    keterangan character varying(255),
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 &   DROP TABLE public.beone_usage_header;
       public         heap    postgres    false            ]           1259    20545 &   beone_usage_header_usage_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_usage_header_usage_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_usage_header_usage_header_id_seq;
       public          postgres    false    350            �           0    0 &   beone_usage_header_usage_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_usage_header_usage_header_id_seq OWNED BY public.beone_usage_header.usage_header_id;
          public          postgres    false    349            2           1259    19093 
   beone_user    TABLE     �  CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(500),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    password_old character varying(1000) DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.beone_user;
       public         heap    postgres    false            3           1259    19101    beone_user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_user_user_id_seq;
       public          postgres    false    306            �           0    0    beone_user_user_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;
          public          postgres    false    307            4           1259    19103    beone_voucher_detail    TABLE     n  CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200),
    currency_id integer DEFAULT 1 NOT NULL
);
 (   DROP TABLE public.beone_voucher_detail;
       public         heap    postgres    false            5           1259    19106 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq;
       public          postgres    false    308            �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;
          public          postgres    false    309            6           1259    19108    beone_voucher_header    TABLE       CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(50),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date,
    deleted_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    grandtotal double precision DEFAULT 0 NOT NULL
);
 (   DROP TABLE public.beone_voucher_header;
       public         heap    postgres    false            7           1259    19113 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_header_voucher_header_id_seq;
       public          postgres    false    310            �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;
          public          postgres    false    311            8           1259    19115    t_po    TABLE     ^   CREATE TABLE public.t_po (
    purchase_header_id integer,
    grandtotal double precision
);
    DROP TABLE public.t_po;
       public         heap    postgres    false            E           1259    19901    temp_satuan    TABLE     �   CREATE TABLE public.temp_satuan (
    satuan_code character varying(5),
    keterangan character varying(5),
    flag integer,
    item_id integer,
    rasio integer
);
    DROP TABLE public.temp_satuan;
       public         heap    postgres    false            k           2604    19118    beone_adjustment adjustment_id    DEFAULT     �   ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);
 M   ALTER TABLE public.beone_adjustment ALTER COLUMN adjustment_id DROP DEFAULT;
       public          postgres    false    203    202            n           2604    19119    beone_coa coa_id    DEFAULT     t   ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);
 ?   ALTER TABLE public.beone_coa ALTER COLUMN coa_id DROP DEFAULT;
       public          postgres    false    205    204            o           2604    19120    beone_coa_jurnal coa_jurnal_id    DEFAULT     �   ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);
 M   ALTER TABLE public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id DROP DEFAULT;
       public          postgres    false    207    206            p           2604    19121    beone_country country_id    DEFAULT     �   ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);
 G   ALTER TABLE public.beone_country ALTER COLUMN country_id DROP DEFAULT;
       public          postgres    false    209    208            
           2604    19835    beone_currency currency_id    DEFAULT     �   ALTER TABLE ONLY public.beone_currency ALTER COLUMN currency_id SET DEFAULT nextval('public.beone_currency_currency_id_seq'::regclass);
 I   ALTER TABLE public.beone_currency ALTER COLUMN currency_id DROP DEFAULT;
       public          postgres    false    318    317    318            

           2604    19503    beone_customer customer_id    DEFAULT     �   ALTER TABLE ONLY public.beone_customer ALTER COLUMN customer_id SET DEFAULT nextval('public.beone_customer_customer_id_seq'::regclass);
 I   ALTER TABLE public.beone_customer ALTER COLUMN customer_id DROP DEFAULT;
       public          postgres    false    314    313    314            s           2604    19122    beone_custsup custsup_id    DEFAULT     �   ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);
 G   ALTER TABLE public.beone_custsup ALTER COLUMN custsup_id DROP DEFAULT;
       public          postgres    false    211    210            u           2604    19123 $   beone_export_detail export_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_detail ALTER COLUMN export_detail_id DROP DEFAULT;
       public          postgres    false    213    212            |           2604    19124 $   beone_export_header export_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_header ALTER COLUMN export_header_id DROP DEFAULT;
       public          postgres    false    215    214            
           2604    19891 &   beone_general_ledger general_ledger_id    DEFAULT     �   ALTER TABLE ONLY public.beone_general_ledger ALTER COLUMN general_ledger_id SET DEFAULT nextval('public.beone_general_ledger_general_ledger_id_seq'::regclass);
 U   ALTER TABLE public.beone_general_ledger ALTER COLUMN general_ledger_id DROP DEFAULT;
       public          postgres    false    323    324    324            �           2604    19125    beone_gl gl_id    DEFAULT     p   ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);
 =   ALTER TABLE public.beone_gl ALTER COLUMN gl_id DROP DEFAULT;
       public          postgres    false    217    216            �           2604    19126    beone_gudang gudang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);
 E   ALTER TABLE public.beone_gudang ALTER COLUMN gudang_id DROP DEFAULT;
       public          postgres    false    221    218            �           2604    19127 $   beone_gudang_detail gudang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_gudang_detail ALTER COLUMN gudang_detail_id DROP DEFAULT;
       public          postgres    false    220    219            -
           2604    20412 $   beone_hutang_detail hutang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_detail ALTER COLUMN hutang_detail_id SET DEFAULT nextval('public.beone_hutang_detail_hutang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_hutang_detail ALTER COLUMN hutang_detail_id DROP DEFAULT;
       public          postgres    false    340    339    340            )
           2604    20401 $   beone_hutang_header hutang_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_header ALTER COLUMN hutang_header_id SET DEFAULT nextval('public.beone_hutang_header_hutang_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_hutang_header ALTER COLUMN hutang_header_id DROP DEFAULT;
       public          postgres    false    338    337    338            �           2604    19128 &   beone_hutang_piutang hutang_piutang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);
 U   ALTER TABLE public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id DROP DEFAULT;
       public          postgres    false    225    224            �           2604    19129 $   beone_import_detail import_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_detail ALTER COLUMN import_detail_id DROP DEFAULT;
       public          postgres    false    227    226            �           2604    19130 $   beone_import_header import_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_header ALTER COLUMN import_header_id DROP DEFAULT;
       public          postgres    false    229    228            �           2604    19131     beone_inventory intvent_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);
 O   ALTER TABLE public.beone_inventory ALTER COLUMN intvent_trans_id DROP DEFAULT;
       public          postgres    false    231    230            �           2604    19132    beone_item item_id    DEFAULT     x   ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);
 A   ALTER TABLE public.beone_item ALTER COLUMN item_id DROP DEFAULT;
       public          postgres    false    234    232            �           2604    19133    beone_item_type item_type_id    DEFAULT     �   ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);
 K   ALTER TABLE public.beone_item_type ALTER COLUMN item_type_id DROP DEFAULT;
       public          postgres    false    236    235            
           2604    19862 $   beone_jurnal_detail jurnal_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_jurnal_detail ALTER COLUMN jurnal_detail_id SET DEFAULT nextval('public.beone_jurnal_detail_jurnal_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_jurnal_detail ALTER COLUMN jurnal_detail_id DROP DEFAULT;
       public          postgres    false    322    321    322            
           2604    19852 $   beone_jurnal_header jurnal_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_jurnal_header ALTER COLUMN jurnal_header_id SET DEFAULT nextval('public.beone_jurnal_header_jurnal_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_jurnal_header ALTER COLUMN jurnal_header_id DROP DEFAULT;
       public          postgres    false    320    319    320            �           2604    19134    beone_kode_trans kode_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);
 M   ALTER TABLE public.beone_kode_trans ALTER COLUMN kode_trans_id DROP DEFAULT;
       public          postgres    false    238    237            �           2604    19135    beone_komposisi komposisi_id    DEFAULT     �   ALTER TABLE ONLY public.beone_komposisi ALTER COLUMN komposisi_id SET DEFAULT nextval('public.beone_komposisi_komposisi_id_seq'::regclass);
 K   ALTER TABLE public.beone_komposisi ALTER COLUMN komposisi_id DROP DEFAULT;
       public          postgres    false    240    239            �           2604    19136 2   beone_konversi_stok_detail konversi_stok_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id SET DEFAULT nextval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id DROP DEFAULT;
       public          postgres    false    243    242            �           2604    19137 2   beone_konversi_stok_header konversi_stok_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id SET DEFAULT nextval('public.beone_konversi_stok_header_konversi_stok_header_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id DROP DEFAULT;
       public          postgres    false    245    244            �           2604    19138    beone_log log_id    DEFAULT     t   ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);
 ?   ALTER TABLE public.beone_log ALTER COLUMN log_id DROP DEFAULT;
       public          postgres    false    247    246            �           2604    19139 .   beone_mutasi_stok_detail mutasi_stok_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail ALTER COLUMN mutasi_stok_detail_id SET DEFAULT nextval('public.beone_mutasi_stok_detail_mutasi_stok_detail_id_seq'::regclass);
 ]   ALTER TABLE public.beone_mutasi_stok_detail ALTER COLUMN mutasi_stok_detail_id DROP DEFAULT;
       public          postgres    false    249    248            �           2604    19140 .   beone_mutasi_stok_header mutasi_stok_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_mutasi_stok_header ALTER COLUMN mutasi_stok_header_id SET DEFAULT nextval('public.beone_mutasi_stok_header_mutasi_stok_header_id_seq'::regclass);
 ]   ALTER TABLE public.beone_mutasi_stok_header ALTER COLUMN mutasi_stok_header_id DROP DEFAULT;
       public          postgres    false    251    250            �           2604    19141 $   beone_opname_detail opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_detail ALTER COLUMN opname_detail_id DROP DEFAULT;
       public          postgres    false    253    252            �           2604    19142 $   beone_opname_header opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_header ALTER COLUMN opname_header_id DROP DEFAULT;
       public          postgres    false    255    254            �           2604    19143    beone_peleburan peleburan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);
 K   ALTER TABLE public.beone_peleburan ALTER COLUMN peleburan_id DROP DEFAULT;
       public          postgres    false    257    256            �           2604    19144 0   beone_pemakaian_bahan_detail pemakaian_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id SET DEFAULT nextval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id DROP DEFAULT;
       public          postgres    false    259    258            �           2604    19145 0   beone_pemakaian_bahan_header pemakaian_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id SET DEFAULT nextval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id DROP DEFAULT;
       public          postgres    false    223    222            '
           2604    20392 &   beone_piutang_detail piutang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_piutang_detail ALTER COLUMN piutang_detail_id SET DEFAULT nextval('public.beone_piutang_detail_piutang_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_piutang_detail ALTER COLUMN piutang_detail_id DROP DEFAULT;
       public          postgres    false    336    335    336            #
           2604    20381 &   beone_piutang_header piutang_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_piutang_header ALTER COLUMN piutang_header_id SET DEFAULT nextval('public.beone_piutang_header_piutang_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_piutang_header ALTER COLUMN piutang_header_id DROP DEFAULT;
       public          postgres    false    334    333    334            �           2604    19146    beone_pm_detail pm_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_detail ALTER COLUMN pm_detail_id SET DEFAULT nextval('public.beone_pm_detail_pm_detail_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_detail ALTER COLUMN pm_detail_id DROP DEFAULT;
       public          postgres    false    261    260            �           2604    19147    beone_pm_header pm_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_header ALTER COLUMN pm_header_id SET DEFAULT nextval('public.beone_pm_header_pm_header_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_header ALTER COLUMN pm_header_id DROP DEFAULT;
       public          postgres    false    263    262            �           2604    19148 )   beone_po_import_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public          postgres    false    265    264            �           2604    19149 )   beone_po_import_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public          postgres    false    267    266            �           2604    19150 (   beone_produksi_detail produksi_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_detail ALTER COLUMN produksi_detail_id SET DEFAULT nextval('public.beone_produksi_detail_produksi_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_detail ALTER COLUMN produksi_detail_id DROP DEFAULT;
       public          postgres    false    269    268            �           2604    19151 (   beone_produksi_header produksi_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_header ALTER COLUMN produksi_header_id SET DEFAULT nextval('public.beone_produksi_header_produksi_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_header ALTER COLUMN produksi_header_id DROP DEFAULT;
       public          postgres    false    271    270            �           2604    19152 (   beone_purchase_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public          postgres    false    273    272            �           2604    19153 (   beone_purchase_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public          postgres    false    275    274            �           2604    19154 !   beone_received_import received_id    DEFAULT     �   ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);
 P   ALTER TABLE public.beone_received_import ALTER COLUMN received_id DROP DEFAULT;
       public          postgres    false    277    276            �           2604    19155    beone_role role_id    DEFAULT     x   ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);
 A   ALTER TABLE public.beone_role ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    279    278            �           2604    19156    beone_role_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);
 F   ALTER TABLE public.beone_role_user ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    281    280            !
           2604    19951    beone_roles_modul role_modul_id    DEFAULT     �   ALTER TABLE ONLY public.beone_roles_modul ALTER COLUMN role_modul_id SET DEFAULT nextval('public.beone_roles_modul_role_modul_id_seq'::regclass);
 N   ALTER TABLE public.beone_roles_modul ALTER COLUMN role_modul_id DROP DEFAULT;
       public          postgres    false    332    331    332            �           2604    19157    beone_roles_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_roles_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_roles_user_role_id_seq'::regclass);
 G   ALTER TABLE public.beone_roles_user ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    283    282            �           2604    19158 "   beone_sales_detail sales_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_detail ALTER COLUMN sales_detail_id DROP DEFAULT;
       public          postgres    false    285    284            �           2604    19159 "   beone_sales_header sales_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_header ALTER COLUMN sales_header_id DROP DEFAULT;
       public          postgres    false    287    286            �           2604    19160    beone_satuan_item satuan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);
 J   ALTER TABLE public.beone_satuan_item ALTER COLUMN satuan_id DROP DEFAULT;
       public          postgres    false    289    288            
           2604    19924 .   beone_stok_opname_detail stok_opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_stok_opname_detail ALTER COLUMN stok_opname_detail_id SET DEFAULT nextval('public.beone_stok_opname_detail_stok_opname_detail_id_seq'::regclass);
 ]   ALTER TABLE public.beone_stok_opname_detail ALTER COLUMN stok_opname_detail_id DROP DEFAULT;
       public          postgres    false    329    328    329            
           2604    19914 .   beone_stok_opname_header stok_opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_stok_opname_header ALTER COLUMN stok_opname_header_id SET DEFAULT nextval('public.beone_stok_opname_header_stok_opname_header_id_seq'::regclass);
 ]   ALTER TABLE public.beone_stok_opname_header ALTER COLUMN stok_opname_header_id DROP DEFAULT;
       public          postgres    false    326    327    327            :
           2604    20454 :   beone_sub_kontraktor_in_detail sub_kontraktor_in_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail ALTER COLUMN sub_kontraktor_in_detail_id SET DEFAULT nextval('public.beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq'::regclass);
 i   ALTER TABLE public.beone_sub_kontraktor_in_detail ALTER COLUMN sub_kontraktor_in_detail_id DROP DEFAULT;
       public          postgres    false    343    344    344            /
           2604    20428 :   beone_sub_kontraktor_in_header sub_kontraktor_in_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_header ALTER COLUMN sub_kontraktor_in_header_id SET DEFAULT nextval('public.beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq'::regclass);
 i   ALTER TABLE public.beone_sub_kontraktor_in_header ALTER COLUMN sub_kontraktor_in_header_id DROP DEFAULT;
       public          postgres    false    342    341    342            K
           2604    20506 <   beone_sub_kontraktor_out_detail sub_kontraktor_out_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail ALTER COLUMN sub_kontraktor_out_detail_id SET DEFAULT nextval('public.beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq'::regclass);
 k   ALTER TABLE public.beone_sub_kontraktor_out_detail ALTER COLUMN sub_kontraktor_out_detail_id DROP DEFAULT;
       public          postgres    false    347    348    348            A
           2604    20481 <   beone_sub_kontraktor_out_header sub_kontraktor_out_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_header ALTER COLUMN sub_kontraktor_out_header_id SET DEFAULT nextval('public.beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq'::regclass);
 k   ALTER TABLE public.beone_sub_kontraktor_out_header ALTER COLUMN sub_kontraktor_out_header_id DROP DEFAULT;
       public          postgres    false    345    346    346            �           2604    19161 *   beone_subkon_in_detail subkon_in_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id SET DEFAULT nextval('public.beone_subkon_in_detail_subkon_in_detail_id_seq'::regclass);
 Y   ALTER TABLE public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id DROP DEFAULT;
       public          postgres    false    291    290            �           2604    19162 #   beone_subkon_in_header subkon_in_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_header ALTER COLUMN subkon_in_id SET DEFAULT nextval('public.beone_subkon_in_header_subkon_in_id_seq'::regclass);
 R   ALTER TABLE public.beone_subkon_in_header ALTER COLUMN subkon_in_id DROP DEFAULT;
       public          postgres    false    293    292            �           2604    19163 ,   beone_subkon_out_detail subkon_out_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id SET DEFAULT nextval('public.beone_subkon_out_detail_subkon_out_detail_id_seq'::regclass);
 [   ALTER TABLE public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id DROP DEFAULT;
       public          postgres    false    295    294            �           2604    19164 %   beone_subkon_out_header subkon_out_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_header ALTER COLUMN subkon_out_id SET DEFAULT nextval('public.beone_subkon_out_header_subkon_out_id_seq'::regclass);
 T   ALTER TABLE public.beone_subkon_out_header ALTER COLUMN subkon_out_id DROP DEFAULT;
       public          postgres    false    297    296            

           2604    19515    beone_supplier supplier_id    DEFAULT     �   ALTER TABLE ONLY public.beone_supplier ALTER COLUMN supplier_id SET DEFAULT nextval('public.beone_supplier_supplier_id_seq'::regclass);
 I   ALTER TABLE public.beone_supplier ALTER COLUMN supplier_id DROP DEFAULT;
       public          postgres    false    316    315    316            �           2604    19165    beone_tipe_coa tipe_coa_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);
 I   ALTER TABLE public.beone_tipe_coa ALTER COLUMN tipe_coa_id DROP DEFAULT;
       public          postgres    false    299    298            �           2604    19166 *   beone_tipe_coa_transaksi tipe_coa_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);
 Y   ALTER TABLE public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id DROP DEFAULT;
       public          postgres    false    301    300            �           2604    19167 &   beone_transfer_stock transfer_stock_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);
 U   ALTER TABLE public.beone_transfer_stock ALTER COLUMN transfer_stock_id DROP DEFAULT;
       public          postgres    false    305    302            �           2604    19168 4   beone_transfer_stock_detail transfer_stock_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);
 c   ALTER TABLE public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id DROP DEFAULT;
       public          postgres    false    304    303            V
           2604    20560 "   beone_usage_detail usage_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_usage_detail ALTER COLUMN usage_detail_id SET DEFAULT nextval('public.beone_usage_detail_usage_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_usage_detail ALTER COLUMN usage_detail_id DROP DEFAULT;
       public          postgres    false    351    352    352            S
           2604    20550 "   beone_usage_header usage_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_usage_header ALTER COLUMN usage_header_id SET DEFAULT nextval('public.beone_usage_header_usage_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_usage_header ALTER COLUMN usage_header_id DROP DEFAULT;
       public          postgres    false    350    349    350            
           2604    19169    beone_user user_id    DEFAULT     x   ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);
 A   ALTER TABLE public.beone_user ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    307    306            
           2604    19170 &   beone_voucher_detail voucher_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_detail ALTER COLUMN voucher_detail_id DROP DEFAULT;
       public          postgres    false    309    308            
           2604    19171 &   beone_voucher_header voucher_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_header ALTER COLUMN voucher_header_id DROP DEFAULT;
       public          postgres    false    311    310            �          0    18758    beone_adjustment 
   TABLE DATA           �   COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
    public          postgres    false    202   e�      �          0    18763 	   beone_coa 
   TABLE DATA           �   COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    204   ��      �          0    18773    beone_coa_jurnal 
   TABLE DATA           q   COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
    public          postgres    false    206   ��      �          0    18781 
   beone_country 
   TABLE DATA           M   COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
    public          postgres    false    208   ��      >          0    19832    beone_currency 
   TABLE DATA           P   COPY public.beone_currency (currency_id, currency_code, keterangan) FROM stdin;
    public          postgres    false    318   ��      :          0    19500    beone_customer 
   TABLE DATA           {   COPY public.beone_customer (customer_id, customer_code, nama, alamat, ppn, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    314   �      �          0    18786 
   beone_custsup 
   TABLE DATA           X  COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    210   -�      �          0    18793    beone_export_detail 
   TABLE DATA             COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag, sales_header_id, gudang_id, sales_detail_id, price_hpp, satuan_id, rasio, amount, amount_sys) FROM stdin;
    public          postgres    false    212   J�      �          0    18799    beone_export_header 
   TABLE DATA             COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal, deleted_at, created_at, updated_at, sisabayar, terbayar, currency_id, kurs) FROM stdin;
    public          postgres    false    214   g�      D          0    19888    beone_general_ledger 
   TABLE DATA           �   COPY public.beone_general_ledger (general_ledger_id, coa_id, trans_no, trans_date, keterangan, currency_id, kurs, debet, kredit, debet_idr, kredit_idr) FROM stdin;
    public          postgres    false    324   ��      �          0    18809    beone_gl 
   TABLE DATA           �   COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
    public          postgres    false    216   ��      �          0    18814    beone_gudang 
   TABLE DATA           z   COPY public.beone_gudang (gudang_id, nama, keterangan, flag, deleted_at, created_at, updated_at, gudang_code) FROM stdin;
    public          postgres    false    218   ��      �          0    18819    beone_gudang_detail 
   TABLE DATA           �   COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
    public          postgres    false    219   ��      T          0    20409    beone_hutang_detail 
   TABLE DATA           k   COPY public.beone_hutang_detail (hutang_detail_id, hutang_header_id, import_header_id, amount) FROM stdin;
    public          postgres    false    340   ��      R          0    20398    beone_hutang_header 
   TABLE DATA           �   COPY public.beone_hutang_header (hutang_header_id, trans_no, trans_date, supplier_id, grandtotal, keterangan, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    338   �      �          0    18833    beone_hutang_piutang 
   TABLE DATA           �   COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
    public          postgres    false    224   2�      �          0    18838    beone_import_detail 
   TABLE DATA           �  COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id, purchase_detail_id, gudang_id, isppn, bc_barang_id, amount, amount_sys, amount_ppn, amount_ppn_sys, satuan_id, rasio, bc_barang_uraian) FROM stdin;
    public          postgres    false    226   O�      �          0    18844    beone_import_header 
   TABLE DATA           �  COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no, import_no, trans_date, deleted_at, created_at, updated_at, sisabayar, terbayar, dpp, ppn, grandtotal, bc_no_aju, currency_id, bc_nama_vendor, bc_nama_pemilik, bc_nama_penerima_barang, bc_nama_pengangkut, bc_nama_pengirim, purchase_header_id, bc_kurs, bc_header_id, grandtotal_sys, dpp_sys, ppn_sys) FROM stdin;
    public          postgres    false    228   l�      �          0    18852    beone_inventory 
   TABLE DATA             COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date, gudang_id, invent_trans_detail_id, price_hpp) FROM stdin;
    public          postgres    false    230   ��      �          0    18858 
   beone_item 
   TABLE DATA           �   COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id, item_category_id, deleted_at, created_at, updated_at, level_item) FROM stdin;
    public          postgres    false    232   ��      �          0    18867    beone_item_category 
   TABLE DATA           W   COPY public.beone_item_category (item_category_id, nama, keterangan, flag) FROM stdin;
    public          postgres    false    233   ��      �          0    18872    beone_item_type 
   TABLE DATA           s   COPY public.beone_item_type (item_type_id, nama, keterangan, flag, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    235   ��      B          0    19859    beone_jurnal_detail 
   TABLE DATA           �   COPY public.beone_jurnal_detail (jurnal_detail_id, jurnal_header_id, coa_id, amount, kurs, amount_idr, keterangan_detail, status, deleted_at, created_at, updated_at, currency_id) FROM stdin;
    public          postgres    false    322   �      @          0    19849    beone_jurnal_header 
   TABLE DATA           �   COPY public.beone_jurnal_header (jurnal_header_id, jurnal_no, keterangan, trans_date, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    320   #�      �          0    18879    beone_kode_trans 
   TABLE DATA           `   COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
    public          postgres    false    237   @�      �          0    18887    beone_komposisi 
   TABLE DATA           h   COPY public.beone_komposisi (komposisi_id, item_jadi_id, item_baku_id, qty_item_baku, flag) FROM stdin;
    public          postgres    false    239   ]�      �          0    18892    beone_konfigurasi_perusahaan 
   TABLE DATA           �   COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan, cabang) FROM stdin;
    public          postgres    false    241   z�      �          0    18898    beone_konversi_stok_detail 
   TABLE DATA           �   COPY public.beone_konversi_stok_detail (konversi_stok_detail_id, konversi_stok_header_id, item_id, qty, qty_real, satuan_qty, gudang_id, qty_allocation, price_hpp, satuan_id, rasio) FROM stdin;
    public          postgres    false    242   ��      �          0    18905    beone_konversi_stok_header 
   TABLE DATA           �   COPY public.beone_konversi_stok_header (konversi_stok_header_id, item_id, konversi_stok_no, konversi_stok_date, flag, qty, satuan_qty, gudang_id, qty_real, deleted_at, created_at, updated_at, price_hpp) FROM stdin;
    public          postgres    false    244   ��      �          0    18914 	   beone_log 
   TABLE DATA           S   COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
    public          postgres    false    246   ��      J          0    19936    beone_modul 
   TABLE DATA           ]   COPY public.beone_modul (modul_id, modul, url, parent_id, is_hidden, icon_class) FROM stdin;
    public          postgres    false    330   ��      �          0    18919    beone_mutasi_stok_detail 
   TABLE DATA           �   COPY public.beone_mutasi_stok_detail (mutasi_stok_detail_id, mutasi_stok_header_id, item_id, qty, gudang_id_asal, gudang_id_tujuan) FROM stdin;
    public          postgres    false    248   ��      �          0    18924    beone_mutasi_stok_header 
   TABLE DATA           �   COPY public.beone_mutasi_stok_header (mutasi_stok_header_id, mutasi_stok_no, trans_date, keterangan, deleted_at, created_at, updated_at, flag) FROM stdin;
    public          postgres    false    250   ��      �          0    18932    beone_opname_detail 
   TABLE DATA           �   COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
    public          postgres    false    252   �      �          0    18937    beone_opname_header 
   TABLE DATA             COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    254   0�                 0    18944    beone_peleburan 
   TABLE DATA           �   COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
    public          postgres    false    256   M�                0    18949    beone_pemakaian_bahan_detail 
   TABLE DATA           z   COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM stdin;
    public          postgres    false    258   j�      �          0    18826    beone_pemakaian_bahan_header 
   TABLE DATA           �   COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    222   ��      P          0    20389    beone_piutang_detail 
   TABLE DATA           n   COPY public.beone_piutang_detail (piutang_detail_id, piutang_header_id, export_header_id, amount) FROM stdin;
    public          postgres    false    336   ��      N          0    20378    beone_piutang_header 
   TABLE DATA           �   COPY public.beone_piutang_header (piutang_header_id, trans_no, trans_date, customer_id, grandtotal, keterangan, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    334   ��                0    18954    beone_pm_detail 
   TABLE DATA           ^   COPY public.beone_pm_detail (pm_detail_id, item_id, qty, satuan_id, pm_header_id) FROM stdin;
    public          postgres    false    260   ��                0    18959    beone_pm_header 
   TABLE DATA           �   COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    262   ��                0    18969    beone_po_import_detail 
   TABLE DATA           �   COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag, deleted_at, created_at, updated_at, amount_sys, satuan_id, rasio) FROM stdin;
    public          postgres    false    264   �      
          0    18974    beone_po_import_header 
   TABLE DATA           �   COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date, deleted_at, created_at, updated_at, total_hitung, currency_id, kurs, grandtotal_sys) FROM stdin;
    public          postgres    false    266   5�                0    18981    beone_produksi_detail 
   TABLE DATA           l   COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM stdin;
    public          postgres    false    268   R�                0    18986    beone_produksi_header 
   TABLE DATA           �   COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date, pm_header_id, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    270   o�                0    18993    beone_purchase_detail 
   TABLE DATA           z   COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public          postgres    false    272   ��                0    18998    beone_purchase_header 
   TABLE DATA           �   COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal, realisasi, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    274   ��                0    19006    beone_received_import 
   TABLE DATA           �   COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
    public          postgres    false    276   ��                0    19011 
   beone_role 
   TABLE DATA           -  COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    278   ��                0    19018    beone_role_user 
   TABLE DATA           �  COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
    public          postgres    false    280    �      L          0    19948    beone_roles_modul 
   TABLE DATA           c   COPY public.beone_roles_modul (role_modul_id, role_id, modul_id, parent_id, is_hidden) FROM stdin;
    public          postgres    false    332   �                0    19023    beone_roles_user 
   TABLE DATA           n   COPY public.beone_roles_user (role_id, nama_role, keterangan, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    282   ��                0    19030    beone_sales_detail 
   TABLE DATA           �   COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag, amount_sys, satuan_id, rasio) FROM stdin;
    public          postgres    false    284   �                0    19035    beone_sales_header 
   TABLE DATA             COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status, realisasi, deleted_at, created_at, updated_at, grandtotal_sys, currency_id, kurs) FROM stdin;
    public          postgres    false    286   )�                 0    19043    beone_satuan_item 
   TABLE DATA           e   COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag, item_id, rasio) FROM stdin;
    public          postgres    false    288   F�      I          0    19921    beone_stok_opname_detail 
   TABLE DATA           y   COPY public.beone_stok_opname_detail (stok_opname_detail_id, stok_opname_header_id, item_id, qty, gudang_id) FROM stdin;
    public          postgres    false    329   c�      G          0    19911    beone_stok_opname_header 
   TABLE DATA           �   COPY public.beone_stok_opname_header (stok_opname_header_id, stok_opname_no, trans_date, keterangan, approved_by, approved_at, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    327   ��      X          0    20451    beone_sub_kontraktor_in_detail 
   TABLE DATA           �   COPY public.beone_sub_kontraktor_in_detail (sub_kontraktor_in_detail_id, sub_kontraktor_in_header_id, item_id, qty, price, gudang_id, bc_barang_id, bc_barang_uraian, isppn, amount, amount_ppn) FROM stdin;
    public          postgres    false    344   ��      V          0    20425    beone_sub_kontraktor_in_header 
   TABLE DATA           �  COPY public.beone_sub_kontraktor_in_header (sub_kontraktor_in_header_id, trans_no, trans_date, supplier_id, currency_id, sub_kontraktor_out_header_id, biaya, dpp, ppn, grandtotal, sisabayar, terbayar, jenis_bc, bc_no, bc_date, bc_no_aju, bc_nama_vendor, bc_nama_pemilik, bc_nama_penerima_barang, bc_nama_pengangkut, bc_nama_pengirim, bc_kurs, bc_header_id, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    342   ��      \          0    20503    beone_sub_kontraktor_out_detail 
   TABLE DATA           �   COPY public.beone_sub_kontraktor_out_detail (sub_kontraktor_out_detail_id, sub_kontraktor_out_header_id, item_id, qty, price, gudang_id, bc_barang_id, bc_barang_uraian, isppn, amount, amount_ppn, price_hpp) FROM stdin;
    public          postgres    false    348   ��      Z          0    20478    beone_sub_kontraktor_out_header 
   TABLE DATA           r  COPY public.beone_sub_kontraktor_out_header (sub_kontraktor_out_header_id, trans_no, trans_date, supplier_id, currency_id, dpp, ppn, grandtotal, sisabayar, terbayar, jenis_bc, bc_no, bc_date, bc_no_aju, bc_nama_vendor, bc_nama_pemilik, bc_nama_penerima_barang, bc_nama_pengangkut, bc_nama_pengirim, bc_kurs, bc_header_id, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    346   ��      "          0    19049    beone_subkon_in_detail 
   TABLE DATA           n   COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM stdin;
    public          postgres    false    290   �      $          0    19054    beone_subkon_in_header 
   TABLE DATA           �   COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM stdin;
    public          postgres    false    292   .�      &          0    19059    beone_subkon_out_detail 
   TABLE DATA           }   COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM stdin;
    public          postgres    false    294   K�      (          0    19064    beone_subkon_out_header 
   TABLE DATA           �   COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    296   h�      <          0    19512    beone_supplier 
   TABLE DATA           �   COPY public.beone_supplier (supplier_id, supplier_code, nama, alamat, ppn, pph21, pph23, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    316   ��      *          0    19071    beone_tipe_coa 
   TABLE DATA           ;   COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
    public          postgres    false    298   ��      ,          0    19076    beone_tipe_coa_transaksi 
   TABLE DATA           Q   COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
    public          postgres    false    300   ��      .          0    19081    beone_transfer_stock 
   TABLE DATA           �   COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    302   ��      /          0    19086    beone_transfer_stock_detail 
   TABLE DATA           �   COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi, price_hpp, satuan_id, rasio) FROM stdin;
    public          postgres    false    303   ��      `          0    20557    beone_usage_detail 
   TABLE DATA           g   COPY public.beone_usage_detail (usage_detail_id, usage_header_id, item_id, qty, gudang_id) FROM stdin;
    public          postgres    false    352   �      ^          0    20547    beone_usage_header 
   TABLE DATA           �   COPY public.beone_usage_header (usage_header_id, trans_no, trans_date, keterangan, deleted_at, created_at, updated_at) FROM stdin;
    public          postgres    false    350   3�      2          0    19093 
   beone_user 
   TABLE DATA           �   COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag, deleted_at, created_at, updated_at, password_old) FROM stdin;
    public          postgres    false    306   P�      4          0    19103    beone_voucher_detail 
   TABLE DATA           �   COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail, currency_id) FROM stdin;
    public          postgres    false    308   m�      6          0    19108    beone_voucher_header 
   TABLE DATA           �   COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date, deleted_at, created_at, updated_at, grandtotal) FROM stdin;
    public          postgres    false    310   ��      8          0    19115    t_po 
   TABLE DATA           >   COPY public.t_po (purchase_header_id, grandtotal) FROM stdin;
    public          postgres    false    312   ��      E          0    19901    temp_satuan 
   TABLE DATA           T   COPY public.temp_satuan (satuan_code, keterangan, flag, item_id, rasio) FROM stdin;
    public          postgres    false    325   ��      �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 1, false);
          public          postgres    false    203            �           0    0    beone_coa_coa_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);
          public          postgres    false    205            �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 1, false);
          public          postgres    false    207            �           0    0    beone_country_country_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_country_country_id_seq', 1, false);
          public          postgres    false    209            �           0    0    beone_currency_currency_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.beone_currency_currency_id_seq', 2, true);
          public          postgres    false    317            �           0    0    beone_customer_customer_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_customer_customer_id_seq', 1, false);
          public          postgres    false    313            �           0    0    beone_custsup_custsup_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 1, false);
          public          postgres    false    211            �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 1, false);
          public          postgres    false    213            �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 1, false);
          public          postgres    false    215            �           0    0 *   beone_general_ledger_general_ledger_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_general_ledger_general_ledger_id_seq', 1, false);
          public          postgres    false    323            �           0    0    beone_gl_gl_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 1, false);
          public          postgres    false    217            �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 1, false);
          public          postgres    false    220            �           0    0    beone_gudang_gudang_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 1, false);
          public          postgres    false    221            �           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq', 1, false);
          public          postgres    false    223            �           0    0 (   beone_hutang_detail_hutang_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_hutang_detail_hutang_detail_id_seq', 1, false);
          public          postgres    false    339            �           0    0 (   beone_hutang_header_hutang_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_hutang_header_hutang_header_id_seq', 1, false);
          public          postgres    false    337            �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 1, false);
          public          postgres    false    225            �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 1, false);
          public          postgres    false    227            �           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 1, false);
          public          postgres    false    229            �           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 1, false);
          public          postgres    false    231            �           0    0    beone_item_item_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_item_item_id_seq', 1, false);
          public          postgres    false    234            �           0    0     beone_item_type_item_type_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 1, false);
          public          postgres    false    236            �           0    0 (   beone_jurnal_detail_jurnal_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_jurnal_detail_jurnal_detail_id_seq', 1, false);
          public          postgres    false    321            �           0    0 (   beone_jurnal_header_jurnal_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_jurnal_header_jurnal_header_id_seq', 1, false);
          public          postgres    false    319            �           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 1, false);
          public          postgres    false    238            �           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_komposisi_komposisi_id_seq', 1, false);
          public          postgres    false    240            �           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE SET     e   SELECT pg_catalog.setval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq', 1, false);
          public          postgres    false    243            �           0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE SET     e   SELECT pg_catalog.setval('public.beone_konversi_stok_header_konversi_stok_header_id_seq', 1, false);
          public          postgres    false    245            �           0    0    beone_log_log_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_log_log_id_seq', 1, false);
          public          postgres    false    247            �           0    0 2   beone_mutasi_stok_detail_mutasi_stok_detail_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.beone_mutasi_stok_detail_mutasi_stok_detail_id_seq', 1, false);
          public          postgres    false    249            �           0    0 2   beone_mutasi_stok_header_mutasi_stok_header_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.beone_mutasi_stok_header_mutasi_stok_header_id_seq', 1, false);
          public          postgres    false    251            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 1, false);
          public          postgres    false    253            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 1, false);
          public          postgres    false    255            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 1, false);
          public          postgres    false    257            �           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq', 1, false);
          public          postgres    false    259            �           0    0 *   beone_piutang_detail_piutang_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_piutang_detail_piutang_detail_id_seq', 1, false);
          public          postgres    false    335            �           0    0 *   beone_piutang_header_piutang_header_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_piutang_header_piutang_header_id_seq', 1, false);
          public          postgres    false    333            �           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_detail_pm_detail_id_seq', 1, false);
          public          postgres    false    261            �           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_header_pm_header_id_seq', 1, false);
          public          postgres    false    263            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 1, false);
          public          postgres    false    265            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 1, false);
          public          postgres    false    267            �           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_detail_produksi_detail_id_seq', 1, false);
          public          postgres    false    269            �           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_header_produksi_header_id_seq', 1, false);
          public          postgres    false    271            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 1, false);
          public          postgres    false    273            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 1, false);
          public          postgres    false    275            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 1, false);
          public          postgres    false    277            �           0    0    beone_role_role_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_role_role_id_seq', 1, false);
          public          postgres    false    279            �           0    0    beone_role_user_role_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 1, false);
          public          postgres    false    281            �           0    0 #   beone_roles_modul_role_modul_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.beone_roles_modul_role_modul_id_seq', 1, false);
          public          postgres    false    331            �           0    0    beone_roles_user_role_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_roles_user_role_id_seq', 1, false);
          public          postgres    false    283            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 1, false);
          public          postgres    false    285            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 1, false);
          public          postgres    false    287            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 1, false);
          public          postgres    false    289            �           0    0 2   beone_stok_opname_detail_stok_opname_detail_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.beone_stok_opname_detail_stok_opname_detail_id_seq', 1, false);
          public          postgres    false    328            �           0    0 2   beone_stok_opname_header_stok_opname_header_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.beone_stok_opname_header_stok_opname_header_id_seq', 1, false);
          public          postgres    false    326            �           0    0 >   beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq    SEQUENCE SET     m   SELECT pg_catalog.setval('public.beone_sub_kontraktor_in_detail_sub_kontraktor_in_detail_id_seq', 1, false);
          public          postgres    false    343            �           0    0 >   beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq    SEQUENCE SET     m   SELECT pg_catalog.setval('public.beone_sub_kontraktor_in_header_sub_kontraktor_in_header_id_seq', 1, false);
          public          postgres    false    341            �           0    0 ?   beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq    SEQUENCE SET     n   SELECT pg_catalog.setval('public.beone_sub_kontraktor_out_detai_sub_kontraktor_out_detail_id_seq', 1, false);
          public          postgres    false    347            �           0    0 ?   beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq    SEQUENCE SET     n   SELECT pg_catalog.setval('public.beone_sub_kontraktor_out_heade_sub_kontraktor_out_header_id_seq', 1, false);
          public          postgres    false    345            �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_subkon_in_detail_subkon_in_detail_id_seq', 1, false);
          public          postgres    false    291            �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.beone_subkon_in_header_subkon_in_id_seq', 1, false);
          public          postgres    false    293            �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.beone_subkon_out_detail_subkon_out_detail_id_seq', 1, false);
          public          postgres    false    295            �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_subkon_out_header_subkon_out_id_seq', 1, false);
          public          postgres    false    297            �           0    0    beone_supplier_supplier_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_supplier_supplier_id_seq', 1, false);
          public          postgres    false    315            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);
          public          postgres    false    299            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 1, false);
          public          postgres    false    301            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE SET     g   SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 1, false);
          public          postgres    false    304            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 1, false);
          public          postgres    false    305            �           0    0 &   beone_usage_detail_usage_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_usage_detail_usage_detail_id_seq', 1, false);
          public          postgres    false    351            �           0    0 &   beone_usage_header_usage_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_usage_header_usage_header_id_seq', 1, false);
          public          postgres    false    349            �           0    0    beone_user_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_user_user_id_seq', 1, false);
          public          postgres    false    307            �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 1, false);
          public          postgres    false    309            �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 1, false);
          public          postgres    false    311            X
           2606    19173 &   beone_adjustment beone_adjustment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);
 P   ALTER TABLE ONLY public.beone_adjustment DROP CONSTRAINT beone_adjustment_pkey;
       public            postgres    false    202            \
           2606    19175 &   beone_coa_jurnal beone_coa_jurnal_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);
 P   ALTER TABLE ONLY public.beone_coa_jurnal DROP CONSTRAINT beone_coa_jurnal_pkey;
       public            postgres    false    206            Z
           2606    19177    beone_coa beone_coa_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);
 B   ALTER TABLE ONLY public.beone_coa DROP CONSTRAINT beone_coa_pkey;
       public            postgres    false    204            ^
           2606    19179     beone_country beone_country_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);
 J   ALTER TABLE ONLY public.beone_country DROP CONSTRAINT beone_country_pkey;
       public            postgres    false    208            �
           2606    19837 "   beone_currency beone_currency_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_currency
    ADD CONSTRAINT beone_currency_pkey PRIMARY KEY (currency_id);
 L   ALTER TABLE ONLY public.beone_currency DROP CONSTRAINT beone_currency_pkey;
       public            postgres    false    318            �
           2606    19509 /   beone_customer beone_customer_customer_code_key 
   CONSTRAINT     s   ALTER TABLE ONLY public.beone_customer
    ADD CONSTRAINT beone_customer_customer_code_key UNIQUE (customer_code);
 Y   ALTER TABLE ONLY public.beone_customer DROP CONSTRAINT beone_customer_customer_code_key;
       public            postgres    false    314            �
           2606    19507 "   beone_customer beone_customer_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_customer
    ADD CONSTRAINT beone_customer_pkey PRIMARY KEY (customer_id);
 L   ALTER TABLE ONLY public.beone_customer DROP CONSTRAINT beone_customer_pkey;
       public            postgres    false    314            `
           2606    19181 ,   beone_export_detail beone_export_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);
 V   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_pkey;
       public            postgres    false    212            b
           2606    19530 *   beone_export_header beone_export_header_pk 
   CONSTRAINT     v   ALTER TABLE ONLY public.beone_export_header
    ADD CONSTRAINT beone_export_header_pk PRIMARY KEY (export_header_id);
 T   ALTER TABLE ONLY public.beone_export_header DROP CONSTRAINT beone_export_header_pk;
       public            postgres    false    214            �
           2606    19896 .   beone_general_ledger beone_general_ledger_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_general_ledger
    ADD CONSTRAINT beone_general_ledger_pkey PRIMARY KEY (general_ledger_id);
 X   ALTER TABLE ONLY public.beone_general_ledger DROP CONSTRAINT beone_general_ledger_pkey;
       public            postgres    false    324            d
           2606    19183    beone_gl beone_gl_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);
 @   ALTER TABLE ONLY public.beone_gl DROP CONSTRAINT beone_gl_pkey;
       public            postgres    false    216            i
           2606    19185 ,   beone_gudang_detail beone_gudang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);
 V   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_pkey;
       public            postgres    false    219            g
           2606    19187    beone_gudang beone_gudang_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);
 H   ALTER TABLE ONLY public.beone_gudang DROP CONSTRAINT beone_gudang_pkey;
       public            postgres    false    218            k
           2606    19189 >   beone_pemakaian_bahan_header beone_header_pemakaian_bahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header
    ADD CONSTRAINT beone_header_pemakaian_bahan_pkey PRIMARY KEY (pemakaian_header_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_header DROP CONSTRAINT beone_header_pemakaian_bahan_pkey;
       public            postgres    false    222            �
           2606    20415 ,   beone_hutang_detail beone_hutang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_hutang_detail
    ADD CONSTRAINT beone_hutang_detail_pkey PRIMARY KEY (hutang_detail_id);
 V   ALTER TABLE ONLY public.beone_hutang_detail DROP CONSTRAINT beone_hutang_detail_pkey;
       public            postgres    false    340            �
           2606    20406 ,   beone_hutang_header beone_hutang_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_hutang_header
    ADD CONSTRAINT beone_hutang_header_pkey PRIMARY KEY (hutang_header_id);
 V   ALTER TABLE ONLY public.beone_hutang_header DROP CONSTRAINT beone_hutang_header_pkey;
       public            postgres    false    338            m
           2606    19191 .   beone_hutang_piutang beone_hutang_piutang_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);
 X   ALTER TABLE ONLY public.beone_hutang_piutang DROP CONSTRAINT beone_hutang_piutang_pkey;
       public            postgres    false    224            o
           2606    19193 ,   beone_import_detail beone_import_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);
 V   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_pkey;
       public            postgres    false    226            q
           2606    19195 ,   beone_import_header beone_import_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);
 V   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_pkey;
       public            postgres    false    228            s
           2606    19197 $   beone_inventory beone_inventory_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);
 N   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_pkey;
       public            postgres    false    230            x
           2606    19199 ,   beone_item_category beone_item_category_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_item_category
    ADD CONSTRAINT beone_item_category_pkey PRIMARY KEY (item_category_id);
 V   ALTER TABLE ONLY public.beone_item_category DROP CONSTRAINT beone_item_category_pkey;
       public            postgres    false    233            v
           2606    19201    beone_item beone_item_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);
 D   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_pkey;
       public            postgres    false    232            z
           2606    19203 $   beone_item_type beone_item_type_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);
 N   ALTER TABLE ONLY public.beone_item_type DROP CONSTRAINT beone_item_type_pkey;
       public            postgres    false    235            �
           2606    19869 ,   beone_jurnal_detail beone_jurnal_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_jurnal_detail
    ADD CONSTRAINT beone_jurnal_detail_pkey PRIMARY KEY (jurnal_detail_id);
 V   ALTER TABLE ONLY public.beone_jurnal_detail DROP CONSTRAINT beone_jurnal_detail_pkey;
       public            postgres    false    322            �
           2606    19856 ,   beone_jurnal_header beone_jurnal_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_jurnal_header
    ADD CONSTRAINT beone_jurnal_header_pkey PRIMARY KEY (jurnal_header_id);
 V   ALTER TABLE ONLY public.beone_jurnal_header DROP CONSTRAINT beone_jurnal_header_pkey;
       public            postgres    false    320            |
           2606    19205 &   beone_kode_trans beone_kode_trans_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);
 P   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_pkey;
       public            postgres    false    237            ~
           2606    19207 $   beone_komposisi beone_komposisi_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_pkey PRIMARY KEY (komposisi_id);
 N   ALTER TABLE ONLY public.beone_komposisi DROP CONSTRAINT beone_komposisi_pkey;
       public            postgres    false    239            �
           2606    19209 :   beone_konversi_stok_detail beone_konversi_stok_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_pkey PRIMARY KEY (konversi_stok_detail_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_pkey;
       public            postgres    false    242            �
           2606    19211 :   beone_konversi_stok_header beone_konversi_stok_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_pkey PRIMARY KEY (konversi_stok_header_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_header DROP CONSTRAINT beone_konversi_stok_header_pkey;
       public            postgres    false    244            �
           2606    19213    beone_log beone_log_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);
 B   ALTER TABLE ONLY public.beone_log DROP CONSTRAINT beone_log_pkey;
       public            postgres    false    246            �
           2606    19945 !   beone_modul beone_modul_modul_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_modul
    ADD CONSTRAINT beone_modul_modul_key UNIQUE (modul);
 K   ALTER TABLE ONLY public.beone_modul DROP CONSTRAINT beone_modul_modul_key;
       public            postgres    false    330            �
           2606    19943    beone_modul beone_modul_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.beone_modul
    ADD CONSTRAINT beone_modul_pkey PRIMARY KEY (modul_id);
 F   ALTER TABLE ONLY public.beone_modul DROP CONSTRAINT beone_modul_pkey;
       public            postgres    false    330            �
           2606    19215 6   beone_mutasi_stok_detail beone_mutasi_stok_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail
    ADD CONSTRAINT beone_mutasi_stok_detail_pkey PRIMARY KEY (mutasi_stok_detail_id);
 `   ALTER TABLE ONLY public.beone_mutasi_stok_detail DROP CONSTRAINT beone_mutasi_stok_detail_pkey;
       public            postgres    false    248            �
           2606    19217 6   beone_mutasi_stok_header beone_mutasi_stok_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_header
    ADD CONSTRAINT beone_mutasi_stok_header_pkey PRIMARY KEY (mutasi_stok_header_id);
 `   ALTER TABLE ONLY public.beone_mutasi_stok_header DROP CONSTRAINT beone_mutasi_stok_header_pkey;
       public            postgres    false    250            �
           2606    19219 ,   beone_opname_detail beone_opname_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);
 V   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_pkey;
       public            postgres    false    252            �
           2606    19221 ,   beone_opname_header beone_opname_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);
 V   ALTER TABLE ONLY public.beone_opname_header DROP CONSTRAINT beone_opname_header_pkey;
       public            postgres    false    254            �
           2606    19223 $   beone_peleburan beone_peleburan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);
 N   ALTER TABLE ONLY public.beone_peleburan DROP CONSTRAINT beone_peleburan_pkey;
       public            postgres    false    256            �
           2606    19225 >   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_pkey PRIMARY KEY (pemakaian_detail_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_pkey;
       public            postgres    false    258            �
           2606    20395 .   beone_piutang_detail beone_piutang_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_piutang_detail
    ADD CONSTRAINT beone_piutang_detail_pkey PRIMARY KEY (piutang_detail_id);
 X   ALTER TABLE ONLY public.beone_piutang_detail DROP CONSTRAINT beone_piutang_detail_pkey;
       public            postgres    false    336            �
           2606    20386 .   beone_piutang_header beone_piutang_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_piutang_header
    ADD CONSTRAINT beone_piutang_header_pkey PRIMARY KEY (piutang_header_id);
 X   ALTER TABLE ONLY public.beone_piutang_header DROP CONSTRAINT beone_piutang_header_pkey;
       public            postgres    false    334            �
           2606    19227 $   beone_pm_detail beone_pm_detail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_detail
    ADD CONSTRAINT beone_pm_detail_pkey PRIMARY KEY (pm_detail_id);
 N   ALTER TABLE ONLY public.beone_pm_detail DROP CONSTRAINT beone_pm_detail_pkey;
       public            postgres    false    260            �
           2606    19229 $   beone_pm_header beone_pm_header_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_header
    ADD CONSTRAINT beone_pm_header_pkey PRIMARY KEY (pm_header_id);
 N   ALTER TABLE ONLY public.beone_pm_header DROP CONSTRAINT beone_pm_header_pkey;
       public            postgres    false    262            �
           2606    19231 2   beone_po_import_detail beone_po_import_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);
 \   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_pkey;
       public            postgres    false    264            �
           2606    19233 2   beone_po_import_header beone_po_import_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);
 \   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_pkey;
       public            postgres    false    266            �
           2606    19235 0   beone_produksi_detail beone_produksi_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_pkey PRIMARY KEY (produksi_detail_id);
 Z   ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_pkey;
       public            postgres    false    268            �
           2606    19237 0   beone_produksi_header beone_produksi_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_header
    ADD CONSTRAINT beone_produksi_header_pkey PRIMARY KEY (produksi_header_id);
 Z   ALTER TABLE ONLY public.beone_produksi_header DROP CONSTRAINT beone_produksi_header_pkey;
       public            postgres    false    270            �
           2606    19239 0   beone_purchase_detail beone_purchase_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);
 Z   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_pkey;
       public            postgres    false    272            �
           2606    19241 0   beone_purchase_header beone_purchase_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);
 Z   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_pkey;
       public            postgres    false    274            �
           2606    19243 0   beone_received_import beone_received_import_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);
 Z   ALTER TABLE ONLY public.beone_received_import DROP CONSTRAINT beone_received_import_pkey;
       public            postgres    false    276            �
           2606    19245    beone_role beone_role_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.beone_role DROP CONSTRAINT beone_role_pkey;
       public            postgres    false    278            �
           2606    19247 $   beone_role_user beone_role_user_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);
 N   ALTER TABLE ONLY public.beone_role_user DROP CONSTRAINT beone_role_user_pkey;
       public            postgres    false    280            �
           2606    19953 (   beone_roles_modul beone_roles_modul_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY public.beone_roles_modul
    ADD CONSTRAINT beone_roles_modul_pkey PRIMARY KEY (role_modul_id);
 R   ALTER TABLE ONLY public.beone_roles_modul DROP CONSTRAINT beone_roles_modul_pkey;
       public            postgres    false    332            �
           2606    19249 &   beone_roles_user beone_roles_user_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_roles_user
    ADD CONSTRAINT beone_roles_user_pkey PRIMARY KEY (role_id);
 P   ALTER TABLE ONLY public.beone_roles_user DROP CONSTRAINT beone_roles_user_pkey;
       public            postgres    false    282            �
           2606    19251 *   beone_sales_detail beone_sales_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);
 T   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_pkey;
       public            postgres    false    284            �
           2606    19253 *   beone_sales_header beone_sales_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);
 T   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_pkey;
       public            postgres    false    286            �
           2606    19255 (   beone_satuan_item beone_satuan_item_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);
 R   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_pkey;
       public            postgres    false    288            �
           2606    19926 6   beone_stok_opname_detail beone_stok_opname_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_stok_opname_detail
    ADD CONSTRAINT beone_stok_opname_detail_pkey PRIMARY KEY (stok_opname_detail_id);
 `   ALTER TABLE ONLY public.beone_stok_opname_detail DROP CONSTRAINT beone_stok_opname_detail_pkey;
       public            postgres    false    329            �
           2606    19918 6   beone_stok_opname_header beone_stok_opname_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_stok_opname_header
    ADD CONSTRAINT beone_stok_opname_header_pkey PRIMARY KEY (stok_opname_header_id);
 `   ALTER TABLE ONLY public.beone_stok_opname_header DROP CONSTRAINT beone_stok_opname_header_pkey;
       public            postgres    false    327            �
           2606    20465 B   beone_sub_kontraktor_in_detail beone_sub_kontraktor_in_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail
    ADD CONSTRAINT beone_sub_kontraktor_in_detail_pkey PRIMARY KEY (sub_kontraktor_in_detail_id);
 l   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail DROP CONSTRAINT beone_sub_kontraktor_in_detail_pkey;
       public            postgres    false    344            �
           2606    20443 B   beone_sub_kontraktor_in_header beone_sub_kontraktor_in_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_header
    ADD CONSTRAINT beone_sub_kontraktor_in_header_pkey PRIMARY KEY (sub_kontraktor_in_header_id);
 l   ALTER TABLE ONLY public.beone_sub_kontraktor_in_header DROP CONSTRAINT beone_sub_kontraktor_in_header_pkey;
       public            postgres    false    342            �
           2606    20518 D   beone_sub_kontraktor_out_detail beone_sub_kontraktor_out_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail
    ADD CONSTRAINT beone_sub_kontraktor_out_detail_pkey PRIMARY KEY (sub_kontraktor_out_detail_id);
 n   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail DROP CONSTRAINT beone_sub_kontraktor_out_detail_pkey;
       public            postgres    false    348            �
           2606    20495 D   beone_sub_kontraktor_out_header beone_sub_kontraktor_out_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_header
    ADD CONSTRAINT beone_sub_kontraktor_out_header_pkey PRIMARY KEY (sub_kontraktor_out_header_id);
 n   ALTER TABLE ONLY public.beone_sub_kontraktor_out_header DROP CONSTRAINT beone_sub_kontraktor_out_header_pkey;
       public            postgres    false    346            �
           2606    19257 2   beone_subkon_in_detail beone_subkon_in_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_pkey PRIMARY KEY (subkon_in_detail_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_pkey;
       public            postgres    false    290            �
           2606    19259 2   beone_subkon_in_header beone_subkon_in_header_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_pkey PRIMARY KEY (subkon_in_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_header DROP CONSTRAINT beone_subkon_in_header_pkey;
       public            postgres    false    292            �
           2606    19261 4   beone_subkon_out_detail beone_subkon_out_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_pkey PRIMARY KEY (subkon_out_detail_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_pkey;
       public            postgres    false    294            �
           2606    19263 4   beone_subkon_out_header beone_subkon_out_header_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_pkey PRIMARY KEY (subkon_out_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_header DROP CONSTRAINT beone_subkon_out_header_pkey;
       public            postgres    false    296            �
           2606    19519 "   beone_supplier beone_supplier_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_supplier
    ADD CONSTRAINT beone_supplier_pkey PRIMARY KEY (supplier_id);
 L   ALTER TABLE ONLY public.beone_supplier DROP CONSTRAINT beone_supplier_pkey;
       public            postgres    false    316            �
           2606    19521 /   beone_supplier beone_supplier_supplier_code_key 
   CONSTRAINT     s   ALTER TABLE ONLY public.beone_supplier
    ADD CONSTRAINT beone_supplier_supplier_code_key UNIQUE (supplier_code);
 Y   ALTER TABLE ONLY public.beone_supplier DROP CONSTRAINT beone_supplier_supplier_code_key;
       public            postgres    false    316            �
           2606    19265 "   beone_tipe_coa beone_tipe_coa_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);
 L   ALTER TABLE ONLY public.beone_tipe_coa DROP CONSTRAINT beone_tipe_coa_pkey;
       public            postgres    false    298            �
           2606    19267 <   beone_transfer_stock_detail beone_transfer_stock_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);
 f   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_pkey;
       public            postgres    false    303            �
           2606    19269 .   beone_transfer_stock beone_transfer_stock_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);
 X   ALTER TABLE ONLY public.beone_transfer_stock DROP CONSTRAINT beone_transfer_stock_pkey;
       public            postgres    false    302            �
           2606    20562 *   beone_usage_detail beone_usage_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_usage_detail
    ADD CONSTRAINT beone_usage_detail_pkey PRIMARY KEY (usage_detail_id);
 T   ALTER TABLE ONLY public.beone_usage_detail DROP CONSTRAINT beone_usage_detail_pkey;
       public            postgres    false    352            �
           2606    20554 *   beone_usage_header beone_usage_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_usage_header
    ADD CONSTRAINT beone_usage_header_pkey PRIMARY KEY (usage_header_id);
 T   ALTER TABLE ONLY public.beone_usage_header DROP CONSTRAINT beone_usage_header_pkey;
       public            postgres    false    350            �
           2606    19271    beone_user beone_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.beone_user DROP CONSTRAINT beone_user_pkey;
       public            postgres    false    306            �
           2606    19273 .   beone_voucher_detail beone_voucher_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);
 X   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_pkey;
       public            postgres    false    308            �
           2606    19275 .   beone_voucher_header beone_voucher_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);
 X   ALTER TABLE ONLY public.beone_voucher_header DROP CONSTRAINT beone_voucher_header_pkey;
       public            postgres    false    310            e
           1259    19885    beone_gudang_gudang_code_uindex    INDEX     f   CREATE UNIQUE INDEX beone_gudang_gudang_code_uindex ON public.beone_gudang USING btree (gudang_code);
 3   DROP INDEX public.beone_gudang_gudang_code_uindex;
       public            postgres    false    218            t
           1259    19843    beone_item_item_code_uindex    INDEX     ^   CREATE UNIQUE INDEX beone_item_item_code_uindex ON public.beone_item USING btree (item_code);
 /   DROP INDEX public.beone_item_item_code_uindex;
       public            postgres    false    232            �
           1259    20418 )   beone_roles_modul_role_id_modul_id_uindex    INDEX     {   CREATE UNIQUE INDEX beone_roles_modul_role_id_modul_id_uindex ON public.beone_roles_modul USING btree (role_id, modul_id);
 =   DROP INDEX public.beone_roles_modul_role_id_modul_id_uindex;
       public            postgres    false    332    332            �
           1259    19844 &   beone_satuan_item_item_id_rasio_uindex    INDEX     u   CREATE UNIQUE INDEX beone_satuan_item_item_id_rasio_uindex ON public.beone_satuan_item USING btree (item_id, rasio);
 :   DROP INDEX public.beone_satuan_item_item_id_rasio_uindex;
       public            postgres    false    288    288            8           2620    19486 2   beone_export_detail trg_create_beone_export_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_export_detail BEFORE INSERT OR UPDATE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_detail();
 K   DROP TRIGGER trg_create_beone_export_detail ON public.beone_export_detail;
       public          postgres    false    212    368            :           2620    19488 2   beone_export_header trg_create_beone_export_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_export_header BEFORE INSERT OR UPDATE ON public.beone_export_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_header();
 K   DROP TRIGGER trg_create_beone_export_header ON public.beone_export_header;
       public          postgres    false    370    214            ;           2620    19484 2   beone_import_detail trg_create_beone_import_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_import_detail BEFORE INSERT OR UPDATE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_detail();
 K   DROP TRIGGER trg_create_beone_import_detail ON public.beone_import_detail;
       public          postgres    false    371    226            =           2620    19489 2   beone_import_header trg_create_beone_import_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_import_header BEFORE INSERT OR UPDATE ON public.beone_import_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_header();
 K   DROP TRIGGER trg_create_beone_import_header ON public.beone_import_header;
       public          postgres    false    372    228            >           2620    19490 @   beone_konversi_stok_detail trg_create_beone_konversi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_konversi_stok_detail BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_detail();
 Y   DROP TRIGGER trg_create_beone_konversi_stok_detail ON public.beone_konversi_stok_detail;
       public          postgres    false    242    378            @           2620    19492 @   beone_konversi_stok_header trg_create_beone_konversi_stok_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_konversi_stok_header BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_header();
 Y   DROP TRIGGER trg_create_beone_konversi_stok_header ON public.beone_konversi_stok_header;
       public          postgres    false    244    367            B           2620    19840 <   beone_mutasi_stok_detail trg_create_beone_mutasi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_mutasi_stok_detail BEFORE INSERT OR UPDATE ON public.beone_mutasi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_mutasi_stok_detail();
 U   DROP TRIGGER trg_create_beone_mutasi_stok_detail ON public.beone_mutasi_stok_detail;
       public          postgres    false    380    248            G           2620    20539 H   beone_sub_kontraktor_in_detail trg_create_beone_sub_kontraktor_in_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_sub_kontraktor_in_detail BEFORE INSERT OR UPDATE ON public.beone_sub_kontraktor_in_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_sub_kontraktor_in_detail();
 a   DROP TRIGGER trg_create_beone_sub_kontraktor_in_detail ON public.beone_sub_kontraktor_in_detail;
       public          postgres    false    344    386            F           2620    20543 H   beone_sub_kontraktor_in_header trg_create_beone_sub_kontraktor_in_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_sub_kontraktor_in_header BEFORE INSERT OR UPDATE ON public.beone_sub_kontraktor_in_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_sub_kontraktor_in_header();
 a   DROP TRIGGER trg_create_beone_sub_kontraktor_in_header ON public.beone_sub_kontraktor_in_header;
       public          postgres    false    342    388            J           2620    20533 J   beone_sub_kontraktor_out_detail trg_create_beone_sub_kontraktor_out_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_sub_kontraktor_out_detail BEFORE INSERT OR UPDATE ON public.beone_sub_kontraktor_out_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_sub_kontraktor_out_detail();
 c   DROP TRIGGER trg_create_beone_sub_kontraktor_out_detail ON public.beone_sub_kontraktor_out_detail;
       public          postgres    false    348    383            I           2620    20537 J   beone_sub_kontraktor_out_header trg_create_beone_sub_kontraktor_out_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_sub_kontraktor_out_header BEFORE INSERT OR UPDATE ON public.beone_sub_kontraktor_out_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_sub_kontraktor_out_header();
 c   DROP TRIGGER trg_create_beone_sub_kontraktor_out_header ON public.beone_sub_kontraktor_out_header;
       public          postgres    false    346    385            D           2620    19496 B   beone_transfer_stock_detail trg_create_beone_transfer_stock_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_transfer_stock_detail BEFORE INSERT OR UPDATE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_transfer_stock_detail();
 [   DROP TRIGGER trg_create_beone_transfer_stock_detail ON public.beone_transfer_stock_detail;
       public          postgres    false    303    379            9           2620    19487 2   beone_export_detail trg_delete_beone_export_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_export_detail BEFORE DELETE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_export_detail();
 K   DROP TRIGGER trg_delete_beone_export_detail ON public.beone_export_detail;
       public          postgres    false    212    374            <           2620    19485 2   beone_import_detail trg_delete_beone_import_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_import_detail BEFORE DELETE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_import_detail();
 K   DROP TRIGGER trg_delete_beone_import_detail ON public.beone_import_detail;
       public          postgres    false    354    226            ?           2620    19491 @   beone_konversi_stok_detail trg_delete_beone_konversi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_konversi_stok_detail BEFORE DELETE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_detail();
 Y   DROP TRIGGER trg_delete_beone_konversi_stok_detail ON public.beone_konversi_stok_detail;
       public          postgres    false    375    242            A           2620    19493 @   beone_konversi_stok_header trg_delete_beone_konversi_stok_header    TRIGGER     �   CREATE TRIGGER trg_delete_beone_konversi_stok_header BEFORE DELETE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_header();
 Y   DROP TRIGGER trg_delete_beone_konversi_stok_header ON public.beone_konversi_stok_header;
       public          postgres    false    244    389            C           2620    19841 <   beone_mutasi_stok_detail trg_delete_beone_mutasi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_mutasi_stok_detail BEFORE DELETE ON public.beone_mutasi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_mutasi_stok_detail();
 U   DROP TRIGGER trg_delete_beone_mutasi_stok_detail ON public.beone_mutasi_stok_detail;
       public          postgres    false    381    248            H           2620    20541 H   beone_sub_kontraktor_in_detail trg_delete_beone_sub_kontraktor_in_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_sub_kontraktor_in_detail BEFORE DELETE ON public.beone_sub_kontraktor_in_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_sub_kontraktor_in_detail();
 a   DROP TRIGGER trg_delete_beone_sub_kontraktor_in_detail ON public.beone_sub_kontraktor_in_detail;
       public          postgres    false    387    344            K           2620    20535 J   beone_sub_kontraktor_out_detail trg_delete_beone_sub_kontraktor_out_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_sub_kontraktor_out_detail BEFORE DELETE ON public.beone_sub_kontraktor_out_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_sub_kontraktor_out_detail();
 c   DROP TRIGGER trg_delete_beone_sub_kontraktor_out_detail ON public.beone_sub_kontraktor_out_detail;
       public          postgres    false    348    384            E           2620    19497 B   beone_transfer_stock_detail trg_delete_beone_transfer_stock_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_transfer_stock_detail BEFORE DELETE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_transfer_stock_detail();
 [   DROP TRIGGER trg_delete_beone_transfer_stock_detail ON public.beone_transfer_stock_detail;
       public          postgres    false    369    303            �
           2606    19531 O   beone_export_detail beone_export_detail_beone_export_header_export_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_beone_export_header_export_header_id_fk FOREIGN KEY (export_header_id) REFERENCES public.beone_export_header(export_header_id);
 y   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_beone_export_header_export_header_id_fk;
       public          postgres    false    3426    212    214            �
           2606    19591 A   beone_export_detail beone_export_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 k   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    3431    218    212            �
           2606    19596 =   beone_export_detail beone_export_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 g   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_beone_item_item_id_fk;
       public          postgres    false    212    232    3446            �
           2606    20336 M   beone_export_detail beone_export_detail_beone_sales_detail_sales_detail_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_beone_sales_detail_sales_detail_id_fk FOREIGN KEY (sales_detail_id) REFERENCES public.beone_sales_detail(sales_detail_id);
 w   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_beone_sales_detail_sales_detail_id_fk;
       public          postgres    false    3498    284    212            �
           2606    19601 M   beone_export_detail beone_export_detail_beone_sales_header_sales_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_beone_sales_header_sales_header_id_fk FOREIGN KEY (sales_header_id) REFERENCES public.beone_sales_header(sales_header_id);
 w   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_beone_sales_header_sales_header_id_fk;
       public          postgres    false    3500    286    212            �
           2606    19731 =   beone_gudang_detail beone_gudang_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 g   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_beone_item_item_id_fk;
       public          postgres    false    232    219    3446            �
           2606    19691 A   beone_import_detail beone_import_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 k   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    226    3431    218            �
           2606    19536 O   beone_import_detail beone_import_detail_beone_import_header_import_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_beone_import_header_import_header_id_fk FOREIGN KEY (import_header_id) REFERENCES public.beone_import_header(import_header_id);
 y   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_beone_import_header_import_header_id_fk;
       public          postgres    false    3441    226    228            �
           2606    19696 =   beone_import_detail beone_import_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 g   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_beone_item_item_id_fk;
       public          postgres    false    232    3446    226            �
           2606    19721 G   beone_import_detail beone_import_detail_beone_item_type_item_type_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_beone_item_type_item_type_id_fk FOREIGN KEY (item_type_id) REFERENCES public.beone_item_type(item_type_id);
 q   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_beone_item_type_item_type_id_fk;
       public          postgres    false    226    3450    235            �
           2606    19701 S   beone_import_detail beone_import_detail_beone_purchase_detail_purchase_detail_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_beone_purchase_detail_purchase_detail_id_fk FOREIGN KEY (purchase_detail_id) REFERENCES public.beone_purchase_detail(purchase_detail_id);
 }   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_beone_purchase_detail_purchase_detail_id_fk;
       public          postgres    false    272    3486    226            �
           2606    19811 E   beone_import_header beone_import_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 o   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_beone_supplier_supplier_id_fk;
       public          postgres    false    316    3529    228            �
           2606    19746 9   beone_inventory beone_inventory_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 c   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_beone_gudang_gudang_id_fk;
       public          postgres    false    230    218    3431                        2606    19751 5   beone_inventory beone_inventory_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 _   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_beone_item_item_id_fk;
       public          postgres    false    230    232    3446                       2606    19736 =   beone_item beone_item_beone_item_category_item_category_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_beone_item_category_item_category_id_fk FOREIGN KEY (item_category_id) REFERENCES public.beone_item_category(item_category_id);
 g   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_beone_item_category_item_category_id_fk;
       public          postgres    false    232    233    3448                       2606    19741 5   beone_item beone_item_beone_item_type_item_type_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_beone_item_type_item_type_id_fk FOREIGN KEY (item_type_id) REFERENCES public.beone_item_type(item_type_id);
 _   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_beone_item_type_item_type_id_fk;
       public          postgres    false    3450    232    235            .           2606    20351 ;   beone_jurnal_detail beone_jurnal_detail_beone_coa_coa_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_jurnal_detail
    ADD CONSTRAINT beone_jurnal_detail_beone_coa_coa_id_fk FOREIGN KEY (coa_id) REFERENCES public.beone_coa(coa_id);
 e   ALTER TABLE ONLY public.beone_jurnal_detail DROP CONSTRAINT beone_jurnal_detail_beone_coa_coa_id_fk;
       public          postgres    false    3418    322    204            /           2606    20356 E   beone_jurnal_detail beone_jurnal_detail_beone_currency_currency_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_jurnal_detail
    ADD CONSTRAINT beone_jurnal_detail_beone_currency_currency_id_fk FOREIGN KEY (currency_id) REFERENCES public.beone_currency(currency_id);
 o   ALTER TABLE ONLY public.beone_jurnal_detail DROP CONSTRAINT beone_jurnal_detail_beone_currency_currency_id_fk;
       public          postgres    false    318    322    3533            0           2606    20361 O   beone_jurnal_detail beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_jurnal_detail
    ADD CONSTRAINT beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk FOREIGN KEY (jurnal_header_id) REFERENCES public.beone_jurnal_header(jurnal_header_id);
 y   ALTER TABLE ONLY public.beone_jurnal_detail DROP CONSTRAINT beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk;
       public          postgres    false    320    3535    322                       2606    19781 5   beone_kode_trans beone_kode_trans_beone_coa_coa_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_beone_coa_coa_id_fk FOREIGN KEY (coa_id) REFERENCES public.beone_coa(coa_id);
 _   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_beone_coa_coa_id_fk;
       public          postgres    false    3418    204    237                       2606    19771 5   beone_komposisi beone_komposisi_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_beone_item_item_id_fk FOREIGN KEY (item_jadi_id) REFERENCES public.beone_item(item_id);
 _   ALTER TABLE ONLY public.beone_komposisi DROP CONSTRAINT beone_komposisi_beone_item_item_id_fk;
       public          postgres    false    239    3446    232                       2606    19776 7   beone_komposisi beone_komposisi_beone_item_item_id_fk_2 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_beone_item_item_id_fk_2 FOREIGN KEY (item_baku_id) REFERENCES public.beone_item(item_id);
 a   ALTER TABLE ONLY public.beone_komposisi DROP CONSTRAINT beone_komposisi_beone_item_item_id_fk_2;
       public          postgres    false    232    239    3446                       2606    19666 O   beone_konversi_stok_detail beone_konversi_stok_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 y   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    3431    218    242            	           2606    19671 K   beone_konversi_stok_detail beone_konversi_stok_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 u   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_beone_item_item_id_fk;
       public          postgres    false    242    232    3446                       2606    19581 Z   beone_konversi_stok_detail beone_konversi_stok_detail_beone_konversi_stok_header_konversi_ 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_beone_konversi_stok_header_konversi_ FOREIGN KEY (konversi_stok_header_id) REFERENCES public.beone_konversi_stok_header(konversi_stok_header_id);
 �   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_beone_konversi_stok_header_konversi_;
       public          postgres    false    242    244    3458                       2606    19586 T   beone_konversi_stok_detail beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk FOREIGN KEY (satuan_qty) REFERENCES public.beone_satuan_item(satuan_id);
 ~   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk;
       public          postgres    false    242    3503    288            
           2606    19806 O   beone_konversi_stok_header beone_konversi_stok_header_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 y   ALTER TABLE ONLY public.beone_konversi_stok_header DROP CONSTRAINT beone_konversi_stok_header_beone_gudang_gudang_id_fk;
       public          postgres    false    244    3431    218                       2606    19816 K   beone_konversi_stok_header beone_konversi_stok_header_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 u   ALTER TABLE ONLY public.beone_konversi_stok_header DROP CONSTRAINT beone_konversi_stok_header_beone_item_item_id_fk;
       public          postgres    false    3446    244    232            
           2606    19706 K   beone_mutasi_stok_detail beone_mutasi_stok_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail
    ADD CONSTRAINT beone_mutasi_stok_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id_tujuan) REFERENCES public.beone_gudang(gudang_id);
 u   ALTER TABLE ONLY public.beone_mutasi_stok_detail DROP CONSTRAINT beone_mutasi_stok_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    3431    248    218                       2606    19711 M   beone_mutasi_stok_detail beone_mutasi_stok_detail_beone_gudang_gudang_id_fk_2 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail
    ADD CONSTRAINT beone_mutasi_stok_detail_beone_gudang_gudang_id_fk_2 FOREIGN KEY (gudang_id_asal) REFERENCES public.beone_gudang(gudang_id);
 w   ALTER TABLE ONLY public.beone_mutasi_stok_detail DROP CONSTRAINT beone_mutasi_stok_detail_beone_gudang_gudang_id_fk_2;
       public          postgres    false    218    248    3431                       2606    19716 G   beone_mutasi_stok_detail beone_mutasi_stok_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail
    ADD CONSTRAINT beone_mutasi_stok_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 q   ALTER TABLE ONLY public.beone_mutasi_stok_detail DROP CONSTRAINT beone_mutasi_stok_detail_beone_item_item_id_fk;
       public          postgres    false    3446    232    248                       2606    19576 X   beone_mutasi_stok_detail beone_mutasi_stok_detail_beone_mutasi_stok_header_mutasi_stok_h 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_mutasi_stok_detail
    ADD CONSTRAINT beone_mutasi_stok_detail_beone_mutasi_stok_header_mutasi_stok_h FOREIGN KEY (mutasi_stok_header_id) REFERENCES public.beone_mutasi_stok_header(mutasi_stok_header_id);
 �   ALTER TABLE ONLY public.beone_mutasi_stok_detail DROP CONSTRAINT beone_mutasi_stok_detail_beone_mutasi_stok_header_mutasi_stok_h;
       public          postgres    false    250    248    3464                       2606    19676 =   beone_opname_detail beone_opname_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 g   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_beone_item_item_id_fk;
       public          postgres    false    3446    232    252                       2606    19616 O   beone_opname_detail beone_opname_detail_beone_opname_header_opname_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_beone_opname_header_opname_header_id_fk FOREIGN KEY (opname_header_id) REFERENCES public.beone_opname_header(opname_header_id);
 y   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_beone_opname_header_opname_header_id_fk;
       public          postgres    false    252    3468    254                       2606    19661 O   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 y   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_beone_item_item_id_fk;
       public          postgres    false    232    258    3446                       2606    19561 \   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_beone_pemakaian_bahan_header_pemak 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_beone_pemakaian_bahan_header_pemak FOREIGN KEY (pemakaian_header_id) REFERENCES public.beone_pemakaian_bahan_header(pemakaian_header_id);
 �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_beone_pemakaian_bahan_header_pemak;
       public          postgres    false    258    222    3435                       2606    19646 C   beone_po_import_detail beone_po_import_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 m   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_beone_item_item_id_fk;
       public          postgres    false    3446    232    264                       2606    19641 V   beone_po_import_detail beone_po_import_detail_beone_po_import_header_purchase_header_i 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_beone_po_import_header_purchase_header_i FOREIGN KEY (purchase_header_id) REFERENCES public.beone_po_import_header(purchase_header_id);
 �   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_beone_po_import_header_purchase_header_i;
       public          postgres    false    3480    264    266                       2606    20325 L   beone_po_import_detail beone_po_import_detail_beone_satuan_item_satuan_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_beone_satuan_item_satuan_id_fk FOREIGN KEY (satuan_id) REFERENCES public.beone_satuan_item(satuan_id);
 v   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_beone_satuan_item_satuan_id_fk;
       public          postgres    false    288    264    3503                       2606    19766 K   beone_po_import_header beone_po_import_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 u   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_beone_supplier_supplier_id_fk;
       public          postgres    false    266    316    3529                       2606    19566 U   beone_produksi_detail beone_produksi_detail_beone_pemakaian_bahan_header_pemakaian_he 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_beone_pemakaian_bahan_header_pemakaian_he FOREIGN KEY (pemakaian_header_id) REFERENCES public.beone_pemakaian_bahan_header(pemakaian_header_id);
    ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_beone_pemakaian_bahan_header_pemakaian_he;
       public          postgres    false    3435    268    222                       2606    19571 U   beone_produksi_detail beone_produksi_detail_beone_produksi_header_produksi_header_id_ 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_beone_produksi_header_produksi_header_id_ FOREIGN KEY (produksi_header_id) REFERENCES public.beone_produksi_header(produksi_header_id);
    ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_beone_produksi_header_produksi_header_id_;
       public          postgres    false    270    3484    268                       2606    19651 A   beone_purchase_detail beone_purchase_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 k   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_beone_item_item_id_fk;
       public          postgres    false    272    232    3446                       2606    19556 U   beone_purchase_detail beone_purchase_detail_beone_purchase_header_purchase_header_id_ 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_beone_purchase_header_purchase_header_id_ FOREIGN KEY (purchase_header_id) REFERENCES public.beone_purchase_header(purchase_header_id);
    ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_beone_purchase_header_purchase_header_id_;
       public          postgres    false    272    274    3488                       2606    19796 I   beone_purchase_header beone_purchase_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 s   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_beone_supplier_supplier_id_fk;
       public          postgres    false    274    3529    316            1           2606    20371 5   beone_roles_modul beone_roles_modul_beone_modul_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_roles_modul
    ADD CONSTRAINT beone_roles_modul_beone_modul_id_fk FOREIGN KEY (modul_id) REFERENCES public.beone_modul(modul_id);
 _   ALTER TABLE ONLY public.beone_roles_modul DROP CONSTRAINT beone_roles_modul_beone_modul_id_fk;
       public          postgres    false    3547    332    330                       2606    19656 ;   beone_sales_detail beone_sales_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 e   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_beone_item_item_id_fk;
       public          postgres    false    284    3446    232                       2606    19551 K   beone_sales_detail beone_sales_detail_beone_sales_header_sales_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_beone_sales_header_sales_header_id_fk FOREIGN KEY (sales_header_id) REFERENCES public.beone_sales_header(sales_header_id);
 u   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_beone_sales_header_sales_header_id_fk;
       public          postgres    false    284    3500    286                       2606    20366 D   beone_sales_detail beone_sales_detail_beone_satuan_item_satuan_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_beone_satuan_item_satuan_id_fk FOREIGN KEY (satuan_id) REFERENCES public.beone_satuan_item(satuan_id);
 n   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_beone_satuan_item_satuan_id_fk;
       public          postgres    false    284    3503    288            !           2606    20341 C   beone_sales_header beone_sales_header_beone_currency_currency_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_beone_currency_currency_id_fk FOREIGN KEY (currency_id) REFERENCES public.beone_currency(currency_id);
 m   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_beone_currency_currency_id_fk;
       public          postgres    false    3533    318    286                        2606    19791 C   beone_sales_header beone_sales_header_beone_customer_customer_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_beone_customer_customer_id_fk FOREIGN KEY (customer_id) REFERENCES public.beone_customer(customer_id);
 m   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_beone_customer_customer_id_fk;
       public          postgres    false    286    314    3527            "           2606    19786 9   beone_satuan_item beone_satuan_item_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 c   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_beone_item_item_id_fk;
       public          postgres    false    288    232    3446            4           2606    20471 W   beone_sub_kontraktor_in_detail beone_sub_kontraktor_in_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail
    ADD CONSTRAINT beone_sub_kontraktor_in_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail DROP CONSTRAINT beone_sub_kontraktor_in_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    218    344    3431            3           2606    20466 S   beone_sub_kontraktor_in_detail beone_sub_kontraktor_in_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail
    ADD CONSTRAINT beone_sub_kontraktor_in_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 }   ALTER TABLE ONLY public.beone_sub_kontraktor_in_detail DROP CONSTRAINT beone_sub_kontraktor_in_detail_beone_item_item_id_fk;
       public          postgres    false    344    232    3446            2           2606    20444 [   beone_sub_kontraktor_in_header beone_sub_kontraktor_in_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_header
    ADD CONSTRAINT beone_sub_kontraktor_in_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 �   ALTER TABLE ONLY public.beone_sub_kontraktor_in_header DROP CONSTRAINT beone_sub_kontraktor_in_header_beone_supplier_supplier_id_fk;
       public          postgres    false    342    316    3529            7           2606    20524 Y   beone_sub_kontraktor_out_detail beone_sub_kontraktor_out_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail
    ADD CONSTRAINT beone_sub_kontraktor_out_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail DROP CONSTRAINT beone_sub_kontraktor_out_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    3431    218    348            6           2606    20519 U   beone_sub_kontraktor_out_detail beone_sub_kontraktor_out_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail
    ADD CONSTRAINT beone_sub_kontraktor_out_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
    ALTER TABLE ONLY public.beone_sub_kontraktor_out_detail DROP CONSTRAINT beone_sub_kontraktor_out_detail_beone_item_item_id_fk;
       public          postgres    false    348    232    3446            5           2606    20496 ]   beone_sub_kontraktor_out_header beone_sub_kontraktor_out_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_header
    ADD CONSTRAINT beone_sub_kontraktor_out_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 �   ALTER TABLE ONLY public.beone_sub_kontraktor_out_header DROP CONSTRAINT beone_sub_kontraktor_out_header_beone_supplier_supplier_id_fk;
       public          postgres    false    346    316    3529            $           2606    19636 C   beone_subkon_in_detail beone_subkon_in_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 m   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_beone_item_item_id_fk;
       public          postgres    false    290    232    3446            #           2606    19626 T   beone_subkon_in_detail beone_subkon_in_detail_beone_subkon_in_header_subkon_in_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_beone_subkon_in_header_subkon_in_id_fk FOREIGN KEY (subkon_in_header_id) REFERENCES public.beone_subkon_in_header(subkon_in_id);
 ~   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_beone_subkon_in_header_subkon_in_id_fk;
       public          postgres    false    3507    292    290            %           2606    19761 K   beone_subkon_in_header beone_subkon_in_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 u   ALTER TABLE ONLY public.beone_subkon_in_header DROP CONSTRAINT beone_subkon_in_header_beone_supplier_supplier_id_fk;
       public          postgres    false    316    3529    292            '           2606    19631 E   beone_subkon_out_detail beone_subkon_out_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 o   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_beone_item_item_id_fk;
       public          postgres    false    294    3446    232            &           2606    19621 W   beone_subkon_out_detail beone_subkon_out_detail_beone_subkon_out_header_subkon_out_id_f 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_beone_subkon_out_header_subkon_out_id_f FOREIGN KEY (subkon_out_header_id) REFERENCES public.beone_subkon_out_header(subkon_out_id);
 �   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_beone_subkon_out_header_subkon_out_id_f;
       public          postgres    false    3511    296    294            (           2606    19756 M   beone_subkon_out_header beone_subkon_out_header_beone_supplier_supplier_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_beone_supplier_supplier_id_fk FOREIGN KEY (supplier_id) REFERENCES public.beone_supplier(supplier_id);
 w   ALTER TABLE ONLY public.beone_subkon_out_header DROP CONSTRAINT beone_subkon_out_header_beone_supplier_supplier_id_fk;
       public          postgres    false    316    3529    296            +           2606    19686 Q   beone_transfer_stock_detail beone_transfer_stock_detail_beone_gudang_gudang_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_beone_gudang_gudang_id_fk FOREIGN KEY (gudang_id) REFERENCES public.beone_gudang(gudang_id);
 {   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_beone_gudang_gudang_id_fk;
       public          postgres    false    3431    303    218            *           2606    19681 M   beone_transfer_stock_detail beone_transfer_stock_detail_beone_item_item_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_beone_item_item_id_fk FOREIGN KEY (item_id) REFERENCES public.beone_item(item_id);
 w   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_beone_item_item_id_fk;
       public          postgres    false    303    3446    232            )           2606    19546 [   beone_transfer_stock_detail beone_transfer_stock_detail_beone_transfer_stock_transfer_stock 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_beone_transfer_stock_transfer_stock FOREIGN KEY (transfer_stock_header_id) REFERENCES public.beone_transfer_stock(transfer_stock_id);
 �   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_beone_transfer_stock_transfer_stock;
       public          postgres    false    3515    302    303            -           2606    20346 G   beone_voucher_detail beone_voucher_detail_beone_currency_currency_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_beone_currency_currency_id_fk FOREIGN KEY (currency_id) REFERENCES public.beone_currency(currency_id);
 q   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_beone_currency_currency_id_fk;
       public          postgres    false    308    318    3533            ,           2606    19541 S   beone_voucher_detail beone_voucher_detail_beone_voucher_header_voucher_header_id_fk 
   FK CONSTRAINT     �   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_beone_voucher_header_voucher_header_id_fk FOREIGN KEY (voucher_header_id) REFERENCES public.beone_voucher_header(voucher_header_id);
 }   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_beone_voucher_header_voucher_header_id_fk;
       public          postgres    false    3523    310    308            �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      >   '   x�3��t	�

�t��2�
v�t���q����� ov�      :   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      D   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      T   
   x������ � �      R   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �      x�3��
���\1z\\\ *`�      �   
   x������ � �      B   
   x������ � �      @   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      J   �  x�}WYo�6~�~��}K`���l��$��wї %36W
IL7���Ӥ��K �w��3���4���^��
z���i�1��d�����dtߟ�8/�;��p�i�8)���3��ioz��`����0����b�7�؊ �9����N`}����$S������������y�
�G�0>��i}�uG|j��qSӮJK��_2�
z$G<�?'B_1�bIiN����A���D��� �� ��[%���r�����AS����*��-ڑ�HGڻ*�f"��{x/v��`� ���z�3�h��Y�L�K�|pO�x�� ��[���8Q��k0�]v|�lrG��vY*t�������#�&���b�!Ux
����ۨx��8�.�������܆��o��LAX��;��b�B%z!-�$�0���I<j<�h�s��cg�}���IfL;��������#I��+\ZE�ē1H��1�ye�*�4��`�Pw�T]�K搭_��U���?
�+�BQ��$H�X��t
]�"R]3K7.]Z:d�8������=�I��[Ǟ@z�t�<A����ᵩl��mz�JOK�E�<�;z1���av��M�g���� ��寕Yah�J�<�}bؾ%�K��FԻ슷zd�L&
5E[�&�2����P=��X�z�1=��ʀ1�9u���BU��z��AJ�5� ������9P���6[�MD�^jg�o�/e�n��e���L�R_���l�D{��m��O/o��d�����*WC��o��ӭ%��k�9Jb��jiư����A��B�.D�ޗ�U� n��P74�UL��
�Se�'P�p��ݐ8��UF�C/�^+�4+���˅H~W"W1��.�5�f��ź��$H��c�x1��������eF��?����5�G1�\�h%`��R|?�jt���G�|9f
}����Yq�QlE1@uV�ͱn}yqE�*�J���f5�=�k�"UDk�bɓ����bv�ap��Q����D�%�3�i�͐����l�8���.�����Е�!�2*�o9�a���rQC��e$�W��
��mnF3IoED^�/^������3�`�:�X�jQz���u�]�FVn�.Q�г��E�_ZQ��ᓜ�[������Y�xm�oF|e�I��0������@�-.�� �I�6y"��^��0���埋2W���?l�4�B���,(�Tj��m�u��%      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �      �   
   x������ � �          
   x������ � �         
   x������ � �      �   
   x������ � �      P   
   x������ � �      N   
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �      
   
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �         
   x������ � �      L   �  x�5��m1�}�'�z5�
��Y200��JG�O����y���3ő1#�T���ʸwƣx2^ś��8�*�ZJa�q
�H� c��
��WpF,,2f��1���3���皧��]c+3x)� \4
S � .�
]!C��C���/�nz`4�����P�7��:�+�y�<�
~�<�_��s��Q��+��(y ���(�D>S>�ϒO�3�9pA\�f�]P�>k�&�Y���k�[��X�  .L x��	� vey�W���?@*�l�Bg]6�ݪ l�F
\�vu#.�Qm�U��r���:xN��G�\��#d�#t.���p8�s��T�Lp�[>��
'\.Y��*L da!Y�
h.N��\�?����r��         A   x�3�tt����	r�B���q���)�[��YZ�Y�Xj������� jO�         
   x������ � �         
   x������ � �          
   x������ � �      I   
   x������ � �      G   
   x������ � �      X   
   x������ � �      V   
   x������ � �      \   
   x������ � �      Z   
   x������ � �      "   
   x������ � �      $   
   x������ � �      &   
   x������ � �      (   
   x������ � �      <   
   x������ � �      *   
   x������ � �      ,   
   x������ � �      .   
   x������ � �      /   
   x������ � �      `   
   x������ � �      ^   
   x������ � �      2   
   x������ � �      4   
   x������ � �      6   
   x������ � �      8   
   x������ � �      E   
   x������ � �     
