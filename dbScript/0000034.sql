alter table beone_mutasi_stok_detail
    add if not exists satuan_id int not null;

alter table beone_mutasi_stok_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_satuan_item_satuan_id_fk;

alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_opname_detail
    add if not exists satuan_id int not null;

alter table beone_opname_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_opname_detail
    drop constraint if exists beone_opname_detail_beone_satuan_item_satuan_id_fk;

alter table beone_opname_detail
    add constraint beone_opname_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_konversi_stok_header
    add if not exists satuan_id int not null;

alter table beone_konversi_stok_header
    add if not exists rasio double precision default 1 not null;

alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_satuan_item_satuan_id_fk;

alter table beone_konversi_stok_header
    add constraint beone_konversi_stok_header_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_po_import_detail
    alter column purchase_header_id set not null;
alter table beone_po_import_detail
    alter column item_id set not null;
alter table beone_item
    alter column item_code set not null;
alter table beone_item
    alter column item_type_id set not null;

alter table beone_po_import_detail
    add if not exists satuan_id int not null;
alter table beone_po_import_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_po_import_detail
    drop constraint if exists beone_po_import_detail_beone_satuan_item_satuan_id_fk;
alter table beone_po_import_detail
    add constraint beone_po_import_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_satuan_item
    alter column item_id set not null;
alter table beone_satuan_item
    alter column rasio set not null;
alter table beone_import_detail
    alter column item_id set not null;
alter table beone_import_detail
    alter column import_header_id set not null;
alter table beone_import_detail
    alter column purchase_detail_id set not null;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_satuan_item_satuan_id_fk;
alter table beone_import_detail
    add if not exists satuan_id int not null;
alter table beone_import_detail
    add if not exists rasio double precision default 1 not null;
alter table beone_purchase_header
    alter column supplier_id set not null;
alter table beone_po_import_header
    alter column supplier_id set not null;
alter table beone_sales_detail
    alter column sales_header_id set not null;
alter table beone_sales_detail
    alter column item_id set not null;
alter table beone_sales_detail
    add if not exists satuan_id int not null;
alter table beone_sales_detail
    add if not exists rasio double precision default 1 not null;
alter table beone_sales_header
    alter column customer_id set not null;
alter table beone_export_detail
    alter column item_id set not null;

alter table beone_export_detail
    alter column sales_header_id set not null;

alter table beone_export_detail
    alter column sales_detail_id set not null;

alter table beone_export_detail
    add if not exists satuan_id int not null;

alter table beone_export_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk_2;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_sales_detail_sales_detail_id_fk;
alter table beone_export_detail
    add constraint beone_export_detail_beone_sales_detail_sales_detail_id_fk
        foreign key (sales_detail_id) references beone_sales_detail;
alter table beone_export_detail
    alter column item_id set not null;

alter table beone_export_detail
    alter column sales_header_id set not null;

alter table beone_export_detail
    alter column sales_detail_id set not null;

alter table beone_export_detail
    add if not exists satuan_id int not null;

alter table beone_export_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk_2;

alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_sales_detail_sales_detail_id_fk;

alter table beone_export_detail
    add constraint beone_export_detail_beone_sales_detail_sales_detail_id_fk
        foreign key (sales_detail_id) references beone_sales_detail;

alter table beone_sales_header
    drop constraint if exists beone_sales_header_beone_currency_currency_id_fk;

alter table beone_sales_header
    add constraint beone_sales_header_beone_currency_currency_id_fk
        foreign key (currency_id) references beone_currency;

alter table beone_transfer_stock_detail
    alter column transfer_stock_header_id set not null;

alter table beone_transfer_stock_detail
    alter column item_id set not null;

alter table beone_transfer_stock_detail
    alter column gudang_id set not null;
alter table beone_transfer_stock_detail
    add if not exists satuan_id int not null;

alter table beone_transfer_stock_detail
    add if not exists rasio double precision default 1 not null;
alter table beone_konversi_stok_detail
    alter column konversi_stok_header_id set not null;

alter table beone_konversi_stok_detail
    alter column item_id set not null;

alter table beone_konversi_stok_detail
    add if not exists satuan_id int not null;

alter table beone_konversi_stok_detail
    add if not exists rasio double precision default 1 not null;
alter table beone_konversi_stok_header
    alter column item_id set not null;

alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_satuan_item_satuan_id_fkbeone_;

alter table beone_inventory
    alter column item_id set not null;

alter table beone_voucher_detail
    drop constraint if exists beone_voucher_detail_beone_currency_currency_id_fk;

alter table beone_voucher_detail
    add constraint beone_voucher_detail_beone_currency_currency_id_fk
        foreign key (currency_id) references beone_currency;

alter table beone_jurnal_detail
    drop constraint if exists beone_jurnal_detail_beone_coa_coa_id_fk;

alter table beone_jurnal_detail
    add constraint beone_jurnal_detail_beone_coa_coa_id_fk
        foreign key (coa_id) references beone_coa;

alter table beone_jurnal_detail
    drop constraint if exists beone_jurnal_detail_beone_currency_currency_id_fk;

alter table beone_jurnal_detail
    add constraint beone_jurnal_detail_beone_currency_currency_id_fk
        foreign key (currency_id) references beone_currency;

alter table beone_jurnal_detail
    drop constraint if exists beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk;

alter table beone_jurnal_detail
    add constraint beone_jurnal_detail_beone_jurnal_header_jurnal_header_id_fk
        foreign key (jurnal_header_id) references beone_jurnal_header;

alter table beone_gudang_detail
    alter column gudang_id set not null;

alter table beone_gudang_detail
    alter column item_id set not null;
alter table beone_export_detail
    add if not exists amount double precision default 0 not null;

alter table beone_export_detail
    add if not exists amount_sys double precision default 0 not null;

alter table beone_sales_detail
    drop constraint if exists beone_sales_detail_beone_satuan_item_satuan_id_fk;

alter table beone_sales_detail
    add constraint beone_sales_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_roles_modul
    alter column modul_id set not null;

alter table beone_roles_modul
    drop constraint if exists beone_roles_modul_beone_modul_id_fk;

alter table beone_roles_modul
    add constraint beone_roles_modul_beone_modul_id_fk
        foreign key (modul_id) references beone_modul;

create unique index if not exists beone_coa_nomor_uindex
	on beone_coa (nomor);
