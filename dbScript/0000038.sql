create table if not exists beone_sub_kontraktor_in_header
(
    sub_kontraktor_in_header_id  serial primary key,
    trans_no                     varchar(200)                       not null,
    trans_date                   date                               not null,
    supplier_id                  integer
        constraint beone_sub_kontraktor_in_header_beone_supplier_supplier_id_fk
            references beone_supplier,
    currency_id                  integer                  default 1 not null,
    sub_kontraktor_out_header_id integer,
    biaya                        double precision         default 0 not null,
    dpp                          double precision         default 0 not null,
    ppn                          double precision         default 0 not null,
    grandtotal                   double precision         default 0 not null,
    sisabayar                    double precision         default 0 not null,
    terbayar                     double precision         default 0 not null,
    jenis_bc                     integer,
    bc_no                        varchar(50),
    bc_date                      date,
    bc_no_aju                    varchar(500),
    bc_nama_vendor               varchar(500),
    bc_nama_pemilik              varchar(500),
    bc_nama_penerima_barang      varchar(500),
    bc_nama_pengangkut           varchar(500),
    bc_nama_pengirim             varchar(500),
    bc_kurs                      double precision         default 1 not null,
    bc_header_id                 integer,
    deleted_at                   timestamp with time zone,
    created_at                   timestamp with time zone default CURRENT_TIMESTAMP,
    updated_at                   timestamp with time zone default CURRENT_TIMESTAMP
);

create table if not exists beone_sub_kontraktor_in_detail
(
    sub_kontraktor_in_detail_id serial primary key,
    sub_kontraktor_in_header_id integer,
    item_id                     integer
        constraint beone_sub_kontraktor_in_detail_beone_item_item_id_fk
            references beone_item,
    qty                         double precision default 0 not null,
    price                       double precision default 0 not null,
    gudang_id                   integer          default 0 not null
        constraint beone_sub_kontraktor_in_detail_beone_gudang_gudang_id_fk
            references beone_gudang,
    bc_barang_id                integer,
    bc_barang_uraian            varchar(1000),
    isppn                       integer          default 0 not null,
    amount                      double precision default 0 not null,
    amount_ppn                  double precision default 0 not null
);

create table if not exists beone_sub_kontraktor_out_header
(
    sub_kontraktor_out_header_id serial primary key,
    trans_no                     varchar(200)                       not null,
    trans_date                   date                               not null,
    supplier_id                  integer
        constraint beone_sub_kontraktor_out_header_beone_supplier_supplier_id_fk
            references beone_supplier,
    currency_id                  integer                  default 1 not null,
    dpp                          double precision         default 0 not null,
    ppn                          double precision         default 0 not null,
    grandtotal                   double precision         default 0 not null,
    sisabayar                    double precision         default 0 not null,
    terbayar                     double precision         default 0 not null,
    jenis_bc                     integer,
    bc_no                        varchar(50),
    bc_date                      date,
    bc_no_aju                    varchar(500),
    bc_nama_vendor               varchar(500),
    bc_nama_pemilik              varchar(500),
    bc_nama_penerima_barang      varchar(500),
    bc_nama_pengangkut           varchar(500),
    bc_nama_pengirim             varchar(500),
    bc_kurs                      double precision         default 1 not null,
    bc_header_id                 integer,
    deleted_at                   timestamp with time zone,
    created_at                   timestamp with time zone default CURRENT_TIMESTAMP,
    updated_at                   timestamp with time zone default CURRENT_TIMESTAMP
);

create table if not exists beone_sub_kontraktor_out_detail
(
    sub_kontraktor_out_detail_id serial primary key,
    sub_kontraktor_out_header_id integer,
    item_id                      integer
        constraint beone_sub_kontraktor_out_detail_beone_item_item_id_fk
            references beone_item,
    qty                          double precision default 0 not null,
    price                        double precision default 0 not null,
    gudang_id                    integer          default 0 not null
        constraint beone_sub_kontraktor_out_detail_beone_gudang_gudang_id_fk
            references beone_gudang,
    bc_barang_id                 integer,
    bc_barang_uraian             varchar(1000),
    isppn                        integer          default 0 not null,
    amount                       double precision default 0 not null,
    amount_ppn                   double precision default 0 not null,
    price_hpp                    double precision default 0 not null
);

alter table beone_import_detail
    add if not exists bc_barang_uraian varchar(100);

alter table beone_export_header
    add if not exists grandtotal double precision default 0 not null;

alter table beone_sub_kontraktor_in_detail
    add if not exists satuan_id int not null;

alter table beone_sub_kontraktor_in_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_sub_kontraktor_in_detail
    drop constraint if exists beone_sub_kontraktor_in_detail_beone_satuan_item_satuan_id_fk;

alter table beone_sub_kontraktor_in_detail
    add constraint beone_sub_kontraktor_in_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_sub_kontraktor_out_detail
    add if not exists satuan_id int not null;

alter table beone_sub_kontraktor_out_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_sub_kontraktor_out_detail
    drop constraint if exists beone_sub_kontraktor_out_detail_beone_satuan_item_satuan_id_fk;

alter table beone_sub_kontraktor_out_detail
    add constraint beone_sub_kontraktor_out_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;
