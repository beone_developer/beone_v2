ALTER TABLE public.beone_produksi_header ADD COLUMN IF NOT EXISTS pm_header_id INTEGER;
CREATE TABLE IF NOT EXISTS public.beone_pm_detail (
   pm_detail_id SERIAL PRIMARY KEY,
   item_id INTEGER,
   qty FLOAT,
   satuan_id INTEGER,
   pm_header_id INTEGER
);

/* ADD field in produksi_header and create table pm detail*/