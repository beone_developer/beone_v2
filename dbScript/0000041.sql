CREATE TABLE IF NOT EXISTS beone_usage_header
(
    usage_header_id SERIAL PRIMARY KEY,
    trans_no        VARCHAR(50) not null,
    trans_date      date        not null,
    keterangan      VARCHAR(255),
    deleted_at      timestamp with time zone,
    created_at      timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at      timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_usage_detail
(
    usage_detail_id SERIAL PRIMARY KEY,
    usage_header_id int              not null,
    item_id         int              not null,
    qty             double precision not null,
    gudang_id       int              not null
);

alter table beone_usage_detail
    add if not exists satuan_id int not null;

alter table beone_usage_detail
    add if not exists rasio double precision default 1 not null;

alter table beone_usage_detail
    drop constraint if exists beone_usage_detail_beone_satuan_item_satuan_id_fk;

alter table beone_usage_detail
    add constraint beone_usage_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_id) references beone_satuan_item;

alter table beone_konfigurasi_perusahaan
    add if not exists receiving_multi_po int not null default 0;
