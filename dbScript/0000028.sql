drop procedure if exists recalculate_stok_hpp();

CREATE OR REPLACE PROCEDURE recalculate_stok_hpp(p_trans_date_awal date, p_trans_date_akhir date)
    LANGUAGE plpgsql AS
$$
BEGIN
    DROP TABLE IF EXISTS temp_trans;
    DROP TABLE IF EXISTS temp_trans_for_cursor;

    CREATE TEMP TABLE temp_trans AS
    SELECT 1                       as priority,
           beh.receive_date        as trans_date,
           beh.import_header_id    as trans_id,
           bed.import_detail_id    as trans_detail_id,
           beh.receive_no          as trans_no,
           'IMPORT'                as keterangan,
           bed.gudang_id           as gudang_id,
           bed.item_id             as item_id,
           bed.qty                 as qty,
           bed.price * beh.bc_kurs as price
    FROM beone_import_header beh
             INNER JOIN beone_import_detail bed
                        on beh.
                               import_header_id = bed.import_header_id
                            AND beh.trans_date between p_trans_date_awal and p_trans_date_akhir
    WHERE coalesce(beh.receive_no, '') <> ''
    UNION ALL
    SELECT 2                           as priority,
           beh.konversi_stok_date,
           beh.konversi_stok_header_id,
           beh.konversi_stok_header_id as trans_detail_id,
           beh.konversi_stok_no,
           'KONVERSI STOK IN'          as keterangan,
           beh.gudang_id,
           beh.item_id,
           beh.qty_real,
           0                           as price
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 4                    as priority,
           beh.delivery_date    as trans_date,
           beh.export_header_id as trans_id,
           bed.export_detail_id as trans_detail_id,
           beh.delivery_no      as trans_no,
           'EXPORT'             as keterangan,
           bed.gudang_id        as gudang_id,
           bed.item_id          as item_id,
           bed.qty * -1         as qty,
           0                    as price
    FROM beone_export_header beh
             INNER JOIN beone_export_detail bed
                        on beh.export_header_id = bed.export_header_id
    WHERE coalesce(beh.delivery_no, '') <> ''
      AND beh.delivery_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 3                           as priority,
           beh.konversi_stok_date,
           beh.konversi_stok_header_id,
           bed.konversi_stok_detail_id as trans_detail_id,
           beh.konversi_stok_no,
           'KONVERSI STOK OUT'         as keterangan,
           bed.gudang_id,
           bed.item_id,
           bed.qty_real * -1,
           0                           as price
    FROM beone_konversi_stok_header beh
             INNER JOIN beone_konversi_stok_detail bed
                        on beh.konversi_stok_header_id = bed.konversi_stok_header_id
    WHERE coalesce(bed.qty_real, 0) > 0
      AND beh.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir
    UNION ALL
    SELECT 5                            as priority,
           beh.transfer_date,
           beh.transfer_stock_id,
           bed.transfer_stock_detail_id as trans_detail_id,
           beh.transfer_no,
           'TRANSFER STOCK'             as keterangan,
           bed.gudang_id,
           bed.item_id,
           case (lower(bed.tipe_transfer_stock))
               when 'bb' then bed.qty * -1
               when 'wip'
                   then bed.qty end     as qty,
           0                            as price
    FROM beone_transfer_stock beh
             INNER JOIN beone_transfer_stock_detail bed
                        on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE beh.transfer_date between p_trans_date_awal and p_trans_date_akhir;

    DELETE
    FROM beone_inventory
    WHERE keterangan <> 'SALDO AWAL'
      AND trans_date between p_trans_date_awal and p_trans_date_akhir;

    INSERT INTO beone_inventory (intvent_trans_no, item_id, trans_date, keterangan, qty_in,
                                 qty_out, flag, gudang_id, invent_trans_detail_id)
    SELECT trans_no,
           item_id,
           trans_date,
           keterangan,
           case when (coalesce(qty, 0) > 0) then abs(coalesce(qty, 0)) else 0 end as qty_in,
           case when (coalesce(qty, 0) < 0) then abs(coalesce(qty, 0)) else 0 end as qty_out,
           1                                                                      as flag,
           gudang_id,
           trans_detail_id
    FROM temp_trans;

    CREATE TEMP TABLE temp_trans_for_cursor AS
    SELECT iv.intvent_trans_id, iv.invent_trans_detail_id, te.*
    FROM beone_inventory iv
             JOIN temp_trans te
                  ON iv.invent_trans_detail_id = te.trans_detail_id
                      AND iv.item_id = te.item_id
                      AND iv.intvent_trans_no = te.trans_no;
    DECLARE
        temp_trans_cursor CURSOR
            FOR
            SELECT t.*
            FROM temp_trans_for_cursor t
            ORDER BY t.trans_date, t.priority asc;
    BEGIN
        FOR rec IN temp_trans_cursor
            LOOP
                DROP TABLE IF EXISTS prev_rec;

                CREATE TEMP TABLE prev_rec AS
                SELECT i.*
                FROM beone_inventory i
                WHERE i.item_id = rec.item_id
                  AND i.qty_in > 0
                  AND i.trans_date <= rec.trans_date
                  AND i.intvent_trans_id < rec.intvent_trans_id
                ORDER BY i.trans_date, i.intvent_trans_id asc
                LIMIT 1;

                IF (select count(*) from prev_rec) THEN
                    UPDATE beone_inventory i
                    SET price_hpp     = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2),
                        update_by     = '2',
                        value_in      = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2) *
                                        i.qty_in,
                        value_out     = ((coalesce(nullif(rec.price, 0), pr.price_hpp) + pr.price_hpp) / 2) *
                                        i.qty_out,
--                             sa_amount     = (i.value_in - i.value_out) + (pr.value_in - pr.value_out),
                        sa_unit_price = rec.price
                    FROM prev_rec pr
                    WHERE i.intvent_trans_no = rec.trans_no
                      AND i.item_id = rec.item_id;
                ELSE
                    UPDATE beone_inventory i
                    SET price_hpp     = rec.price,
                        update_by     = '1',
                        value_in      = rec.price * rec.qty,
                        value_out     = 0,
--                             sa_amount     = rec.price * rec.qty,
                        sa_unit_price = rec.price
                    WHERE i.intvent_trans_no = rec.trans_no
                      AND i.item_id = rec.item_id;
                END IF;
            END LOOP;
    END;

    UPDATE beone_konversi_stok_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_konversi_stok_header h,
         beone_inventory iv
    WHERE d.konversi_stok_detail_id = iv.invent_trans_detail_id
      AND d.konversi_stok_header_id = h.konversi_stok_header_id
      AND iv.intvent_trans_no = h.konversi_stok_no
      AND h.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_konversi_stok_header h
    SET price_hpp = x.price_hpp
    FROM (
             SELECT d.konversi_stok_header_id, avg(d.price_hpp) as price_hpp
             FROM beone_konversi_stok_detail d
             GROUP BY d.konversi_stok_header_id
         ) x
    WHERE x.konversi_stok_header_id = h.konversi_stok_header_id
      AND h.konversi_stok_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_export_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_export_header h,
         beone_inventory iv
    WHERE d.export_detail_id = iv.invent_trans_detail_id
      AND d.export_header_id = h.export_header_id
      AND iv.intvent_trans_no = h.delivery_no
      AND h.delivery_date between p_trans_date_awal and p_trans_date_akhir;

    UPDATE beone_transfer_stock_detail d
    SET price_hpp = iv.price_hpp
    FROM beone_transfer_stock h,
         beone_inventory iv
    WHERE d.transfer_stock_detail_id = iv.invent_trans_detail_id
      AND d.transfer_stock_header_id = h.transfer_stock_id
      AND iv.intvent_trans_no = h.transfer_no
      AND iv.trans_date between p_trans_date_awal and p_trans_date_akhir;
END
$$;

drop procedure if exists recalculate_gl();

CREATE OR REPLACE PROCEDURE recalculate_gl(p_trans_date_awal date, p_trans_date_akhir date)
    LANGUAGE plpgsql AS
$$
DECLARE
    coa_id_persediaan_bb  int;
    coa_id_hutang_usaha   int;
    coa_id_pemakaian_bb   int;
    coa_id_hpp_produksi   int;
    coa_id_persediaan_bj  int;
    coa_id_persediaan_wip int;
    coa_id_hpp_penjualan  int;
    coa_id_piutang_usaha  int;
    coa_id_penjualan      int;
BEGIN
    coa_id_persediaan_bb = (select coa_id from beone_coa where nomor = '116.01.01');
    coa_id_persediaan_wip = (select coa_id from beone_coa where nomor = '116.05.01');
    coa_id_persediaan_bj = (select coa_id from beone_coa where nomor = '116.06.01');

    coa_id_pemakaian_bb = (select coa_id from beone_coa where nomor = '510.02.01');

    coa_id_hpp_produksi = (select coa_id from beone_coa where nomor = '511.01.01');
    coa_id_hpp_penjualan = (select coa_id from beone_coa where nomor = '500.01.01');

    coa_id_hutang_usaha = (select coa_id from beone_coa where nomor = '211.02.01');
    coa_id_piutang_usaha = (select coa_id from beone_coa where nomor = '115.01.01');
    coa_id_penjualan = (select coa_id from beone_coa where nomor = '411.01.01');

    DROP TABLE IF EXISTS temp_trans;
    DROP TABLE IF EXISTS temp_trans_for_cursor;

    CREATE TEMP TABLE temp_trans AS
--         IMPORT
    SELECT coa_id_persediaan_bb as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           beh.grandtotal       as debet,
           0                    as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
    union all
    SELECT coa_id_hutang_usaha  as coa_id,
           beh.receive_date     as trans_date,
           beh.import_header_id as trans_id,
           beh.receive_no       as trans_no,
           beh.bc_no_aju        as keterangan,
           beh.bc_kurs          as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           beh.grandtotal       as kredit
    FROM beone_import_header beh
    WHERE coalesce(beh.receive_no, '') <> ''
    union all
--         KONVERSI STOK WIP
    SELECT coa_id_pemakaian_bb          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
    SELECT coa_id_persediaan_bb         as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
--         KONVERSI STOK WIP 2
    SELECT coa_id_persediaan_wip        as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           beh.qty_real * beh.price_hpp as debet,
           0                            as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
    SELECT coa_id_hpp_produksi          as coa_id,
           beh.konversi_stok_date       as trans_date,
           beh.konversi_stok_header_id  as trans_id,
           beh.konversi_stok_no         as trans_no,
           beh.konversi_stok_no         as keterangan,
           1                            as kurs,
           1                            as currency_id,
           0                            as debet,
           beh.qty_real * beh.price_hpp as kredit
    FROM beone_konversi_stok_header beh
    WHERE coalesce(beh.qty_real, 0) > 0
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'wip'
    union all
--         KONVERSI STOK BARANG JADI
    SELECT coa_id_persediaan_wip   as coa_id,
           beh.transfer_date       as trans_date,
           beh.transfer_stock_id   as trans_id,
           beh.transfer_no         as trans_no,
           beh.transfer_no         as keterangan,
           1                       as kurs,
           1                       as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_transfer_stock beh
             JOIN beone_transfer_stock_detail bed
                  on beh.
                         transfer_stock_id = bed.transfer_stock_header_id
    WHERE lower(bed.tipe_transfer_stock) = 'bb'
    union all
--         EXPORT HPP
    SELECT coa_id_hpp_penjualan    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           bed.qty * bed.price_hpp as debet,
           0                       as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT coa_id_persediaan_bj    as coa_id,
           beh.invoice_date        as trans_date,
           beh.export_header_id    as trans_id,
           beh.invoice_no          as trans_no,
           beh.invoice_no          as keterangan,
           1                       as kurs,
           beh.currency_id         as currency_id,
           0                       as debet,
           bed.qty * bed.price_hpp as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
                  on beh.
                         export_header_id = bed.export_header_id
    union all
--         EXPORT USAHA
    SELECT coa_id_piutang_usaha as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           bed.qty * bsd.price  as debet,
           0                    as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT coa_id_penjualan     as coa_id,
           beh.invoice_date     as trans_date,
           beh.export_header_id as trans_id,
           beh.invoice_no       as trans_no,
           beh.invoice_no       as keterangan,
           1                    as kurs,
           beh.currency_id      as currency_id,
           0                    as debet,
           bed.qty * bsd.price  as kredit
    FROM beone_export_header beh
             JOIN beone_export_detail bed
             JOIN beone_sales_detail bsd on bed.sales_detail_id = bsd.sales_detail_id
                  on beh.
                         export_header_id = bed.export_header_id
    union all
    SELECT bed.coa_id                                                 as coa_id,
           beh.trans_date                                             as trans_date,
           beh.jurnal_header_id                                       as trans_id,
           beh.jurnal_no                                              as trans_no,
           bed.keterangan_detail                                      as keterangan,
           bed.kurs                                                   as kurs,
           bed.currency_id                                            as currency_id,
           case lower(bed.status) when 'd' then bed.amount else 0 end as debet,
           case lower(bed.status) when 'k' then bed.amount else 0 end as kredit
    FROM beone_jurnal_header beh
             JOIN beone_jurnal_detail bed
                  on beh.jurnal_header_id = bed.jurnal_header_id
    union all
    SELECT bed.coa_id_lawan                                                              as coa_id,
           beh.voucher_date                                                              as trans_date,
           beh.voucher_header_id                                                         as trans_id,
           beh.voucher_number                                                            as trans_no,
           bed.keterangan_detail                                                         as keterangan,
           bed.kurs                                                                      as kurs,
           bed.currency_id                                                               as currency_id,
           case coalesce(beh.tipe, 1) when 1 then bed.jumlah_valas * bed.kurs else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then bed.jumlah_valas * bed.kurs else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    union all
    SELECT beh.coa_id_cash_bank                                                               as coa_id,
           beh.voucher_date                                                                   as trans_date,
           beh.voucher_header_id                                                              as trans_id,
           beh.voucher_number                                                                 as trans_no,
           beh.keterangan                                                                     as keterangan,
           1                                                                                  as kurs,
           1                                                                                  as currency_id,
           case coalesce(beh.tipe, 1) when 1 then sum(bed.jumlah_valas * bed.kurs) else 0 end as debet,
           case coalesce(beh.tipe, 1) when 0 then sum(bed.jumlah_valas * bed.kurs) else 0 end as kredit
    FROM beone_voucher_header beh
             JOIN beone_voucher_detail bed
                  on beh.voucher_header_id = bed.voucher_header_id
    group by beh.coa_id_cash_bank, beh.voucher_date, beh.voucher_header_id, beh.voucher_number, beh.keterangan,
             beh.tipe;

    DELETE FROM beone_general_ledger WHERE coalesce(keterangan, '') <> 'SALDO AWAL';
    INSERT INTO beone_general_ledger (coa_id, trans_no, trans_date, keterangan, currency_id, kurs, debet, kredit,
                                      debet_idr, kredit_idr)
    SELECT coa_id,
           trans_no,
           trans_date,
           keterangan,
           currency_id,
           kurs,
           debet,
           kredit,
           (kurs * debet)  as debet_idr,
           (kurs * kredit) as kredit_idr
    FROM temp_trans;
END
$$;
