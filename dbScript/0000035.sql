CREATE TABLE IF NOT EXISTS beone_piutang_header
(
    piutang_header_id SERIAL PRIMARY KEY,
    trans_no          VARCHAR(50)                        not null,
    trans_date        date                               not null,
    customer_id       int                                not null,
    grandtotal        double precision         default 0 not null,
    keterangan        VARCHAR(255),
    deleted_at        timestamp with time zone,
    created_at        timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at        timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_piutang_detail
(
    piutang_detail_id SERIAL PRIMARY KEY,
    piutang_header_id int                        not null,
    export_header_id  int                        not null,
    amount            double precision default 0 not null
);

CREATE TABLE IF NOT EXISTS beone_hutang_header
(
    hutang_header_id SERIAL PRIMARY KEY,
    trans_no         VARCHAR(50)                        not null,
    trans_date       date                               not null,
    supplier_id      int                                not null,
    grandtotal       double precision         default 0 not null,
    keterangan       VARCHAR(255),
    deleted_at       timestamp with time zone,
    created_at       timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at       timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS beone_hutang_detail
(
    hutang_detail_id SERIAL PRIMARY KEY,
    hutang_header_id int                        not null,
    import_header_id int                        not null,
    amount           double precision default 0 not null
);
