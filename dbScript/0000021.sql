create table if not exists beone_currency
(
    currency_id   SERIAL PRIMARY KEY,
    currency_code VARCHAR(3)   NOT NULL,
    keterangan    VARCHAR(200) NOT NULL
);

insert into beone_currency (currency_code, keterangan)
select currency_code, keterangan
from (
         select 'IDR' as currency_code, 'RUPIAH' as keterangan
         union
         select 'USD', 'DOLLAR'
     ) x
where not exists(select * from beone_currency where beone_currency.currency_code = x.currency_code);

ALTER TABLE beone_import_header
    DROP COLUMN IF EXISTS no_pengajuan;

ALTER TABLE beone_import_header
    DROP IF EXISTS no_pengajuan;

ALTER TABLE beone_import_header
    ADD IF NOT EXISTS bc_no_aju VARCHAR(500);

alter table beone_import_header
    add if not exists currency_id int not null default 1;

alter table beone_import_header
    add if not exists bc_nama_vendor varchar(500);

alter table beone_import_header
    add if not exists bc_nama_pemilik varchar(500);

alter table beone_import_header
    add if not exists bc_nama_penerima_barang varchar(500);

alter table beone_import_header
    add if not exists bc_nama_pengangkut varchar(500);

alter table beone_import_header
    add if not exists bc_nama_pengirim varchar(500);

ALTER TABLE beone_import_header
    ADD IF NOT EXISTS purchase_header_id int;

alter table beone_import_header
    add if not exists bc_kurs double precision default 1 not null;
