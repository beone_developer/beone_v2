alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_export_header_export_header_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_import_header_import_header_id_fk;
alter table beone_voucher_detail
    drop constraint if exists beone_voucher_detail_beone_voucher_header_voucher_header_id_fk;
alter table beone_transfer_stock_detail
    drop constraint if exists beone_transfer_stock_detail_beone_transfer_stock_transfer_stock_id_fk;
alter table beone_sales_detail
    drop constraint if exists beone_sales_detail_beone_sales_header_sales_header_id_fk;
alter table beone_purchase_detail
    drop constraint if exists beone_purchase_detail_beone_purchase_header_purchase_header_id_fk;
alter table beone_pemakaian_bahan_detail
    drop constraint if exists beone_pemakaian_bahan_detail_beone_pemakaian_bahan_header_pemakaian_header_id_fk;
alter table beone_produksi_detail
    drop constraint if exists beone_produksi_detail_beone_pemakaian_bahan_header_pemakaian_header_id_fk;
alter table beone_produksi_detail
    drop constraint if exists beone_produksi_detail_beone_produksi_header_produksi_header_id_fk;
alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_mutasi_stok_header_mutasi_stok_header_id_fk;
alter table beone_konversi_stok_detail
    drop constraint if exists beone_konversi_stok_detail_beone_konversi_stok_header_konversi_stok_header_id_fk;
alter table beone_konversi_stok_detail
    drop constraint if exists beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk;
alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_gudang_gudang_id_fk;
alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_item_item_id_fk;
alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_sales_header_sales_header_id_fk;
alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk;
alter table beone_export_detail
    drop constraint if exists beone_export_detail_beone_satuan_item_satuan_id_fk_2;
alter table beone_opname_detail
    drop constraint if exists beone_opname_detail_beone_opname_header_opname_header_id_fk;
alter table beone_subkon_out_detail
    drop constraint if exists beone_subkon_out_detail_beone_subkon_out_header_subkon_out_id_fk;
alter table beone_subkon_in_detail
    drop constraint if exists beone_subkon_in_detail_beone_subkon_in_header_subkon_in_id_fk;
alter table beone_subkon_out_detail
    drop constraint if exists beone_subkon_out_detail_beone_item_item_id_fk;
alter table beone_subkon_in_detail
    drop constraint if exists beone_subkon_in_detail_beone_item_item_id_fk;
alter table beone_po_import_detail
    drop constraint if exists beone_po_import_detail_beone_po_import_header_purchase_header_id_fk;

alter table beone_export_detail
    add constraint beone_export_detail_beone_export_header_export_header_id_fk
        foreign key (export_header_id) references beone_export_header;
alter table beone_import_detail
    add constraint beone_import_detail_beone_import_header_import_header_id_fk
        foreign key (import_header_id) references beone_import_header;
alter table beone_voucher_detail
    add constraint beone_voucher_detail_beone_voucher_header_voucher_header_id_fk
        foreign key (voucher_header_id) references beone_voucher_header;
alter table beone_transfer_stock_detail
    add constraint beone_transfer_stock_detail_beone_transfer_stock_transfer_stock_id_fk
        foreign key (transfer_stock_header_id) references beone_transfer_stock;
alter table beone_sales_detail
    add constraint beone_sales_detail_beone_sales_header_sales_header_id_fk
        foreign key (sales_header_id) references beone_sales_header;
alter table beone_purchase_detail
    add constraint beone_purchase_detail_beone_purchase_header_purchase_header_id_fk
        foreign key (purchase_header_id) references beone_purchase_header;
alter table beone_pemakaian_bahan_detail
    add constraint beone_pemakaian_bahan_detail_beone_pemakaian_bahan_header_pemakaian_header_id_fk
        foreign key (pemakaian_header_id) references beone_pemakaian_bahan_header;
alter table beone_produksi_detail
    add constraint beone_produksi_detail_beone_pemakaian_bahan_header_pemakaian_header_id_fk
        foreign key (pemakaian_header_id) references beone_pemakaian_bahan_header;
alter table beone_produksi_detail
    add constraint beone_produksi_detail_beone_produksi_header_produksi_header_id_fk
        foreign key (produksi_header_id) references beone_produksi_header;
alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_mutasi_stok_header_mutasi_stok_header_id_fk
        foreign key (mutasi_stok_header_id) references beone_mutasi_stok_header;
alter table beone_konversi_stok_detail
    add constraint beone_konversi_stok_detail_beone_konversi_stok_header_konversi_stok_header_id_fk
        foreign key (konversi_stok_header_id) references beone_konversi_stok_header;
alter table beone_konversi_stok_detail
    add constraint beone_konversi_stok_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_qty) references beone_satuan_item;
alter table beone_export_detail
    add constraint beone_export_detail_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_export_detail
    add constraint beone_export_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_export_detail
    add constraint beone_export_detail_beone_sales_header_sales_header_id_fk
        foreign key (sales_header_id) references beone_sales_header;
alter table beone_export_detail
    add constraint beone_export_detail_beone_satuan_item_satuan_id_fk
        foreign key (satuan_qty) references beone_satuan_item;
alter table beone_export_detail
    add constraint beone_export_detail_beone_satuan_item_satuan_id_fk_2
        foreign key (satuan_pack) references beone_satuan_item;
alter table beone_opname_detail
    add constraint beone_opname_detail_beone_opname_header_opname_header_id_fk
        foreign key (opname_header_id) references beone_opname_header;
alter table beone_subkon_out_detail
    add constraint beone_subkon_out_detail_beone_subkon_out_header_subkon_out_id_fk
        foreign key (subkon_out_header_id) references beone_subkon_out_header;
alter table beone_subkon_in_detail
    add constraint beone_subkon_in_detail_beone_subkon_in_header_subkon_in_id_fk
        foreign key (subkon_in_header_id) references beone_subkon_in_header;
alter table beone_subkon_out_detail
    add constraint beone_subkon_out_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_subkon_in_detail
    add constraint beone_subkon_in_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_po_import_detail
    add constraint beone_po_import_detail_beone_po_import_header_purchase_header_id_fk
        foreign key (purchase_header_id) references beone_po_import_header;
alter table beone_po_import_detail
    drop constraint if exists beone_po_import_detail_beone_item_item_id_fk;
alter table beone_purchase_detail
    drop constraint if exists beone_purchase_detail_beone_item_item_id_fk;
alter table beone_sales_detail
    drop constraint if exists beone_sales_detail_beone_item_item_id_fk;
alter table beone_pemakaian_bahan_detail
    drop constraint if exists beone_pemakaian_bahan_detail_beone_item_item_id_fk;
alter table beone_konversi_stok_detail
    drop constraint if exists beone_konversi_stok_detail_beone_gudang_gudang_id_fk;
alter table beone_konversi_stok_detail
    drop constraint if exists beone_konversi_stok_detail_beone_item_item_id_fk;
alter table beone_opname_detail
    drop constraint if exists beone_opname_detail_beone_item_item_id_fk;
alter table beone_transfer_stock_detail
    drop constraint if exists beone_transfer_stock_detail_beone_item_item_id_fk;
alter table beone_transfer_stock_detail
    drop constraint if exists beone_transfer_stock_detail_beone_gudang_gudang_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_gudang_gudang_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_item_item_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_purchase_detail_purchase_detail_id_fk;
alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_gudang_gudang_id_fk;
alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_gudang_gudang_id_fk_2;
alter table beone_mutasi_stok_detail
    drop constraint if exists beone_mutasi_stok_detail_beone_item_item_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_item_type_item_type_id_fk;
alter table beone_import_detail
    drop constraint if exists beone_import_detail_beone_satuan_item_satuan_id_fk;
alter table beone_gudang_detail
    drop constraint if exists beone_gudang_detail_beone_item_item_id_fk;
alter table beone_item
    drop constraint if exists beone_item_beone_item_category_item_category_id_fk;
alter table beone_item
    drop constraint if exists beone_item_beone_item_type_item_type_id_fk;
alter table beone_inventory
    drop constraint if exists beone_inventory_beone_gudang_gudang_id_fk;
alter table beone_inventory
    drop constraint if exists beone_inventory_beone_item_item_id_fk;
alter table beone_subkon_out_header
    drop constraint if exists beone_subkon_out_header_beone_supplier_supplier_id_fk;
alter table beone_subkon_in_header
    drop constraint if exists beone_subkon_in_header_beone_supplier_supplier_id_fk;
alter table beone_po_import_header
    drop constraint if exists beone_po_import_header_beone_supplier_supplier_id_fk;
alter table beone_komposisi
    drop constraint if exists beone_komposisi_beone_item_item_id_fk;
alter table beone_komposisi
    drop constraint if exists beone_komposisi_beone_item_item_id_fk_2;
-- alter table beone_user
--     drop constraint if exists beone_user_beone_roles_user_role_id_fk;
alter table beone_kode_trans
    drop constraint if exists beone_kode_trans_beone_coa_coa_id_fk;
alter table beone_satuan_item
    drop constraint if exists beone_satuan_item_beone_item_item_id_fk;
alter table beone_sales_header
    drop constraint if exists beone_sales_header_beone_customer_customer_id_fk;
alter table beone_purchase_header
    drop constraint if exists beone_purchase_header_beone_supplier_supplier_id_fk;
alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_satuan_item_satuan_id_fk;
alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_gudang_gudang_id_fk;
alter table beone_import_header
    drop constraint if exists beone_import_header_beone_supplier_supplier_id_fk;
alter table beone_konversi_stok_header
    drop constraint if exists beone_konversi_stok_header_beone_item_item_id_fk;
-- alter table beone_export_header
-- 	drop constraint if exists beone_export_header_pk;
alter table beone_po_import_detail
    add constraint beone_po_import_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_purchase_detail
    add constraint beone_purchase_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_sales_detail
    add constraint beone_sales_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_pemakaian_bahan_detail
    add constraint beone_pemakaian_bahan_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_konversi_stok_detail
    add constraint beone_konversi_stok_detail_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_konversi_stok_detail
    add constraint beone_konversi_stok_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_opname_detail
    add constraint beone_opname_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_transfer_stock_detail
    add constraint beone_transfer_stock_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_transfer_stock_detail
    add constraint beone_transfer_stock_detail_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_import_detail
    add constraint beone_import_detail_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_import_detail
    add constraint beone_import_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_import_detail
    add constraint beone_import_detail_beone_po_purchase_detail_purchase_detail_id_fk
        foreign key (purchase_detail_id) references beone_po_purchase_detail;
alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_gudang_gudang_id_fk
        foreign key (gudang_id_tujuan) references beone_gudang;
alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_gudang_gudang_id_fk_2
        foreign key (gudang_id_asal) references beone_gudang;
alter table beone_mutasi_stok_detail
    add constraint beone_mutasi_stok_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_gudang_detail
    add constraint beone_gudang_detail_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_item
    add constraint beone_item_beone_item_category_item_category_id_fk
        foreign key (item_category_id) references beone_item_category;
alter table beone_item
    add constraint beone_item_beone_item_type_item_type_id_fk
        foreign key (item_type_id) references beone_item_type;
alter table beone_inventory
    add constraint beone_inventory_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_inventory
    add constraint beone_inventory_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_subkon_out_header
    add constraint beone_subkon_out_header_beone_supplier_supplier_id_fk
        foreign key (supplier_id) references beone_supplier;
alter table beone_subkon_in_header
    add constraint beone_subkon_in_header_beone_supplier_supplier_id_fk
        foreign key (supplier_id) references beone_supplier;
alter table beone_po_import_header
    add constraint beone_po_import_header_beone_supplier_supplier_id_fk
        foreign key (supplier_id) references beone_supplier;
alter table beone_komposisi
    add constraint beone_komposisi_beone_item_item_id_fk
        foreign key (item_jadi_id) references beone_item;
alter table beone_komposisi
    add constraint beone_komposisi_beone_item_item_id_fk_2
        foreign key (item_baku_id) references beone_item;
-- alter table beone_user
--     add constraint beone_user_beone_roles_user_role_id_fk
--         foreign key (role_id) references beone_roles_user;
alter table beone_kode_trans
    add constraint beone_kode_trans_beone_coa_coa_id_fk
        foreign key (coa_id) references beone_coa;
alter table beone_satuan_item
    add constraint beone_satuan_item_beone_item_item_id_fk
        foreign key (item_id) references beone_item;
alter table beone_sales_header
    add constraint beone_sales_header_beone_customer_customer_id_fk
        foreign key (customer_id) references beone_customer;
alter table beone_purchase_header
    add constraint beone_purchase_header_beone_supplier_supplier_id_fk
        foreign key (supplier_id) references beone_supplier;
alter table beone_konversi_stok_header
    add constraint beone_konversi_stok_header_beone_satuan_item_satuan_id_fkbeone_import_header
        foreign key (satuan_qty) references beone_satuan_item;
alter table beone_konversi_stok_header
    add constraint beone_konversi_stok_header_beone_gudang_gudang_id_fk
        foreign key (gudang_id) references beone_gudang;
alter table beone_import_header
    add constraint beone_import_header_beone_supplier_supplier_id_fk
        foreign key (supplier_id) references beone_supplier;
alter table beone_konversi_stok_header
    add constraint beone_konversi_stok_header_beone_item_item_id_fk
        foreign key (item_id) references beone_item;

alter table beone_export_header
    add if not exists sisabayar double precision default 0 not null;
alter table beone_export_header
    add if not exists terbayar double precision default 0 not null;
alter table beone_import_header
    add if not exists sisabayar double precision default 0 not null;
alter table beone_import_header
    add if not exists terbayar double precision default 0 not null;
alter table beone_import_header
    add if not exists dpp double precision default 0 not null;
alter table beone_import_header
    add if not exists ppn double precision default 0 not null;
alter table beone_import_header
    add if not exists discount double precision default 0 not null;
alter table beone_import_header
    add if not exists grandtotal double precision default 0 not null;
alter table beone_import_detail
    add if not exists isppn int default 0 not null;


alter table beone_inventory
    add if not exists invent_trans_detail_id int default 0 not null;

-- alter table beone_export_header
-- 	add constraint beone_export_header_pk
-- 		primary key (export_header_id);
