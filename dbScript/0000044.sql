ALTER TABLE beone_hutang_header
    ADD COLUMN IF NOT EXISTS cek_bg_no VARCHAR (200);
ALTER TABLE beone_hutang_header
    ADD COLUMN IF NOT EXISTS setor_ke_coa_id INTEGER;
ALTER TABLE beone_hutang_header
    ADD COLUMN IF NOT EXISTS bank VARCHAR (20);
ALTER TABLE beone_hutang_header
    ADD COLUMN IF NOT EXISTS no_rek VARCHAR (20);
ALTER TABLE beone_hutang_header
    ADD COLUMN IF NOT EXISTS bentuk_dana VARCHAR (20) NOT NULL;

ALTER TABLE beone_piutang_header
    ADD COLUMN IF NOT EXISTS cek_bg_no VARCHAR (200);
ALTER TABLE beone_piutang_header
    ADD COLUMN IF NOT EXISTS setor_ke_coa_id INTEGER;
ALTER TABLE beone_piutang_header
    ADD COLUMN IF NOT EXISTS bank VARCHAR (20);
ALTER TABLE beone_piutang_header
    ADD COLUMN IF NOT EXISTS no_rek VARCHAR (20);
ALTER TABLE beone_piutang_header
    ADD COLUMN IF NOT EXISTS bentuk_dana VARCHAR (20) NOT NULL;

alter table beone_export_header
    add if not exists bc_nama_vendor varchar(500),
    add if not exists bc_nama_pemilik varchar(500),
    add if not exists bc_nama_penerima_barang varchar(500),
    add if not exists bc_nama_pengangkut varchar(500),
    add if not exists bc_nama_pengirim varchar(500),
    add if not exists bc_kurs double precision default 1 not null,
    add if not exists bc_header_id integer,
    add if not exists discount double precision;

alter table beone_export_header
    add if not exists ppn double precision not null default 0;

alter table beone_export_header
    add if not exists dpp double precision not null default 0;

ALTER TABLE beone_export_header
    ADD IF NOT EXISTS bc_no_aju VARCHAR(500);
alter table beone_export_header
    add column if not exists bc_header_id integer;
alter table beone_export_header
    add if not exists grandtotal_sys double precision not null default 0;
alter table beone_export_header
    add if not exists dpp_sys double precision not null default 0;
alter table beone_export_header
    add if not exists ppn_sys double precision not null default 0;

alter table beone_export_detail
    add column if not exists bc_barang_id integer;
alter table beone_export_detail
    add if not exists amount double precision not null default 0;
alter table beone_export_detail
    add if not exists amount_sys double precision not null default 0;
alter table beone_export_detail
    add if not exists amount_ppn double precision not null default 0;
alter table beone_export_detail
    add if not exists amount_ppn_sys double precision not null default 0;
alter table beone_export_detail
    alter column amount set not null;
alter table beone_export_detail
    alter column price set not null;
alter table beone_export_detail
    alter column qty set not null;
