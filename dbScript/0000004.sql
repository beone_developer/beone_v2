ALTER TABLE beone_konversi_stok_detail
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 1;
ALTER TABLE beone_inventory
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 1;
ALTER TABLE beone_export_detail
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 1;

DROP FUNCTION IF EXISTS get_stok(integer, integer, date, varchar);
-- CREATE PROCEDURE GET STOK
CREATE OR REPLACE FUNCTION get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date,
                                    p_intvent_trans_no varchar(200))
    RETURNS INTEGER AS
$$
DECLARE
    stok_akhir float;
BEGIN
    select sum(x.stok_akhir) as stok
    into stok_akhir
    from (
             select 'saldo_awal' as type, coalesce(sum(it.saldo_qty), 0) stok_akhir
             from beone_item it
             where it.item_id = p_item_id
               and p_gudang_id = 1 --PATEN 1
             union
             select 'stok' as type, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir
             from beone_inventory i
             where i.item_id = p_item_id
               and i.gudang_id = p_gudang_id
--                and i.trans_date <= p_trans_date
             union
             select 'transaksi' as type, coalesce(sum(i.qty_out) - sum(i.qty_in), 0) stok_akhir
             from beone_inventory i
             where i.item_id = p_item_id
--                and i.gudang_id = p_gudang_id
               and i.intvent_trans_no = p_intvent_trans_no) x;
    RETURN stok_akhir;
END;
$$ LANGUAGE plpgsql;


DROP PROCEDURE IF EXISTS SET_STOK(integer, integer, date,
                                    varchar, varchar, float, float);
-- CREATE PROCEDURE SET STOK
CREATE OR REPLACE PROCEDURE set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date,
                                     p_intvent_trans_no varchar(200), p_keterangan varchar(200), p_qty_in float,
                                     p_qty_out float)
    LANGUAGE plpgsql AS
$$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
BEGIN
    SELECT get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) INTO v_stok_akhir;
    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in    = p_qty_in,
                qty_out   = p_qty_out,
                gudang_id = p_gudang_id
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id;
        END IF;

    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, trans_date, keterangan, qty_in, qty_out)
        SELECT p_intvent_trans_no, p_item_id, p_gudang_id, p_trans_date, p_keterangan, p_qty_in, p_qty_out;
    END IF;
END
$$;

-- CREATE TRIGGER beone_konversi_stok_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_konversi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty_real);

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_konversi_stok_detail on beone_konversi_stok_detail;

CREATE TRIGGER trg_create_beone_konversi_stok_detail
    BEFORE INSERT OR UPDATE
    ON beone_konversi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_konversi_stok_detail();

-- CREATE TRIGGER beone_konversi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_konversi_stok_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0);

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_konversi_stok_detail on beone_konversi_stok_detail;

CREATE TRIGGER trg_delete_beone_konversi_stok_detail
    BEFORE DELETE
    ON beone_konversi_stok_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_konversi_stok_detail();

-- CREATE TRIGGER beone_konversi_stok_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    call set_stok(NEW.item_id, 0, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_konversi_stok_header on beone_konversi_stok_header;

CREATE TRIGGER trg_create_beone_konversi_stok_header
    BEFORE INSERT OR UPDATE
    ON beone_konversi_stok_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_konversi_stok_header();

-- CREATE TRIGGER beone_konversi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    call set_stok(OLD.item_id, 0, v_trans_date, v_trans_no, v_keterangan, 0, 0);
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_konversi_stok_header on beone_konversi_stok_header;

CREATE TRIGGER trg_delete_beone_konversi_stok_header
    BEFORE DELETE
    ON beone_konversi_stok_header
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_konversi_stok_header();
