<?php
Route::group(['middleware' => ['web', 'revalidate']], function () {
    Route::get('login', 'Admin\Auth\ALoginController@index')->name('login');
    Route::get('logout', 'Admin\Auth\ALoginController@logout')->name('logout');
    Route::post('login', 'Admin\Auth\ALoginController@authenticate');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'Admin\Dashboard\ADashboardController@index')->name('home');
        Route::group(['prefix' => 'tools'], function () {
            Route::get('outstanding_po', 'Admin\Tools\Select2@outstandingPO')->name('tools.s2.outstanding_po');
            Route::get('item', 'Admin\Tools\Select2@item')->name('tools.s2.item');
            Route::get('item_type', 'Admin\Tools\Select2@itemType')->name('tools.s2.item_type');
            Route::get('satuan_item', 'Admin\Tools\Select2@satuanItem')->name('tools.s2.satuan_item');
            Route::get('customer', 'Admin\Tools\Select2@searchCustomer')->name('tools.s2.customer');
            Route::get('outsanding_so', 'Admin\Tools\Select2@searchSalesOrder')->name('tools.s2.outsanding_so');
            Route::get('sub_kon_out', 'Admin\Tools\Select2@searchSubkonOut')->name('tools.s2.sub_kon_out');

            Route::get('coa', 'Admin\Tools\Select2@searchCoa')->name('tools.s2.coa');
            Route::get('coa_all', 'Admin\Tools\Select2@searchCoaAll')->name('tools.s2.coa_all');
            Route::get('coa_kas_bank', 'Admin\Tools\Select2@searchCoaKasBank')->name('tools.s2.coa_kas_bank');
        });
        Route::group(['prefix' => 'sistem'], function () {
            Route::group(['prefix' => 'rekalkulasi'], function () {
                Route::get('stok', 'Admin\Sistem\ASistemRekalkulasiController@indexStokHPP')->name('rekal.stok');
                Route::post('stok', 'Admin\Sistem\ASistemRekalkulasiController@postFilterStokHPP')->name('rekal.stok');
                // Route::get('gl', 'Admin\Sistem\ASistemRekalkulasiController@recalcGL');
                Route::get('gl', 'Admin\Sistem\ASistemRekalkulasiController@indexGL')->name('rekal.gl');
                Route::post('gl', 'Admin\Sistem\ASistemRekalkulasiController@postFilterGL')->name('rekal.gl');
            });
            Route::group(['prefix' => 'user'], function () {
                Route::get('del', 'Admin\Sistem\ASistemUserController@deleteData')->name('user.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexDetail')->name('user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemUserController@postDetail')->name('user.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexList')->name('user.list');
                });
            });
            Route::group(['prefix' => 'role'], function () {
                Route::get('s2', 'Admin\Sistem\ASistemRoleController@searchData')->name('s2.role');
                Route::get('del', 'Admin\Sistem\ASistemRoleController@deleteData')->name('role.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleController@indexDetail')->name('role.detail');
                    Route::post('/', 'Admin\Sistem\ASistemRoleController@postDetail')->name('role.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleController@indexList')->name('role.list');
                });
            });
            Route::group(['prefix' => 'role_user'], function () {
                Route::get('s2', 'Admin\Sistem\ASistemRoleUserController@searchData')->name('s2.role_user');
                Route::get('del', 'Admin\Sistem\ASistemRoleUserController@deleteData')->name('role_user.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexDetail')->name('role_user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemRoleUserController@postDetail')->name('role_user.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexList')->name('role_user.list');
                });
            });
        });
        Route::group(['prefix' => 'master'], function () {
            Route::group(['prefix' => 'item'], function () {
                Route::get('s2', 'Admin\Master\AMasterItemController@searchData')->name('s2.item');
                Route::get('s3', 'Admin\Master\AMasterItemController@searchSatuan')->name('s3.satuan_item');
                Route::get('del', 'Admin\Master\AMasterItemController@deleteData')->name('item.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterItemController@indexDetail')->name('item.detail');
                    Route::post('/', 'Admin\Master\AMasterItemController@postDetail')->name('item.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterItemController@indexList')->name('item.list');
                });
            });
            Route::group(['prefix' => 'jenis_item'], function () {
                Route::get('del', 'Admin\Master\AMasterJenisItemController@deleteData')->name('jenis_item.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterJenisItemController@indexDetail')->name('jenis_item.detail');
                    Route::post('/', 'Admin\Master\AMasterJenisItemController@postDetail')->name('jenis_item.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterJenisItemController@indexList')->name('jenis_item.list');
                });
            });
            Route::group(['prefix' => 'kategori_item'], function () {
                Route::get('del', 'Admin\Master\AMasterKategoriItemController@deleteData')->name('kategori_item.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterKategoriItemController@indexDetail')->name('kategori_item.detail');
                    Route::post('/', 'Admin\Master\AMasterKategoriItemController@postDetail')->name('kategori_item.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterKategoriItemController@indexList')->name('kategori_item.list');
                });
            });
            Route::group(['prefix' => 'satuan_item'], function () {
                Route::get('del', 'Admin\Master\AMasterSatuanItemController@deleteData')->name('satuan_item.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterSatuanItemController@indexDetail')->name('satuan_item.detail');
                    Route::post('/', 'Admin\Master\AMasterSatuanItemController@postDetail')->name('satuan_item.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterSatuanItemController@indexList')->name('satuan_item.list');
                });
            });
            Route::group(['prefix' => 'customer'], function () {
                Route::get('s2', 'Admin\Master\AMasterCustomerController@searchData')->name('s2.customer');
                Route::get('del', 'Admin\Master\AMasterCustomerController@deleteData')->name('customer.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterCustomerController@indexDetail')->name('customer.detail');
                    Route::post('/', 'Admin\Master\AMasterCustomerController@postDetail')->name('customer.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterCustomerController@indexList')->name('customer.list');
                });
            });
            Route::group(['prefix' => 'supplier'], function () {
                Route::get('s2', 'Admin\Master\AMasterSupplierController@searchData')->name('s2.supplier');
                Route::get('del', 'Admin\Master\AMasterSupplierController@deleteData')->name('supplier.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterSupplierController@indexDetail')->name('supplier.detail');
                    Route::post('/', 'Admin\Master\AMasterSupplierController@postDetail')->name('supplier.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterSupplierController@indexList')->name('supplier.list');
                });
            });
            Route::group(['prefix' => 'gudang'], function () {
                Route::get('s2', 'Admin\Master\AMasterGudangController@searchData')->name('s2.gudang');
                Route::get('del', 'Admin\Master\AMasterGudangController@deleteData')->name('gudang.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterGudangController@indexDetail')->name('gudang.detail');
                    Route::post('/', 'Admin\Master\AMasterGudangController@postDetail')->name('gudang.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterGudangController@indexList')->name('gudang.list');
                });
            });
            Route::group(['prefix' => 'coa'], function () {
                Route::get('s2', 'Admin\Master\AMasterCoaController@searchDataTipe')->name('s2.tipe_coa');
//                Route::get('s3', 'Admin\Master\AMasterCoaController@searchCoaKasBank')->name('s2.coa_kas_bank');
//                Route::get('s5', 'Admin\Master\AMasterCoaController@searchCoa')->name('s2.coa');
//                Route::get('s2all', 'Admin\Master\AMasterCoaController@searchCoaAll')->name('s2.coaall');
                Route::get('del', 'Admin\Master\AMasterCoaController@deleteData')->name('coa.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterCoaController@indexDetail')->name('coa.detail');
                    Route::post('/', 'Admin\Master\AMasterCoaController@postDetail')->name('coa.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterCoaController@indexList')->name('coa.list');
                });
            });
            Route::group(['prefix' => 'komposisi'], function () {
                Route::get('del', 'Admin\Master\AMasterKomposisiController@deleteData')->name('komposisi.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMasterKomposisiController@indexDetail')->name('komposisi.detail');
                    Route::post('/', 'Admin\Master\AMasterKomposisiController@postDetail')->name('komposisi.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMasterKomposisiController@indexList')->name('komposisi.list');
                });
            });
        });
        Route::group(['prefix' => 'import'], function () {
            Route::group(['prefix' => 'po_import'], function () {
                Route::get('s2', 'Admin\Import\AImporPoImportController@searchPo')->name('s2.outstanding_po');
                Route::get('del', 'Admin\Import\AImporPoImportController@deleteData')->name('po_import.delete');
                Route::get('print', 'Admin\Import\AImporPoImportController@print')->name('po_import.print');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Import\AImporPoImportController@indexDetail')->name('po_import.detail');
                    Route::post('/', 'Admin\Import\AImporPoImportController@postDetail')->name('po_import.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Import\AImporPoImportController@indexList')->name('po_import.list');
                });
            });
            Route::group(['prefix' => 'receive_import'], function () {
                Route::get('s2currency', 'Admin\Import\AImporReceiveImportController@searchCurrency')->name('s2.currency');
                Route::get('s2gudang', 'Admin\Import\AImporReceiveImportController@searchGudang')->name('s2.gudang');
                Route::get('s2import', 'Admin\Import\AImporReceiveImportController@searchImport')->name('s2.import');
                Route::get('del', 'Admin\Import\AImporReceiveImportController@deleteData')->name('receive_import.delete');
                Route::get('del', 'Admin\Import\AImporReceiveImportController@deleteData')->name('receive_import.delete');
                Route::group(['prefix' => 'outstanding'], function () {
                    Route::get('/', 'Admin\Import\AImporReceiveImportController@indexOutstanding')->name('receive_import.outstanding');
                });
                Route::group(['prefix' => 'terima'], function () {
                    Route::get('/', 'Admin\Import\AImporReceiveImportController@indexDetailTerima')->name('receive_import.terima');
                    Route::post('/', 'Admin\Import\AImporReceiveImportController@postDetailTerima')->name('receive_import.terima');
                });
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Import\AImporReceiveImportController@indexDetail')->name('receive_import.detail');
                    Route::post('/', 'Admin\Import\AImporReceiveImportController@postDetailTerima')->name('receive_import.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Import\AImporReceiveImportController@indexList')->name('receive_import.list');
                });
            });
        });
        Route::group(['prefix' => 'export'], function () {
            Route::group(['prefix' => 'so_export'], function () {
                Route::get('s2', 'Admin\Export\AExportSoExportController@searchSo')->name('s2.outstanding_so');
                Route::get('del', 'Admin\Export\AExportSoExportController@deleteData')->name('so_export.delete');
                Route::get('print', 'Admin\Export\AExportSoExportController@print')->name('so_export.print');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Export\AExportSoExportController@indexDetail')->name('so_export.detail');
                    Route::post('/', 'Admin\Export\AExportSoExportController@postDetail')->name('so_export.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Export\AExportSoExportController@indexList')->name('so_export.list');
                });
            });
            Route::group(['prefix' => 'export'], function () {
                Route::get('s2currency', 'Admin\Import\AImporReceiveImportController@searchCurrency')->name('s2.currency');
                Route::get('s2', 'Admin\Export\AExportExportController@searchPo')->name('s2.outstanding_so');
                Route::get('del', 'Admin\Export\AExportExportController@deleteData')->name('export.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Export\AExportExportController@indexDetail')->name('export.detail');
                    Route::post('/', 'Admin\Export\AExportExportController@postDetail')->name('export.detail');
                });
                Route::group(['prefix' => 'kirim'], function () {
                    Route::get('/', 'Admin\Export\AExportKirimController@indexDetail')->name('export.kirim');
                    Route::post('/', 'Admin\Export\AExportKirimController@postDetailKirim')->name('export.kirim');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Export\AExportExportController@indexList')->name('export.list');
                });
                Route::group(['prefix' => 'outstanding_25'], function () {
                    Route::get('/', 'Admin\Export\AExportOutstanding25Controller@indexOutstanding')->name('export.outstanding_25');
                });
                Route::group(['prefix' => 'outstanding_27'], function () {
                    Route::get('/', 'Admin\Export\AExportOutstanding27Controller@indexOutstanding')->name('export.outstanding_27');
                });
                Route::group(['prefix' => 'outstanding_41'], function () {
                    Route::get('/', 'Admin\Export\AExportOutstanding41Controller@indexOutstanding')->name('export.outstanding_41');
                });
            });
        });
        Route::group(['prefix' => 'inventory'], function () {
            Route::group(['prefix' => 'mutasi_barang'], function () {
                Route::get('del', 'Admin\Inventory\AInventoryMutasiBarangController@deleteData')->name('mutasi_barang.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryMutasiBarangController@indexDetail')->name('mutasi_barang.detail');
                    Route::post('/', 'Admin\Inventory\AInventoryMutasiBarangController@postDetail')->name('mutasi_barang.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryMutasiBarangController@indexList')->name('mutasi_barang.list');
                });
            });
            Route::group(['prefix' => 'stok_opname'], function () {
                Route::get('del', 'Admin\Inventory\AInventoryStokOpnameController@deleteData')->name('stok_opname.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryStokOpnameController@indexDetail')->name('stok_opname.detail');
                    Route::post('/', 'Admin\Inventory\AInventoryStokOpnameController@postDetail')->name('stok_opname.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryStokOpnameController@indexList')->name('stok_opname.list');
                    // Route::get('dt', 'Admin\Inventory\AInventoryStokOpnameController@dataTable')->name('stok_opname.list');
                });
            });
            Route::group(['prefix' => 'usage'], function () {
                Route::get('del', 'Admin\Inventory\AInventoryUsageBarangController@deleteData')->name('usage.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryUsageBarangController@indexDetail')->name('usage.detail');
                    Route::post('/', 'Admin\Inventory\AInventoryUsageBarangController@postDetail')->name('usage.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Inventory\AInventoryUsageBarangController@indexList')->name('usage.list');
                    // Route::get('dt', 'Admin\Inventory\AInventoryUsageBarangController@dataTable')->name('stok_opname.list');
                });
            });
        });
        Route::group(['prefix' => 'produksi'], function () {
            Route::group(['prefix' => 'konversi_stok'], function () {
                Route::get('load-komposisi', 'Admin\Produksi\AProduksiKonversiStokController@loadKomposisi')->name('konversi_stok.load_komposisi');
                Route::get('del', 'Admin\Produksi\AProduksiKonversiStokController@deleteData')->name('konversi_stok.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Produksi\AProduksiKonversiStokController@indexDetail')->name('konversi_stok.detail');
                    Route::post('/', 'Admin\Produksi\AProduksiKonversiStokController@postDetail')->name('konversi_stok.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Produksi\AProduksiKonversiStokController@indexList')->name('konversi_stok.list');
                });
            });
            Route::group(['prefix' => 'bom'], function () {
                Route::get('del', 'Admin\Produksi\AProduksiBOMController@deleteData')->name('bom.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Produksi\AProduksiBOMController@indexDetail')->name('bom.detail');
                    Route::post('/', 'Admin\Produksi\AProduksiBOMController@postDetail')->name('bom.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Produksi\AProduksiBOMController@indexList')->name('bom.list');
                });
            });
        });
        Route::group(['prefix' => 'sub-kontraktor'], function () {
            Route::group(['prefix' => 'out'], function () {
                Route::get('del', 'Admin\SubKontraktor\ASubKontraktorOutController@deleteData')->name('sub_kontraktor.out.delete');
                Route::group(['prefix' => 'outstanding'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorOutController@indexOutstanding')->name('sub_kontraktor.out.outstanding');
                });
                Route::group(['prefix' => 'kirim'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorOutController@indexDetailKirim')->name('sub_kontraktor.out.kirim');
                    Route::post('/', 'Admin\SubKontraktor\ASubKontraktorOutController@postDetailKirim')->name('sub_kontraktor.out.kirim');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorOutController@indexList')->name('sub_kontraktor.out.list');
                });
            });
            Route::group(['prefix' => 'in'], function () {
                Route::get('del', 'Admin\SubKontraktor\ASubKontraktorInController@deleteData')->name('sub_kontraktor.in.delete');
                Route::group(['prefix' => 'outstanding'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorInController@indexOutstanding')->name('sub_kontraktor.in.outstanding');
                });
                Route::group(['prefix' => 'terima'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorInController@indexDetailTerima')->name('sub_kontraktor.in.terima');
                    Route::post('/', 'Admin\SubKontraktor\ASubKontraktorInController@postDetailTerima')->name('sub_kontraktor.in.terima');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\SubKontraktor\ASubKontraktorInController@indexList')->name('sub_kontraktor.in.list');
                });
            });
        });
        Route::group(['prefix' => 'ar'], function () {
            Route::group(['prefix' => 'pelunasan_piutang'], function () {
                Route::get('load-piutang', 'Admin\Ar\AArPelunasanPiutangController@loadPiutang')->name('pelunasan_piutang.load_piutang');
                Route::get('del', 'Admin\Ar\AArPelunasanPiutangController@deleteData')->name('pelunasan_piutang.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Ar\AArPelunasanPiutangController@indexDetail')->name('pelunasan_piutang.detail');
                    Route::post('/', 'Admin\Ar\AArPelunasanPiutangController@postDetail')->name('pelunasan_piutang.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Ar\AArPelunasanPiutangController@indexList')->name('pelunasan_piutang.list');
                });
            });
        });
        Route::group(['prefix' => 'ap'], function () {
            Route::group(['prefix' => 'pelunasan_hutang'], function () {
                Route::get('load-hutang', 'Admin\Ap\AApPelunasanHutangController@loadHutang')->name('pelunasan_hutang.load_hutang');
                Route::get('del', 'Admin\Ap\AApPelunasanHutangController@deleteData')->name('pelunasan_hutang.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Ap\AApPelunasanHutangController@indexDetail')->name('pelunasan_hutang.detail');
                    Route::post('/', 'Admin\Ap\AApPelunasanHutangController@postDetail')->name('pelunasan_hutang.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Ap\AApPelunasanHutangController@indexList')->name('pelunasan_hutang.list');
                });
            });
        });
        Route::group(['prefix' => 'keuangan'], function () {
            Route::group(['prefix' => 'kasbank'], function () {
                Route::get('del', 'Admin\Keuangan\AKasBankController@deleteData')->name('kasbank.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Keuangan\AKasBankController@indexDetail')->name('kasbank.detail');
                    Route::post('/', 'Admin\Keuangan\AKasBankController@postDetail')->name('kasbank.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Keuangan\AKasBankController@indexList')->name('kasbank.list');
                });
            });
            Route::group(['prefix' => 'jurnal_umum'], function () {
                Route::get('del', 'Admin\Keuangan\AJurnalUmumController@deleteData')->name('jurnal_umum.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Keuangan\AJurnalUmumController@indexDetail')->name('jurnal_umum.detail');
                    Route::post('/', 'Admin\Keuangan\AJurnalUmumController@postDetail')->name('jurnal_umum.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Keuangan\AJurnalUmumController@indexList')->name('jurnal_umum.list');
                });
            });
        });
        Route::group(['prefix' => 'olap'], function () {
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexDetail')->name('olap.detail');
                Route::post('/', 'Admin\OLAP\AOLAPController@postDetail')->name('olap.detail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexList')->name('olap.list');
            });
            Route::group(['prefix' => 'print_out'], function () {
                Route::get('data', 'Admin\OLAP\APrintOutController@getData')->name('olap.print_out.data');
                Route::group(['prefix' => 'view'], function () {
                    Route::get('/', 'Admin\OLAP\APrintOutController@index')->name('olap.print_out.view');
                    Route::post('/', 'Admin\OLAP\APrintOutController@index')->name('olap.print_out.view');
                });
            });
        });
        Route::group(['prefix' => 'report'], function () {
            Route::group(['prefix' => 'keuangan-buku-besar'], function () {
                Route::get('/', 'Admin\Report\AKeuanganBukuBesarController@index')->name('report.buku_besar');
                Route::get('print', 'Admin\Report\AKeuanganBukuBesarController@attemptPrint')->name('by.print.report.buku_besar');
            });
            Route::group(['prefix' => 'keuangan-neraca-mutasi'], function () {
                Route::get('/', 'Admin\Report\AKeuanganNeracaMutasiController@index')->name('report.neraca_mutasi');
                Route::get('print', 'Admin\Report\AKeuanganNeracaMutasiController@attemptPrint')->name('by.print.report.neraca_mutasi');
            });
            Route::group(['prefix' => 'keuangan-customer-piutang'], function () {
                Route::get('/', 'Admin\Report\AKeuanganCustomerController@index')->name('report.customer_piutang');
                Route::get('print', 'Admin\Report\AKeuanganCustomerController@attemptPrint')->name('by.print.report.customer_piutang');
            });
            Route::group(['prefix' => 'keuangan-supplier-hutang'], function () {
                Route::get('/', 'Admin\Report\AKeuanganSupplierController@index')->name('report.supplier_hutang');
                Route::get('print', 'Admin\Report\AKeuanganSupplierController@attemptPrint')->name('by.print.report.supplier_hutang');
            });
            Route::group(['prefix' => 'inventory-kartu-stok'], function () {
                Route::get('/', 'Admin\Report\AInventoryKartuStokController@index')->name('report.kartu_stok');
                Route::post('/', 'Admin\Report\AInventoryKartuStokController@postFilter')->name('by.print.report.kartu_stok');
            });
            Route::group(['prefix' => 'pabean-pemasukkan'], function () {
                Route::get('/', 'Admin\Report\APabeanPemasukanController@index')->name('report.pb_pemasukan');
                Route::get('print', 'Admin\Report\APabeanPemasukanController@attemptPrint')->name('by.print.report.pb_pemasukan');
            });
            Route::group(['prefix' => 'pabean-pengeluaran'], function () {
                Route::get('/', 'Admin\Report\APabeanPengeluaranController@index')->name('report.pb_pengeluaran');
                Route::get('print', 'Admin\Report\APabeanPengeluaranController@attemptPrint')->name('by.print.report.pb_pengeluaran');
            });
            Route::group(['prefix' => 'pabean-mutasi-wip'], function () {
                Route::get('/', 'Admin\Report\APabeanMutasiWipController@index')->name('report.pb_mutasi_wip');
                Route::get('print', 'Admin\Report\APabeanMutasiWipController@attemptPrint')->name('by.print.report.pb_mutasi_wip');
            });
            Route::group(['prefix' => 'pabean-mutasi-bahan-baku'], function () {
                Route::get('/', 'Admin\Report\APabeanMutasiBahanBakuController@index')->name('report.pb_mutasi_bahan_baku');
                Route::get('print', 'Admin\Report\APabeanMutasiBahanBakuController@attemptPrint')->name('by.print.report.pb_mutasi_bahan_baku');
            });
            Route::group(['prefix' => 'pabean-mutasi-scrap'], function () {
                Route::get('/', 'Admin\Report\APabeanMutasiScrapController@index')->name('report.pb_mutasi_scrap');
                Route::get('print', 'Admin\Report\APabeanMutasiScrapController@attemptPrint')->name('by.print.report.pb_mutasi_scrap');
            });
            Route::group(['prefix' => 'pabean-mutasi-barang-jadi'], function () {
                Route::get('/', 'Admin\Report\APabeanMutasiBarangJadiController@index')->name('report.pb_mutasi_barang_jadi');
                Route::get('print', 'Admin\Report\APabeanMutasiBarangJadiController@attemptPrint')->name('by.print.report.pb_mutasi_barang_jadi');
            });
            Route::group(['prefix' => 'pabean-mutasi-mesin'], function () {
                Route::get('/', 'Admin\Report\APabeanMutasiMesinController@index')->name('report.pb_mutasi_mesin');
                Route::get('print', 'Admin\Report\APabeanMutasiMesinController@attemptPrint')->name('by.print.report.pb_mutasi_mesin');
            });
        });
    });
});
