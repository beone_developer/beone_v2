<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            2020&nbsp;&copy;&nbsp;<a href="{{url('/')}}" target="_blank" class="kt-link">Beone</a>
        </div>
        <div class="kt-footer__menu">
            <p class="kt-footer__menu-link kt-link" style="color:red">{{$stats}}</p>
        </div>
    </div>
</div>
