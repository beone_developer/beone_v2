<script src="{{ asset('js/app.js') }}"></script>
<script>
    $(function () {
        defForm.init();
    }), defForm = {
        init: function () {
            this.ps_init();
            this.ic_init();
            this.pd_init();
            this.fr_init();
            this.s2_init();
            this.da_init();
            $('form.form-filter').on('submit', function (e) {
                e.preventDefault();
                $dt.draw();
            });
            $('form.form-submit').on('submit', function (e) {
                $(this).find(':submit').attr("disabled", true);
            });
        },
        ps_init: function () {
            $(".form-validatejs").each(function () {
                $(this).parsley({
                    successClass: "was-validate is-valid",
                    errorClass: "was-validate is-invalid",
                    classHandler: function (el) {
                        if (el.$element.hasClass('kt-select2')) {
                            return el.$element.closest(".form-group");
                        }
                        return el.$element.closest(".form-control");
                    },
                    errorsContainer: function (el) {
                        if (el.$element.parents('div.kt-form__group--inline').length) {
                            return el.$element.closest(".kt-form__group--inline");
                        }
                        return el.$element.closest(".form-group");
                    },
                    errorsWrapper: "<span class='invalid-feedback'></span>",
                    errorTemplate: "<span></span>"
                }).on('form:submit', function (formInstance) {
                    $(".input-currency").each(function () {
                        $(this).inputmask('remove');
                    });
                });
            });
        },
        ic_init: function () {
            $(".input-currency").each(function () {
                $digits = 4;
                if ($(this).data('im-digits') !== undefined) {
                    $digits = $(this).data('im-digits');
                }
                $(this).inputmask('currency', {
                    digits: $digits,
                    autoUnmask: true,
                    prefix: '',
                    rightAlign: true
                });
            });
        },
        pd_init: function () {
            $(".prevent-dialog").each(function () {
                $title = 'Yakin Hapus?';
                if ($(this).data('sw-title') !== undefined) {
                    $title = $(this).data('sw-title');
                }
                $(this).on('click', function (event) {
                    event.preventDefault();
                    $link = $(this).data('url');
                    swal.fire({
                        title: $title,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak'
                    }).then(function (result) {
                        if (result.value) {
                            location.href = $link;
                        }
                    });
                })
            });
        },
        fr_init: function () {
            $(".form-repeater").each(function () {
                $default = false;
                $undelete_first = true;
                if ($(this).data('default') !== undefined) {
                    $default = $(this).data('default');
                }
                if ($(this).data('repeater-undelete-first') !== undefined) {
                    $undelete_first = $(this).data('repeater-undelete-first');
                }
                // console.log($default);
                $eval = $(this).data('eval');
                $repeater = $(this).repeater({
                    initEmpty: true,
                    isFirstItemUndeletable: $undelete_first,
                    show: function () {
                        $(this).slideDown();
                        defForm.s2_init();
                        defForm.ic_init();
                        eval($eval);
                    },
                    hide: function (deleteElement) {
                        swal.fire({
                            title: 'Yakin Hapus?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Ya',
                            cancelButtonText: 'Tidak'
                        }).then(function (result) {
                            if (result.value) {
                                $(this).slideUp(deleteElement);
                            }
                        });
                    },
                    afterHide: function (deletedElement) {
                        eval($eval);
                    }
                });
                if ($default) {
                    $repeater.setList($default);
                }
                eval($eval);
            });
        },
        da_init: function () {
            $(".date-picker").each(function () {
                $dateFormat = 'yyyy-mm-dd';
                if ($(this).data('da-locale') !== undefined) {
                    $dateFormat = $(this).data('da-locale');
                }
                $datepicker = $(this).datepicker({
                    rtl: false,
                    autoclose: true,
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    },
                    format: $dateFormat
                });
                $datepicker.inputmask({alias: "datetime", inputFormat: $dateFormat, "clearIncomplete": true});
                if (!$datepicker.val()) {
                    $datepicker.datepicker("setDate", 'now');
                }
            });
            $(".date-range-picker").each(function () {
                $dateFormat = 'yyyy-mm-dd';
                if ($(this).data('da-locale') !== undefined) {
                    $dateFormat = $(this).data('da-locale');
                }
                $(this).daterangepicker({
                    singleDatePicker: false,
                    format: $dateFormat
                });
            });
        },
        s2_init: function () {
            $(".s2-ajax").each(function () {
                $url = $(this).data('s2-url');
                $placeholder = $(this).data('s2-placeholder');
                $selected = false;
                if ($(this).data('s2-selected') !== undefined) {
                    $selected = $(this).data('s2-selected');
                }
                $select2 = $(this).select2({
                    width: '100%',
                    placeholder: $placeholder,
                    allowClear: true,
                    ajax: {
                        url: $url,
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            $active_element = $(this).get(0);
                            $parameters = {
                                q: params.term,
                            };
                            $.map($active_element.attributes, function (v, i) {
                                if (v.name.toLowerCase().indexOf("data-s2-p-") >= 0) {
                                    $name = v.name.toLowerCase().replace("data-s2-p-", "");
                                    $parameters[$name] = v.value;
                                }
                            });
                            return $parameters;
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: formatTemplateResult,
                    templateSelection: formatTemplateSelection
                });
                $select2.on("select2:close", function (e) {
                    if ($(this).parents('div.form-group.was-validate').length) {
                        $(this).parsley().validate();
                    }
                });
                defForm.s2_set($select2, $selected);
                // if ($selected) {
                //     $text = '';
                //     $value = '';
                //     if ($.type($selected) === "string") {
                //         $value = JSON.parse($selected);
                //         $text = JSON.parse($value)[$selectedText];
                //     } else {
                //         $value = $selected[$elementName];
                //         $text = $selected[$selectedText];
                //     }
                //     console.log($text, $value);
                //     $options = new Option($text, $value);
                //     $select2.empty().append($options).val($value).trigger('change');
                // }
            });
            $(".s2-default").each(function () {
                $placeholder = $(this).data('s2-placeholder');
                $(this).select2({
                    placeholder: $placeholder,
                    allowClear: true,
                });
            });
        },
        s2_set: function ($select2, $selected, $child = '', $param = '') {
            if (!$selected) return;
            $elementName = "id";
            $selectedText = 'text';
            if ($select2.data('s2-selected-text') !== undefined) {
                $selectedText = $select2.data('s2-selected-text');
            }
            $text = '';
            $value = '';
            if ($param) {
                $attr = 'data-s2-p-' + $param;
                $select2.attr($attr, $selected);
            }
            if ($child && $.type($selected) === "string") {
                $selected = JSON.parse($selected)[$child];
            }
            if ($.type($selected) === "string") {
                $value = JSON.parse($selected);
                $text = JSON.parse($value)[$selectedText];
            } else {
                $value = $selected[$elementName];
                $text = $selected[$selectedText];
            }
            $options = new Option($text, $value);
            $select2.empty().append($options).val($value).trigger('change');
        },
        dt_createdRow: function (row, data, dataIndex) {
            if (typeof $dt !== 'undefined') {
                $(row).find('td').each(function (index, cell) {
                    $idx = $dt.cell(cell).index().column;
                    $th = $($dt.column($idx).header());
                    if ($th.hasClass('currency')) {
                        $(this).addClass('input-currency');
                    }
                });
            }
        },
    };

    function formatTemplateResult(data) {
        if (data.loading) return data.text;
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository'>";
        // "<div class='select2-result-repository__title'><b>" + data.code + "</b></div>";
        if (data.text) {
            markup += "<div class='select2-result-repository__description'>" + data.text + "</div>";
        }
        markup += "<div class='select2-result-repository__statistics'>" +
            "</div>" +
            "</div></div>";

        return markup;
    }

    function formatTemplateSelection(data) {
        // console.log(data);
        if (data.id === "") {
            return data.text;
        }
        return data.text;
    }
</script>
