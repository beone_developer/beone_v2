<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
     id="kt_aside">
    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="{{url('/')}}">
                <img alt="Logo" class="sidebar-logo" src="{{asset('images/logo.png')}}"/>
            </a>
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                         class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"/>
                            <path
                                d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                                fill="#000000" fill-rule="nonzero"
                                transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
                            <path
                                d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                                fill="#000000" fill-rule="nonzero" opacity="0.3"
                                transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
                        </g>
                    </svg>
                </span>
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"/>
                            <path
                                d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                fill="#000000" fill-rule="nonzero"/>
                            <path
                                d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                fill="#000000" fill-rule="nonzero" opacity="0.3"
                                transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                        </g>
                    </svg>
                </span>
            </button>
        </div>
    </div>

    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
             data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__section">
                    <h4 class="kt-menu__section-text">Modul</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2"></i>
                </li>
                <li class="kt-menu__item {{ Request::is('sistem/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Sistem</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('sistem/rekalkulasi/stok') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('sistem/rekalkulasi/stok')}}"
                                   class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Rekalkulasi Stok</span>
                                </a>
                            </li>
                            <li class="kt-menu__item {{ Request::is('sistem/rekalkulasi/gl') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('sistem/rekalkulasi/gl')}}"
                                   class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Rekalkulasi GL</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('sistem/user/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{route('user.list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">User</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('sistem/role/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{route('role.list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Role</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('sistem/role_user/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{route('role_user.list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Role User</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('master/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-server" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Master</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('master/item/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/item/list')}}" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Item</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('master/customer/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/customer/list')}}" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Customer</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('master/supplier/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/supplier/list')}}" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Supplier</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('master/jenis_item/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/jenis_item/list')}}" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Jenis Item</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('master/satuan_item/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/satuan_item/list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Satuan Item</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('master/gudang/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/gudang/list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Gudang</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('master/coa/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/coa/list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Chart Of Account</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('master/komposisi/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="{{url('master/komposisi/list')}}" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Komposisi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('import/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Import</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('import/po_import/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">PO Import</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  {{ Request::is('import/po_import/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('import/po_import/detail')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item  {{ Request::is('import/po_import/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('import/po_import/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('import/receive_import/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Kedatangan Barang</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        {{--                                        <li class="kt-menu__item kt-menu__item--active" aria-haspopup="true">--}}
                                        {{--                                            <a href="{{url('import/receive_import/detail')}}" class="kt-menu__link ">--}}
                                        {{--                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">--}}
                                        {{--                                                    <span></span>--}}
                                        {{--                                                </i>--}}
                                        {{--                                                <span class="kt-menu__link-text">Baru</span>--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </li>--}}
                                        <li class="kt-menu__item" aria-haspopup="true">
                                            <a href="{{url('import/receive_import/outstanding')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Outstanding</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item" aria-haspopup="true">
                                            <a href="{{route('receive_import.list')}}"
                                               class="kt-menu__link">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('export/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-truck-moving" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Export</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('export/so_export/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">SO Import</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  {{ Request::is('export/so_export/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('export/so_export/detail')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item  {{ Request::is('export/so_export/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('export/so_export/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('export/export/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Export</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  {{ Request::is('export/export/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('export/export/detail')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item  {{ Request::is('export/export/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('export/export/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('sub-kontraktor/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-file-contract" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Sub Kontraktor</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('sub-kontraktor/out/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Out</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  {{ Request::is('sub-kontraktor/out/outstanding') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('sub-kontraktor/out/outstanding')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Outstanding</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('sub-kontraktor/in/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">In</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  {{ Request::is('sub-kontraktor/in/outstanding') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('sub-kontraktor/in/outstanding')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Outstanding</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('inventory/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-boxes" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Inventory</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('inventory/mutasi_barang/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Mutasi Barang</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('inventory/mutasi_barang/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/mutasi_barang/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('inventory/mutasi_barang/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/mutasi_barang/list')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('inventory/stok_opname/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Stok Opname</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('inventory/stok_opname/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/stok_opname/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('inventory/stok_opname/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/stok_opname/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('inventory/usage/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Usage</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('inventory/usage/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/usage/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('inventory/usage/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('inventory/usage/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('produksi/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-industry" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Produksi</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('produksi/bom/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Bill Of Material</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('produksi/bom/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('produksi/bom/detail')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('produksi/bom/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('produksi/bom/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="kt-menu__item  {{ Request::is('produksi/konversi_stok/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Konversi Stok</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('produksi/konversi_stok/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('produksi/konversi_stok/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('produksi/konversi_stok/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('produksi/konversi_stok/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('ar/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-money-bill" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Account Receivable</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('ar/pelunasan_piutang/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Pelunasan Piutang</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('ar/pelunasan_piutang/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('ar/pelunasan_piutang/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('ar/pelunasan_piutang/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('ar/pelunasan_piutang/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('ap/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-money-bill" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Account Payable</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('ap/pelunasan_hutang/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Pelunasan Hutang</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('ap/pelunasan_hutang/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('ap/pelunasan_hutang/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('ap/pelunasan_hutang/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('ap/pelunasan_hutang/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('keuangan/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-money-bill" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Keuangan</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  {{ Request::is('keuangan/kasbank/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Kas & Bank</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('keuangan/kasbank/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('keuangan/kasbank/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('keuangan/kasbank/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('keuangan/kasbank/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="kt-menu__item  {{ Request::is('keuangan/jurnal_umum/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Jurnal Umum</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('keuangan/jurnal_umum/detail') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('keuangan/jurnal_umum/detail')}}"
                                               class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Baru</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item {{ Request::is('keuangan/jurnal_umum/list') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true">
                                            <a href="{{url('keuangan/jurnal_umum/list')}}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">List</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="kt-menu__item {{ Request::is('report/*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                    aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <span class="kt-menu__link-icon">
                            <i class="fa fa-chart-bar" aria-hidden="true"></i>
                        </span>
                        <span class="kt-menu__link-text">Laporan</span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item {{ Request::is('report/keuangan*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Keuangan</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/keuangan-buku-besar') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/keuangan-buku-besar')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Buku Besar</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="kt-menu__item {{ Request::is('report/inventory*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Inventory</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/inventory-kartu-stok') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/inventory-kartu-stok')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Kartu Stok</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="kt-menu__item {{ Request::is('report/pabean*') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="kt-menu__link-text">Beacukai</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-pemasukkan') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-pemasukkan')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Pabean Pemasukkan</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-pengeluaran') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-pengeluaran')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Pabean Pengeluaran</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-mutasi-wip') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-mutasi-wip')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="kt-menu__link-text">Pabean Mutasi WIP</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-mutasi-bahan-baku') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-mutasi-bahan-baku')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span
                                                    class="kt-menu__link-text">Pabean Mutasi Bahan Baku & Penolong</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-mutasi-scrap') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-mutasi-scrap')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span
                                                    class="kt-menu__link-text">Pabean Mutasi Barang Sisa & Scrap</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-mutasi-barang-jadi') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-mutasi-barang-jadi')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span
                                                    class="kt-menu__link-text">Pabean Mutasi Barang Jadi</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item {{ Request::is('report/pabean-mutasi-mesin') ? 'kt-menu__item--submenu kt-menu__item--open kt-menu__item--here' : '' }}"
                                            aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="{{url('report/pabean-mutasi-mesin')}}"
                                               class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span
                                                    class="kt-menu__link-text">Pabean Mutasi Mesin & Peralatan Kantor</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>
