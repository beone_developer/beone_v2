<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                {{$title}}
            </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                @foreach(request()->segments() as $url_segment)
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{url()->current()}}" class="kt-subheader__breadcrumbs-link">
                        {{ucwords(str_replace("-"," ",str_replace("_"," ",$url_segment)))}}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
