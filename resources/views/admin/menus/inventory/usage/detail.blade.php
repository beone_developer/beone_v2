@extends('admin.layouts.default')
@section('title', $title='Usage')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="usage_header_id"
                                       value="{{$detail->usage_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Usage No:</label>
                                        <input type="text" name="trans_no" class="form-control"
                                               placeholder="Trans No"
                                               value="{{old('trans_no',(isset($detail)? $detail->trans_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="trans_date"
                                                   value="{{old('trans_date',(isset($detail)? $detail->trans_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="beone_gudang"
                                                data-s2-url="{{url('master/gudang/s2')}}"
                                                data-s2-selected="{{Helper::encode(old('beone_gudang', (isset($detail)? $detail->beone_usage_details->first()->beone_gudang : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Gudang">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <input type="text" name="keterangan" class="form-control"
                                               placeholder="Keterangan"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_usage_details" class="col-lg-12">
                                        @php
                                            $list = collect(old('beone_usage_details', (isset($detail)? $detail->beone_usage_details : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                @isset($val['usage_detail_id'])
                                                    <input type="hidden" name="usage_detail_id"
                                                           class="form-control"
                                                           value="{{$val['usage_detail_id']}}">
                                                @endisset
                                                <div class="col-md-8">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{route('tools.s2.item')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_item'])}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                data-s2-selected="{{Helper::encode($val['beone_satuan_item'])}}"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Pemasukan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_in"
                                                                   value="{{$val['qty_in']}}"
                                                                   class="form-control qty_in input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Pengeluaran:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_out"
                                                                   value="{{$val['qty_out']}}"
                                                                   class="form-control qty_out input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="usage_header_id"
                                                       class="form-control">
                                                <div class="col-md-8">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{route('tools.s2.item')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Pemasukan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_in"
                                                                   class="form-control qty_in input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Pengeluaran:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_out"
                                                                   class="form-control qty_out input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        <!-- Delete -->
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('select.beone_item').unbind('change');
                $('select.beone_item').bind('change', curForm.change_row);

                $('input.qty_in').unbind('change');
                $('input.qty_in').bind('change', function(){
                    $parent = $(this).closest('.form-detail-menu');
                    $qty_out = $parent.find('input.qty_out');
                    if($(this).val()>0){
                        $qty_out.val(0);
                    }
                });

                $('input.qty_out').unbind('change');
                $('input.qty_out').bind('change', function(){
                    $parent = $(this).closest('.form-detail-menu');
                    $qty_in = $parent.find('input.qty_in');
                    if($(this).val()>0){
                        $qty_in.val(0);
                    }
                });
                curForm.init();
            },
            change_row: function () {
                $parent = $(this).closest('.form-detail-menu');
                $beone_item = $parent.find('select.beone_item');
                $beone_satuan_item = $parent.find('select.beone_satuan_item');
                // console.log($qty_in.val());
                // console.log($qty_out.val());
                // $beone_satuan_item.val('');
                // $beone_satuan_item.trigger('change');
                // $beone_satuan_item.attr('data-s2-p-beone_item', $beone_item.val());
                defForm.s2_set($beone_satuan_item, $beone_item.val(), 'satuan_def', 'beone_item');
            },
            calc_subtotal: function () {
                $parent = $(this).closest('.form-detail-menu');
                // $qty = $parent.find('input.qty');
                // $price = $parent.find('input.price');
                // $amount = $parent.find('input.amount');
                // $amount.val($qty.val() * $price.val());
                // curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotal = parseFloat(0);
                $(".form-group.form-detail-menu").each(function () {
                    $amount = $(this).find('.amount');
                    // console.log('amount:' + $amount.val());
                    $grandTotal += parseFloat($amount.val()) || 0;
                    // console.log('grandTotal:' + $grandTotal);
                });
                $('#grand_total').val($grandTotal);
            }
        };
    </script>
@endpush
