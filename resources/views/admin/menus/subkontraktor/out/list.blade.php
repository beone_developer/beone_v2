@extends('admin.layouts.default')
@section('title', $title='List Subkontraktor Out')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        @include('admin.includes.filter')
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th>No Pengajuan</th>
                                <th>Kode Pabean</th>
                                <th>Supplier</th>
                                <th>Currency</th>
                                <th class="currency">DPP</th>
                                <th class="currency">PPN</th>
                                <th class="currency">GrandTotal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'sub_kontraktor_out_header_id', name: 'sub_kontraktor_out_header_id'},
                        {data: 'trans_date', name: 'trans_date'},
                        {data: 'bc_no_aju', name: 'bc_no_aju'},
                        {data: 'jenis_bc', name: 'jenis_bc'},
                        {data: 'beone_supplier.nama', name: 'beone_supplier.nama', searchable: false},
                        {data: 'beone_currency.currency_code', name: 'beone_currency.currency_code', searchable: false},
                        {data: 'dpp', name: 'dpp', searchable: false},
                        {data: 'ppn', name: 'ppn', searchable: false},
                        {data: 'grandtotal', name: 'grandtotal', searchable: false},
                        {data: 'action', name: 'action'},
                    ],
                    initComplete: function (settings, json) {
                        $dt.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
                    },
                    drawCallback: function (settings) {
                        defForm.init();
                        $dt.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
                    },
                });
            },
        }
    </script>
@endpush
