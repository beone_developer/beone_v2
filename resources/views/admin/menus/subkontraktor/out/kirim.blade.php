@extends('admin.layouts.default')
@section('title', $title='Sub Kontraktor Out')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            <input type="hidden" name="tpb_id" value="{{$detail->ID}}"/>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>No Sub Kontraktor Out:</label>
                                        <input type="text" name="trans_no"
                                               class="form-control"
                                               value="{{old('trans_no',(isset($detail->trans_no)? $detail->trans_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="trans_date"
                                                   value="{{old('trans_date',(isset($detail)? $detail->trans_date : ''))}}"
                                                   placeholder="Select date"
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nomor Pengajuan:</label>
                                        <input type="text" name="NOMOR_AJU" class="form-control"
                                               value="{{old('NOMOR_AJU',(isset($detail)? $detail->NOMOR_AJU : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nomor Jaminan:</label>
                                        <input type="text" name="NOMOR_JAMINAN" class="form-control"
                                               value="{{old('NOMOR_JAMINAN',(isset($detail)? $detail->tpb_jaminan->NOMOR_JAMINAN : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nomor BPJ:</label>
                                        <input type="text" name="NOMOR_BPJ" class="form-control"
                                               value="{{old('NOMOR_BPJ',(isset($detail)? $detail->tpb_jaminan->NOMOR_BPJ : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Supplier:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_supplier"
                                                    data-s2-url="{{url('master/supplier/s2')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_supplier', (isset($detail)? $detail->beone_supplier : '')))}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option value=''></opiton>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_gudang"
                                                    data-s2-url="{{url('import/receive_import/s2gudang')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_gudang', (isset($detail)? $detail->beone_gudang : '')))}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <input type="text" name="keterangan" class="form-control"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="tpb_barangs" class="col-lg-12">
                                        @php
                                            $list = collect(old('tpb_barangs', (isset($detail)? $detail->tpb_barangs : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="bc_barang_id" class="form-control"
                                                       value="{{isset($val['ID'])?$val['ID']:$val['bc_barang_id']}}">
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item TPB:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="uraian"
                                                                   value="{{isset($val['URAIAN'])?$val['URAIAN']:$val['uraian']}}"
                                                                   class="form-control"
                                                                   placeholder="Item TPB"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-selected="{{isset($val['beone_item']) ? Helper::encode($val['beone_item']) : ''}}"
                                                                    data-s2-url="{{route('tools.s2.item')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   value="{{isset($val['JUMLAH_SATUAN'])?$val['JUMLAH_SATUAN']:$val['qty']}}"
                                                                   class="form-control input-currency qty"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Price:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="price"
                                                                   value="{{isset($val['HARGA'])?$val['HARGA']:$val['price']}}"
                                                                   class="form-control input-currency price"
                                                                   placeholder="Price">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">PPN:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="checkbox"
                                                                   name="isppn"
                                                                   class="form-control input-currency isppn"
                                                                   placeholder="PPN"
                                                                   value="1"
                                                                   {{ (isset($val['isppn']) ? '1' : '0')== '1' ? 'checked':'' }}
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Subtotal:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_sys"
                                                                   class="form-control amount_sys input-currency"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">
                                                                Subtotal (Custom):</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   class="form-control amount input-currency"
                                                                   value="{{isset($val['amount'])?$val['amount']:0}}"
                                                            >
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">SubtotalPPN:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_ppn_sys"
                                                                   class="form-control amount_ppn_sys input-currency"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3" hidden>
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">
                                                                SubtotalPPN (Custom):</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_ppn"
                                                                   class="form-control amount_ppn input-currency"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                            @if (!$loop->last)
                                                <div class="kt-separator kt-separator--border-dashed"></div>
                                            @endif
                                        @empty

                                        @endforelse
                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--fit" hidden></div>
                                <div class="row" hidden>
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">DPP:</label>
                                            <div class="col-9">
                                                <input id="dpp_sys" class="form-control input-currency"
                                                       name="dpp_sys"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">DPP (Custom):</label>
                                            <div class="col-9">
                                                <input id="dpp" class="form-control input-currency"
                                                       name="dpp"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">PPN:</label>
                                            <div class="col-9">
                                                <input id="ppn_sys" class="form-control input-currency"
                                                       name="ppn_sys"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">PPN (Custom):</label>
                                            <div class="col-9">
                                                <input id="ppn" class="form-control input-currency"
                                                       name="ppn"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">Discount:</label>
                                            <div class="col-9">
                                                <input id="discount" class="form-control input-currency"
                                                       name="discount"
                                                       value="{{old('discount',(isset($detail->discount)? $detail->discount : '0'))}}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">GrandTotal</label>
                                            <div class="col-9">
                                                <input id="grand_total_sys" class="form-control input-currency"
                                                       name="grand_total_sys"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label
                                                class="col-3 col-form-label">GrandTotal (Custom):</label>
                                            <div class="col-9">
                                                <input id="grand_total" class="form-control input-currency"
                                                       name="grand_total"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);

                $('#discount').unbind('change');
                $('#discount').bind('change', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('select.beone_item').unbind('change');
                $('select.beone_item').bind('change', curForm.change_row);

                $('input.isppn').unbind('change');
                $('input.isppn').bind('change', curForm.calc_subtotal);
                $('input.qty').unbind('change');
                $('input.qty').bind('change', curForm.calc_subtotal);
                $('input.price').unbind('change');
                $('input.price').bind('change', curForm.calc_subtotal);

                $('input.amount').unbind('change');
                $('input.amount').bind('change', curForm.calc_subtotal);

                curForm.init();
            },
            change_row: function () {
                $parent = $(this).closest('.form-detail-menu');
                $beone_item = $parent.find('select.beone_item');
                $beone_satuan_item = $parent.find('select.beone_satuan_item');
                // $beone_satuan_item.val('');
                // $beone_satuan_item.trigger('change');
                // $beone_satuan_item.attr('data-s2-p-beone_item', $beone_item.val());
                defForm.s2_set($beone_satuan_item, $beone_item.val(), 'satuan_def', 'beone_item');
            },
            calc_subtotal: function () {
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotal = parseFloat(0);
                $grandTotal_sys = parseFloat(0);
                $ppn = parseFloat(0);
                $ppn_sys = parseFloat(0);
                $dpp = parseFloat(0);
                $dpp_sys = parseFloat(0);
                $discount = parseFloat($('#discount').val()) || 0;

                $(".form-group.form-detail-menu").each(function () {
                    $price = $(this).find('input.price');
                    $isppn = $(this).find('input.isppn');
                    $qty = $(this).find('input.qty');
                    $amount = $(this).find('input.amount');
                    $amount_sys = $(this).find('input.amount_sys');
                    $amount_ppn = $(this).find('input.amount_ppn');
                    $amount_ppn_sys = $(this).find('input.amount_ppn_sys');

                    $ppnpersen = ($isppn.is(":checked") ? 10 : 0);
                    $multiplier = ((100 + $ppnpersen) / 100) || 1;
                    $qty = parseFloat($qty.val()) || 0;
                    $price = parseFloat($price.val()) || 0;

                    $amount_sys.val($qty * $price);

                    $amount_total = parseFloat($amount_sys.val()) || 0;
                    if ($amount.val() > 0) {
                        $amount_total = parseFloat($amount.val()) || 0;
                    }

                    $amount_ppn.val($amount_total * $multiplier);
                    $amount_ppn_sys.val($amount_sys.val() * $multiplier);

                    $dpp += parseFloat($amount_total) || 0;
                    $dpp_sys += parseFloat($amount_sys.val()) || 0;

                    $ppn += parseFloat($amount_ppn.val() - $amount_total) || 0;
                    $ppn_sys += parseFloat($amount_ppn_sys.val() - $amount_sys.val()) || 0;
                });
                $grandTotal = ($dpp - $discount) + $ppn;
                $grandTotal_sys = ($dpp_sys - $discount) + $ppn_sys;

                $('#dpp').val($dpp);
                $('#dpp_sys').val($dpp_sys);

                $('#ppn').val($ppn);
                $('#ppn_sys').val($ppn_sys);

                $('#grand_total').val($grandTotal);
                $('#grand_total_sys').val($grandTotal_sys);
            }
        };
    </script>
@endpush
