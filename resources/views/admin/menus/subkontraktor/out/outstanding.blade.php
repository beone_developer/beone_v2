@extends('admin.layouts.default')
@section('title', $title='Outstanding Sub Kontraktor Out')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th>No Pengajuan</th>
                                <th>No Daftar</th>
                                <th>No Jaminan</th>
                                <th>No Bpj</th>
                                <th>Kode Pabean</th>
                                <th class="currency">Berat</th>
                                <th>Currency</th>
                                <th class="currency">Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: "{{url()->current()}}",
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'ID', name: 'ID'},
                        {data: 'TANGGAL_AJU', name: 'TANGGAL_AJU'},
                        {data: 'NOMOR_AJU', name: 'NOMOR_AJU'},
                        {data: 'NOMOR_DAFTAR', name: 'NOMOR_DAFTAR'},
                        {data: 'tpb_jaminan.NOMOR_JAMINAN', name: 'tpb_jaminan.NOMOR_JAMINAN'},
                        {data: 'tpb_jaminan.NOMOR_BPJ', name: 'tpb_jaminan.NOMOR_BPJ'},
                        {data: 'JENIS', name: 'JENIS'},
                        {data: 'BRUTO', name: 'BRUTO'},
                        {data: 'CURRENCY', name: 'CURRENCY'},
                        {data: 'AMOUNT', name: 'AMOUNT'},
                        {data: 'action', name: 'action'},
                    ],
                    initComplete: function (settings, json) {
                        $dt.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
                    },
                    drawCallback: function (settings) {
                        defForm.init();
                        $dt.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
                    },
                    order: [[1, "desc"]],
                });
            },
        }
    </script>
@endpush
