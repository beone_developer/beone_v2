@extends('admin.layouts.default')
@section('title', $title='Sistem User')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->user_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nama:</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama"
                                       value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" name="username" class="form-control" placeholder="Username"
                                       value="{{old('username',(isset($detail)? $detail->username : ''))}}"
                                       required>
                            </div>
                            @php
                                $list = collect(old('password', (isset($detail)? $detail->password : [])))->toArray();
                            @endphp
                            @forelse($list as $i => $val)
                                <div class="form-group">
                                    <label>Password:</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Kosongi jika tidak ingin mengubah password"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label>Validasi Password:</label>
                                    <input type="password" name="validasi_password" id="validasi_password" class="form-control"
                                           placeholder="Kosongi jika tidak ingin mengubah password"
                                           value="">
                                    <span id='message'></span>
                                </div>
                            @empty
                                <div class="form-group">
                                    <label>Password:</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group">
                                    <label>Validasi Password:</label>
                                    <input type="password" name="validasi_password" id="validasi_password" class="form-control"
                                           placeholder="Validasi Password"
                                           value=""
                                           required>
                                    <span id='message'></span>
                                </div>
                            @endforelse
                            <div class="form-group">
                                <label>Role</label>
                                <div class="input-group">
                                    <select class="form-control kt-select2 s2-ajax"
                                            name="beone_roles_user"
                                            data-s2-url="{{route('s2.role_user')}}"
                                            data-s2-selected="{{Helper::encode(old('beone_roles_users', (isset($detail)? $detail->beone_roles_users : '')))}}"
                                            data-s2-placeholder="Cari Role"
                                            required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#password, #validasi_password').on('keyup', function () {
            if ($('#password').val() == $('#validasi_password').val()) {
                $('#message').html('Password Matching').css('color', 'green');
            } else
                $('#message').html('Password Not Matching').css('color', 'red');
        });
    </script>
@endpush
