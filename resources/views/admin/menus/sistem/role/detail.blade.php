@extends('admin.layouts.default')
@section('title', $title='Sistem Role')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                     <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->modul_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Modul:</label>
                                <input type="text" name="modul" class="form-control" placeholder="Modul"
                                       value="{{old('modul',(isset($detail)? $detail->modul : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Url:</label>
                                <input type="text" name="url" class="form-control" placeholder="url"
                                       value="{{old('url',(isset($detail)? $detail->url : ''))}}">
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
