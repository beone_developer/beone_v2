@extends('admin.layouts.default')
@section('title', $title='Sistem Role User')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form id="form" class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->role_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nama Role User:</label>
                                <input type="text" name="nama_role" class="form-control" placeholder="Nama"
                                       value="{{old('nama_role',(isset($detail)? $detail->nama_role : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" name="keterangan" class="form-control" placeholder="Keterangan"
                                       value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="d-flex bd-highlight">
                                        <div class="p-2 flex-grow-1 bd-highlight">All Moduls:</div>
                                        <div class="p-2 bd-highlight">
                                            <button type="button" id="add_all" class="btn btn-primary">Add All</button>
                                        </div>
                                    </div>
                                    <div class="box p-3 border border-info">
                                        <div id="all_modul" style="min-height: 700px"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex bd-highlight">
                                        <div class="p-2 flex-grow-1 bd-highlight">Active Moduls:</div>
                                        <div class="p-2 bd-highlight">
                                            <button type="button" id="remove_all" class="btn btn-primary">Remove All
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box p-3 border border-success">
                                        <div id="act_modul" style="min-height: 500px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .fa.k-sprite,
        .fa.k-sprite::before {
            padding-top: 2px;
            margin-top: 2px;
            font-size: 12px;
            line-height: 12px;
            vertical-align: middle;
        }

        .k-group:empty::after {
            content: 'Drop Menu Here';
            color: #333333;
            font-size: 20px;
        }

    </style>
@endpush
@push('script')
    <script>
        $(function () {

            $all_modul = $("#all_modul").kendoTreeView({
                dragAndDrop: true,
                dataSource: @json($all_modul),
                // expand: onExpand,
            }).data("kendoTreeView");
            $act_modul = $("#act_modul").kendoTreeView({
                dragAndDrop: true,
                dataSource: @json($act_modul),
                // expand: onExpand,
            }).data("kendoTreeView");

            $all_modul.expand(".k-item");
            $act_modul.expand(".k-item");

            $('#form').submit(function (e) {
                $data = $act_modul.dataSource.view();
                $data = JSON.stringify($data);
                $('<input>', {
                    type: 'hidden',
                    name: 'beone_moduls',
                    value: $data
                }).appendTo($(this));
                return true;
            });
            $('#remove_all').on('click', function () {
                if (!$act_modul.dataSource.view().length > 0) return;
                $all_modul.append($act_modul.dataSource.view());
                $act_modul.dataSource.data([]);
            });
            $('#add_all').on('click', function () {
                if (!$all_modul.dataSource.view().length > 0) return;
                $act_modul.append($all_modul.dataSource.view());
                $all_modul.dataSource.data([]);
            });

        });
    </script>
@endpush
