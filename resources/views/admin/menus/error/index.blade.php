<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Beone | Login </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>

    {{--    <link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="shortcut icon"/>--}}
    <style>
        .landing-logo {
            width: 415px;
            height: 100px;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v6"
         style="background-image: url({{asset('assets/media/error/bg6.jpg')}});">
        <div class="kt-error_container">
            <div class="kt-error_subtitle kt-font-light">
                <h1>Oops...</h1>
            </div>
            <p class="kt-error_description kt-font-light">
                Looks like something went wrong.<br>
                We're working on it
            </p>
        </div>
    </div>
</div>

<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
