@extends('admin.layouts.default')
@section('title', $title='Form Export')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="export_header_id" value="{{$detail->export_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Invoice No:</label>
                                        <input type="text" name="invoice_no" class="form-control"
                                               placeholder="Invoice No"
                                               value="{{old('invoice_no',(isset($detail)? $detail->invoice_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="invoice_date"
                                                   value="{{old('invoice_date',(isset($detail)? $detail->invoice_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>BC Car</label>
                                    <input type="text" name="car_no" class="form-control"
                                           placeholder="BC Car"
                                           value="{{old('car_no',(isset($detail)? $detail->car_no : ''))}}">
                                </div>
                                <div class="col-lg-4">
                                    <label>Nomor BC</label>
                                    <input type="text" name="bc_no" class="form-control"
                                           placeholder="Nomor BC"
                                           value="{{old('bc_no',(isset($detail)? $detail->bc_no : ''))}}"
                                        {{(isset($detail)? 'disabled="disabled"' : '')}}
                                    />
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 form-group">
                                    <label>Jenis BC:</label>
                                    <select class="form-control kt-select2"
                                            name="jenis_bc"
                                            required
                                            data-s2-placeholder="Jenis BC">
                                        <option
                                            value="30" {{(old('jenis_bc', isset($detail->jenis_bc) ? $detail->jenis_bc : '' ) == 30 ? 'selected':'') }}>
                                            3.0
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Customer:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="beone_receiver"
                                                data-s2-url="{{url('master/customer/s2')}}"
                                                data-s2-selected="{{Helper::encode(old('beone_receiver', (isset($detail)? $detail->beone_receiver : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Customer">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="gudang"
                                                data-s2-url="{{url('master/gudang/s2')}}"
                                                data-s2-selected="{{Helper::encode(old('gudang', (isset($detail)? $detail->gudang : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Gudang">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Mata Uang:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="beone_currency"
                                                data-s2-url="{{url('import/receive_import/s2currency')}}"
                                                data-s2-selected="{{Helper::encode(old('beone_currency', (isset($detail)? $detail->beone_currency : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Mata Uang">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kurs:</label>
                                        <input type="text" name="kurs" class="form-control input-currency"
                                               placeholder="kurs"
                                               value="{{old('kurs',(isset($detail)? $detail->kurs : '1'))}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Amount FOB ($)</label>
                                        <input type="text" name="valas_value" class="form-control input-currency"
                                               placeholder="Amount FOB ($)"
                                               value="{{old('valas_value',(isset($detail)? $detail->valas_value : ''))}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Freight</label>
                                        <input type="text" name="freight" class="form-control input-currency"
                                               placeholder="Freight"
                                               value="{{old('freight',(isset($detail)? $detail->freight : ''))}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tipe Pembayaran</label>
                                        <select class="form-control kt-select2"
                                                name="price_type"
                                                required
                                                data-s2-placeholder="Jenis BC">
                                            <option
                                                value="CFR" {{(old('price_type', isset($detail->price_type) ? $detail->price_type : '' ) == "CFR" ? 'selected':'') }}>
                                                CFR
                                            </option>
                                            <option
                                                value="CIF" {{(old('price_type', isset($detail->price_type) ? $detail->price_type : '' ) == "CIF" ? 'selected':'') }}>
                                                CIF
                                            </option>
                                            <option
                                                value="FOB" {{(old('price_type', isset($detail->price_type) ? $detail->price_type : '' ) == "FOB" ? 'selected':'') }}>
                                                FOB
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Insurance</label>
                                        <input type="text" name="insurance_value" class="form-control input-currency"
                                               placeholder="Insurance"
                                               value="{{old('insurance_value',(isset($detail)? $detail->insurance_value : ''))}}">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="sales_headers" class="col-lg-12">
                                        @php
                                            $list = old('sales_headers', (isset($detail)? $detail->sales_headers : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="sales_header_id" class="form-control">
                                                <div class="col-md-10">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Sales Order:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    data-s2-selected="{{Helper::encode($val['beone_sales_header'])}}"
                                                                    name="beone_sales_header"
                                                                    data-s2-url="{{url('export/so_export/s2')}}"
                                                                    data-s2-placeholder="Cari SO"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        <!-- Delete -->
                                                    </a>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="sales_header_id" class="form-control">
                                                <div class="col-md-10">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Sales Order:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_sales_header"
                                                                    data-s2-url="{{url('export/so_export/s2')}}"
                                                                    data-s2-placeholder="Cari SO"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        <!-- Delete -->
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('input.qty').unbind('change');
                $('input.qty').bind('change', curForm.calc_subtotal);
                $('input.price').unbind('change');
                $('input.price').bind('change', curForm.calc_subtotal);
                curForm.init();
            },
            calc_subtotal: function () {
                $parent = $(this).closest('.form-detail-menu');
                $qty = $parent.find('input.qty');
                $price = $parent.find('input.price');
                $amount = $parent.find('input.amount');
                $amount.val($qty.val() * $price.val());
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotal = parseFloat(0);
                $(".form-group.form-detail-menu").each(function () {
                    $amount = $(this).find('.amount');
                    // console.log('amount:' + $amount.val());
                    $grandTotal += parseFloat($amount.val()) || 0;
                    // console.log('grandTotal:' + $grandTotal);
                });
                $('#grand_total').val($grandTotal);
            }
        };
    </script>
@endpush
