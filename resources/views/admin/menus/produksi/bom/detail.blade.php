@extends('admin.layouts.default')
@section('title', $title='Bill Of Material')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="transfer_stock_id" value="{{$detail->transfer_stock_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>BOM No:</label>
                                        <input type="text" name="transfer_no" class="form-control"
                                               placeholder="BOM No"
                                               value="{{old('transfer_no',(isset($detail)? $detail->transfer_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-9 col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input name="transfer_date" type="text" class="form-control date-picker"
                                                   readonly
                                                   value="{{old('transfer_date',(isset($detail)? $detail->transfer_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis:</label>
                                        <select class="form-control kt-select2"
                                                name="coa_kode_biaya"
                                                required
                                                data-s2-placeholder="Jenis"
                                            {{(isset($detail)? 'disabled="disabled"' : '')}}
                                        >
                                            <option
                                                value="1" {{(old('coa_kode_biaya', isset($detail->coa_kode_biaya) ? $detail->coa_kode_biaya : '' ) == "1" ? 'selected':'') }}>
                                                WIP
                                            </option>
                                            <option
                                                value="2" {{(old('coa_kode_biaya', isset($detail->coa_kode_biaya) ? $detail->coa_kode_biaya : '' ) == "2" ? 'selected':'') }}>
                                                Barang Jadi
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <input type="text" name="keterangan" class="form-control"
                                               placeholder="Keterangan"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gudang Bahan:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="gudang_bahan"
                                                data-s2-url="{{url('master/gudang/s2')}}"
                                                data-s2-selected="{{Helper::encode(old('gudang_bahan', (isset($detail)? $detail->gudang_bahan : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Gudang Bahan"
                                        >
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gudang Produksi:</label>
                                        <select class="form-control kt-select2 s2-ajax"
                                                name="gudang_produksi"
                                                data-s2-url="{{url('master/gudang/s2')}}"
                                                data-s2-selected="{{Helper::encode(old('gudang_produksi', (isset($detail)? $detail->gudang_produksi : '')))}}"
                                                required
                                                data-s2-placeholder="Cari Gudang Produksi"
                                        >
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <p class="font-weight-bold">Dari Item:</p>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_transfer_stock_details_bb" class="col-lg-12">
                                        @php
                                            $list = old('beone_transfer_stock_details_bb', (isset($detail)? $detail->beone_transfer_stock_details_bb : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="transfer_stock_detail_id"
                                                       class="form-control"
                                                       value="{{$val['transfer_stock_detail_id']}}">
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{route('tools.s2.item')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_item'])}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                data-s2-selected="{{Helper::encode($val['beone_satuan_item'])}}"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   value="{{$val['qty']}}"
                                                                   class="form-control input-currency"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="transfer_stock_detail_id"
                                                       class="form-control">
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{route('tools.s2.item')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   class="form-control input-currency"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <p class="font-weight-bold">Jadi Item:</p>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_transfer_stock_details_wip" class="col-lg-12">
                                        @php
                                            $list = old('beone_transfer_stock_details_wip', (isset($detail)? $detail->beone_transfer_stock_details_wip : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_item'])}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                data-s2-selected="{{Helper::encode($val['beone_satuan_item'])}}"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   value="{{$val['qty']}}"
                                                                   class="form-control input-currency"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>% Hasil:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="persen_produksi"
                                                                   value="{{$val['persen_produksi']}}"
                                                                   class="form-control input-currency"
                                                                   placeholder="% Hasil">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                                    name="beone_item"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   class="form-control input-currency"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>% Hasil:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="persen_produksi"
                                                                   class="form-control input-currency"
                                                                   placeholder="% Hasil">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('select.beone_item').unbind('change');
                $('select.beone_item').bind('change', curForm.change_row);

                $('input.jumlah_valas').unbind('change');
                $('input.jumlah_valas').bind('change', curForm.calc_subtotal);
                $('input.kurs').unbind('change');
                $('input.kurs').bind('change', curForm.calc_subtotal);
                curForm.init();
            },
            change_row: function () {
                $parent = $(this).closest('.form-detail-menu');
                $beone_item = $parent.find('select.beone_item');
                $beone_satuan_item = $parent.find('select.beone_satuan_item');
                // $beone_satuan_item.val('');
                // $beone_satuan_item.trigger('change');
                // $beone_satuan_item.attr('data-s2-p-beone_item', $beone_item.val());
                defForm.s2_set($beone_satuan_item, $beone_item.val(), 'satuan_def', 'beone_item');
            },
            calc_subtotal: function () {
                $parent = $(this).closest('.form-detail-menu');
                $valas = $parent.find('input.jumlah_valas');
                $kurs = $parent.find('input.kurs');
                $amount.val($valas.val() * $kurs.val());
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotal = parseFloat(0);
                $(".form-group.form-detail-menu").each(function () {
                    $amount = $(this).find('.jumlah_idr');
                    // console.log('amount:' + $amount.val());
                    $grandTotal += parseFloat($amount.val()) || 0;
                    // console.log('grandTotal:' + $grandTotal);
                });
                $('#grand_total').val($grandTotal);
            }
        };
    </script>
@endpush
