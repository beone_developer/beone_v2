@extends('admin.layouts.default')
@section('title', $title='Konversi Stock')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="konversi_stok_header_id"
                                       value="{{$detail->konversi_stok_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Konversi No:</label>
                                        <input type="text" name="transfer_no" class="form-control"
                                               placeholder="Konversi No"
                                               value="{{old('konversi_stok_no',(isset($detail)? $detail->konversi_stok_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-9 col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="konversi_stok_date"
                                                   readonly
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Item:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax beone_item"
                                                    id="beone_item"
                                                    name="beone_item"
                                                    data-s2-url="{{url('master/item/s2')}}"
                                                    data-s2-placeholder="Cari Item"
                                                    data-s2-selected="{{Helper::encode(old('beone_item', (isset($detail)? $detail->beone_item : '')))}}"
                                                    required
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            >
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Satuan:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                    id="beone_satuan_item"
                                                    name="beone_satuan_item"
                                                    data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                    data-s2-placeholder="Cari Satuan"
                                                    data-s2-selected="{{Helper::encode(old('beone_satuan_item', (isset($detail)? $detail->beone_satuan_item : '')))}}"
                                                    required
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            >
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_gudang"
                                                    data-s2-url="{{url('master/gudang/s2')}}"
                                                    data-s2-placeholder="Cari Gudang"
                                                    data-s2-selected="{{Helper::encode(old('beone_gudang', (isset($detail)? $detail->beone_gudang : '')))}}"
                                                    required
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            >
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Qty:</label>
                                        <input type="text" name="qty" class="form-control input-currency"
                                               id="qty"
                                               placeholder="Qty"
                                               value="{{old('qty',(isset($detail)? $detail->qty : '0'))}}"
                                               data-parsley-min="1"
                                               required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Real Qty:</label>
                                        <input type="text" name="qty_real" class="form-control input-currency"
                                               id="qty_real"
                                               placeholder="Real Qty"
                                               value="{{old('qty',(isset($detail)? $detail->qty_real : '0'))}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <p class="font-weight-bold">Dari Item:</p>
                            <div id="form-repeater">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_konversi_stok_details" id="table_list"
                                         class="col-lg-12">
                                        @php
                                            $list = collect(old('beone_konversi_stok_details', (isset($detail)? $detail->beone_konversi_stok_details : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="item_id"
                                                       value="{{$val['item_id']}}"/>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item Baku:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="item_nama"
                                                                   class="form-control item_baku_text beone_item"
                                                                   placeholder="Bahan Baku"
                                                                   value="{{$val['item_nama']}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                data-s2-selected="{{Helper::encode($val['beone_satuan_item'])}}"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Base Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   value="{{$val['qty']}}"
                                                                   class="form-control input-currency bb_qty_item_baku"
                                                                   placeholder="Base Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Allocation
                                                                Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_allocation"
                                                                   value="{{$val['qty_allocation']}}"
                                                                   class="form-control input-currency bb_qty_allocation"
                                                                   readonly
                                                                   placeholder="Allocation Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Real Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_real"
                                                                   value="{{$val['qty_real']}}"
                                                                   class="form-control input-currency bb_qty_real"
                                                                   placeholder="Real Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="item_id"/>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item Baku:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="item_nama"
                                                                   class="form-control item_baku_text beone_item"
                                                                   placeholder="Bahan Baku"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Satuan Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select
                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"
                                                                name="beone_satuan_item"
                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"
                                                                data-s2-placeholder="Cari Item"
                                                                required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Base Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   class="form-control input-currency bb_qty_item_baku"
                                                                   value="0"
                                                                   placeholder="Base Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Allocation
                                                                Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_allocation"
                                                                   class="form-control input-currency bb_qty_allocation"
                                                                   value="0"
                                                                   readonly
                                                                   placeholder="Allocation Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Real Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_real"
                                                                   class="form-control input-currency bb_qty_real"
                                                                   value="0"
                                                                   placeholder="Real Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                $('#beone_item').on('change', function () {
                    defForm.s2_set($('#beone_satuan_item'), $(this).val(), 'satuan_def', 'beone_item');
                });
                $form = $('#form-repeater').repeater({});
                curForm.calc_total();
                $('#beone_item').on('select2:select', function (e) {
                    $data = e.params.data;
                    $.ajax({
                        type: 'GET',
                        url: "{{url('produksi/konversi_stok/load-komposisi')}}",
                        data: $data,
                        success: function (data) {
                            const $res = [];
                            $.each(data, function (index, value) {
                                const $text = '<div data-repeater-item\n' +
                                    '                                                 class="form-group row align-items-center form-detail-menu">\n' +
                                    '                                                <input type="hidden" name="item_id" value="' + value['item_id'] + '"/>\n' +
                                    '                                                <div class="col-md-6">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label>Item Baku:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="item_nama"\n' +
                                    '                                                                   class="form-control item_baku_text"\n' +
                                    '                                                                   placeholder="Bahan Baku"\n' +
                                    '                                                                   readonly\n' +
                                    '                                                                   value="' + value['item_nama'] + '">\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-6">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label>Satuan:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                           <select\n' +
                                    '                                                                class="form-control kt-select2 s2-ajax beone_satuan_item"\n' +
                                    '                                                                name="beone_satuan_item"\n' +
                                    '                                                                data-s2-url="{{route('tools.s2.satuan_item')}}"\n' +
                                    '                                                                data-s2-placeholder="Cari Item"\n' +
                                    '                                                                data-s2-p-beone_item=\'' + value['beone_item_baku']['id'] + '\'"\n' +
                                    '                                                                data-s2-selected=\'' + JSON.stringify(value['beone_item_baku']['satuan_def']) + '\'"\n' +
                                    '                                                                required>\n' +
                                    '                                                            </select>' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-6">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label class="kt-label m-label--single">Base Qty:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="qty"\n' +
                                    '                                                                   class="form-control input-currency bb_qty_item_baku"\n' +
                                    '                                                                   placeholder="Base Qty"\n' +
                                    '                                                                   value="' + value['qty'] + '">\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-6">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label class="kt-label m-label--single">Allocation\n' +
                                    '                                                                Qty:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="qty_allocation"\n' +
                                    '                                                                   class="form-control input-currency bb_qty_allocation"\n' +
                                    '                                                                   value="0"\n' +
                                    '                                                                   readonly\n' +
                                    '                                                                   placeholder="Allocation Qty">\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-6">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label class="kt-label m-label--single">Real Qty:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="qty_real"\n' +
                                    '                                                                   class="form-control input-currency bb_qty_real"\n' +
                                    '                                                                   value="0"\n' +
                                    '                                                                   placeholder="Real Qty">\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                            </div>';
                                $res.push($text);
                            });
                            $('#table_list').html($res.join(""));

                            $form = $('#form-repeater').repeater({
                                // isFirstItemUndeletable: true,
                                // show: function () {
                                //     $(this).slideDown();
                                //     defForm.s2_init();
                                //     defForm.ic_init();
                                //     curForm.init_detail();
                                // },
                            });
                            defForm.s2_init();
                            defForm.ic_init();
                            curForm.init_detail();
                        }
                    });
                });
            },
            init_detail: function () {
                // $('select.beone_item').unbind('change');
                // $('select.beone_item').bind('change', curForm.change_row);

                $('input.bb_qty_item_baku').unbind('change');
                $('input.bb_qty_item_baku').bind('change', curForm.calc_total);
                $('input.bb_qty_allocation').unbind('change');
                $('input.bb_qty_allocation').bind('change', curForm.calc_total);
                $('input.qty_real').unbind('change');
                $('input.qty_real').bind('change', curForm.calc_total);

                $('#qty').unbind('change');
                $('#qty').bind('change', curForm.calc_total);
                $('#qty_real').unbind('change');
                $('#qty_real').bind('change', curForm.calc_total);
            },
            // change_row: function () {
            //     $parent = $(this).closest('.form-detail-menu');
            //     $beone_item = $parent.find('select.beone_item');
            //     $beone_satuan_item = $parent.find('select.beone_satuan_item');
            //     $beone_satuan_item.val('');
            //     $beone_satuan_item.trigger('change');
            //     $beone_satuan_item.attr('data-s2-p-beone_item', $beone_item.val());
            // },
            calc_total: function () {
                $qty = $('#qty');
                $qty_real = $('#qty_real');
                $(".form-group.form-detail-menu").each(function () {
                    $bb_qty_item_baku = $(this).find('input.bb_qty_item_baku');
                    $bb_qty_allocation = $(this).find('input.bb_qty_allocation');
                    $bb_qty_real = $(this).find('input.bb_qty_real');

                    $t_qty_alocation = $bb_qty_item_baku.val() * $qty.val();
                    $t_qty_percent = $qty_real.val() / $qty.val();

                    $bb_qty_allocation.val($t_qty_alocation);
                    $bb_qty_real.val($t_qty_alocation * $t_qty_percent);
                });
            }
        };
    </script>
@endpush
