@extends('admin.layouts.default')
@section('title', $title='List Konversi Stock')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="{{url('produksi/konversi_stok/detail')}}"
                                       class="btn btn-success btn-elevate btn-icon-sm">
                                        <i class="la la-plus"></i>
                                        Buat Baru
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        @include('admin.includes.filter')
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>No Konversi</th>
                                <th>Item Code</th>
                                <th>Item Nama</th>
                                <th>Tanggal</th>
                                <th>Qty</th>
                                <th>Satuan Qty</th>
                                <th>Gudang</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'konversi_stok_no', name: 'konversi_stok_no'},
                        {data: 'beone_item.item_code', name: 'beone_item.item_code'},
                        {data: 'beone_item.nama', name: 'beone_item.nama'},
                        {data: 'konversi_stok_date', name: 'konversi_stok_date'},
                        {data: 'qty', name: 'qty'},
                        {data: 'satuan_qty', name: 'satuan_qty'},
                        {data: 'gudang_id', name: 'gudang_id'},
                        {data: 'action', name: 'action'},
                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[-1, 10, 25, 100], ["All", 10, 25, 100]],
                });
            },
        }
    </script>
@endpush
