@extends('admin.layouts.default')
@section('title', $title='OLAP Preview')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="gridContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                const options = localStorage["grid-options"];
                $("#gridContainer").kendoGrid({
                    // autoBind: !options,
                    toolbar: ["excel", "pdf", "search", {
                        name: "best_fit",
                        text: "Best Fit All"
                    }],
                    dataSource: {
                        data: @json($result),
                        pageSize: 100
                    },
                    // dataSource: {
                    //     type: "odata",
                    //     transport: {
                    //         read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
                    //     },
                    // },
                    excel: {
                        fileName: "Excel.xlsx",
                        filterable: true
                    },
                    selectable: "multiple",
                    scrollable: {
                        virtual: true
                    },
                    filterable: true,
                    columnMenu: true,
                    sortable: true,
                    resizable: true,
                    reorderable: true,
                    groupable: true,
                    pageable: false,
                });
                const grid = $("#grid").data("kendoGrid");
                if (options) {
                    // grid.setOptions(JSON.parse(options));
                }
                window.onbeforeunload = function () {
                    localStorage["grid-options"] = kendo.stringify(grid.getOptions());
                };
                $(".k-grid-best_fit", "#grid").bind("click", function (e) {
                    for (let i = 0; i < grid.columns.length; i++) {
                        grid.autoFitColumn(i);
                    }
                });
            },
        }
    </script>
@endpush
