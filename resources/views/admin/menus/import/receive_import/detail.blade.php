@extends('admin.layouts.default')
@section('title', $title='Receive PO Import')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            <input type="hidden" name="tpb_id" value="{{$detail->ID}}"/>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>No Import:</label>
                                        <input type="text" name="import_no"
                                               class="form-control"
                                               value="{{old('import_no',(isset($detail->import_no)? $detail->import_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="receive_date"
                                                   value="{{old('receive_date',(isset($detail)? $detail->receive_date : ''))}}"
                                                   placeholder="Select date"
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nomor Pengajuan:</label>
                                        <input type="text" name="NOMOR_AJU" class="form-control"
                                               value="{{old('NOMOR_AJU',(isset($detail)? $detail->NOMOR_AJU : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nomor PO:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_purchase_order"
                                                    data-s2-url="{{url('import/po_import/s2')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_purchase_order', (isset($detail)? $detail->beone_purchase_order : '')))}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supplier:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_supplier"
                                                    data-s2-url="{{url('master/supplier/s2')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_supplier', (isset($detail)? $detail->beone_supplier : '')))}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option value=''></opiton>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_gudang"
                                                    data-s2-url="{{url('import/receive_import/s2gudang')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_gudang', (isset($detail)? $detail->beone_gudang : '')))}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Currency:</label>
                                        <div class="input-group">
                                            <select
                                                class="form-control kt-select2 s2-ajax"
                                                name="beone_currency"
                                                data-s2-url="{{url('import/receive_import/s2currency')}}"
                                                data-s2-selected="{{Helper::encode(old('beone_purchase_order', (isset($detail)? $detail->beone_currency : '')))}}"
                                                data-s2-placeholder="Cari Item"
                                                required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Kurs:</label>
                                        <input type="text" name="kurs" class="form-control"
                                               placeholder="Kurs"
                                               value="{{old('kurs',(isset($detail)? ($detail->NDPBM ? $detail->NDPBM : 1 ) : 1))}}"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="tpb_barangs" class="col-lg-12">
                                        @php
                                            $list = collect(old('tpb_barangs', (isset($detail)? $detail->tpb_barangs : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="ID" class="form-control"
                                                       value="{{$val['ID']}}">
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item TPB:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="uraian"
                                                                   value="{{$val['URAIAN']}}"
                                                                   class="form-control"
                                                                   placeholder="Item TPB"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_item"
                                                                    data-s2-selected="{{isset($val['beone_item']) ? Helper::encode($val['beone_item']) : ''}}"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   value="{{$val['JUMLAH_SATUAN']}}"
                                                                   class="form-control input-currency qty"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Price:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="price"
                                                                   value="{{($val['HARGA'])}}"
                                                                   class="form-control input-currency price"
                                                                   placeholder="Price"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">PPN:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="checkbox"
                                                                   name="isppn"
                                                                   class="form-control input-currency isppn"
                                                                   placeholder="PPN"
                                                                   value="1"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Subtotal:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="subtotal"
                                                                   class="form-control input-currency amount"
                                                                   placeholder="Subtotal"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                            @if (!$loop->last)
                                                <div class="kt-separator kt-separator--border-dashed"></div>
                                            @endif
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item TPB:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="uraian"
                                                                   class="form-control"
                                                                   placeholder="Item TPB"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_item"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty"
                                                                   class="form-control input-currency qty"
                                                                   placeholder="Qty">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Price:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="price"
                                                                   class="form-control input-currency price"
                                                                   placeholder="Price"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">PPN:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="checkbox"
                                                                   name="isppn"
                                                                   class="form-control input-currency isppn"
                                                                   placeholder="PPN"
                                                                   value="1"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Subtotal:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="subtotal"
                                                                   class="form-control input-currency amount"
                                                                   placeholder="Subtotal"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--fit"></div>
                                <div class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">DPP:</label>
                                            <div class="col-9">
                                                <input id="dpp_total" class="form-control input-currency"
                                                       name="dpp_total"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">PPN:</label>
                                            <div class="col-9">
                                                <input id="ppn_total" class="form-control input-currency"
                                                       name="ppn_total"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">Discount:</label>
                                            <div class="col-9">
                                                <input id="discount_total" class="form-control input-currency"
                                                       name="discount_total"
                                                       value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">GrandTotal</label>
                                            <div class="col-9">
                                                <input id="grand_total" class="form-control input-currency"
                                                       name="grand_total"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);

                $('#discount_total').unbind('change');
                $('#discount_total').bind('change', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('input.isppn').unbind('change');
                $('input.isppn').bind('change', curForm.calc_subtotal);
                $('input.qty').unbind('change');
                $('input.qty').bind('change', curForm.calc_subtotal);
                $('input.price').unbind('change');
                $('input.price').bind('change', curForm.calc_subtotal);
                curForm.init();
            },
            calc_subtotal: function () {
                $parent = $(this).closest('.form-detail-menu');
                $isppn = $parent.find('input.isppn');
                $qty = $parent.find('input.qty');
                $price = $parent.find('input.price');
                $amount = $parent.find('input.amount');
                // $dpp = $qty.val() * $price.val();
                //
                // $ppnpersen = ($isppn.is(":checked") ? 10 : 0);
                // $multiplier = (100 + $ppnpersen) / 100;
                // $multiplier = ($multiplier ? $multiplier : 1);
                //
                // $subTotal = $dpp * $multiplier;
                // $subPPN = parseFloat($subTotal / $ppnpersen) || 0;
                //
                // $ppnTotal += isFinite($subPPN) ? $subPPN : 0;
                // $dppTotal += parseFloat($dpp) || 0;
                // $amount.val($subTotal);
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                console.log('discount_total');
                $ppnTotal = parseFloat(0);
                $grandTotal = parseFloat(0);
                $dppTotal = parseFloat(0);
                $discountTotal = parseFloat($('#discount_total').val());
                $(".form-group.form-detail-menu").each(function () {
                    $price = $(this).find('input.price');
                    $isppn = $(this).find('input.isppn');
                    $qty = $(this).find('input.qty');
                    $amount = $(this).find('input.amount');

                    $dpp = $qty.val() * $price.val();

                    $ppnpersen = ($isppn.is(":checked") ? 10 : 0);
                    $multiplier = (100 + $ppnpersen) / 100;
                    $multiplier = ($multiplier ? $multiplier : 1);

                    $subTotal = $dpp * $multiplier;
                    $subPPN = parseFloat($subTotal / $ppnpersen) || 0;
                    $subPPN = isFinite($subPPN) ? $subPPN : 0;

                    $ppnTotal += $subPPN;
                    $dpp = parseFloat($dpp) || 0;
                    $dppTotal += $dpp;

                    $amount.val($dpp + $subPPN);
                });
                $grandTotal = ($dppTotal - $discountTotal) + $ppnTotal;
                $('#dpp_total').val($dppTotal);
                $('#ppn_total').val($ppnTotal);
                $('#grand_total').val($grandTotal);
            }
        };
    </script>
@endpush
