@php
    $title='Chartt Of Account';
@endphp
@extends('admin.layouts.default')
@section('title', $title)
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->coa_id}}"/>
                            @endisset

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor:</label>
                                        <input type="text" name="nomor" class="form-control" placeholder="Nomor"
                                               value="{{old('nomor',(isset($detail)? $detail->nomor : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nama:</label>
                                        <input type="text" name="nama" class="form-control" placeholder="Nama"
                                               value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Tipe Coa:</label>
                                <select class="form-control s2-ajax" name="tipe_akun"
                                        data-s2-url="{{url('master/coa/s2')}}"
                                        data-s2-selected="{{Helper::encode(old('tipe_coa_id', (isset($detail)? $detail->beone_tipe_coa : '')))}}"
                                        data-s2-placeholder="Cari Tipe Coa"
                                        required>
                                </select>
                            </div>
                            <div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Tipe:</label>
                                        <div class="kt-radio-inline">
                                            <label class="kt-radio">
                                                <input type="radio" name="dk" value="D"
                                                    {{(old('dk',(isset($detail)? $detail->dk : ''))=='D'? 'checked':'')}}
                                                > Debit
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" name="dk" value="K"
                                                    {{(old('dk',(isset($detail)? $detail->dk : ''))=='K'? 'checked':'')}}
                                                > Credit
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
