@extends('admin.layouts.default')
@section('title', $title='Master Item')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->item_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Kode Item:</label>
                                <input type="text" name="item_code" class="form-control" placeholder="Kode"
                                       value="{{old('item_code',(isset($detail)? $detail->item_code : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Nama Item:</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama"
                                       value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <div class="kt-form__label">
                                    <label>Item Type:</label>
                                </div>
                                <div class="kt-form__control">
                                    <select
                                        class="form-control kt-select2 s2-ajax beone_item_type"
                                        data-s2-selected="{{Helper::encode(old('beone_item_type', (isset($detail)? $detail->beone_item_type : '')))}}"
                                        name="beone_item_type"
                                        data-s2-url="{{route('tools.s2.item_type')}}"
                                        data-s2-placeholder="Cari Item"
                                        required>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" name="keterangan" class="form-control" placeholder="Keterangan"
                                       value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                            </div>
                            {{--                            <div class="form-group">--}}
                            {{--                                <label>Keterangan:</label>--}}
                            {{--                                <input type="text" name="keterangan" class="form-control input-currency" placeholder="Keterangan"--}}
                            {{--                                       value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">--}}
                            {{--                            </div>--}}
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="form-repeater" data-eval="">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_satuan_items" class="col-lg-12">
                                        @php
                                            $list = old('beone_satuan_items', (isset($detail)? $detail->beone_satuan_items : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="satuan_id" class="form-control"
                                                       value="{{$val['satuan_id']}}">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kode Satuan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="satuan_code"
                                                                   value="{{$val['satuan_code']}}"
                                                                   class="form-control"
                                                                   required
                                                                {{(isset($detail)? 'disabled="disabled"' : '')}}>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Rasio:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="rasio"
                                                                   value="{{$val['rasio']}}"
                                                                   class="form-control input-currency"
                                                                   required
                                                                {{(isset($detail)? 'disabled="disabled"' : '')}}>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan"
                                                                   value="{{$val['keterangan']}}"
                                                                   class="form-control"
                                                                {{(isset($detail)? 'disabled="disabled"' : '')}}>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                {{--                                                <div class="col-md-2">--}}
                                                {{--                                                    <div class="kt-form__label">--}}
                                                {{--                                                        <label class="kt-label m-label--single"--}}
                                                {{--                                                               style="height: 20px;"></label>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <a href="javascript:;" data-repeater-delete=""--}}
                                                {{--                                                       class="btn-sm btn btn-label-danger btn-bold">--}}
                                                {{--                                                        <i class="la la-trash-o"></i>--}}
                                                {{--                                                    </a>--}}
                                                {{--                                                </div>--}}
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="satuan_id" class="form-control">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kode Satuan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="satuan_code"
                                                                   class="form-control"
                                                                   value="PCS"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Rasio:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="rasio"
                                                                   class="form-control input-currency"
                                                                   data-im-digits="2"
                                                                   value="1"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        <!-- Delete -->
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                @if(!isset($detail))
                                    <div class="form-group form-group-last row">
                                        <label></label>
                                        <div class="col-lg-4">
                                            <a href="javascript:;" data-repeater-create=""
                                               class="btn btn-bold btn-sm btn-label-brand">
                                                <i class="la la-plus"></i> Add
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
