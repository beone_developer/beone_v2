@extends('admin.layouts.default')
@section('title', $title='Master Customer')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->customer_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Kode Customer:</label>
                                <input type="text" name="customer_code" class="form-control" placeholder="Kode"
                                       value="{{old('customer_code',(isset($detail)? $detail->customer_code : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Nama Customer:</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama"
                                       value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Alamat Customer:</label>
                                <input type="text" name="alamat" class="form-control" placeholder="Alamat"
                                       value="{{old('alamat',(isset($detail)? $detail->alamat : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>PPN:</label>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox">
                                        <input
                                            type="checkbox"
                                            name="ppn"
                                            {{old('ppn',(isset($detail)? ($detail->ppn?'checked':'') : ''))}}>Ya
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
