@extends('admin.layouts.default')
@section('title', $title='Master Kategori Item')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="{{url('master/kategori_item/detail')}}"
                                       class="btn btn-success btn-elevate btn-icon-sm">
                                        <i class="la la-plus"></i>
                                        Buat Baru
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kategori Kode</th>
                                <th>Nama</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: "{{url()->current()}}",
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'item_category_id', name: 'item_category_id'},
                        {data: 'item_category_code', name: 'item_category_code'},
                        {data: 'nama', name: 'nama'},
                        {data: 'keterangan', name: 'keterangan'},
                        {data: 'action', name: 'action'},
                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                });
            },
        }
    </script>
@endpush
