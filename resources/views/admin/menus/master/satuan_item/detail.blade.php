@extends('admin.layouts.default')
@section('title', $title='Master Satuan Item')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->satuan_id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Item:</label>
                                <select class="form-control s2-ajax" name="item_id"
                                        data-s2-url="{{url('master/item/s2')}}"
                                        data-s2-selected="{{Helper::encode(old('item_id', (isset($detail)? $detail->beone_item : '')))}}"
                                        data-s2-placeholder="Cari Item"
                                        required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kode Satuan:</label>
                                <input type="text" name="satuan_code" class="form-control" placeholder="Kode Satuan"
                                       value="{{old('satuan_code',(isset($detail)? $detail->satuan_code : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Rasio:</label>
                                <input type="text" name="rasio" class="form-control" placeholder="Rasio"
                                       value="{{old('rasio',(isset($detail)? $detail->rasio : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Keterangan:</label>
                                <input type="text" name="keterangan" class="form-control" placeholder="Keterangan"
                                       value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
