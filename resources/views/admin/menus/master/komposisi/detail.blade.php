@extends('admin.layouts.default')
@section('title', $title='Master Komposisi')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->item_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Item Jadi:</label>
                                        <select class="form-control s2-ajax" name="beone_item_jadi"
                                                data-s2-url="{{url('master/item/s2')}}"
                                                data-s2-placeholder="Cari Item Jadi"
                                                data-s2-selected="{{Helper::encode(old('beone_item_jadi', (isset($detail)? $detail : '')))}}"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="form-repeater">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_komposisi_from" class="col-lg-12">
                                        @php
                                            $list = collect(old('beone_komposisi_from', (isset($detail)? $detail->beone_komposisi_from : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-6">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item Baku:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    data-s2-selected="{{Helper::encode($val['beone_item_baku'])}}"
                                                                    name="beone_item_baku"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_item_baku"
                                                                   value="{{$val['qty_item_baku']}}"
                                                                   class="form-control qty input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Item Baku:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_item_baku"
                                                                    data-s2-url="{{url('master/item/s2')}}"
                                                                    data-s2-placeholder="Cari Item"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Qty:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="qty_item_baku"
                                                                   class="form-control qty input-currency">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
