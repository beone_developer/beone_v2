@extends('admin.layouts.default')
@section('title', $title='Pelunasan')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="piutang_header_id"
                                       value="{{$detail->piutang_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Pelunasan No:</label>
                                        <input type="text" name="trans_no" class="form-control"
                                               placeholder="Pelunasan No"
                                               value="{{old('trans_no',(isset($detail)? $detail->trans_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-9 col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="trans_date"
                                                   readonly
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Customer:</label>
                                        <div class="input-group">
                                            <select id="customer_id" class="form-control kt-select2 s2-ajax"
                                                    name="beone_customer"
                                                    data-s2-url="{{url('master/customer/s2')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_customer', (isset($detail)? $detail->beone_customer : '')))}}"
                                                    data-s2-placeholder="Cari Customer"
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                    required>
                                                <option value=''></opiton>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Bentuk Dana :</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    id="bentuk_dana"
                                                    name="bentuk_dana"
                                                    required>
                                                <option value="tunai"> Tunai</option>
                                                <option value="transfer"> Transfer</option>
                                                <option value="cek"> CEK</option>
                                                <option value="bg"> BG</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 d-none" id="cek_bg">
                                    <div class="kt-form__group--inline">
                                        <div class="form-group">
                                            <label>No. Cek / BG:</label>
                                            <input type="text" name="cek_bg_no" class="form-control"
                                                   placeholder="No. Cek / BG">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-3 d-none" id="akun_bank">
                                    <div class="kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label>Akun Bank</label>
                                        </div>
                                        <div class="kt-form__control">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="setor_ke_coa_id"
                                                    data-s2-url="{{route('tools.s2.coa_kas_bank')}}"
                                                    data-s2-placeholder="Cari Akun">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-3 d-none" id="bank">
                                    <div class="kt-form__group--inline">
                                        <div class="form-group">
                                            <label>Bank:</label>
                                            <input type="text" name="bank" class="form-control"
                                                   placeholder="Bank">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                                <div class="col-md-3 d-none" id="no_rek">
                                    <div class="kt-form__group--inline">
                                        <div class="form-group">
                                            <label>No. Rek:</label>
                                            <input type="text" name="no_rek" class="form-control"
                                                   placeholder="No. Rek">
                                        </div>
                                    </div>
                                    <div class="d-md-none kt-margin-b-10"></div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <!-- <p class="font-weight-bold">Dari Item:</p> -->
                            <div id="form-repeater">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_piutang_details" id="table_list"
                                         class="col-lg-12">
                                        @php
                                            $list = collect(old('beone_piutang_details', (isset($detail)? $detail->beone_piutang_details : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="export_header_id"
                                                       value="{{$val['export_header_id']}}"/>
                                                <input type="hidden" name="piutang_detail_id"
                                                       value="{{$val['piutang_detail_id']}}"/>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Invoice No:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="invoice_no"
                                                                   class="form-control item_baku_text"
                                                                    value="{{$val['invoice_no']}}"
                                                                   placeholder="Invoice No"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Sisa Bayar:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="sisabayar"
                                                                   class="form-control input-currency sisabayar"
                                                                   value="{{old('sisabayar', (isset($val['beone_export_header']['sisabayar'])? $val['beone_export_header']['sisabayar']+ $val['amount'] : $val['sisabayar']+ $val['amount'])) }}"
                                                                   placeholder="Sisa Bayar"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   class="form-control input-currency amount"
                                                                   value="{{$val['amount']}}"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <input type="hidden" name="export_header_id"/>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Invoice No:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="invoice_no"
                                                                   class="form-control item_baku_text"
                                                                   placeholder="Invoice No"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Sisa Bayar:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="sisabayar"
                                                                   class="form-control input-currency sisabayar"
                                                                   placeholder="Sisa Bayar"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   class="form-control input-currency amount"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--fit"></div>
                                        <div class="row">
                                            <div class="col-md-8"></div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label
                                                        class="col-3 col-form-label">GrandTotal:</label>
                                                    <div class="col-9">
                                                        <input id="grandtotal" class="form-control input-currency"
                                                            name="grandtotal"
                                                            value="0"
                                                            readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                $form = $('#form-repeater').repeater({});
                curForm.calc_total();
                $.viewMap = {
                    '0': $([]),
                    'transfer': $('#akun_bank, #bank, #no_rek'),
                    'tunai': $([]),
                    'cek': $('#cek_bg'),
                    'bg': $('#cek_bg')
                };
                $('#bentuk_dana').change(function () {
                    $.each($.viewMap, function () {
                        $(this).addClass('d-none');
                    });
                    $.viewMap[$(this).val()].removeClass('d-none');
                });
                $('#bentuk_dana').trigger('change');
                $('#customer_id').on('select2:select', function (e) {
                    $data = e.params.data;
                    $.ajax({
                        type: 'GET',
                        url: "{{url('ar/pelunasan_piutang/load-piutang')}}",
                        data: $data,
                        success: function (data) {
                            const $res = [];
                            $.each(data, function (index, value) {
                                const $text = '<div data-repeater-item\n' +
                                    '                                                 class="form-group row align-items-center form-detail-menu">\n' +
                                    '                                                <input type="hidden" name="export_header_id" value="' + value['export_header_id'] + '"/>\n' +
                                    '                                                <input type="hidden" name="piutang_detail_id"/>\n' +
                                    '                                                <div class="col-md-4">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label>Invoice No:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="invoice_no"\n' +
                                    '                                                                   class="form-control invoice_no "\n' +
                                    '                                                                   placeholder="Invoice No"\n' +
                                    '                                                                    value="' + value['invoice_no'] + '"\n' +
                                    '                                                                   readonly>\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-4">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label class="kt-label m-label--single">Sisa Bayar:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="sisabayar"\n' +
                                    '                                                                   class="form-control input-currency sisabayar"\n' +
                                    '                                                                   placeholder="Sisa Bayar"\n' +
                                    '                                                                    value="' + value['sisabayar'] + '"\n' +
                                    '                                                                   readonly>\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                                <div class="col-md-4">\n' +
                                    '                                                    <div class="kt-form__group--inline">\n' +
                                    '                                                        <div class="kt-form__label">\n' +
                                    '                                                            <label class="kt-label m-label--single">Amount:</label>\n' +
                                    '                                                        </div>\n' +
                                    '                                                        <div class="kt-form__control">\n' +
                                    '                                                            <input type="text"\n' +
                                    '                                                                   name="amount"\n' +
                                    '                                                                   class="form-control input-currency amount"\n' +
                                    '                                                                   placeholder="Amount">\n' +
                                    '                                                        </div>\n' +
                                    '                                                    </div>\n' +
                                    '                                                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                                    '                                                </div>\n' +
                                    '                                            </div>';
                                $res.push($text);
                            });
                            $('#table_list').html($res.join(""));

                            $form = $('#form-repeater').repeater({
                                // isFirstItemUndeletable: true,
                                // show: function () {
                                //     $(this).slideDown();
                                //     defForm.s2_init();
                                //     defForm.ic_init();
                                //     curForm.init_detail();
                                // },
                            });
                            defForm.s2_init();
                            defForm.ic_init();
                            curForm.init_detail();
                        }
                    });
                });
            },
            init_detail: function () {
                $('input.sisabayar').unbind('change');
                $('input.sisabayar').bind('change', curForm.calc_total);
                $('input.amount').unbind('change');
                $('input.amount').bind('change', curForm.calc_total);
            },
            calc_total: function () {
                $grandTotal = parseFloat(0);
                $(".form-group.form-detail-menu").each(function () {
                    $amount = $(this).find('.amount');
                    $sisabayar = $(this).find('.sisabayar');
                    if (parseFloat($amount.val()) > parseFloat($sisabayar.val())) {
                        $amount.val($sisabayar.val());
                    }
                    $grandTotal += parseFloat($amount.val()) || 0;
                });
                $('#grandtotal').val($grandTotal);
            }
        };
    </script>
@endpush
