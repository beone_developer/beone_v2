@extends('admin.layouts.default')
@section('title', $title='Transaksi Kas Bank')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="voucher_header_id" value="{{$detail->voucher_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor Voucher:</label>
                                        <input type="text" name="purchase_no" class="form-control"
                                               placeholder="Voucher No"
                                               value="{{old('voucher_number',(isset($detail)? $detail->voucher_number : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="voucher_date"
                                                   value="{{old('voucher_date',(isset($detail)? $detail->voucher_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Akun Kas Bank</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_coa"
                                                    data-s2-url="{{route('tools.s2.coa_kas_bank')}}"
                                                    data-s2-selected="{{Helper::encode(old('beone_coa', (isset($detail)? $detail->beone_coa : '')))}}"
                                                    data-s2-placeholder="Cari Kas Bank"
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Keterangan :</label>
                                        <input type="text" name="keterangan" class="form-control"
                                               placeholder="Keterangan"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Tipe Voucher:</label>
                                        <div class="kt-radio-inline">
                                            <label class="kt-radio">
                                                <input type="radio" name="tipe" value="1"
                                                    {{(old('tipe',(isset($detail)? $detail->tipe : ''))==1? 'checked':'')}}
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                > Debit
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" name="tipe" value="0"
                                                    {{(old('tipe',(isset($detail)? $detail->tipe : ''))==0? 'checked':'')}}
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                > Credit
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="form-repeater" data-eval="curForm.init_detail()"
                                 data-repeater-undelete-first="false">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_voucher_details" class="col-lg-12">
                                        @php
                                            $list = collect(old('beone_voucher_details', (isset($detail)? $detail->beone_voucher_details : [])))->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_coa'])}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   class="form-control"
                                                                   value="{{$val['keterangan_detail']}}"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Jumlah
                                                                Valas:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="jumlah_valas"
                                                                   class="form-control jumlah_valas input-currency"
                                                                   value="{{$val['jumlah_valas']}}"
                                                                   placeholder="Jumlah Valas">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   class="form-control kurs input-currency"
                                                                   value="{{$val['kurs']}}"
                                                                   placeholder="Kurs"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Jumlah IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="jumlah_idr"
                                                                   class="form-control jumlah_idr input-currency"
                                                                   value="{{$val['jumlah_idr']}}"
                                                                   readonly
                                                                   placeholder="Jumlah IDR">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__label">
                                                        <label class="kt-label m-label--single"
                                                               style="height: 20px;"></label>
                                                    </div>
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   class="form-control"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Jumlah
                                                                Valas:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="jumlah_valas"
                                                                   class="form-control jumlah_valas input-currency"
                                                                   placeholder="Jumlah Valas">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   class="form-control kurs input-currency"
                                                                   placeholder="Kurs"
                                                                   value="1"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Jumlah IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="jumlah_idr"
                                                                   class="form-control jumlah_idr input-currency"
                                                                   readonly
                                                                   placeholder="Jumlah IDR">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <h3>Grand Total: <span id="grand_total" class="input-currency">0</span></h3>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu').unbind('destroyed');
                $('div.form-detail-menu').bind('destroyed', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('input.jumlah_valas').unbind('change');
                $('input.jumlah_valas').bind('change', curForm.calc_subtotal);
                $('input.kurs').unbind('change');
                $('input.kurs').bind('change', curForm.calc_subtotal);
                curForm.init();
            },
            calc_subtotal: function () {
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotal = parseFloat(0);
                $(".form-group.form-detail-menu").each(function () {
                    $jumlah_valas = $(this).find('input.jumlah_valas');
                    $kurs = $(this).find('input.kurs');
                    $jumlah_idr = $(this).find('input.jumlah_idr');

                    $jumlah_idr.val($jumlah_valas.val() * $kurs.val());
                    $grandTotal += parseFloat($jumlah_idr.val()) || 0;
                });
                $('#grand_total').val($grandTotal);
            }
        };
    </script>
@endpush
