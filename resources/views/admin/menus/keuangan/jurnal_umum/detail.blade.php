@extends('admin.layouts.default')
@section('title', $title='Transaksi Jurnal Umum')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="jurnal_header_id" value="{{$detail->jurnal_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nomor Journal:</label>
                                        <input type="text" name="jurnal_no" class="form-control"
                                               placeholder="Jurnal No"
                                               value="{{old('jurnal_no',(isset($detail)? $detail->jurnal_no : 'Auto Generate'))}}"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="trans_date"
                                                   value="{{old('trans_date',(isset($detail)? $detail->trans_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Keterangan :</label>
                                        <input type="text" name="keterangan" class="form-control"
                                               placeholder="Keterangan"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <p class="font-weight-bold">DEBET:</p>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_jurnal_detail_debets" class="col-lg-12">
                                        @php
                                            $list = old('beone_jurnal_detail_debets', (isset($detail)? $detail->beone_jurnal_detail_debets : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu-debet">
                                                <input type="hidden" name="jurnal_detail_id"
                                                       class="form-control"
                                                       value="{{$val['jurnal_detail_id']}}">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_coa'])}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   value="{{$val['keterangan_detail']}}"
                                                                   class="form-control"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   value="{{$val['amount']}}"
                                                                   class="form-control amount input-currency"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   value="{{$val['kurs']}}"
                                                                   class="form-control kurs input-currency"
                                                                   placeholder="Kurs"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Amount IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_idr"
                                                                   value="{{$val['amount_idr']}}"
                                                                   class="form-control amount_idr input-currency"
                                                                   placeholder="Amount IDR" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu-debet">
                                                <input type="hidden" name="jurnal_detail_id"
                                                       class="form-control">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   class="form-control"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   class="form-control amount input-currency"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   class="form-control kurs input-currency"
                                                                   placeholder="Kurs"
                                                                   value="1"
                                                                   required>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Amount IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_idr"
                                                                   class="form-control amount_idr input-currency"
                                                                   placeholder="Amount IDR" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                            <p class="font-weight-bold">KREDIT:</p>
                            <div class="form-repeater" data-eval="curForm.init_detail()">
                                <div class="form-group form-group-last row">
                                    <div data-repeater-list="beone_jurnal_detail_kredits" class="col-lg-12">
                                        @php
                                            $list = old('beone_jurnal_detail_kredits', (isset($detail)? $detail->beone_jurnal_detail_kredits : []));
                                            $list = collect($list)->toArray();
                                        @endphp
                                        @forelse($list as $i => $val)
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu-kredit">
                                                <input type="hidden" name="jurnal_detail_id"
                                                       class="form-control"
                                                       value="{{$val['jurnal_detail_id']}}">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-selected="{{Helper::encode($val['beone_coa'])}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   value="{{$val['keterangan_detail']}}"
                                                                   class="form-control"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   value="{{$val['amount']}}"
                                                                   class="form-control amount input-currency"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   value="{{$val['kurs']}}"
                                                                   class="form-control kurs input-currency"
                                                                   placeholder="Kurs">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Amount IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_idr"
                                                                   value="{{$val['amount_idr']}}"
                                                                   class="form-control amount_idr input-currency"
                                                                   placeholder="Amount IDR" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @empty
                                            <div data-repeater-item
                                                 class="form-group row align-items-center form-detail-menu-kredit">
                                                <input type="hidden" name="jurnal_detail_id"
                                                       class="form-control">
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Akun:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <select class="form-control kt-select2 s2-ajax"
                                                                    name="beone_coa"
                                                                    data-s2-url="{{route('tools.s2.coa')}}"
                                                                    data-s2-placeholder="Cari Akun"
                                                                    required>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Keterangan:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="keterangan_detail"
                                                                   class="form-control"
                                                                   placeholder="Keterangan">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Amount:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount"
                                                                   class="form-control amount input-currency"
                                                                   placeholder="Amount">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label class="kt-label m-label--single">Kurs:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="kurs"
                                                                   class="form-control kurs input-currency"
                                                                   placeholder="Kurs"
                                                                   value="1">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="kt-form__group--inline">
                                                        <div class="kt-form__label">
                                                            <label>Amount IDR:</label>
                                                        </div>
                                                        <div class="kt-form__control">
                                                            <input type="text"
                                                                   name="amount_idr"
                                                                   class="form-control amount_idr input-currency"
                                                                   placeholder="Amount IDR" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete=""
                                                       class="btn-sm btn btn-label-danger btn-bold">
                                                        <i class="la la-trash-o"></i>
                                                        Delete
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-0 kt-separator kt-separator--border-dashed"></div>
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label></label>
                                    <div class="col-lg-4">
                                        <a href="javascript:;" data-repeater-create=""
                                           class="btn btn-bold btn-sm btn-label-brand">
                                            <i class="la la-plus"></i> Add
                                        </a>
                                    </div>
                                </div>
                                <div class="kt-separator kt-separator--fit"></div>
                                <div class="row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">GrandTotal Debet</label>
                                            <div class="col-9">
                                                <input id="grand_total_debet" class="form-control debet input-currency"
                                                       name="grand_total_debet"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input"
                                                   class="col-3 col-form-label">GrandTotal Kredit</label>
                                            <div class="col-9">
                                                <input id="grand_total_kredit"
                                                       class="form-control kredit input-currency"
                                                       name="grand_total_kredit"
                                                       value="0"
                                                       readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.init_header();
                curForm.calc_grand_total();
            },
            init_header: function () {
                $('div.form-detail-menu-debet').unbind('destroyed');
                $('div.form-detail-menu-debet').bind('destroyed', curForm.calc_grand_total);

                $('div.form-detail-menu-kredit').unbind('destroyed');
                $('div.form-detail-menu-kredit').bind('destroyed', curForm.calc_grand_total);
            },
            init_detail: function () {
                $('input.amount').unbind('change');
                $('input.amount').bind('change', curForm.calc_subtotal);
                $('input.kurs').unbind('change');
                $('input.kurs').bind('change', curForm.calc_subtotal);
                $('input.amount_idr').unbind('change');
                $('input.amount_idr').bind('change', curForm.calc_subtotal);
                curForm.init();
            },
            calc_subtotal: function () {
                curForm.calc_grand_total();
            },
            calc_grand_total: function () {
                $grandTotalDebet = parseFloat(0);
                $grandTotalKredit = parseFloat(0);
                $(".form-group.form-detail-menu-debet").each(function () {
                    $amount = $(this).find('.amount');
                    $kurs = $(this).find('.kurs');
                    $amount_idr = $(this).find('.amount_idr');

                    $amount_idr.val($amount.val() * $kurs.val());
                    console.log($amount_idr.val())
                    $grandTotalDebet += parseFloat($amount_idr.val()) || 0;
                });
                $(".form-group.form-detail-menu-kredit").each(function () {
                    $amount = $(this).find('.amount');
                    $kurs = $(this).find('.kurs');
                    $amount_idr = $(this).find('.amount_idr');

                    $amount_idr.val($amount.val() * $kurs.val());
                    $grandTotalKredit += parseFloat($amount_idr.val()) || 0;
                });
                $('#grand_total_debet').val($grandTotalDebet);
                $('#grand_total_kredit').val($grandTotalKredit);
            }
        };
    </script>
@endpush
