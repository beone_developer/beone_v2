<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Beone | Login </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    {{--    <link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="shortcut icon"/>--}}
    <style>
        .landing-logo {
            width: 415px;
            height: 100px;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
             style="background-color: #3a62e8">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img class="landing-logo" src="{{asset('images/logo.png')}}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">BeOne Login</h3>
                        </div>
                        <form class="kt-form" method="post" action="{{url()->current()}}">
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text">
                                        <ul>
                                            @foreach ($errors->get('messages') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            {{--                            <div class="alert alert-danger" role="alert">--}}
                            {{--                                <div class="alert-text">--}}
                            {{--                                    <h4 class="alert-heading">There are some problems.</h4>--}}
                            {{--                                    <ul>--}}
                            {{--                                        @foreach ($errors->get('status') as $error)--}}
                            {{--                                            <li>{{ $error }}</li>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </ul>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Username" name="username"
                                       autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                            </div>
                            {{--                            <div class="row kt-login__extra">--}}
                            {{--                                <div class="col">--}}
                            {{--                                    <label class="kt-checkbox">--}}
                            {{--                                        <input type="checkbox" name="remember"> Remember me--}}
                            {{--                                        <span></span>--}}
                            {{--                                    </label>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="kt-login__actions">
                                <button id="kt_login_signin_submit" type="submit"
                                        class="btn btn-pill kt-login__btn-primary">Sign In
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
