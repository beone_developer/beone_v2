<style>
    .pdf-center {
        text-align: center;
    }

    .m-0 {
        margin: 0px;
    }

    .mb-5 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .table-full {
        width: 100%
    }

    table, td, th {
        font-family: "verdana", "sans-serif";
        font-size: 12px;
    }

    table {
        border-collapse: collapse;
        border: 1px solid black;
        font-family: arial;
        font-size:11pt;
        width:100%;
    }

    .text-center{
        text-align:center;
    }
    
    .text-right{
        text-align:right;
    }
    
    .text-left{
        text-align:left;
    }
    div.responsive {
        overflow: auto;
        max-height: 100px;
    }
    .cell-border{
      border-collapse: collapse;
      border: 1px solid black;
      padding:5px;
    }
    .left-border{
      border-collapse: collapse;
      border-left: 1px solid black;
    }
    .top-border{
      border-collapse: collapse;
      border-top: 1px solid black;
    }
    .u{
        text-decoration: underline;
    }
    td{
        padding:5px;
    }
</style>

<h3 class="pdf-center m-0">{{$title}}</h3>
<h3 class="pdf-center m-0">{{$company->nama_perusahaan}}</h3>
<h3 class="pdf-center mb-5"></h3>
<div class="pdf-center">
    <table class="table-full header" page-break-inside: auto;>
        <tbody>
            <div id="header">
                <table >
                    <tbody>
                        <tr>
                            <td width="70%"></td>
                            <td class="left-border" width="10%">No. PO</td>
                            <td width="2%">:</td>
                            <td width="28%">{{$detail->sales_no}}</td>
                        </tr>
                        <tr>
                            <td width="70%"></td>
                            <td class="left-border" width="10%">Tanggal</td>
                            <td width="2%">:</td>
                            <td width="28%">{{$detail->trans_date}}</td>
                        </tr>
                        <tr>
                            <td width="70%"></td>
                            <td class="left-border" width="10%">No. PR</td>
                            <td width="2%">:</td>
                            <td width="28%">3325476</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td width="50%" rowspan="3">
                                Kepada : {{ $detail->beone_supplier->nama ?? ''}}
                            </td>
                            <td width="20%">Attention</td>
                            <td width="2%">:</td>
                            <td width="28%"></td>
                        </tr>
                        <tr>
                            <td width="20%">Fax</td>
                            <td width="2%">:</td>
                            <td width="28%"></td>
                        </tr>
                        <tr>
                            <td width="20%">Tgl. Kirim</td>
                            <td width="2%">:</td>
                            <td width="28%">{{$detail->trans_date}}</td>
                        </tr>
                        <tr>
                            <td width="50%"><u>Harap kirim barang-barang tersebut dibawah ini:<u></td>
                            <td width="20%">Jangka Waktu Kredit</td>
                            <td width="2%">:</td>
                            <td width="28%">0 Hari</td>
                        </tr>
                        <tr>
                            <td width="50%">Please supply the following goods/material</td>
                            <td width="20%"></td>
                            <td width="2%"></td>
                            <td width="28%" style="text-align:right;">Hal. : 1 dari 1</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <thead>
                        <tr>
                            <th class="cell-border text-center" width="1%">No</th>
                            <th class="cell-border text-center" width="15%">Kode Barang</th>
                            <th class="cell-border text-center" width="29%">Keterangan <br>Discription</br></th>
                            <th class="cell-border text-center" width="10%">Jumlah <br>Quantity</br></th>
                            <th class="cell-border text-center" width="5%">Sat</th>
                            <th class="cell-border text-center" width="20%">Harga Satuan <br>Unit Price</br></th>
                            <th class="cell-border text-center" width="20%">Jumlah Uang <br>Total Amount</br></th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($detail->beone_sales_details as $i => $vals)
                        <tr>
                            <td class="left-border text-center">{{$no++}}</td>
                            <td class="left-border text-center">{{$vals->beone_item->item_code}}</td>
                            <td class="left-border">{{$vals->beone_item->nama}}</td>
                            <td class="left-border text-center">{{$vals->qty}}</td>
                            <td class="left-border text-center">{{$vals->beone_item->satuan_id}}</td>
                            <td class="left-border text-right">{{$vals->price}}</td>
                            <td class="left-border text-right">{{$vals->amount}}</td>
                        </tr>
                    @endforeach
                        <tr>
                            <td class="top-border" width="60%" colspan="5" rowspan="3">
                                Catatan : 
                            </td>
                           <td class="top-border left-border text-right" width="20%">DPP</td>
                           <td class="top-border left-border text-right" width="20%">0</td>
                        </tr>
                       <tr>
                           <td class="left-border text-right" width="20%">PPN</td>
                           <td class="left-border text-right" width="20%">0</td>
                       </tr>
                        <tr>
                            <td class="top-border left-border text-right" width="20%">Total</td>
                            <td class="top-border left-border text-right u" width="20%">{{$detail->grandtotal}}</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td width="40%" rowspan="5">
                                Bila pengiriman barang diatas bertahap harap dicantumkan nomor "SALES ORDER"
                                ini pada setiap pengiriman & tagihan PO lembar asli ini mohon dikembalikan bersama
                                Tagihan / Faktur terakhir.
                            </td>
                            <td width="20%">Agreed by,</td>
                            <td class="text-center" width="20%" colspan="2">E.&.O.E.</td>
                        </tr>
                        <tr>
                            <td width="20%"></td>
                            <td width="20%"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="20%"></td>
                            <td width="20%"></td>
                            <td width="20%"></td>
                        </tr>
                        <tr>
                            <td width="20%">_______________</td>
                            <td width="20%">_______________</td>
                            <td width="20%">_______________</td>
                        </tr>
                        <tr>
                            <td width="20%" style="text-decoration:italic;font-size:8pt;">SUPLIER</td>
                            <td width="20%" style="text-decoration:italic;font-size:8pt;">MANAGING DIRECTOR</td>
                            <td width="20%" style="text-decoration:italic;font-size:8pt;">SALES DEPARTMENT</td>
                        </tr>
                    </tbody>
                </table>
                <p>Lembar : (1) Suplier, (2) Pembelian, (3) Hutang, (4) Gudang</p>
            </div>
        </tbody>
    </table>
</div>
