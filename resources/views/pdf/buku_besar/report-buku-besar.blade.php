<style>
    .pdf-center {
        text-align: center;
    }

    .m-0 {
        margin: 0px;
    }

    .mb-5 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .table-full {
        width: 100%
    }

    table, td, th {
        font-family: "verdana", "sans-serif";
        border: 1px solid black;
        font-size: 12px;
    }

    table {
        margin-top: 10px;
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: center;
    }

    div.responsive {
        overflow: auto;
        max-height: 100px;
    }

    td.center {
        text-align: center;
    }
</style>

<h3 class="pdf-center m-0">{{$title}}</h3>
<h3 class="pdf-center mb-5">{{$company->nama_perusahaan}}</h3>
<h3 class="pdf-center mb-5">({{$coa}}</h3>
<h3 class="pdf-center mb-5">{{$periode}}</h3>
<div class="pdf-center">
    <table class="table-full header" page-break-inside: auto;>
        <thead>
        <tr>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>No Transaksi</th>
            <th>Keterangan</th>
            <th>Debet (IDR)</th>
            <th>Kredit (IDR)</th>
            <th>Saldo Akhir</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $i => $val)
            <tr class="list_row">
                <td>{{$val->nomor}}</td>
                <td>{{$val->trans_date}}</td>
                <td>{{$val->trans_no}}</td>
                <td>{{$val->keterangan}}</td>
                <td>{{number_format($val->debet_idr,4)}}</td>
                <td>{{number_format($val->kredit_idr,4)}}</td>
                <td>{{number_format($val->saldo_akhir_idr,4)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
