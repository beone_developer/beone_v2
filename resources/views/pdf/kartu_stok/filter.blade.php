@php
    $title='Filter Report Kartu Stok';
@endphp
@extends('admin.layouts.default')
@section('title', $title)
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @if(session('status'))
                                <div class="alert {{session('status')['alert']}} fade show" role="alert">
                                    <div class="alert-text">
                                        <p>
                                            <strong>{{session('status')['status']}}</strong>{{session('status')['message']}}
                                        </p>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text">
                                        <p><strong>Error! </strong>There are some problems.</p>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->voucher_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <div class="input-group date">
                                            <input name="filter_awal_date" type="text" class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <div class="input-group date">
                                            <input name="filter_akhir_date" type="text" class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Item:</label>
                                        <div class="kt-form__control">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_item"
                                                    data-s2-url="{{url('master/item/s2')}}"
                                                    data-s2-placeholder="Cari Item"
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Gudang:</label>
                                        <div class="kt-form__control">
                                            <select class="form-control kt-select2 s2-ajax"
                                                    name="beone_gudang"
                                                    data-s2-url="{{url('master/gudang/s2')}}"
                                                    data-s2-placeholder="Cari Gudang"
                                                    required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Filter</button>
                                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
