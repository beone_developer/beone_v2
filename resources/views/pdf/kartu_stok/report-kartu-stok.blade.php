<style>
    .pdf-center {
        text-align: center;
    }

    .m-0 {
        margin: 0px;
    }

    .mb-5 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .table-full {
        width: 100%
    }

    table, td, th {
        font-family: "verdana", "sans-serif";
        border: 1px solid black;
        font-size: 12px;
    }

    table {
        margin-top: 10px;
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: center;
    }

    div.responsive {
        overflow: auto;
        max-height: 100px;
    }

    td.center {
        text-align: center;
    }
</style>

<h3 class="pdf-center m-0">{{$title}}</h3>
<h3 class="pdf-center mb-5">{{$company->nama_perusahaan}}</h3>
<h3 class="pdf-center mb-5">({{$item->item_code}}) {{$item->nama}}</h3>
<h3 class="pdf-center mb-5">{{$gudang->gudang_code}} {{$gudang->nama}}</h3>
<h3 class="pdf-center mb-5">{{$periode}}</h3>
<div class="pdf-center">
    <table class="table-full header" page-break-inside: auto;>
        <thead>
        <tr>
            <th>Tanggal</th>
            <th>No Transaksi</th>
            <th>Keterangan</th>
            <th>In</th>
            <th>Out</th>
            <th>Saldo Qty</th>
        </tr>
        </thead>
        <tbody>
        @php
            $stok_akhir = 0;
        @endphp
        @foreach($data as $i => $header)
            <tr class="list_row">
                <td>{{$header->trans_date}}</td>
                <td>{{$header->trans_no}}</td>
                <td>{{$header->keterangan}}</td>
                <td>{{number_format($header->qty_in, 4)}}</td>
                <td>{{number_format($header->qty_out, 4)}}</td>
                <td>{{number_format($stok_akhir+=$header->stok_akhir,4)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
