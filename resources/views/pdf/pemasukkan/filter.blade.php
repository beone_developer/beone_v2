@extends('admin.layouts.default')
@section('title', $title='Pabean Pemasukkan')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @if(session('status'))
            <div class="alert {{session('status')['alert']}} fade show" role="alert">
                <div class="alert-text">
                    <p>
                        <strong>{{session('status')['status']}}</strong>{{session('status')['message']}}
                    </p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form kt-form--label-right form-validatejs form-filter" method="get"
                              action="{{url()->current()}}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <div class="input-group date">
                                            <input name="filter_awal_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <div class="input-group date">
                                            <input name="filter_akhir_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>BC</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    id="bc_no"
                                                    name="bc_no"
                                                    required>
                                                <option value="23"> BC 2.3 </option>
                                                <option value="27"> BC 2.7 </option>
                                                <option value="40"> BC 4.0 </option>
                                                <option value="262"> BC 26.2 </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success">
                                            Filter
                                            <i class="la la-filter"></i>
                                        </button>
                                        <a href="javascript:;" data-url="{{url()->current().'/print'}}"
                                           class="btn btn-primary btn-print">
                                            Print
                                            <i class="la la-print"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                        </form>
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th colspan="3">Dokumen Pabean</th>
                                <th colspan="2">Bukti Penerimaan Barang</th>
                                <th rowspan="2">Supplier</th>
                                <th rowspan="2">Kode Barang</th>
                                <th rowspan="2">Uraian Barang</th>
                                <th rowspan="2">Sat</th>
                                <th rowspan="2">Jml</th>
                                <th rowspan="2">Valas</th>
                                <th rowspan="2">Nilai Barang</th>
                            </tr>
                            <tr>
                                <th>Jenis</th>
                                <th>Nomor</th>
                                <th>Tanggal</th>
                                <th>Nomor</th>
                                <th>Tanggal</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'no', name: 'no'},
                        {data: 'jenis_bc', name: 'jenis_bc'},
                        {data: 'bc_no', name: 'bc_no'},
                        {data: 'bc_date', name: 'bc_date'},
                        {data: 'receive_no', name: 'receive_no'},
                        {data: 'receive_date', name: 'receive_date'},
                        {data: 'supplier', name: 'supplier'},
                        {data: 'item_code', name: 'item_code'},
                        {data: 'nama', name: 'nama'},
                        {data: 'satuan_code', name: 'satuan_code'},
                        {data: 'qty', name: 'qty'},
                        {data: 'currency_code', name: 'currency_code'},
                        {data: 'amount', name: 'amount'}

                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[-1, 10, 25, 100], ["All", 10, 25, 100]],
                });
                $('.form-filter').on('submit', function (e) {
                    e.preventDefault();
                    $dt.draw();
                });
                $('.btn-print').on('click', function (e) {
                    e.preventDefault();
                    $dt.draw();
                    const url = $(this).data('url') + "?" + $('.form-filter').serialize();
                    window.open(url, '_blank');
                });
            },
        }
    </script>
@endpush
