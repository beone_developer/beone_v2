@php
    $title='Report Neraca Mutasi';
@endphp
@extends('admin.layouts.default')
@section('title', $title)
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form kt-form--label-right form-validatejs form-filter" method="get"
                              action="{{url()->current()}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <div class="input-group date">
                                            <input name="filter_awal_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <div class="input-group date">
                                            <input name="filter_akhir_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                <div class="col-md-4">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Nomor COA:</label>--}}
{{--                                        <div class="kt-form__control">--}}
{{--                                            <select class="form-control kt-select2 s2-ajax"--}}
{{--                                                    name="beone_coa"--}}
{{--                                                    data-s2-url="{{url('master/coa/s2all')}}"--}}
{{--                                                    data-s2-placeholder="Cari Item"--}}
{{--                                                    required>--}}
{{--                                                <option></option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success">
                                            Filter
                                            <i class="la la-filter"></i>
                                        </button>
                                        <a href="javascript:;" data-url="{{url()->current().'/print'}}"
                                           class="btn btn-primary btn-print">
                                            Print
                                            <i class="la la-print"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                        </form>
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>COA No</th>
                                <th>COA Nama</th>
                                <th class="currency">Saldo Awal Debit</th>
                                <th class="currency">Saldo Awal Kredit</th>
                                <th class="currency">Debet (IDR)</th>
                                <th class="currency">Kredit (IDR)</th>
                                <th class="currency">Saldo Akhir Debet (IDR)</th>
                                <th class="currency">Saldo Akhir Kredit (IDR)</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    colReorder: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        // coa_id, nomor, nama, debet_awal, kredit_awal, debet, kredit, debet_akhir, kredit_akhir
                        {data: 'nomor', name: 'nomor'},
                        {data: 'nama', name: 'nama'},
                        {data: 'debet_awal', name: 'debet_awal'},
                        {data: 'kredit_awal', name: 'kredit_awal'},
                        {data: 'debet', name: 'debet'},
                        {data: 'kredit', name: 'kredit'},
                        {data: 'debet_akhir', name: 'debet_akhir'},
                        {data: 'kredit_akhir', name: 'kredit_akhir'}
                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[-1, 10, 25, 100], ["All", 10, 25, 100]],
                });
                $('.form-filter').on('submit', function (e) {
                    e.preventDefault();
                    $dt.draw();
                });
                $('.btn-print').on('click', function (e) {
                    e.preventDefault();
                    $dt.draw();
                    const url = $(this).data('url') + "?" + $('.form-filter').serialize();
                    window.open(url, '_blank');
                });
            },
        }
    </script>
@endpush
