<style>
    .pdf-center {
        text-align: center;
    }

    .m-0 {
        margin: 0px;
    }

    .mb-5 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .table-full {
        width: 100%
    }

    table, td, th {
        font-family: "verdana", "sans-serif";
        border: 1px solid black;
        font-size: 12px;
    }

    table {
        margin-top: 10px;
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: center;
    }

    div.responsive {
        overflow: auto;
        max-height: 100px;
    }

    td.center {
        text-align: center;
    }

    td.left {
        text-align: left;
    }
    td.right {
        text-align: right;
    }
</style>

<h3 class="pdf-center m-0">{{$title}}</h3>
<h3 class="pdf-center m-0">{{$company->nama_perusahaan}}</h3>
<h3 class="pdf-center mb-5">{{$periode}}</h3>
<div class="pdf-center">
    <table class="table-full header" page-break-inside: auto;>
        <thead>
        <!-- <tr>
            <th rowspan="2">No</th>
            <th colspan="3">Dokumen Pabean</th>
            <th colspan="2">Surat Jalan</th>
            <th rowspan="2">Penerimaan</th>
            <th rowspan="2">Kode Barang</th>
            <th rowspan="2">Nama Barang</th>
            <th rowspan="2">Sat</th>
            <th rowspan="2">Jml</th>
            <th rowspan="2">Valas</th>
            <th rowspan="2">Nilai Barang</th>
        </tr> -->
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Sat</th>
            <th>Jumlah</th>
            <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $i => $header)
            <tr class="list_row">
                <td class="center">{{$no++}}</td>
                <td class="center">{{$header->item_code}}</td>
                <td>{{$header->nama}}</td>
                <td class="center">{{$header->satuan_code}}</td>
                <td class="right">{{number_format($header->saldo_akhir_qty, 4)}}</td>
                <td class="center">{{$header->keterangan}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
