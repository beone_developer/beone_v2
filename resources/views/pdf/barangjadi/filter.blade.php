@extends('admin.layouts.default')
@section('title', $title='Pabean Mutasi Barang Jadi')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @if(session('status'))
            <div class="alert {{session('status')['alert']}} fade show" role="alert">
                <div class="alert-text">
                    <p>
                        <strong>{{session('status')['status']}}</strong>{{session('status')['message']}}
                    </p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form kt-form--label-right form-validatejs form-filter" method="get"
                              action="{{url()->current()}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Awal</label>
                                        <div class="input-group date">
                                            <input name="filter_awal_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Akhir</label>
                                        <div class="input-group date">
                                            <input name="filter_akhir_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success">
                                            Filter
                                            <i class="la la-filter"></i>
                                        </button>
                                        <a href="javascript:;" data-url="{{url()->current().'/print'}}"
                                           class="btn btn-primary btn-print">
                                            Print
                                            <i class="la la-print"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                        </form>
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Sat</th>
                                <th>Saldo Awal </th>
                                <th>Pemasukan </th>
                                <th>Pengeluaran </th>
                                <th>Penyesuaian </th>
                                <th>Saldo Akhir </th>
                                <th>Stok Opname </th>
                                <th>Selisih </th>
                                <th>Keterangan </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'item_code', name: 'item_code'},
                        {data: 'nama', name: 'nama'},
                        {data: 'satuan_code', name: 'satuan_code'},
                        {data: 'saldo_awal_qty', name: 'saldo_awal_qty'},
                        {data: 'total_in', name: 'total_in'},
                        {data: 'total_out', name: 'total_out'},
                        {data: 'penyesuaian', name: 'penyesuaian'},
                        {data: 'saldo_akhir_qty', name: 'saldo_akhir_qty'},
                        {data: 'stok_opname', name: 'stok_opname'},
                        {data: 'selisih', name: 'selisih'},
                        {data: 'keterangan', name: 'keterangan'},

                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[-1, 10, 25, 100], ["All", 10, 25, 100]],
                });
                $('.form-filter').on('submit', function (e) {
                    e.preventDefault();
                    $dt.draw();
                });
                $('.btn-print').on('click', function (e) {
                    e.preventDefault();
                    $dt.draw();
                    const url = $(this).data('url') + "?" + $('.form-filter').serialize();
                    window.open(url, '_blank');
                });
            },
        }
    </script>
@endpush
