<style>
    .pdf-center {
        text-align: center;
    }

    .m-0 {
        margin: 0px;
    }

    .mb-5 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .table-full {
        width: 100%
    }

    table, td, th {
        font-family: "verdana", "sans-serif";
        border: 1px solid black;
        font-size: 12px;
    }

    table {
        margin-top: 10px;
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: center;
    }

    div.responsive {
        overflow: auto;
        max-height: 100px;
    }

    td.center {
        text-align: center;
    }

    td.left {
        text-align: left;
    }
    td.right {
        text-align: right;
    }
</style>

<h3 class="pdf-center m-0">{{$title}}</h3>
<h3 class="pdf-center m-0">{{$company->nama_perusahaan}}</h3>
<h3 class="pdf-center mb-5">{{$periode}}</h3>
<div class="pdf-center">
    <table class="table-full header" page-break-inside: auto;>
        <thead>
        <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Sat</th>
            <th>Saldo Awal <br/> {{$tglawal}}</th>
            <th>Pemasukan</th>
            <th>Pengeluaran</th>
            <th>Penyesuaian</th>
            <th>Saldo Akhir <br/> {{$tglakhir}}</th>
            <th>Stok Opname</th>
            <th>Selisih</th>
            <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $i => $header)
            <tr class="list_row">
                <td class="center">{{$no++}}</td>
                <td class="center">{{$header->item_code}}</td>
                <td class="left">{{$header->nama}}</td>
                <td class="center">{{$header->satuan_code}}</td>
                <td class="right">{{number_format($header->saldo_awal_qty, 4)}}</td>
                <td class="right">{{number_format($header->total_in, 4)}}</td>
                <td class="right">{{number_format($header->total_out, 4)}}</td>
                <td class="right">{{number_format('0', 4)}}</td>
                <td class="right">{{number_format($header->saldo_akhir_qty, 4)}}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
