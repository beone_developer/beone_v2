@php
    $title='Report Supplier Hutang';
@endphp
@extends('admin.layouts.default')
@section('title', $title)
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form kt-form--label-right form-validatejs form-filter" method="get"
                              action="{{url()->current()}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Tanggal</label>
                                        <div class="input-group date">
                                            <input name="filter_akhir_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Select date"/>
                                            <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success">
                                            Filter
                                            <i class="la la-filter"></i>
                                        </button>
                                        <a href="javascript:;" data-url="{{url()->current().'/print'}}"
                                           class="btn btn-primary btn-print">
                                            Print
                                            <i class="la la-print"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--fit"></div>
                        </form>
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>Supplier</th>
                                <th>Tanggal</th>
                                <th>Import No</th>
                                <th>No Aju</th>
                                <th class="currency">Kurs</th>
                                <th>Currency</th>
                                <th class="currency">Sisa Bayar</th>
                                <th class="currency">Terbayar</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                    ],
                    rowGroup: {
                        dataSrc: 'supplier'
                    },
                    colReorder: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{url()->current()}}",
                        data: function (d) {
                            $.each($('input, select ,textarea', '.form-filter'), function (k) {
                                d[$(this).attr('name')] = $(this).val();
                            });
                        }
                    },
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'supplier', name: 'supplier'},
                        {data: 'trans_date', name: 'trans_date'},
                        {data: 'import_no', name: 'import_no'},
                        {data: 'no_aju', name: 'no_aju'},
                        {data: 'kurs', name: 'kurs'},
                        {data: 'currency', name: 'currency'},
                        {data: 'sisabayar', name: 'sisabayar'},
                        {data: 'terbayar', name: 'terbayar'},

                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[-1, 10, 25, 100], ["All", 10, 25, 100]],
                });
                $('.form-filter').on('submit', function (e) {
                    e.preventDefault();
                    $dt.draw();
                });
                $('.btn-print').on('click', function (e) {
                    e.preventDefault();
                    $dt.draw();
                    const url = $(this).data('url') + "?" + $('.form-filter').serialize();
                    window.open(url, '_blank');
                });
            },
        }
    </script>
@endpush
