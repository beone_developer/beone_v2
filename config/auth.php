<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
            'hash' => false,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\BeoneUser::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],
    ],

    'trusted' => [
        'expire' => env('TRUSTED_EXPIRE', 1.5),
        'key' => env('TRUSTED_KEY', 'ABCDEF123ERD456EABCDEF123ERD4500'),
        'chipher' => env('TRUSTED_CHIPHER', 'AES-256-CBC'),
        'gate' => env('TRUSTED_PATH', 'eyJpdiI6IlZtOHVOZVNFV29reGFMSVp2ZGhPd1E9PSIsInZhbHVlIjoiUlwvZXA2V0JLY2FnY3lpZTR1dUp4T2pxUkp2SjlMTGV2dFErRjUrK3hPcTdNS0x3MWswU2h3allNVVFLaEZQdEkiLCJtYWMiOiI4NGI4NDYyY2U0YzEyMjU2NTcxMjkwODhmMGZmZTJhYTcxNWMzYzMyYWJiODc4ZDM1N2E4YWI2MzU2ZDkyNTU5In0='),
        'stats' => env('TRUSTED_STATS', 'eyJpdiI6ImhDXC81dWxITGt5eFwva0N4TUYrXC81eXc9PSIsInZhbHVlIjoid1UybmhOMmNiK3hkVjUzanc0SjJaUT09IiwibWFjIjoiMGJjOWIzNmIwMjdkMGUwMjRmNmI1NWJlOWY1OGFkNGFhNDY2MjA1ZTRiMWNkNDA4MzdjMmQ2NjZiYjA4MzIxNSJ9'),
    ],
    /*
    |--------------------------------------------------------------------------
    | Password Confirmation Timeout
    |--------------------------------------------------------------------------
    |
    | Here you may define the amount of seconds before a password confirmation
    | times out and the user is prompted to re-enter their password via the
    | confirmation screen. By default, the timeout lasts for three hours.
    |
    */

    'password_timeout' => 10800,

];
