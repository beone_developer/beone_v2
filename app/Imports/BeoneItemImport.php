<?php

namespace App\Imports;

use App\Models\BeoneItem;
use Maatwebsite\Excel\Concerns\ToModel;

class BeoneItemImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        if (!isset($row['item_code'])) {
            return null;
        }
        return BeoneItem::updateOrInsert(
            [
                'nama' => $row['nama'],
                'keterangan' => $row['keterangan'],
                'item_type_id' => $row['item_type_id'],
                'item_category_id' => $row['item_category_id'],
                'hscode' => $row['hscode'],
                'flag' => $row['flag'],
            ],
            ['item_code' => $row['item_code']]
        );
//        return new BeoneItem([
//            'nama' => $row['nama'],
//            'keterangan' => $row['keterangan'],
//            'item_type_id' => $row['item_type_id'],
//            'item_category_id' => $row['item_category_id'],
//            'hscode' => $row['hscode'],
//            'flag' => $row['flag'],
//        ]);
    }

    public function headingRow(): int
    {
        return 2;
    }

    public function rules(): array
    {
        return [
            '1' => Rule::in(['patrick@maatwebsite.nl']),

            // Above is alias for as it always validates in batches
            '*.1' => Rule::in(['patrick@maatwebsite.nl']),

            // Can also use callback validation rules
            '0' => function ($attribute, $value, $onFailure) {
                if ($value !== 'Patrick Brouwers') {
                    $onFailure('Name is not Patrick Brouwers');
                }
            }
        ];
    }
}
