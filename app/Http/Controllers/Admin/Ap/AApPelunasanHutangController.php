<?php

namespace App\Http\Controllers\Admin\Ap;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneHutangDetail;
use App\Models\BeoneHutangHeader;
use App\Models\BeoneImportHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AApPelunasanHutangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneHutangHeader::with(['beone_supplier','beone_hutang_details'])->find($uid);
            // return $detail;
            return view('admin.menus.ap.pelunasan_hutang.detail', compact('detail'));
        }
        return view('admin.menus.ap.pelunasan_hutang.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.ap.pelunasan_hutang.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'beone_supplier.required' => 'Supplier required.',
        ];

        $this->validate($request, [
            'beone_supplier' => 'required_without:hutang_header_id',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('hutang_header_id');
            $message = 'Pelunasan Hutang Berhasil Dibuat';

            $data = new BeoneHutangHeader();

            $beone_supplier = $inputs->input('beone_supplier');
            $grandtotal = $inputs->input('grandtotal');
            $bentuk_dana = $inputs->input('bentuk_dana');
            $cek_bg_no = $inputs->input('cek_bg_no');
            $setor_ke_coa_id = $inputs->input('setor_ke_coa_id');
            $bank = $inputs->input('bank');
            $no_rek = $inputs->input('no_rek');

            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneHutangHeader::find($uid);
                $message = 'Pelunasan Hutang Berhasil Diedit';
                $beone_supplier = json_decode($data->beone_supplier->id);
            }

            $beone_hutang_details = [];
            $detail = $inputs->input('beone_hutang_details');
            foreach ($detail as $key => $value) {
                if ($value['import_header_id'] <> '') {
                    $beone_import_header = BeoneImportHeader::find($value['import_header_id']);
                    $beone_hutang_detail = BeoneHutangDetail::find($value['hutang_detail_id']);
                    $mutator = 0;
                    if ($beone_hutang_detail){
                        $mutator = $beone_hutang_detail->amount;
                    }
                    $beone_import_header->update(['terbayar'=>$beone_import_header->terbayar - $mutator + $value['amount']]);
                    $beone_hutang_detail = [
                        'import_header_id' => $value['import_header_id'],
                        'amount' => $value['amount']
                    ];
                    $beone_hutang_details[] = $beone_hutang_detail;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->trans_date = $trans_date;
                $data->trans_no = Helper::getTransNo('PU', $trans_date, BeoneHutangHeader::class, 'trans_no');
                $data->supplier_id = $beone_supplier->id;
            }
            if ($bentuk_dana == 'transfer'){
                $data->setor_ke_coa_id = $setor_ke_coa_id->id;
            }
            $data->grandtotal = $grandtotal;
            $data->bentuk_dana = $bentuk_dana;
            $data->cek_bg_no = $cek_bg_no;
            $data->bank = $bank;
            $data->no_rek = $no_rek;
            $data->save();

            $data->beone_hutang_details()->delete();
            $data->beone_hutang_details()->createMany($beone_hutang_details);

            DB::commit();
            return Helper::redirect('pelunasan_hutang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return $e->getMessage();
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Pelunasan Hutang Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeoneHutangHeader::find($uid);
        if ($data) {
            $data->delete();
        }
        return Helper::redirect('pelunasan_hutang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['hutang_header_id', 'item_id', 'pelunasan_no', 'pelunasan_date', 'flag', 'qty', 'satuan_qty', 'gudang_id'];
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneHutangHeader::query()->with(['beone_supplier'])->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                '<a href="' . route('pelunasan_hutang.detail', ['id' => $item->hutang_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                    <i class="la la-edit"></i>
                </a>
                <a href="javascript:void(0);" data-url="' . route('pelunasan_hutang.delete', ['id' => $item->hutang_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                    <i class="la la-trash"></i>
                </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function IsInvalid ($uid)
    {
        return false;
    }

    public function loadHutang(Request $request)
    {
        $supplier_id = $request->supplier_id;
        $export = BeoneImportHeader::with(['beone_currency', 'beone_supplier', 'beone_import_details.beone_item'])->where('supplier_id', $supplier_id)->where('sisabayar','>', 0)->get();
        return response()->json($export);
    }
}
