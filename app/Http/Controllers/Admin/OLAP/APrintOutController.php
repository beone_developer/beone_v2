<?php

namespace App\Http\Controllers\Admin\OLAP;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeonePrintOut;
use DB;
use Illuminate\Http\Request;
use Vendor\Package\Stimulsoft\Classes\StiResult as CoreResult;
use Vendor\Package\Stimulsoft\CoreHandler;
use Vendor\Package\Stimulsoft\CoreHelper;

class APrintOutController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->handle($request);
        }
        $data = $this->getData($request);
        if (!$data) {
            return Helper::redirect('home', Constant::AlertWarning, Constant::TitleWarning, 'no data found');
        }
        $print_out = BeonePrintOut::firstOrCreate(['nama' => $request->input('print')]);
        $designer = ($print_out->design) ? $print_out->design : $print_out->design_def;
        $options = CoreHelper::createOptions();
        $options->handler = $request->fullUrl();
        $options->timeout = 30;
        $core = new CoreHelper();
        $core->setOptions($options);
        $is_design = $request->has('hm');
        return view('admin.menus.olap.print_out', compact('is_design', 'core', 'designer', 'data'));
    }

    public function getData(Request $request)
    {
        $printOut = $request->input('print');
        $uid = $request->input('id');
        $data = [];
        switch ($printOut) {
            case Constant::PrintBuktiKas:
                $query = "select m.voucher_number, m.voucher_date, m.keterangan as keterangan_header, d.keterangan_detail as keterangan_detail, d.jumlah_idr, d.jumlah_valas, d.kurs, c1.nama as nama_coa_header,c1.nomor as nomor_coa_header, c2.nama as nama_coa_detail, c2.nomor as nomor_coa_detail
                from beone_voucher_header m,
                beone_voucher_detail d,
                beone_coa c1,
                beone_coa c2
                where m.voucher_header_id = d.voucher_header_id and m.coa_id_cash_bank = c1.coa_id and d.coa_id_lawan = c2.coa_id
                    and m.voucher_header_id = '" . $uid . "'";
                $data = DB::select(DB::raw($query));
                break;
            case Constant::PrintSalesOrder:
                $query = "select m.sales_no as trans_no,
                       m.trans_date as trans_date,
                       c.nama as cust_nama,
                       m.keterangan,
                       m.grandtotal_sys as grandtotal_header,
                       d.amount,
                       d.amount_sys,
                       d.qty,
                       i.item_code,
                       i.nama as item_nama
                from beone_sales_header m,
                     beone_sales_detail d,
                     beone_item i,
                     beone_satuan_item s,
                     beone_customer c
                where m.sales_header_id = d.sales_header_id
                  and i.item_id = d.item_id
                  and m.customer_id = c.customer_id
                and m.sales_header_id = '" . $uid . "' LIMIT 10";
                $data = DB::select(DB::raw($query));
                break;
            default:
                break;
        }
        if (count($data) < 1) {
            return false;
        }
        $result = [
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'data' => $data,
        ];
        return $result;
    }

    public function handle(Request $request)
    {
        $handler = new CoreHandler();
        $handler->registerErrorHandlers();

        $handler->onBeginProcessData = function ($event) {
            $database = $event->database;
            $connection = $event->connection;
            $dataSource = $event->dataSource;
            $connectionString = $event->connectionString;
            $queryString = $event->queryString;
            return CoreResult::success();
        };

        $handler->onPrintReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onBeginExportReport = function ($event) {
            $settings = $event->settings;
            $format = $event->format;
            return CoreResult::success();
        };

        $handler->onEndExportReport = function ($event) {
            $format = $event->format;
            $data = $event->data;
            $fileName = $event->fileName;
            file_put_contents('reports/' . $fileName . '.' . strtolower($format), base64_decode($data));
            return CoreResult::success("Export OK. Message from server side.");
        };

        $handler->onEmailReport = function ($event) {
            $event->settings->from = "******@gmail.com";
            $event->settings->host = "smtp.gmail.com";
            $event->settings->login = "******";
            $event->settings->password = "******";
        };

        $handler->onDesignReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onCreateReport = function ($event) {
            $fileName = $event->fileName;
            return CoreResult::success();
        };

        $handler->onSaveReport = function ($event) use ($request) {
            $report = $event->report;
            $reportJson = $event->reportJson;
            $fileName = $event->fileName;

            BeonePrintOut::updateOrCreate(
                ['nama' => $request->input('print')],
                ['design' => $reportJson]
            );
//            file_put_contents('reports/' . $fileName . ".mrt", $reportJson);
            return CoreResult::success("Save Report OK: " . $fileName);
        };

        $handler->onSaveAsReport = function ($event) {
            return CoreResult::success();
        };

        return $handler->process();
    }
}
