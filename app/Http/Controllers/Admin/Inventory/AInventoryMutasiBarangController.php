<?php

namespace App\Http\Controllers\Admin\Inventory;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneMutasiStokHeader;
use App\Models\BeoneSatuanItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AInventoryMutasiBarangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
//            $detail = BeoneMutasiStokHeader::with(['beone_mutasi_stok_details.beone_item'])->find($uid);
            $detail = BeoneMutasiStokHeader::with(['beone_mutasi_stok_details.beone_item', 'beone_mutasi_stok_details.beone_satuan_item'])->find($uid);
            return view('admin.menus.inventory.mutasi_barang.detail', compact('detail'));
        }
        return view('admin.menus.inventory.mutasi_barang.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.inventory.mutasi_barang.list');
    }

    public function dataTable(Request $request)
    {
//        $field = ['mutasi_stok_header_id', 'mutasi_stok_no', 'trans_date', 'keterangan'];

//        $model = DB::table('beone_mutasi_stok_header')->select($field)->whereNull('deleted_at')->orderByDesc('trans_date');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneMutasiStokHeader::query()->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);

        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('inventory/mutasi_barang/detail?id=' . $item->mutasi_stok_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('inventory/mutasi_barang/del?id=' . $item->mutasi_stok_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function postDetail(Request $request)
    {
        $message = [
            'trans_date.required' => 'Date is required.',
            'gudang_asal.required' => 'Gudang Asal is required.',
            'gudang_tujuan.required' => 'Gudang Tujuan is required.',
            'beone_mutasi_stok_details.required' => 'Keterangan is required.',
            'keterangan.max' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'trans_date' => 'required_without:mutasi_stok_header_id',
            'gudang_asal' => 'required|different:gudang_tujuan',
            'gudang_tujuan' => 'required|different:gudang_asal',
            'beone_mutasi_stok_details' => 'required',
            'keterangan' => 'max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('mutasi_stok_header_id');
            $message = 'Mutasi Barang Berhasil Dibuat';

            $data = new BeoneMutasiStokHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneMutasiStokHeader::find($uid);
                $message = 'Mutasi Barang Berhasil Diedit';
            }

            $keterangan = $inputs->input('keterangan');
            $gudang_asal = $inputs->input('gudang_asal');
            $gudang_tujuan = $inputs->input('gudang_tujuan');

            $beone_mutasi_stok_details = [];
            $detail = $inputs->input('beone_mutasi_stok_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_mutasi_stok_detail = [
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'gudang_id_asal' => $gudang_asal->id,
                        'gudang_id_tujuan' => $gudang_tujuan->id,
                    ];
                    $beone_mutasi_stok_details[] = $beone_mutasi_stok_detail;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->trans_date = $trans_date;
                $data->mutasi_stok_no = Helper::getTransNo('MB', $trans_date, BeoneMutasiStokHeader::class, 'mutasi_stok_no');;
            }

            $data->keterangan = $keterangan;
            $data->save();
            $data->beone_mutasi_stok_details()->delete();
            $data->beone_mutasi_stok_details()->createMany($beone_mutasi_stok_details);

            DB::commit();
            return Helper::redirect('mutasi_barang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Mutasi Barang Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneMutasiStokHeader::find($uid);
        $data->beone_mutasi_stok_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('mutasi_barang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchPo(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneMutasiStokHeader::where('purchase_no', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
