<?php

namespace App\Http\Controllers\Admin\Inventory;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneSatuanItem;
use App\Models\BeoneUsageHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AInventoryUsageBarangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
//            $detail = BeoneUsageHeader::with(['beone_usage_details.beone_item','beone_usage_details.beone_gudang'])->find($uid);
            $detail = BeoneUsageHeader::with(['beone_usage_details.beone_item','beone_usage_details.beone_gudang','beone_usage_details.beone_satuan_item'])->find($uid);
            // return $detail;
            return view('admin.menus.inventory.usage.detail', compact('detail'));
        }
        return view('admin.menus.inventory.usage.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.inventory.usage.list');
    }

    public function dataTable(Request $request)
    {
//        $field = ['usage_header_id', 'usage_no', 'trans_date', 'keterangan'];

//        $model = DB::table('beone_usage_header')->select($field)->whereNull('deleted_at')->orderByDesc('trans_date');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneUsageHeader::query()->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);

        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('inventory/usage/detail?id=' . $item->usage_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('inventory/usage/del?id=' . $item->usage_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function postDetail(Request $request)
    {
        $message = [
            'trans_date.required' => 'Date is required.',
            'beone_gudang.required' => 'Gudang is required.',
            'keterangan.required' => 'Keterangan is required.',
            'keterangan.max' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'trans_date' => 'required_without:usage_header_id',
            'beone_gudang' => 'required_without:usage_header_id',
            'keterangan' => 'required',
            'keterangan' => 'max:100',
            'beone_usage_details' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('usage_header_id');
            $message = 'Usage Berhasil Dibuat';

            $data = new BeoneUsageHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneUsageHeader::find($uid);
                $message = 'Usage Berhasil Diedit';
            }

            $keterangan = $inputs->input('keterangan');
            $beone_gudang = $inputs->input('beone_gudang');

            $beone_usage_details = [];
            $detail = $inputs->input('beone_usage_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_usage_detail = [
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty_in' => $value['qty_in'],
                        'qty_out' => $value['qty_out'],
                        'gudang_id' => $beone_gudang->id
                    ];
                    $beone_usage_details[] = $beone_usage_detail;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->trans_date = $trans_date;
                $data->trans_no = Helper::getTransNo('USG', $trans_date, BeoneUsageHeader::class, 'trans_no');;
            }

            $data->keterangan = $keterangan;
            $data->save();
            $data->beone_usage_details()->delete();
            $data->beone_usage_details()->createMany($beone_usage_details);

            DB::commit();
            return Helper::redirect('usage.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Usage Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneUsageHeader::find($uid);
        $data->beone_usage_details()->update(['qty_in' => 0, 'qty_out' => 0]);
        $data->delete();

        return Helper::redirect('usage.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
