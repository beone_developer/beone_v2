<?php

namespace App\Http\Controllers\Admin\Inventory;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneSatuanItem;
use App\Models\BeoneStokOpnameHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AInventoryStokOpnameController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneStokOpnameHeader::with(['beone_stok_opname_details.beone_item', 'beone_stok_opname_details.beone_satuan_item', 'beone_stok_opname_details.beone_gudang'])->find($uid);
            return view('admin.menus.inventory.stok_opname.detail', compact('detail'));
        }
        return view('admin.menus.inventory.stok_opname.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.inventory.stok_opname.list');
    }

    public function postDetail(Request $request)
    {
        // $message = [
        //     'trans_date.required' => 'Date is required.'
        // ];

        // $this->validate($request, [
        //     'trans_date' => 'required',
        // ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('stok_opname_header_id');
            $message = 'Stok Opname Berhasil Dibuat';

            $data = new BeoneStokOpnameHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneStokOpnameHeader::find($uid);
                $message = 'Stok Opname Berhasil Diedit';
            }
            $keterangan = $inputs->input('keterangan');

            $beone_stok_opname_details = [];
            $detail = $inputs->input('beone_stok_opname_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_stok_opname_detail = [
                        'item_id' => $value['beone_item']->id,
                        'qty' => $value['qty'],
                        'gudang_id' => $value['beone_gudang']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                    ];
                    $beone_stok_opname_details[] = $beone_stok_opname_detail;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->stok_opname_no = Helper::getTransNo('OPNAME', $trans_date, BeoneStokOpnameHeader::class, 'stok_opname_no');
                $data->trans_date = $trans_date;

            }
            $data->keterangan = $keterangan;
            // $data->update_by = Auth::id();
            $data->save();
            $data->beone_stok_opname_details()->delete();
            $data->beone_stok_opname_details()->createMany($beone_stok_opname_details);

            DB::commit();
            return Helper::redirect('stok_opname.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Stok Opname Berhasil Dihapus';
        $uid = $request->input('stok_opname_header_id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        // $data = BeoneStokOpnameHeader::find($uid);
        // $data->beone_po_import_details()->update(['qty' => 0]);
        // $data->delete();

        return Helper::redirect('stok_opname.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['stok_opname_header_id', 'stok_opname_no', 'trans_date', 'keterangan'];
//        $model = DB::table('beone_stok_opname_header')->select($field)->whereNull('deleted_at')->orderByDesc('trans_date');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneStokOpnameHeader::query()->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('stok_opname.detail', ['id' => $item->stok_opname_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('stok_opname.delete', ['id' => $item->stok_opname_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
