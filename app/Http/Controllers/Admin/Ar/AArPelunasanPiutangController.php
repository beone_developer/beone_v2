<?php

namespace App\Http\Controllers\Admin\Ar;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneExportHeader;
use App\Models\BeonePiutangDetail;
use App\Models\BeonePiutangHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AArPelunasanPiutangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeonePiutangHeader::with(['beone_customer','beone_piutang_details'])->find($uid);
            // return $detail;
            return view('admin.menus.ar.pelunasan_piutang.detail', compact('detail'));
        }
        return view('admin.menus.ar.pelunasan_piutang.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.ar.pelunasan_piutang.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'beone_customer.required' => 'Customer required.',
        ];

        $this->validate($request, [
            'beone_customer' => 'required_without:piutang_header_id',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('piutang_header_id');
            $message = 'Pelunasan Piutang Berhasil Dibuat';

            $data = new BeonePiutangHeader();

            $beone_customer = $inputs->input('beone_customer');
            $grandtotal = $inputs->input('grandtotal');
            $bentuk_dana = $inputs->input('bentuk_dana');
            $cek_bg_no = $inputs->input('cek_bg_no');
            $setor_ke_coa_id = $inputs->input('setor_ke_coa_id');
            $bank = $inputs->input('bank');
            $no_rek = $inputs->input('no_rek');

            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeonePiutangHeader::find($uid);
                $message = 'Pelunasan Piutang Berhasil Diedit';
                $beone_customer = json_decode($data->beone_customer->id);
            }

            $beone_piutang_details = [];
            $detail = $inputs->input('beone_piutang_details');
            foreach ($detail as $key => $value) {
                if ($value['export_header_id'] <> '') {
                    $beone_export_header = BeoneExportHeader::find($value['export_header_id']);
                    $beone_piutang_detail = BeonePiutangDetail::find($value['piutang_detail_id']);
                    $mutator = 0;
                    if ($beone_piutang_detail){
                        $mutator = $beone_piutang_detail->amount;
                    }
                    $beone_export_header->update(['terbayar'=>$beone_export_header->terbayar - $mutator + $value['amount']]);

                    $beone_piutang_detail = [
                        'export_header_id' => $value['export_header_id'],
                        'amount' => $value['amount']
                    ];
                    $beone_piutang_details[] = $beone_piutang_detail;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->trans_date = $trans_date;
                $data->trans_no = Helper::getTransNo('PU', $trans_date, BeonePiutangHeader::class, 'trans_no');
                $data->customer_id = $beone_customer->id;
            }
            if ($bentuk_dana == 'transfer'){
                $data->setor_ke_coa_id = $setor_ke_coa_id->id;
            }
            $data->grandtotal = $grandtotal;
            $data->bentuk_dana = $bentuk_dana;
            $data->cek_bg_no = $cek_bg_no;
            $data->bank = $bank;
            $data->no_rek = $no_rek;
            $data->save();

            $data->beone_piutang_details()->delete();
            $data->beone_piutang_details()->createMany($beone_piutang_details);

            DB::commit();
            return Helper::redirect('pelunasan_piutang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Pelunasan Piutang Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeonePiutangHeader::find($uid);
        if ($data) {
            $data->delete();
        }
        return Helper::redirect('pelunasan_piutang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['piutang_header_id', 'item_id', 'pelunasan_no', 'pelunasan_date', 'flag', 'qty', 'satuan_qty', 'gudang_id'];
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeonePiutangHeader::query()->with(['beone_customer'])->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                '<a href="' . route('pelunasan_piutang.detail', ['id' => $item->piutang_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                    <i class="la la-edit"></i>
                </a>
                <a href="javascript:void(0);" data-url="' . route('pelunasan_piutang.delete', ['id' => $item->piutang_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                    <i class="la la-trash"></i>
                </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function IsInvalid ($uid)
    {
        return false;
    }

    public function loadPiutang(Request $request)
    {
        $customer_id = $request->customer_id;
        $export = BeoneExportHeader::with(['beone_currency', 'beone_receiver', 'beone_export_details.beone_item'])->where('receiver_id', $customer_id)->get();
        return response()->json($export);
    }
}
