<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Validator;

class ADashboardController extends Controller
{
    public function printTest(Request $request)
    {
        $data = [
            'title' => 'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'data' => BeoneImportHeader::with(['beone_supplier', 'beone_import_details.beone_item'])->get(),
        ];
        // return $data;
        $data = BeoneImportHeader::with(['beone_import_details.beone_item'])->get();
        $pdf = PDF::loadView('pdf.report-pemasukan-barang', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function getRouteList()
    {
        $getRouteCollection = Route::getRoutes();
        $list = [];
        foreach ($getRouteCollection as $route) {
            if ($route->getName() == '') continue;
            if (strpos($route->getName(), 's2') !== false) {
                continue;
            }
            if (strpos($route->getName(), 's3') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'dele') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'login') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'logout') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'print') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'load') !== false) {
                continue;
            }
            $list[] = $route->getName();
        }
        $list = collect(array_unique($list));
        $x = 28;
        foreach ($list as $i => $route) {
            echo '<br>' . "select " . ($x++) . " as modul_id, '" . ucwords(preg_replace('/[^A-Za-z0-9\-]/', ' ', $route)) . "' as modul, '" . $route . "' as url, null as icon_class union all";
        }
        return '';
    }

    public function index(Request $request)
    {
//        return $this->getRouteList();
//        $user = BeoneUser::with(['beone_roles_users.beone_moduls'])->find(2);
//        $moduls = $user->beone_roles_users->beone_moduls;
//        $g_sidebar = Helper::sidebar(request(), collect($moduls)->toArray());
//        return $g_sidebar;
//        return $user;
//        $moduls = collect($moduls)->toArray();
//        return $moduls;
//        $sidebar = Helper::sidebar($request, $moduls);
        return view('admin.menus.dashboard.index');
    }

}
