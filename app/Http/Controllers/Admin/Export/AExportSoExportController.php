<?php

namespace App\Http\Controllers\Admin\Export;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeoneSalesHeader;
use App\Models\BeoneSatuanItem;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AExportSoExportController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneSalesHeader::with(['beone_customer', 'beone_sales_details.beone_item','beone_sales_details.beone_satuan_item'])->find($uid);
            return view('admin.menus.export.so_export.detail', compact('detail'));
        }
        return view('admin.menus.export.so_export.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.export.so_export.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'trans_date.required' => 'Date is required.',
            'beone_customer.required' => 'Supplier is required.',
            'beone_sales_details.required' => 'Item is required.',
            'keterangan.min' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'trans_date' => 'required_without:sales_header_id',
            'beone_customer' => 'required',
            'beone_sales_details' => 'required',
            'keterangan' => 'max:100',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;

        DB::beginTransaction();
        try {
            $uid = $inputs->input('sales_header_id');
            $message = 'Sales Order Berhasil Dibuat';

            $data = new BeoneSalesHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneSalesHeader::find($uid);
                $message = 'Sales Order Berhasil Diedit';
            }

            $beone_customer = $inputs->input('beone_customer');
            $keterangan = $inputs->input('keterangan');
            $beone_currency = $inputs->input('beone_currency');
            $kurs = $inputs->input('kurs');

            $beone_sales_details = [];
            $detail = $inputs->input('beone_sales_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_sales_detail = [
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['qty'] * $value['price'],
                        'flag' => 1,
                    ];
                    $beone_sales_details[] = $beone_sales_detail;
                }
            }
            $grandtotal = collect($beone_sales_details)->pluck('amount')->sum();
            $grandtotal_sys = collect($beone_sales_details)->pluck('amount_sys')->sum();

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->sales_no = Helper::getTransNo('SO', $trans_date, BeoneSalesHeader::class, 'sales_no');
                $data->trans_date = $trans_date;
            }
            $data->customer_id = $beone_customer->id;
            $data->currency_id = $beone_currency->id;
            $data->kurs = $kurs;
            $data->keterangan = $keterangan;
            $data->grandtotal = $grandtotal;
            $data->grandtotal_sys = $grandtotal_sys;
            $data->flag = 1;
            $data->realisasi = 0;
            $data->update_by = Auth::id();
            $data->save();
            $data->beone_sales_details()->delete();
            $data->beone_sales_details()->createMany($beone_sales_details);

            DB::commit();
            return Helper::redirect('so_export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Sales Order Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneSalesHeader::find($uid);
        $data->beone_sales_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('so_export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneSalesHeader::query()->with(['beone_customer'])->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);;
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('so_export.detail', ['id' => $item->sales_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('so_export.delete', ['id' =>  $item->sales_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>
                    <a href="' . route('so_export.print', ['id' => $item->sales_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>
                    <a href="' . route('olap.print_out.view', ['print' => 'SalesOrder', 'id' => $item->sales_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('level_item', '>=', '2')->orWhere('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function isInvalid($uid)
    {
        $data = BeoneSalesHeader::with(['beone_sales_details.beone_export_details.beone_export_header'])->find($uid);
        $result = '';
        foreach ($data->beone_sales_details as $i => $val) {
            if (isset($val->beone_export_details)) {
                $result = 'Sales Order sudah pernah di realisasi di (' . $val->beone_export_details->beone_export_header->invoice_no . ')';
                break;
            }
        }
        return $result;
    }

    public function searchSo(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneSalesHeader::where('sales_no', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function print(Request $request)
    {
        $uid = $request->input('id');
        $detail = BeoneSalesHeader::with(['beone_customer', 'beone_sales_details.beone_item'])->find($uid);
        $data = [
            'title' => 'Sales Order',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'detail' => $detail,
        ];
        // return $data;
        $pdf = PDF::loadView('pdf.export.report-sale-order-export', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
