<?php

namespace App\Http\Controllers\Admin\Export;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneExportHeader;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeoneSatuanItem;
use App\Models\TpbHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AExportKirimController extends Controller
{
    public function indexDetail(Request $request)
    {
        $bc_id = $request->input('bc_id');
        if ($bc_id) {
            $detail = BeoneExportHeader::with(['beone_sales_order', 'beone_import_details.beone_item', 'tpb_header', 'tpb_header.tpb_barangs'])->Where('bc_header_id', $bc_id)->first();
            if (!$detail) {
                $detail = TpbHeader::with(['tpb_barangs'])->find($bc_id);
            }
            return view('admin.menus.export.export.kirim', compact('detail'));
        }
        return view('admin.menus.export.export.list');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.export.export.list');
    }

    public function postDetailKirim(Request $request)
    {
        $_settings = BeoneKonfigurasiPerusahaan::first();
        $message = [
            'invoice_date.required' => 'Date is required.',
            'tpb_id.required' => 'TPB is required.',
        ];

        $this->validate($request, [
            'invoice_date' => 'required_without:import_header_id',
            'tpb_id' => 'required',
            'beone_sales_header' => 'required',
            'beone_customer' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();

        DB::beginTransaction();
        try {
            $uid = $inputs->input('import_header_id');
            $message = 'Sending Export Berhasil Dibuat';

            $data = new BeoneExportHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneExportHeader::find($uid);
                $message = 'Sending Export Berhasil Diedit';
            }

            $tpb_id = $inputs->input('tpb_id');
            $tpb = TpbHeader::with(['tpb_barangs'])->find($tpb_id);

            $bc_nama_vendor = $tpb->NAMA_PEMASOK;
            $bc_nama_pemilik = $tpb->NAMA_PEMILIK;
            $jenis_bc = $tpb->KODE_DOKUMEN_PABEAN;
            $bc_no_aju = $tpb->NOMOR_AJU;
            $bc_no = $tpb->NOMOR_DAFTAR;
            $bc_date = $tpb->TANGGAL_DAFTAR;
            $bc_nama_penerima_barang = $tpb->NAMA_PENERIMA_BARANG;
            $bc_nama_pengangkut = $tpb->NAMA_PENGANGKUT;
            $bc_kurs = ($tpb->NDPBM ? $tpb->NDPBM : 1);
            $bc_nama_pengirim = $tpb->NAMA_PENGIRIM;
            $bc_header_id = $tpb_id;

            $beone_customer = $inputs->input('beone_customer');
            $beone_gudang = $inputs->input('beone_gudang');
            $beone_sales_header = "";
            $beone_sales_header = $inputs->input('beone_sales_header');
            $beone_currency = $inputs->input('beone_currency');
            $isppn = $inputs->has('isppn');

            $beone_import_details = [];
            $detail = $inputs->input('tpb_barangs');
            foreach ($detail as $key => $value) {
                $item_id = 0;
                $purchase_detail_id = 0;
                $item_id = $value['beone_item']->id;
                $beone_sales_header_id = $beone_sales_header->id;
                if ($value['uraian'] <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_import_detail = [
                        'gudang_id' => $beone_gudang->id,
                        'item_id' => $item_id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'isppn' => $isppn,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['amount_sys'],
                        'amount_ppn' => $value['amount_ppn'],
                        'amount_ppn_sys' => $value['amount_ppn_sys'],
                        'flag' => 1,
                        'bc_barang_id' => $value['bc_barang_id'],
                        'purchase_detail_id' => $purchase_detail_id,
                    ];
                    $beone_import_details[] = $beone_import_detail;
                }
            }

            $dpp = $inputs->input('dpp');
            $ppn = $inputs->input('ppn');

            $dpp_sys = $inputs->input('dpp_sys');
            $ppn_sys = $inputs->input('ppn_sys');
            $discount = $inputs->input('discount');

            $grandtotal = $inputs->input('grand_total');
            $grandtotal_sys = $inputs->input('grand_total_sys');

            if (!$uid) {
                $invoice_date = Carbon::createFromDate($inputs->input('invoice_date'));
                $invoice_no = Helper::getTransNo('INV', $invoice_date, BeoneExportHeader::class, 'invoice_no');

                $data->bc_no = $bc_no;
                $data->invoice_no = $invoice_no;
//                $data->receive_no = $invoice_no;
                $data->invoice_date = $invoice_date;
//                $data->receive_date = $invoice_date;

                $data->bc_no_aju = $bc_no_aju;
                $data->bc_date = $bc_date;
                $data->jenis_bc = $jenis_bc;

                $data->bc_nama_vendor = $bc_nama_vendor;
                $data->bc_nama_pemilik = $bc_nama_pemilik;
                $data->bc_nama_pengirim = $bc_nama_pengirim;
                $data->bc_nama_penerima_barang = $bc_nama_penerima_barang;
                $data->bc_nama_pengangkut = $bc_nama_pengangkut;
                $data->bc_header_id = $bc_header_id;
            }

            $data->receiver_id = $beone_customer->id;
            $data->currency_id = $beone_currency->id;

            // if (!$_settings->receiving_multi_po) {
            //     $data->beone_sales_header_id = $beone_sales_header_id->id;
            // }
            $data->ppn = $ppn;
            $data->dpp = $dpp;
            $data->ppn_sys = $ppn_sys;
            $data->dpp_sys = $dpp_sys;
            $data->grandtotal = $grandtotal;
            $data->grandtotal_sys = $grandtotal_sys;
            $data->discount = $discount;
            $data->bc_kurs = $bc_kurs;
            $data->flag = 1;
            $data->update_by = Auth::id();
            $data->save();
            $data->beone_export_details()->delete();
            $data->beone_export_details()->createMany($beone_import_details);

            DB::commit();
            return Helper::redirect('export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return $e->getMessage();
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Data Export Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneExportHeader::find($uid);
        $data->beone_export_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneExportHeader::query()->whereBetween('invoice_date', [$tgl_awal, $tgl_akhir]);;;
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('export/export/detail?id=' . $item->export_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('export/export/del?id=' . $item->export_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('level_item', '>=', '2')->orWhere('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchPo(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneExportHeader::where('sales_no', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
