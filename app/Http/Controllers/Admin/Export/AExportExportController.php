<?php

namespace App\Http\Controllers\Admin\Export;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneExportHeader;
use App\Models\BeoneSalesDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AExportExportController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneExportHeader::with(['beone_currency', 'beone_receiver', 'beone_export_details.beone_item'])->find($uid);
            return view('admin.menus.export.export.detail', compact('detail'));
        }
        return view('admin.menus.export.export.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.export.export.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'invoice_date.required' => 'Date is required.',
            'beone_receiver.required' => 'Customer is required.',
            'sales_headers.required' => 'Sales Order is required.',
        ];

        $this->validate($request, [
            'invoice_date' => 'required_withou  t:export_header_id',
            'beone_receiver' => 'required',
            'sales_headers' => 'required',
            'gudang' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('export_header_id');
            $message = 'Data Export Berhasil Dibuat';

            $data = new BeoneExportHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneExportHeader::find($uid);
                $message = 'Data Export Berhasil Diedit';
            }
            $beone_receiver = $inputs->input('beone_receiver');
            $kurs = $inputs->input('kurs');
            $detail = $inputs->input('sales_headers');
            $beone_sales_headers_id = collect($detail)->pluck('beone_sales_header.id');
            $beone_currency = $inputs->input('beone_currency');

            $car_no = $inputs->input('car_no');
            $price_type = $inputs->input('price_type');
            $bc_no = $inputs->input('bc_no');
            $freight = $inputs->input('freight');
            $valas_value = $inputs->input('valas_value');
            $jenis_bc = $inputs->input('jenis_bc');
            $insurance_value = $inputs->input('insurance_value');;
            $gudang = $inputs->input('gudang');

            $beone_export_details = [];
            $sales_order = BeoneSalesDetail::with(['beone_sales_header'])->whereIn('sales_header_id', $beone_sales_headers_id)->get();
            foreach ($sales_order as $key => $value) {
                $beone_export_detail = [
                    'item_id' => $value->item_id,
                    'qty' => $value->qty,
                    'price' => $value->price,
                    'amount' => $value->amount,
                    'subtotal' => $value->qty * $value->price,
                    'sales_header_id' => $value->sales_header_id,
                    'sales_detail_id' => $value->sales_detail_id,
                    'gudang_id' => $gudang->id,
                    'satuan_id' => $value->satuan_id,
                    'flag' => 1,
                ];
                $beone_export_details[] = $beone_export_detail;
            }

            if (!$uid) {
                $invoice_date = Carbon::createFromDate($inputs->input('invoice_date'));
                $invoice_no = Helper::getTransNo('INV', $invoice_date, BeoneExportHeader::class, 'invoice_no');
                $data->invoice_date = $invoice_date;
                $data->invoice_no = $invoice_no;
                $data->bc_no = $bc_no;
                $data->bc_date = $invoice_date;
            }

            $data->receiver_id = $beone_receiver->id;
            $data->jenis_bc = $jenis_bc;
            $data->valas_value = $valas_value;
            $data->car_no = $car_no;
            $data->freight = $freight;
            $data->price_type = $price_type;
            $data->insurance_value = $insurance_value;
            $data->currency_id = $beone_currency->id;
            $data->kurs = $kurs;
            $data->flag = 1;
            $data->update_by = Auth::id();
            $data->save();
            $data->beone_export_details()->delete();
            $data->beone_export_details()->createMany($beone_export_details);

            DB::commit();
            return Helper::redirect('export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Data Export Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneExportHeader::find($uid);
        $data->beone_export_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('export.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneExportHeader::query()->whereBetween('invoice_date', [$tgl_awal, $tgl_akhir]);;;
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    (!isset($item->export_header_id) ? '<a href="' . route('export.detail', ['id' => $item->export_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                       <i class="la la-edit"></i>
                   </a>' : '') .
                    '
                    <a href="javascript:void(0);" data-url="' . url('export/export/del?id=' . $item->export_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('level_item', '>=', '2')->orWhere('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchPo(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneExportHeader::where('sales_no', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
