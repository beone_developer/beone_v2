<?php

namespace App\Http\Controllers\Admin\Export;

use App\Http\Controllers\Controller;
use App\Models\BeoneExportHeader;
use App\Models\TpbHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AExportOutstanding25Controller extends Controller
{
    public function indexOutstanding(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTableOutstanding($request);
        }
        return view('admin.menus.export.export.outstanding_25');
    }


    public function dataTableOutstanding(Request $request)
    {
        $beone_import_header = BeoneExportHeader::whereNotNull('bc_no_aju')->get();
        $model = TpbHeader::query()->whereNotIn('NOMOR_AJU', $beone_import_header->pluck('bc_no_aju'))->whereNotNull('NOMOR_DAFTAR')->whereIn('KODE_DOKUMEN_PABEAN', [25]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('export.kirim', ['bc_id' => $item->ID]) . '" class="btn btn-sm btn-label-brand btn-bold" title="View">
                    Kirim
                    </a>';
            })
            ->filterColumn('JENIS', function ($query, $keyword) {
                $sql = "concat('BC ',KODE_DOKUMEN_PABEAN) like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('SUPPLIER', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('AMOUNT', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('CURRENCY', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then 'USD' else 'IDR' end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('SUPPLIER', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end'), $order);
            })
            ->orderColumn('JENIS', function ($query, $order) {
                $query->orderBy(DB::raw('concat("BC ",KODE_DOKUMEN_PABEAN)'), $order);
            })
            ->orderColumn('AMOUNT', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end'), $order);
            })
            ->orderColumn('CURRENCY', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then "USD" else "IDR" end'), $order);
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
