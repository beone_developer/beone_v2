<?php

namespace App\Http\Controllers\Admin\SubKontraktor;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneCurrency;
use App\Models\BeoneGudang;
use App\Models\BeoneSatuanItem;
use App\Models\BeoneSubKontraktorInHeader;
use App\Models\TpbHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class ASubKontraktorInController extends Controller
{
    public function indexDetailTerima(Request $request)
    {
//        return $request->session()->all();
        $bc_id = $request->input('bc_id');
        if ($bc_id) {
            $detail = TpbHeader::with(['tpb_barangs'])->find($bc_id);
            return view('admin.menus.subkontraktor.in.terima', compact('detail'));
        }
        return redirect()->route('sub_kontraktor.in.outstanding');
    }

    public function indexOutstanding(Request $request)
    {
//        return $request->session()->all();
        if ($request->ajax()) {
            return $this->dataTableOutstanding($request);
        }
        return view('admin.menus.subkontraktor.in.outstanding');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.subkontraktor.in.list');
    }

    public function postDetailTerima(Request $request)
    {
        $message = [
            'trans_date.required' => 'Date is required.',
            'tpb_id.required' => 'TPB is required.',
        ];

        $this->validate($request, [
            'trans_date' => 'required_without:sub_kontraktor_in_header_id',
            'tpb_id' => 'required',
            'beone_supplier' => 'required',
            'beone_sub_kontraktor_out_header' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('sub_kontraktor_in_header_id');
            $message = 'Sub Kontraktor Out Berhasil Dibuat';

            $data = new BeoneSubKontraktorInHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneSubKontraktorInHeader::find($uid);
                $message = 'Sub Kontraktor Out Berhasil Diedit';
            }

            $tpb_id = $inputs->input('tpb_id');
            $tpb = TpbHeader::with(['tpb_barangs'])->find($tpb_id);

            $bc_nama_vendor = $tpb->NAMA_PEMASOK;
            $bc_nama_pemilik = $tpb->NAMA_PEMILIK;
            $jenis_bc = $tpb->KODE_DOKUMEN_PABEAN;
            $bc_no_aju = $tpb->NOMOR_AJU;
            $bc_no = $tpb->NOMOR_DAFTAR;
            $bc_date = $tpb->TANGGAL_DAFTAR;
            $bc_nama_penerima_barang = $tpb->NAMA_PENERIMA_BARANG;
            $bc_nama_pengangkut = $tpb->NAMA_PENGANGKUT;
            $bc_kurs = ($tpb->NDPBM ? $tpb->NDPBM : 1);
            $bc_nama_pengirim = $tpb->NAMA_PENGIRIM;
            $bc_header_id = $tpb_id;

            $beone_supplier = $inputs->input('beone_supplier');
            $beone_gudang = $inputs->input('beone_gudang');
            $keterangan = $inputs->input('keterangan');
            $beone_sub_kontraktor_out_header = $inputs->input('beone_sub_kontraktor_out_header');
            $isppn = $inputs->has('isppn');

            $beone_sub_kontraktor_in_details = [];
            $detail = $inputs->input('tpb_barangs');
            foreach ($detail as $key => $value) {
                $item_id = $value['beone_item']->id;
                if ($value['uraian'] <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_sub_kontraktor_in_detail = [
                        'gudang_id' => $beone_gudang->id,
                        'item_id' => $item_id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'isppn' => $isppn,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['amount_sys'],
                        'amount_ppn' => $value['amount_ppn'],
                        'amount_ppn_sys' => $value['amount_ppn_sys'],
                        'bc_barang_id' => $value['bc_barang_id'],
                        'bc_barang_uraian' => $value['uraian']
                    ];
                    $beone_sub_kontraktor_in_details[] = $beone_sub_kontraktor_in_detail;
                }
            }

            $dpp = $inputs->input('dpp');
            $ppn = $inputs->input('ppn');

            $dpp_sys = $inputs->input('dpp_sys');
            $ppn_sys = $inputs->input('ppn_sys');
            $discount = $inputs->input('discount');

            $grandtotal = $inputs->input('grand_total');
            $grandtotal_sys = $inputs->input('grand_total_sys');

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $trans_no = Helper::getTransNo('SKO', $trans_date, BeoneSubKontraktorInHeader::class, 'trans_no');

                $data->bc_no = $bc_no;
                $data->trans_no = $trans_no;
                $data->trans_date = $trans_date;

                $data->bc_no_aju = $bc_no_aju;
                $data->bc_date = $bc_date;
                $data->jenis_bc = $jenis_bc;

                $data->bc_nama_vendor = $bc_nama_vendor;
                $data->bc_nama_pemilik = $bc_nama_pemilik;
                $data->bc_nama_pengirim = $bc_nama_pengirim;
                $data->bc_nama_penerima_barang = $bc_nama_penerima_barang;
                $data->bc_nama_pengangkut = $bc_nama_pengangkut;
                $data->bc_header_id = $bc_header_id;
                $data->sub_kontraktor_out_header_id = $beone_sub_kontraktor_out_header->id;
            }

            $data->supplier_id = $beone_supplier->id;
            $data->keterangan = $keterangan;
            $data->ppn = $ppn;
            $data->dpp = $dpp;
            $data->ppn_sys = $ppn_sys;
            $data->dpp_sys = $dpp_sys;
            $data->grandtotal = $grandtotal;
            $data->grandtotal_sys = $grandtotal_sys;
            $data->discount = $discount;
            $data->save();
            $data->beone_sub_kontraktor_in_details()->delete();
            $data->beone_sub_kontraktor_in_details()->createMany($beone_sub_kontraktor_in_details);

            DB::commit();
            return Helper::redirect('sub_kontraktor.in.outstanding', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return $e->getMessage();
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Sub Kontraktor In Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneSubKontraktorInHeader::find($uid);
        $data->beone_sub_kontraktor_in_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('sub_kontraktor.in.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTableOutstanding(Request $request)
    {
        $beone_sub_kontraktor_in_header = BeoneSubKontraktorInHeader::whereNotNull('bc_no_aju')->get();
        $model = TpbHeader::query()->with(['tpb_jaminan'])->whereHas('tpb_jaminan')->whereNotIn('NOMOR_AJU', $beone_sub_kontraktor_in_header->pluck('bc_no_aju'))
            ->where('KODE_DOKUMEN_PABEAN','=', 262)
            ->where('TANGGAL_AJU','>=','2019-01-01');//->limit(10);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('sub_kontraktor.in.terima', ['bc_id' => $item->ID]) . '" class="btn btn-sm btn-label-brand btn-bold" title="View">
                    Terima
                    </a>';
            })
            ->filterColumn('JENIS', function ($query, $keyword) {
                $sql = "concat('BC ',KODE_DOKUMEN_PABEAN) like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('SUPPLIER', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('AMOUNT', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('CURRENCY', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then 'USD' else 'IDR' end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('SUPPLIER', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end'), $order);
            })
            ->orderColumn('JENIS', function ($query, $order) {
                $query->orderBy(DB::raw('concat("BC ",KODE_DOKUMEN_PABEAN)'), $order);
            })
            ->orderColumn('AMOUNT', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end'), $order);
            })
            ->orderColumn('CURRENCY', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then "USD" else "IDR" end'), $order);
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneSubKontraktorInHeader::query()->with(['beone_supplier', 'beone_currency'])->whereNotNull('bc_no_aju')->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
//                    ( isset($item->bc_header_id) ? '<a href="' . route('receive_import.detail', ['id' => $item->import_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
//                        <i class="la la-edit"></i>
//                    </a>' : '' ).
                    '<a href="javascript:void(0);" data-url="' . route('sub_kontraktor.in.delete', ['id' => $item->sub_kontraktor_in_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->setRowClass(function ($item) {
                return isset($item->bc_header_id) ? '' : 'alert-danger';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchCurrency(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCurrency::where('currency_code', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchGudang(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::where('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

}
