<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AStokPerGudangController extends Controller
{
    public function print(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $query = "select bi.item_id,
       i.nama,
       i.item_code,
       si.satuan_code,
       sum(coalesce(saldo_awal_qty, 0))                                    as saldo_awal_qty,
       sum(bi.qty_in)                                                         total_in,
       sum(bi.qty_out)                                                        total_out,
       sum(coalesce(saldo_awal_qty, 0)) + sum(bi.qty_in) - sum(bi.qty_out) as saldo_akhir_qty
        from beone_inventory bi
                 join beone_item i on bi.item_id = i.item_id
                 left join beone_satuan_item si on i.satuan_id = si.satuan_id
                 left join (select s.item_id, sum(coalesce(s.qty_in, 0) - coalesce(s.qty_out, 0)) saldo_awal_qty
                            from beone_inventory s
                            where s.trans_date < '$tgl_awal'
                            group by s.item_id) s
                           on bi.item_id = s.item_id
        where bi.trans_date between '$tgl_awal' and '$tgl_akhir'
        group by bi.item_id,
         i.nama,
         i.item_code,
         si.satuan_code";
        $res = DB::select(DB::raw($query));
        $data = [
            'title' => 'LAPORAN PERTANGGUNGJAWABAN BAHAN BAKU DAN PENOLONG',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'tglawal' => $tgl_awal,
            'tglakhir' => $tgl_akhir,
            'data' => $res,
        ];
        $pdf = PDF::loadView('pdf.bahanbaku.report-mutasi-bahan-baku', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        //return $this->print($request);
        return view('pdf.bahanbaku.filter');
    }

    public function postFilter(Request $request)
    {
        return $this->print($request);
    }

}
