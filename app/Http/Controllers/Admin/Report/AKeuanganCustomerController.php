<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class AKeuanganCustomerController extends Controller
{
    public function getQuery(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_akhir = $inputs->input('filter_akhir_date');
        $query ="select h.invoice_no, h.invoice_date, h.sisabayar, h.terbayar, h.grandtotal,h.kurs as kurs,c.currency_code as currency, s.nama as customer, h.bc_no as no_aju
                from beone_export_header h,
                     beone_currency c,
                     beone_customer s
                where h.currency_id = c.currency_id and h.receiver_id = s.customer_id and h.invoice_date <= '$tgl_akhir'
                and h.deleted_at is null and h.sisabayar >0
                order by h.receiver_id limit 10";

        return Helper::selectMany($query);
    }

    public function attemptPrint(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_akhir = $inputs->input('filter_akhir_date');

        $data = [
            'title' => 'LAPORAN PIUTANG CUSTOMER',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' .$tgl_akhir,
            'tglakhir' => $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.supplier.report-customer-piutang', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.customer.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
