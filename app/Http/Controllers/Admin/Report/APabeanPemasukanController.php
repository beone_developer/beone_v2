<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use DB;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class APabeanPemasukanController extends Controller
{
    public function getQuery(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $bc_no = $request->input('bc_no');
        $query = "select ROW_NUMBER() OVER (ORDER BY d.item_id asc) as no,
        concat('BC ',h.jenis_bc) as jenis_bc, h.bc_no,h.bc_date, h.receive_no, h.receive_date, s.nama as supplier, i.item_code,
        i.nama,si.satuan_code,d.qty,c.currency_code,d.amount
        from beone_import_header h
        join beone_import_detail d on h.import_header_id = d.import_header_id
        join beone_item i on d.item_id = i.item_id
        join beone_supplier s on s.supplier_id = h.supplier_id
        join beone_satuan_item si on d.satuan_id = si.satuan_id
        join beone_currency c on c.currency_id = h.currency_id
        where h.bc_date between '$tgl_awal' and '$tgl_akhir' and h.jenis_bc = '$bc_no'
        union all
select ROW_NUMBER() OVER (ORDER BY d.item_id asc) as no,
       concat('BC ', h.jenis_bc)                  as jenis_bc,
       h.bc_no,
       h.bc_date,
       h.trans_no,
       h.trans_date,
       s.nama                                     as supplier,
       i.item_code,
       i.nama,
       si.satuan_code,
       d.qty,
       c.currency_code,
       d.amount
from beone_sub_kontraktor_in_header h
         join beone_sub_kontraktor_in_detail d on h.sub_kontraktor_in_header_id = d.sub_kontraktor_in_header_id
         join beone_item i on d.item_id = i.item_id
         join beone_supplier s on s.supplier_id = h.supplier_id
         join beone_satuan_item si on d.satuan_id = si.satuan_id
         join beone_currency c on c.currency_id = h.currency_id
        where h.bc_date between '$tgl_awal' and '$tgl_akhir' and h.jenis_bc = '$bc_no';";
        return DB::select(DB::raw($query));
    }

    public function attemptPrint(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $data = [
            'title' => 'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.pemasukkan.report-pemasukan-barang', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.pemasukkan.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
