<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class AKeuanganBukuBesarController extends Controller
{
    public function getQuery(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_awal = $inputs->input('filter_awal_date');
        $tgl_akhir = $inputs->input('filter_akhir_date');

        $beone_coa = $inputs->input('beone_coa');
        $beone_coa = (isset($beone_coa->id) ? $beone_coa->id : "All");
        $query = "
        drop table if exists temp_gl_awal;
        drop table if exists temp_gl_akhir;
        drop table if exists temp_gl;
        drop table if exists temp_gl_final;

        select x.r_coa_id      as coa_id,
               x.r_trans_no    as trans_no,
               x.r_trans_date  as trans_date,
               x.r_keterangan  as keterangan,
               x.r_currency_id as currency_id,
               x.r_kurs        as kurs,
               x.r_debet       as debet,
               x.r_kredit      as kredit,
               x.r_debet_idr   as debet_idr,
               x.r_kredit_idr  as kredit_idr
        into temporary temp_gl
        from general_ledger('2017-01-01', '$tgl_akhir') x;

        select iv.coa_id, coalesce(sum(debet_idr - kredit_idr), 0) as saldo_akhir
        into temporary temp_gl_awal
        from temp_gl iv
        where iv.trans_date < '$tgl_awal'
          and cast(iv.coa_id as varchar) ilike '$beone_coa'
        group by iv.coa_id;

        select 1            as priority,
               coa_id       as coa_id,
               'SALDO AWAL' as trans_no,
               null         as trans_date,
               'SALDO AWAL' as keterangan,
               0            as debet_idr,
               0            as kredit_idr,
               saldo_akhir  as saldo_akhir_idr
        into temporary temp_gl_akhir
        from temp_gl_awal
        union all
        select 2                              as priority,
               iv.coa_id                      as coa_id,
               iv.trans_no                    as trans_no,
               iv.trans_date                  as trans_date,
               iv.keterangan                  as keterangan,
               iv.debet_idr                   as debet_idr,
               iv.kredit_idr                  as kredit_idr,
               (iv.debet_idr - iv.kredit_idr) as saldo_akhir_idr
        from temp_gl iv
        where iv.trans_date between '$tgl_awal' and '$tgl_akhir'
          and cast(iv.coa_id as varchar) ilike '$beone_coa';

        select row_number() over (order by coa_id, priority, trans_date, trans_no, debet_idr, kredit_idr)        as urutan,
               coa_id,
               trans_date,
               trans_no,
               keterangan,
               debet_idr,
               kredit_idr,
               sum(saldo_akhir_idr)
               over (partition by coa_id order by coa_id, priority, trans_date, trans_no, debet_idr, kredit_idr) as saldo_akhir_idr
        into temporary temp_gl_final
        from temp_gl_akhir
        order by coa_id, priority, trans_date, trans_no, debet_idr, kredit_idr;

        select f.urutan,
               c.nomor,
               c.nama,
               f.trans_date,
               f.trans_no,
               f.keterangan,
               f.debet_idr,
               f.kredit_idr,
               f.saldo_akhir_idr
        from temp_gl_final f
         join beone_coa c on f.coa_id = c.coa_id;";
        return Helper::selectMany($query);
    }

    public function attemptPrint(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_awal = $inputs->input('filter_awal_date');
        $tgl_akhir = $inputs->input('filter_akhir_date');
        $beone_coa = $inputs->input('beone_coa');
        $beone_coa = (isset($beone_coa->id) ? $beone_coa->id : "All");

        $data = [
            'title' => 'LAPORAN BUKU BESAR',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'coa' => $beone_coa,
            'tglawal' => $tgl_awal,
            'tglakhir' => $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.buku_besar.report-buku-besar', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.buku_besar.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
