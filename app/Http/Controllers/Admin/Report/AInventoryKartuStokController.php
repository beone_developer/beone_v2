<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneGudang;
use App\Models\BeoneItem;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Validator;

class AInventoryKartuStokController extends Controller
{
    public function print(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_awal = $inputs->input('filter_awal_date');
        $tgl_akhir = $inputs->input('filter_akhir_date');
        $beone_item = $inputs->input('beone_item');
        $beone_gudang = $inputs->input('beone_gudang');

        $query = "
        DROP TABLE IF EXISTS temp_stok_awal;
        SELECT coalesce(sum(qty_in - qty_out), 0) as stok_akhir
        INTO TEMPORARY temp_stok_awal
        FROM beone_inventory iv
        WHERE iv.trans_date < '$tgl_awal'
          AND cast(iv.gudang_id as varchar) ilike '$beone_gudang->id'
          AND cast(iv.item_id as varchar) ilike '$beone_item->id';

        SELECT 1                                       as priority,
               'STOK AWAL'                             as trans_no,
               null                                    as trans_date,
               'STOK AWAL'                             as keterangan,
               ''                                      as item_code,
               ''                                      as nama,
               0                                       as qty_in,
               0                                       as qty_out,
               (select stok_akhir from temp_stok_awal) as stok_akhir
        FROM temp_stok_awal
        UNION ALL
        SELECT ROW_NUMBER() OVER (ORDER BY trans_date asc, qty_in desc) + 1              as priority,
               iv.intvent_trans_no                                          as trans_no,
               iv.trans_date                                                as trans_date,
               iv.keterangan                                                as keterangan,
               i.item_code,
               g.nama,
               qty_in,
               qty_out,
               (qty_in - qty_out) as stok_akhir
        FROM beone_item i
                 LEFT JOIN beone_inventory iv on i.item_id = iv.item_id
                 LEFT JOIN beone_gudang g on g.gudang_id = iv.gudang_id
        WHERE iv.trans_date between '$tgl_awal' and '$tgl_akhir'
          AND cast(iv.gudang_id as varchar) ilike '$beone_gudang->id'
          AND cast(iv.item_id as varchar) ilike '$beone_item->id'
          ORDER BY priority asc";

        $res = Helper::selectMany($query);
//        return $res;
        $data = [
            'title' => 'LAPORAN KARTU STOK',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'item' => BeoneItem::find($beone_item->id),
            'gudang' => BeoneGudang::find($beone_gudang->id),
            'tglawal' => $tgl_awal,
            'tglakhir' => $tgl_akhir,
            'data' => $res,
        ];
        $pdf = PDF::loadView('pdf.kartu_stok.report-kartu-stok', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        return view('pdf.kartu_stok.filter');
    }

    public function postFilter(Request $request)
    {
        return $this->print($request);
    }

}
