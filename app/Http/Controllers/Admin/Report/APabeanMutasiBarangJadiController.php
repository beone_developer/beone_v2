<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class APabeanMutasiBarangJadiController extends Controller
{
    public function getQuery(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $query = "select bi.item_id,
        i.nama,
        i.item_code,
        si.satuan_code,
        sum(coalesce(saldo_awal_qty, 0))                                    as saldo_awal_qty,
        sum(bi.qty_in)                                                         total_in,
        sum(bi.qty_out)                                                        total_out,
        '0'                                                        as penyesuaian,
        sum(coalesce(saldo_awal_qty, 0)) + sum(bi.qty_in) - sum(bi.qty_out) as saldo_akhir_qty,
        '-'                                                        as stok_opname,
        '-'                                                        as selisih,
        '-'                                                        as keterangan
            from beone_inventory bi
                    join beone_item i on bi.item_id = i.item_id
                    left join beone_satuan_item si on i.item_id = si.item_id and si.rasio = 1
                    left join (select s.item_id, sum(coalesce(s.qty_in, 0) - coalesce(s.qty_out, 0)) saldo_awal_qty
                    from beone_inventory s
                    where s.trans_date < '$tgl_awal'
                    group by s.item_id) s
                    on bi.item_id = s.item_id
            where bi.trans_date between '$tgl_awal' and '$tgl_akhir'
            and bi.gudang_id = 2
            group by bi.item_id,
            i.nama,
            i.item_code,
            si.satuan_code";
        return DB::select(DB::raw($query));
    }

    public function attemptPrint(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $data = [
            'title' => 'LAPORAN PERTANGGUNGJAWABAN BARANG JADI',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'tglawal' => $tgl_awal,
            'tglakhir' => $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.barangjadi.report-mutasi-barang-jadi', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.barangjadi.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
