<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class AKeuanganSupplierController extends Controller
{
    public function getQuery(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_akhir = $inputs->input('filter_akhir_date');
        $query ="select h.import_no, h.trans_date, h.sisabayar, h.terbayar, h.grandtotal,h.bc_kurs as kurs,c.currency_code as currency, s.nama as supplier, h.bc_no_aju as no_aju
                from beone_import_header h,
                     beone_currency c,
                     beone_supplier s
                where h.currency_id = c.currency_id and h.supplier_id = s.supplier_id and h.trans_date <= '$tgl_akhir'
                and h.deleted_at is null and h.sisabayar >0
                order by h.supplier_id";

        return Helper::selectMany($query);
    }

    public function attemptPrint(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_akhir = $inputs->input('filter_akhir_date');

        $data = [
            'title' => 'LAPORAN HUTANG SUPPLIER',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' .$tgl_akhir,
            'tglakhir' => $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.supplier.report-supplier-hutang', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.supplier.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
