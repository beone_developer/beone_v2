<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use DB;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class APabeanMutasiWipController extends Controller
{
    public function getQuery(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        // $res = BeoneInventory::qwith(['beone_item'])->where('gudang_id', 3)
        //     ->whereBetween('trans_date', [$tgl_awal, $tgl_akhir])
        //     ->get();
            $query = "
            select
            ROW_NUMBER() OVER (ORDER BY bi.item_id asc) as no,
            bi.item_id,
            i.nama,
            i.item_code,
            si.satuan_code,
            sum(coalesce(saldo_awal_qty, 0)) + sum(bi.qty_in) - sum(bi.qty_out) as saldo_akhir_qty,
            '-'                                                        as keterangan
                from beone_inventory bi
                        join beone_item i on bi.item_id = i.item_id
                        left join beone_satuan_item si on i.item_id = si.item_id and si.rasio = 1
                        left join (select s.item_id, sum(coalesce(s.qty_in, 0) - coalesce(s.qty_out, 0)) saldo_awal_qty
                        from beone_inventory s
                        where s.trans_date < '$tgl_awal'
                        group by s.item_id) s
                        on bi.item_id = s.item_id
                where bi.trans_date between '$tgl_awal' and '$tgl_akhir'
                and bi.gudang_id = 3
                group by bi.item_id,
                i.nama,
                i.item_code,
                si.satuan_code";
            return DB::select(DB::raw($query));
        // return $res;
    }


    public function attemptPrint(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $data = [
            'title' => 'LAPORAN POSISI BARANG DALAM PROSES (WIP)',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.wip.report-mutasi-wip', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        // return $request->all();
        return view('pdf.wip.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }
}
