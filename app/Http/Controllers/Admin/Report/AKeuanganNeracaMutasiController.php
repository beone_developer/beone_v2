<?php

namespace App\Http\Controllers\Admin\Report;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class AKeuanganNeracaMutasiController extends Controller
{
    public function getQuery(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_awal = $inputs->input('filter_awal_date');
        $tgl_akhir = $inputs->input('filter_akhir_date');

        $query = "
        drop table if exists temp_gl;
        drop table if exists temp_gl_awal;
        drop table if exists temp_gl_akhir;
        drop table if exists temp_gl_mutasi;
        drop table if exists temp_gl_final;
        drop table if exists temp_gl_balance;

        select x.r_coa_id      as coa_id,
               x.r_trans_no    as trans_no,
               x.r_trans_date  as trans_date,
               x.r_keterangan  as keterangan,
               x.r_currency_id as currency_id,
               x.r_kurs        as kurs,
               x.r_debet       as debet,
               x.r_kredit      as kredit,
               x.r_debet_idr   as debet_idr,
               x.r_kredit_idr  as kredit_idr
        into temporary temp_gl
        from general_ledger('2017-01-01', '$tgl_akhir') x;

        select iv.coa_id, sum(debet_idr) as debet_awal, sum(kredit_idr) as kredit_awal
        into temporary temp_gl_awal
        from temp_gl iv
        where iv.trans_date < '$tgl_awal'
          and cast(iv.coa_id as varchar) ilike '%'
        group by iv.coa_id;

        select iv.coa_id, sum(debet_idr) as debet, sum(kredit_idr) as kredit
        into temporary temp_gl_akhir
        from temp_gl iv
        where iv.trans_date between '$tgl_awal' and '$tgl_akhir'
          and cast(iv.coa_id as varchar) ilike '%'
        group by iv.coa_id;

        select c.coa_id,
               c.nomor,
               c.nama,
               coalesce(a.debet_awal, 0)                          as debet_awal,
               coalesce(a.kredit_awal, 0)                         as kredit_awal,
               coalesce(b.debet, 0)                               as debet,
               coalesce(b.kredit, 0)                              as kredit,
               coalesce(a.debet_awal, 0) + coalesce(b.debet, 0)   as debet_akhir,
               coalesce(a.kredit_awal, 0) + coalesce(b.kredit, 0) as kredit_akhir
        into temporary temp_gl_final
        from beone_coa c
                 left join temp_gl_awal a on c.coa_id = a.coa_id
                 left join temp_gl_akhir b on a.coa_id = b.coa_id
        order by c.nomor asc;

        select coa_id,
               nomor,
               nama,
               case when (debet_awal - kredit_awal) > 0 then (debet_awal - kredit_awal) else 0 end        as debet_awal,
               case when (debet_awal - kredit_awal) < 0 then abs(debet_awal - kredit_awal) else 0 end     as kredit_awal,
               debet,
               kredit,
               case when (debet_akhir - kredit_akhir) > 0 then (debet_akhir - kredit_akhir) else 0 end    as debet_akhir,
               case when (debet_akhir - kredit_akhir) < 0 then abs(debet_akhir - kredit_akhir) else 0 end as kredit_akhir
        into temporary temp_gl_balance
        from temp_gl_final;

        select coa_id,
               nomor,
               nama,
               debet_awal,
               kredit_awal,
               debet,
               kredit,
               debet_akhir,
               kredit_akhir
        from temp_gl_balance
        union all
        select 99999 coa_id,
               'SALDO AKHIR' nomor,
               'TOTAL : ' nama,
               sum(debet_awal) debet_awal,
               sum(kredit_awal) kredit_awal,
               sum(debet) debet,
               sum(kredit) kredit,
               sum(debet_akhir) debet_akhir,
               sum(kredit_akhir) kredit_akhir
        from temp_gl_balance;";

        return Helper::selectMany($query);
    }

    public function attemptPrint(Request $request)
    {
        $inputs = Helper::merge($request);
        $tgl_awal = $inputs->input('filter_awal_date');
        $tgl_akhir = $inputs->input('filter_akhir_date');

        $data = [
            'title' => 'LAPORAN NERACA MUTASI',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'periode' => 'Periode ' . $tgl_awal . ' S.D ' . $tgl_akhir,
            'tglawal' => $tgl_awal,
            'tglakhir' => $tgl_akhir,
            'data' => $this->getQuery($request),
        ];
        $pdf = PDF::loadView('pdf.neraca_mutasi.report-neraca-mutasi', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('pdf.neraca_mutasi.filter');
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        return Datatables::of($model)->escapeColumns([])->make();
    }

}
