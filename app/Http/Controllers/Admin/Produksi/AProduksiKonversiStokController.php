<?php

namespace App\Http\Controllers\Admin\Produksi;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneGudang;
use App\Models\BeoneKomposisi;
use App\Models\BeoneKonversiStokHeader;
use App\Models\BeoneSatuanItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AProduksiKonversiStokController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneKonversiStokHeader::with(['beone_item', 'beone_satuan_item', 'beone_gudang', 'beone_konversi_stok_details.beone_item', 'beone_konversi_stok_details.beone_satuan_item'])->find($uid);
//            $detail = BeoneKonversiStokHeader::with(['beone_item', 'beone_gudang', 'beone_konversi_stok_details.beone_item','beone_konversi_stok_details.beone_satuan_item'])->find($uid);
//            return $detail;
            return view('admin.menus.produksi.konversi_stok.detail', compact('detail'));
        }
        return view('admin.menus.produksi.konversi_stok.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.produksi.konversi_stok.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'beone_item.required' => 'Item required.',
            'beone_gudang.required' => 'Gudang required.',
            'konversi_stok_date.required' => 'Tanggal Required.',
        ];

        $this->validate($request, [
            'beone_item' => 'required_without:konversi_stok_header_id',
            'konversi_stok_date' => 'required_without:konversi_stok_header_id',
            'beone_gudang' => 'required_without:konversi_stok_header_id',
            'qty' => 'min:1|required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('konversi_stok_header_id');
            $message = 'Konversi Stok Berhasil Dibuat';

            $data = new BeoneKonversiStokHeader();

            $qty = $inputs->input('qty');
            $qty_real = $inputs->input('qty_real');
            $beone_gudang = $inputs->input('beone_gudang');
            $beone_item = $inputs->input('beone_item');
            $beone_satuan_item_header = $inputs->input('beone_satuan_item');

            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneKonversiStokHeader::find($uid);
                $message = 'Konversi Stok Berhasil Diedit';
                $beone_gudang = json_decode($data->beone_gudang->id);
            }

            $beone_konversi_stok_details = [];
            $detail = $inputs->input('beone_konversi_stok_details');
            foreach ($detail as $key => $value) {
                if ($value['item_id'] <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_konversi_stok_detail = [
                        'item_id' => $value['item_id'],
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'qty_allocation' => $value['qty_allocation'],
                        'qty_real' => $value['qty_real'],
                        'gudang_id' => $beone_gudang->id,
                        'flag' => 1,
                    ];
                    $beone_konversi_stok_details[] = $beone_konversi_stok_detail;
                }
            }
            if (!$uid) {
                $konversi_stok_date = Carbon::createFromDate($inputs->input('konversi_stok_date'));
                $data->konversi_stok_date = $konversi_stok_date;
                $data->konversi_stok_no = Helper::getTransNo('KS', $konversi_stok_date, BeoneKonversiStokHeader::class, 'konversi_stok_no');
                $data->item_id = $beone_item->id;
                $data->gudang_id = $beone_gudang->id;
                $data->satuan_id = $beone_satuan_item_header->id;
            }
            $data->qty = $qty;
            $data->qty_real = $qty_real;
            $data->flag = 1;
            $data->save();

            $data->beone_konversi_stok_details()->delete();
            $data->beone_konversi_stok_details()->createMany($beone_konversi_stok_details);

            DB::commit();
            return Helper::redirect('konversi_stok.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Konversi Stok Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeoneKonversiStokHeader::find($uid);
        if ($data) {
            $data->beone_konversi_stok_details()->update(['qty' => 0]);
            $data->delete();
        }
        return Helper::redirect('konversi_stok.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['konversi_stok_header_id', 'item_id', 'konversi_stok_no', 'konversi_stok_date', 'flag', 'qty', 'satuan_qty', 'gudang_id'];
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneKonversiStokHeader::query()->with(['beone_item'])->whereBetween('konversi_stok_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('produksi/konversi_stok/detail?id=' . $item->konversi_stok_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('produksi/konversi_stok/del?id=' . $item->konversi_stok_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function IsInvalid($uid)
    {
        return false;
    }

    public function loadKomposisi(Request $request)
    {
        $item_jadi = $request->item_id;
        $komposisi = BeoneKomposisi::with(['beone_item_baku', 'beone_item_jadi'])->where('item_jadi_id', $item_jadi)->get();
        return response()->json($komposisi);
    }

    public function searchGudang(Request $request)
    {
        $select = [
            'gudang_id as id',
            'nama as code',
            'keterangan as text',
            'keterangan as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::select($select)->where('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
