<?php

namespace App\Http\Controllers\Admin\Produksi;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneSatuanItem;
use App\Models\BeoneTransferStock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AProduksiBOMController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneTransferStock::with(['beone_transfer_stock_details.beone_gudang', 'beone_coa', 'beone_transfer_stock_details.beone_item', 'beone_transfer_stock_details_bb.beone_item', 'beone_transfer_stock_details_wip.beone_item', 'beone_transfer_stock_details_bb.beone_satuan_item', 'beone_transfer_stock_details_wip.beone_satuan_item'])->find($uid);
//            return $detail;
            return view('admin.menus.produksi.bom.detail', compact('detail'));
        }
        return view('admin.menus.produksi.bom.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }

        return view('admin.menus.produksi.bom.list');
    }

    public function dataTable(Request $request)
    {
//        $field = ['transfer_stock_id', 'transfer_no', 'transfer_date', 'keterangan', 'coa_kode_biaya'];
//        $model = DB::table('beone_transfer_stock')->select($field)->whereNull('deleted_at')->orderByDesc('transfer_date');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneTransferStock::query()->whereBetween('transfer_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('bom.detail', ['id' => $item->transfer_stock_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('bom.delete', ['id' => $item->transfer_stock_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function postDetail(Request $request)
    {
        $message = [
            'coa_kode_biaya.required' => 'Jenis Wajib is required',
            'transfer_date.required' => 'Tanggal is required',
            'keterangan.min' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'transfer_date' => 'required_without:transfer_stock_id',
            'coa_kode_biaya' => 'required_without:transfer_stock_id',
            'gudang_bahan' => 'required',
            'gudang_produksi' => 'required',
            'keterangan' => 'max:100',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();

        DB::beginTransaction();
        try {
            $uid = $inputs->input('transfer_stock_id');
            $message = 'BOM Berhasil Dibuat';

            $data = new BeoneTransferStock();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneTransferStock::find($uid);
                $message = 'BOM Berhasil Diedit';
            }

            $keterangan = $inputs->input('keterangan');
            $gudang_bahan = $inputs->input('gudang_bahan');
            $gudang_produksi = $inputs->input('gudang_produksi');

            $beone_transfer_stock_detail_bbs = [];
            $detail_bb = $inputs->input('beone_transfer_stock_details_bb');
            foreach ($detail_bb as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_transfer_stock_detail_bb = [
                        'transfer_stock_id' => $uid,
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'gudang_id' => $gudang_bahan->id,
                        'tipe_transfer_stock' => 'bb',
                        'flag' => 1,
                    ];
                    $beone_transfer_stock_detail_bbs[] = $beone_transfer_stock_detail_bb;
                }
            }
            $beone_transfer_stock_detail_wips = [];
            $detail_wip = $inputs->input('beone_transfer_stock_details_wip');
            foreach ($detail_wip as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_transfer_stock_detail_wip = [
                        'transfer_stock_id' => $uid,
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'gudang_id' => $gudang_produksi->id,
                        'tipe_transfer_stock' => 'wip',
                        'biaya' => 0,
//                        'biaya' => $value['biaya'],
                        'persen_produksi' => $value['persen_produksi'],
                        'flag' => 1,
                    ];
                    $beone_transfer_stock_detail_wips[] = $beone_transfer_stock_detail_wip;
                }
            }

            if (!$uid) {
                $coa_kode_biaya = $request->input('coa_kode_biaya');
                $transfer_date = Carbon::createFromDate($inputs->input('transfer_date'));
                $data->transfer_date = $transfer_date;
                $data->transfer_no = Helper::getTransNo('BOM', $transfer_date, BeoneTransferStock::class, 'transfer_no');
                $data->coa_kode_biaya = $coa_kode_biaya;
            }

            $data->keterangan = $keterangan;
            $data->update_by = Auth::id();
            $data->flag = 1;
            $data->save();
            $data->beone_transfer_stock_details()->delete();
            $data->beone_transfer_stock_details()->createMany($beone_transfer_stock_detail_bbs);
            $data->beone_transfer_stock_details()->createMany($beone_transfer_stock_detail_wips);

            DB::commit();
            return Helper::redirect('bom.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'BOM Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeoneTransferStock::find($uid);
        $data->beone_transfer_stock_details()->update(['qty' => 0]);
        $data->delete();
        return Helper::redirect('bom.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function IsInvalid($uid)
    {
        return false;
    }
}
