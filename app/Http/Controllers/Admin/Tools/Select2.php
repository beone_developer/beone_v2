<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneCoa;
use App\Models\BeoneCustomer;
use App\Models\BeoneItem;
use App\Models\BeoneItemType;
use App\Models\BeonePoImportHeader;
use App\Models\BeoneSalesHeader;
use App\Models\BeoneSubKontraktorOutHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Select2 extends Controller
{
    public static function toSelect($items)
    {
        $iq = [];
        foreach ($items as $item) {
            $ar = [
                'id' => $item->id,
                'text' => $item->text,
            ];
            $iq[] = [
                'id' => json_encode($ar),
                'text' => $item->text
            ];
        }
        return $iq;
    }

    public function satuanItem(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 1) {
            $q = '%' . $q . '%';
            if ($inputs->has('beone_item')) {
                $beone_item = $inputs->get('beone_item');
                $query = "select s.satuan_id as id, concat(s.rasio,' (', s.satuan_code,')') as text
                from beone_satuan_item s
                where s.item_id = '$beone_item->id' and (s.keterangan ilike '$q' or s.satuan_code ilike '$q')";
            } else if ($inputs->has('beone_purchase_order_detail')) {
                $beone_purchase_order_detail = $inputs->get('beone_purchase_order_detail');
                $query = "select s.satuan_id as id, concat(s.rasio, ' (', s.satuan_code, ')') as text
                from beone_satuan_item s,
                     beone_po_import_detail pd
                where pd.item_id = s.item_id
                and pd.purchase_detail_id = '$beone_purchase_order_detail->id' and (s.keterangan ilike '$q' or s.satuan_code ilike '$q')";
            } else {
                return '';
            }
            $items = DB::select(DB::raw($query));
            $items = self::toSelect($items);
            $total_count = count($items);
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function item(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::where('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function itemType(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItemType::where('nama', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function outstandingPO(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            if (!$inputs->has('multiple')) {
                $items = BeonePoImportHeader::with(['beone_po_import_details'])
                    ->doesntHave('beone_import_headers')
                    ->doesntHave('beone_po_import_details.beone_import_details')
                    ->where('purchase_no', 'ilike', $q)
                    ->orWhere('keterangan', 'ilike', $q)
                    ->get();
                $total_count = $items->count();
            } else {
                if (!$inputs->has('beone_supplier')) {
                    return '';
                }
                $beone_supplier = $inputs->get('beone_supplier');
                $query = "select pid.purchase_detail_id                          as id,
                       concat(pih.purchase_no, ' (', i.item_code, ')') as text,
                       si.satuan_id,
                       concat(si.rasio, ' (', si.satuan_code, ')')     as satuan_text
                from beone_po_import_detail pid,
                     beone_item i,
                     beone_po_import_header pih,
                     beone_satuan_item si
                where pid.item_id = i.item_id
                  and i.item_id = si.item_id
                  and pid.purchase_header_id = pih.purchase_header_id
                  and (i.nama ilike '$q' or i.item_code ilike '$q' or pih.purchase_no ilike '$q')
                and pih.supplier_id = '$beone_supplier->id';";

                $items = DB::select(DB::raw($query));
                $iq = [];
                foreach ($items as $item) {
                    $ar_satuan_def = [
                        'id' => $item->satuan_id,
                        'text' => $item->satuan_text,
                    ];
                    $satuan_def = [
                        'id' => json_encode($ar_satuan_def),
                        'text' => $item->satuan_text
                    ];
                    $ar = [
                        'id' => $item->id,
                        'text' => $item->text,
                        'satuan_def' => $satuan_def
                    ];
                    $iq[] = [
                        'id' => json_encode($ar),
                        'text' => $item->text
                    ];
                }
                $items = $iq;
                $total_count = count($iq);
            }
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchCoaKasBank(Request $request)
    {
        $select = [
            'coa_id',
            'nomor',
            'nama',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCoa::select($select)->where('nama', 'ilike', $q)->orWhere('nomor', 'ilike', $q)->where('tipe_akun', 1)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchCoa(Request $request)
    {
        $select = [
            'coa_id',
            'nomor',
            'nama',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCoa::select($select)->where('nama', 'ilike', $q)->orWhere('nomor', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchCoaAll(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];

        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCoa::where('nama', 'ilike', $q)->orWhere('nomor', 'ilike', $q)->get();
            $total_count = $items->count();
            $items = collect($items);
            $all = [
                'id' => '%',
                'text' => 'ALL',
            ];
            $all = [
                'id' => json_encode($all),
                'text' => 'ALL'
            ];
            $items->push($all);
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchCustomer(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCustomer::where('customer_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchSalesOrder(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneSalesHeader::where('sales_no', 'ilike', $q)->orWhere('sales_no', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchSubkonOut(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneSubKontraktorOutHeader::where('trans_no', 'ilike', $q)->orWhere('bc_nomor_jaminan', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
