<?php

namespace App\Http\Controllers\Admin\Import;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneCurrency;
use App\Models\BeoneGudang;
use App\Models\BeoneImportHeader;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeonePoImportDetail;
use App\Models\BeoneSatuanItem;
use App\Models\TpbHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AImporReceiveImportController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneImportHeader::with(['beone_purchase_order', 'beone_import_details.beone_item', 'beone_import_details.beone_satuan_item'])->find($uid);
            return view('admin.menus.import.receive_import.terima', compact('detail'));
        }
        return redirect()->route('receive_import.outstanding');
    }

    public function indexDetailTerima(Request $request)
    {
        $bc_id = $request->input('bc_id');
        if ($bc_id) {
            $detail = BeoneImportHeader::with(['beone_purchase_order', 'beone_import_details.beone_item','tpb_header','tpb_header.tpb_barangs'])->Where('bc_header_id',$bc_id)->first();
            if (!$detail) {
                    $detail = TpbHeader::with(['tpb_barangs'])->find($bc_id);
            }
            // return $detail;
            return view('admin.menus.import.receive_import.terima', compact('detail'));
        }
        return redirect()->route('receive_import.outstanding');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.import.receive_import.list');
    }

    public function indexOutstanding(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTableOutstanding($request);
        }
        return view('admin.menus.import.receive_import.outstanding');
    }

    public function postDetail(Request $request)
    {
//        return $request->all();
        $message = [
//            'receive_date.required' => 'Date is required.',
//            'tpb_id.required' => 'TPB is required.',
        ];

        $this->validate($request, [
            'receive_date' => 'required_without:import_header_id',
//            'tpb_id' => 'required',
            'beone_purchase_order' => 'required_without:import_header_id',
            'beone_supplier' => 'required_without:import_header_id',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();

        DB::beginTransaction();
        try {
            $uid = $inputs->input('import_header_id');
            $message = 'Receive Import Berhasil Dibuat';

            $data = new BeoneImportHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneImportHeader::find($uid);
                $message = 'Receive Import Berhasil Diedit';
            }

            $dpp_sys = $inputs->input('dpp_sys');
            $ppn_sys = $inputs->input('ppn_sys');
            $discount = $inputs->input('discount');
            $grandtotal_sys = $inputs->input('grand_total_sys');

            $beone_import_details = [];
            $detail = $inputs->input('beone_import_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_po_import_detail = [
                        'item_id' => $value['beone_item']->id,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['qty'] * $value['price'],
                        'flag' => 1,
                    ];
                    $beone_import_details[] = $beone_po_import_detail;
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function postDetailTerima(Request $request)
    {
//        return $request->all();
        $_settings = BeoneKonfigurasiPerusahaan::first();
        $message = [
            'receive_date.required' => 'Date is required.',
            'tpb_id.required' => 'TPB is required.',
        ];

        $this->validate($request, [
            'receive_date' => 'required_without:import_header_id',
            'tpb_id' => 'required',
            'beone_purchase_order' => Rule::requiredIf(!$_settings->receiving_multi_po),
            'beone_supplier' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('import_header_id');
            $message = 'Receive Import Berhasil Dibuat';

            $data = new BeoneImportHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneImportHeader::find($uid);
                $message = 'Receive Import Berhasil Diedit';
            }

            $tpb_id = $inputs->input('tpb_id');
            $tpb = TpbHeader::with(['tpb_barangs'])->find($tpb_id);

            $bc_nama_vendor = $tpb->NAMA_PEMASOK;
            $bc_nama_pemilik = $tpb->NAMA_PEMILIK;
            $jenis_bc = $tpb->KODE_DOKUMEN_PABEAN;
            $bc_no_aju = $tpb->NOMOR_AJU;
            $bc_no = $tpb->NOMOR_DAFTAR;
            $bc_date = $tpb->TANGGAL_DAFTAR;
            $bc_nama_penerima_barang = $tpb->NAMA_PENERIMA_BARANG;
            $bc_nama_pengangkut = $tpb->NAMA_PENGANGKUT;
            $bc_kurs = ($tpb->NDPBM ? $tpb->NDPBM : 1);
            $bc_nama_pengirim = $tpb->NAMA_PENGIRIM;
            $bc_header_id = $tpb_id;

            $beone_supplier = $inputs->input('beone_supplier');
            $beone_gudang = $inputs->input('beone_gudang');
            $beone_purchase_order = "";
            if (!$_settings->receiving_multi_po) {
                $beone_purchase_order = $inputs->input('beone_purchase_order');
            }
            $beone_currency = $inputs->input('beone_currency');
            $isppn = $inputs->has('isppn');

            $beone_import_details = [];
            $detail = $inputs->input('tpb_barangs');
            foreach ($detail as $key => $value) {
                $item_id = 0;
                $purchase_detail_id = 0;
                if (!$_settings->receiving_multi_po) {
                    $item_id = $value['beone_item']->id;
                    $purchase_detail_id = $beone_purchase_order->id;
                } else {
                    $purchase_detail = BeonePoImportDetail::find($value['beone_purchase_order_detail']->id);
                    $item_id = $purchase_detail->item_id;
                    $purchase_detail_id = $purchase_detail->purchase_detail_id;
                }
                if ($value['uraian'] <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_import_detail = [
                        'gudang_id' => $beone_gudang->id,
                        'item_id' => $item_id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'isppn' => $isppn,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['amount_sys'],
                        'amount_ppn' => $value['amount_ppn'],
                        'amount_ppn_sys' => $value['amount_ppn_sys'],
                        'flag' => 1,
                        'bc_barang_id' => $value['bc_barang_id'],
                        'purchase_detail_id' => $purchase_detail_id,
                    ];
                    $beone_import_details[] = $beone_import_detail;
                }
            }

            $dpp = $inputs->input('dpp');
            $ppn = $inputs->input('ppn');

            $dpp_sys = $inputs->input('dpp_sys');
            $ppn_sys = $inputs->input('ppn_sys');
            $discount = $inputs->input('discount');

            $grandtotal = $inputs->input('grand_total');
            $grandtotal_sys = $inputs->input('grand_total_sys');

            if (!$uid) {
                $receive_date = Carbon::createFromDate($inputs->input('receive_date'));
                $import_no = Helper::getTransNo('IMP', $receive_date, BeoneImportHeader::class, 'import_no');

                $data->bc_no = $bc_no;
                $data->import_no = $import_no;
                $data->receive_no = $import_no;
                $data->trans_date = $receive_date;
                $data->receive_date = $receive_date;

                $data->bc_no_aju = $bc_no_aju;
                $data->bc_date = $bc_date;
                $data->jenis_bc = $jenis_bc;

                $data->bc_nama_vendor = $bc_nama_vendor;
                $data->bc_nama_pemilik = $bc_nama_pemilik;
                $data->bc_nama_pengirim = $bc_nama_pengirim;
                $data->bc_nama_penerima_barang = $bc_nama_penerima_barang;
                $data->bc_nama_pengangkut = $bc_nama_pengangkut;
                $data->bc_header_id = $bc_header_id;
            }

            $data->supplier_id = $beone_supplier->id;
            $data->currency_id = $beone_currency->id;

            if (!$_settings->receiving_multi_po) {
                $data->purchase_header_id = $beone_purchase_order->id;
            }
            $data->ppn = $ppn;
            $data->dpp = $dpp;
            $data->ppn_sys = $ppn_sys;
            $data->dpp_sys = $dpp_sys;
            $data->grandtotal = $grandtotal;
            $data->grandtotal_sys = $grandtotal_sys;
            $data->discount = $discount;
            $data->bc_kurs = $bc_kurs;
            $data->flag = 1;
            $data->update_by = Auth::id();
            $data->save();
            $data->beone_import_details()->delete();
            $data->beone_import_details()->createMany($beone_import_details);

            DB::commit();
            return Helper::redirect('receive_import.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Receive Import Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneImportHeader::find($uid);
        $data->beone_import_details()->update(['qty' => 0]);
        $data->delete();

        return Helper::redirect('receive_import.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneImportHeader::query()->with(['beone_supplier', 'beone_currency'])->whereNotNull('bc_no_aju')->whereBetween('receive_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
//                   ( isset($item->bc_header_id) ? '<a href="' . route('receive_import.terima', ['bc_id' => $item->bc_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
//                       <i class="la la-edit"></i>
//                   </a>' : '' ).
                    '<a href="javascript:void(0);" data-url="' . route('receive_import.delete', ['id' => $item->import_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->setRowClass(function ($item) {
                return isset($item->bc_header_id) ? '' : 'alert-danger';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function dataTableOutstanding(Request $request)
    {
        $beone_import_header = BeoneImportHeader::whereNotNull('bc_no_aju')->get();
        $model = TpbHeader::query()->whereNotIn('NOMOR_AJU', $beone_import_header->pluck('bc_no_aju'))->whereIn('KODE_DOKUMEN_PABEAN', [23, 40]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('receive_import.terima', ['bc_id' => $item->ID]) . '" class="btn btn-sm btn-label-brand btn-bold" title="View">
                    Terima
                    </a>';
            })
            ->filterColumn('JENIS', function ($query, $keyword) {
                $sql = "concat('BC ',KODE_DOKUMEN_PABEAN) like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('SUPPLIER', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('AMOUNT', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('CURRENCY', function ($query, $keyword) {
                $sql = "case (KODE_DOKUMEN_PABEAN) when 23 then 'USD' else 'IDR' end like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('SUPPLIER', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then NAMA_PEMASOK else NAMA_PEMILIK end'), $order);
            })
            ->orderColumn('JENIS', function ($query, $order) {
                $query->orderBy(DB::raw('concat("BC ",KODE_DOKUMEN_PABEAN)'), $order);
            })
            ->orderColumn('AMOUNT', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then HARGA_INVOICE else HARGA_PENYERAHAN end'), $order);
            })
            ->orderColumn('CURRENCY', function ($query, $order) {
                $query->orderBy(DB::raw('case (KODE_DOKUMEN_PABEAN) when 23 then "USD" else "IDR" end'), $order);
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchCurrency(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneCurrency::where('currency_code', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchGudang(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::where('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchImport(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneImportHeader::where('import_no', 'ilike', $q)->orWhere('receive_no', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

}
