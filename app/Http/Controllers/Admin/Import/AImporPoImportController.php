<?php

namespace App\Http\Controllers\Admin\Import;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeonePoImportHeader;
use App\Models\BeoneSatuanItem;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AImporPoImportController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeonePoImportHeader::with(['beone_supplier', 'beone_po_import_details.beone_item', 'beone_po_import_details.beone_satuan_item'])->find($uid);
            return view('admin.menus.import.po_import.detail', compact('detail'));
        }
        return view('admin.menus.import.po_import.detail');
    }

    public function indexList(Request $request)
    {
//        return $request->all();
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.import.po_import.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            'trans_date.required' => 'Date is required.',
            'beone_supplier.required' => 'Supplier is required.',
            'beone_po_import_details.required' => 'Keterangan is required.',
            'keterangan.min' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'trans_date' => 'required_without:purchase_header_id',
            'beone_supplier' => 'required',
            'beone_po_import_details' => 'required',
            'keterangan' => 'max:100',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();

        DB::beginTransaction();
        try {
            $uid = $inputs->input('purchase_header_id');
            $status = 'Sukses ';
            $message = 'PO Import Berhasil Dibuat';

            $data = new BeonePoImportHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeonePoImportHeader::find($uid);
                $message = 'PO Import Berhasil Diedit';
            }
            $beone_supplier = $inputs->input('beone_supplier');
            $keterangan = $inputs->input('keterangan');
            $beone_currency = $inputs->input('beone_currency');
            $kurs = $inputs->input('kurs');

            $beone_po_import_details = [];
            $detail = $inputs->input('beone_po_import_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_item']->id <> '') {
                    $beone_satuan_item = BeoneSatuanItem::find($value['beone_satuan_item']->id);
                    $beone_po_import_detail = [
                        'item_id' => $value['beone_item']->id,
                        'satuan_id' => $beone_satuan_item->satuan_id,
                        'rasio' => $beone_satuan_item->rasio,
                        'qty' => $value['qty'],
                        'price' => $value['price'],
                        'amount' => $value['amount'],
                        'amount_sys' => $value['qty'] * $value['price'],
                        'flag' => 1,
                    ];
                    $beone_po_import_details[] = $beone_po_import_detail;
                }
            }
            $grandtotal = collect($beone_po_import_details)->pluck('amount')->sum();
            $grandtotal_sys = collect($beone_po_import_details)->pluck('amount_sys')->sum();

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));;
                $data->trans_date = $trans_date;
                $data->purchase_no = Helper::getTransNo('PO', $trans_date, BeonePOImportHeader::class, 'purchase_no');
            }
            $data->supplier_id = $beone_supplier->id;
            $data->currency_id = $beone_currency->id;
            $data->kurs = $kurs;
            $data->keterangan = $keterangan;
            $data->grandtotal = $grandtotal;
            $data->grandtotal_sys = $grandtotal_sys;
            $data->flag = 1;
            $data->update_by = Auth::id();
            $data->save();
            $data->beone_po_import_details()->delete();
            $data->beone_po_import_details()->createMany($beone_po_import_details);

            DB::commit();
            return Helper::redirect('po_import.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Po Import Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeonePoImportHeader::find($uid);
        if ($data) {
            $data->beone_po_import_details()->update(['qty' => 0]);
            $data->delete();
        }
        return Helper::redirect('po_import.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['purchase_header_id', 'purchase_no', 'trans_date', 'keterangan'];
//        $model = DB::table('beone_po_import_header')->select($field)->whereNull('deleted_at')->orderByDesc('trans_date');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeonePoImportHeader::query()->with(['beone_supplier'])->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);;
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('po_import.detail', ['id' => $item->purchase_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('po_import.delete', ['id' => $item->purchase_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>
                    <a href="' . route('po_import.print', ['id' => $item->purchase_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $select = [
            'item_id as id',
            'item_code as code',
            'nama as text',
            'item_code as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::select($select)->where('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function print(Request $request)
    {
        $uid = $request->input('id');
        $detail = BeonePoImportHeader::with(['beone_supplier', 'beone_po_import_details.beone_item'])->find($uid);
        $data = [
            'title' => 'Purchase Order',
            'company' => BeoneKonfigurasiPerusahaan::first(),
            'detail' => $detail,
        ];
        // return $data;
        $pdf = PDF::loadView('pdf.import.report-purchase-order-import', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
