<?php

namespace App\Http\Controllers\Admin\Keuangan;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneJurnalHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AJurnalUmumController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneJurnalHeader::with(['beone_jurnal_detail_debets.beone_coa', 'beone_jurnal_detail_kredits.beone_coa'])->find($uid);
            //    return $detail;
            return view('admin.menus.keuangan.jurnal_umum.detail', compact('detail'));
        }
        return view('admin.menus.keuangan.jurnal_umum.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.keuangan.jurnal_umum.list');
    }

    public function GenerateNota($last_trans, $strKodeAkhir)
    {
        return sprintf('%03d', $last_trans) . $strKodeAkhir;
    }

    public function postDetail(Request $request)
    {
        $message = [
            'grand_total_debet.same' => 'Debet and kredit not same.',
        ];
        $this->validate($request, [
            'grand_total_debet' => 'required|same:grand_total_kredit',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('jurnal_header_id');
            $message = 'Jurnal Umum Berhasil Dibuat';

            $data = new BeoneJurnalHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneJurnalHeader::find($uid);
                $message = 'Jurnal Umum Berhasil Diedit';
            }

            $keterangan = $inputs->input('keterangan');

            $beone_jurnal_detail_debets = [];
            $detail_debet = $inputs->input('beone_jurnal_detail_debets');
            foreach ($detail_debet as $key => $value) {
                if ($value['beone_coa']->id <> '') {
                    $beone_jurnal_detail_debet = [
                        'jurnal_header_id' => $uid,
                        'coa_id' => $value['beone_coa']->id,
                        'amount' => $value['amount'],
                        'kurs' => $value['kurs'],
                        'amount_idr' => $value['amount_idr'],
                        'keterangan_detail' => $value['keterangan_detail'],
                        'status' => 'D',
                    ];
                    $beone_jurnal_detail_debets[] = $beone_jurnal_detail_debet;
                }
            }

            $beone_jurnal_detail_kredits = [];
            $detail_kredit = $inputs->input('beone_jurnal_detail_kredits');
            foreach ($detail_kredit as $key => $value) {
                if ($value['beone_coa']->id <> '') {
                    $beone_jurnal_detail_kredit = [
                        'jurnal_header_id' => $uid,
                        'coa_id' => $value['beone_coa']->id,
                        'amount' => $value['amount'],
                        'kurs' => $value['kurs'],
                        'amount_idr' => $value['amount_idr'],
                        'keterangan_detail' => $value['keterangan_detail'],
                        'status' => 'K',
                    ];
                    $beone_jurnal_detail_kredits[] = $beone_jurnal_detail_kredit;
                }
            }

            if (!$uid) {
                $trans_date = Carbon::createFromDate($inputs->input('trans_date'));
                $data->jurnal_no = Helper::getTransNo('JU', $trans_date, BeoneJurnalHeader::class, 'jurnal_no');
                $data->trans_date = $trans_date;
            }
            $data->keterangan = $keterangan;
            // $data->update_by = Auth::id();
            $data->save();
            $data->beone_jurnal_details()->delete();
            $data->beone_jurnal_details()->createMany($beone_jurnal_detail_debets);
            $data->beone_jurnal_details()->createMany($beone_jurnal_detail_kredits);

            DB::commit();
            return Helper::redirect('jurnal_umum.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Jurnal Umum Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = BeoneJurnalHeader::find($uid);
        // $data->beone_voucher_details()->update(['amount_idr' => 0]);
        $data->delete();
        return Helper::redirect('jurnal_umum.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['jurnal_header_id', 'jurnal_no', 'trans_date', 'keterangan'];
//        $model = DB::table('beone_jurnal_header')->select($field)->whereNull('deleted_at');
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneJurnalHeader::query()->whereBetween('trans_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('keuangan/jurnal_umum/detail?id=' . $item->jurnal_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('keuangan/jurnal_umum/del?id=' . $item->jurnal_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchGudang(Request $request)
    {
        $select = [
            'gudang_id as id',
            'nama as code',
            'keterangan as text',
            'keterangan as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::select($select)->where('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

}
