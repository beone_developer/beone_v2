<?php

namespace App\Http\Controllers\Admin\Keuangan;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneVoucherHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AKasBankController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneVoucherHeader::with(['beone_coa', 'beone_voucher_details.beone_coa'])->find($uid);
            return view('admin.menus.keuangan.kasbank.detail', compact('detail'));
        }
        return view('admin.menus.keuangan.kasbank.detail');
    }

    public function beone_voucher_details()
    {
        return $this->hasMany(\App\Models\BeoneVoucherDetail::class, 'voucher_header_id');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.keuangan.kasbank.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'voucher_date' => 'required_without:voucher_header_id',
            'beone_coa' => 'required_without:voucher_header_id',
            'tipe' => 'required_without:voucher_header_id',
            'keterangan' => 'max:100',
            'beone_voucher_details' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('voucher_header_id');
            $message = 'Kas Bank Berhasil Dibuat';

            $data = new BeoneVoucherHeader();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneVoucherHeader::find($uid);
                $message = 'Kas Bank Berhasil Diedit';
            }

            $keterangan = $inputs->input('keterangan');

            $beone_voucher_details = [];
            $detail = $inputs->input('beone_voucher_details');
            foreach ($detail as $key => $value) {
                if ($value['beone_coa']->id <> '') {
                    $beone_voucher_detail = [
                        'coa_id_lawan' => $value['beone_coa']->id,
                        'keterangan_detail' => $value['keterangan_detail'],
                        'jumlah_valas' => $value['jumlah_valas'],
                        'kurs' => $value['kurs'],
                        'jumlah_idr' => $value['jumlah_idr']
                    ];
                    $beone_voucher_details[] = $beone_voucher_detail;
                }
            }
            $grandtotal = collect($beone_voucher_details)->pluck('jumlah_idr')->sum();
            if (!$uid) {
                $beone_coa = $inputs->input('beone_coa');
                $voucher_date = Carbon::createFromDate($inputs->input('voucher_date'));
                $tipe = $inputs->input('tipe');
                if ($tipe) {
                    $inisial = "BM";
                } else {
                    $inisial = "BK";
                }
                $data->voucher_date = $voucher_date;
                $data->voucher_number = Helper::getTransNo($inisial, $voucher_date, BeoneVoucherHeader::class, 'voucher_number');;
                $data->tipe = $tipe;
                $data->coa_id_cash_bank = $beone_coa->id;
            }

            $data->keterangan = $keterangan;
            $data->update_by = Auth::id();
            $data->grandtotal = $grandtotal;
            $data->save();
            $data->beone_voucher_details()->delete();
            $data->beone_voucher_details()->createMany($beone_voucher_details);

            DB::commit();
            return Helper::redirect('kasbank.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Kas Bank Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        $data = BeoneVoucherHeader::find($uid);
//        $data->beone_voucher_details()->update(['jumlah_idr' => 0]);
        $data->delete();

        return Helper::redirect('kasbank.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        $model = BeoneVoucherHeader::query()->with(['beone_coa'])->whereBetween('voucher_date', [$tgl_awal, $tgl_akhir]);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('keuangan/kasbank/detail?id=' . $item->voucher_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('keuangan/kasbank/del?id=' . $item->voucher_header_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>
                    <a href="' . route('olap.print_out.view', ['print' => 'BuktiKas', 'id' => $item->voucher_header_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>';
            })
            ->editColumn('tipe', function ($item) {
                return ($item->tipe ? 'DEBET' : 'KREDIT');
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchGudang(Request $request)
    {
        $select = [
            'gudang_id as id',
            'nama as code',
            'keterangan as text',
            'keterangan as full_name',
        ];
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::select($select)->wheretipe('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

}
