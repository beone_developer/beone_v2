<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneModul;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemRoleController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneModul::find($uid);
            return view('admin.menus.sistem.role.detail', compact('detail'));
        }
        return view('admin.menus.sistem.role.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.sistem.role.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
//             'modul.required' => 'The modul field is required.',
//             'modul.min' => 'Minimum url is 3 characters.',
//             'url.min' => 'Minimum url is 3 characters.',
        ];

        $this->validate($request, [
            'modul' => 'required|min:3|max:100',
            'url' => 'required|min:3|max:100',
        ], $message);


        $inputs = Helper::merge($request);

        try {
            $uid = $inputs->input('id');
            $message = 'Role Berhasil Dibuat';

            $data = new BeoneModul();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneModul::find($uid);
                $message = 'Role Berhasil Diedit';
            }
            $data->modul = $inputs->input('modul');
            $data->url = $inputs->input('url');
            $data->save();

            DB::commit();
            return Helper::redirect('role.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Role Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneModul::find($uid)->delete();
        return Helper::redirect('role.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $field = ['modul_id','modul','url'];
//        $model = DB::table('beone_roles_user')->select($field)->whereNull('deleted_at');
        $model = BeoneModul::all($field);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('role.detail', ['id' => $item->modul_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('role.delete', ['id' => $item->modul_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->editColumn('url', function ($item) {
                return '<td class="uk-text-center"><a class="try-update" href="'.url($item->url).'">'.url($item->url).'</a></td>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneModul::where('modul', 'ilike', $q)->orWhere('url', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
