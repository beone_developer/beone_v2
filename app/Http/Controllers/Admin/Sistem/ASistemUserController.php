<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemUserController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneUser::with(['beone_roles_users'])->find($uid);
//            return $detail;
            return view('admin.menus.sistem.user.detail', compact('detail'));
        }
        return view('admin.menus.sistem.user.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.sistem.user.list');
    }

    public function postDetail(Request $request)
    {
//        return $request->all();
        $uid = $request->input('id');
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'username' => 'required|min:3|max:100',
            'nama' => 'required|min:3|max:100',
            'password' => 'nullable|min:5|required_with:validasi_password|same:validasi_password',
            'validasi_password' => 'nullable|min:5',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'User Berhasil Dibuat';

            $data = new BeoneUser();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneUser::find($uid);
                $message = 'User Berhasil Diedit';
            }
            $username = $inputs->input('username');
            $nama = $inputs->input('nama');
            $role = $inputs->input('beone_roles_user')->id;
            $password = $inputs->input('password');
            $validasi_password = $inputs->input('validasi_password');
            $updated_date = Carbon::now()->toDateString();
//            return $updated_date;
            if ($password == $validasi_password) {
                if ($password & $validasi_password != "") {
                    $data->password = Hash::make($password);
                } else {
                    unset($password);
                }
                $data->username = $username;
                $data->nama = $nama;
                $data->role_id = $role;
                $data->update_by = auth()->id();
                $data->update_date = $updated_date;
//              return $data;
                $data->save();
            }

            DB::commit();
            return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }

    }

    public function deleteData(Request $request)
    {
        $message = 'User Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        BeoneUser::find($uid)->delete();

        return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['user_id', 'username', 'nama', 'role_id'];
//        $model = DB::table('beone_user')->select($field)->whereNull('deleted_at');
        $model = BeoneUser::query()->with('beone_roles_users');
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('user.detail', ['id' => $item->user_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('user.delete', ['id' => $item->user_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function IsInvalid($uid)
    {
        return false;
    }
}
