<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ASistemRekalkulasiController extends Controller
{
    public function recalcStok(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        DB::select("call recalculate_stok_hpp(cast('$tgl_awal' as date),cast('$tgl_akhir' as date));");
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, "Sukses Rekal");
    }

    public function recalcGL(Request $request)
    {
        $tgl_awal = $request->input('filter_awal_date');
        $tgl_akhir = $request->input('filter_akhir_date');
        DB::select("call recalculate_gl(cast('$tgl_awal' as date),cast('$tgl_akhir' as date));");
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, "Sukses Rekal");
    }

    public function indexGL(Request $request)
    {
        return view('admin.menus.sistem.filter_gl');
    }

    public function indexStokHPP(Request $request)
    {
        return view('admin.menus.sistem.rekalkulasi_stok');
    }

    public function postFilterGL(Request $request)
    {
        return $this->recalcGL($request);
    }

    public function postFilterStokHPP(Request $request)
    {
        return $this->recalcStok($request);
    }
}
