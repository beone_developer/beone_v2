<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneModul;
use App\Models\BeoneRolesUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemRoleUserController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneRolesUser::with(['beone_moduls'])->find($uid);
            $all_modul = BeoneModul::whereNotIn('modul_id', $detail->beone_moduls->pluck('modul_id'))->where('is_hidden', false)->get();
            $all_modul = Helper::groupTreeMenu(collect($all_modul)->toArray());
            $act_modul = Helper::groupTreeMenu(collect($detail->beone_moduls)->toArray());
            return view('admin.menus.sistem.role_user.detail', compact('detail', 'all_modul', 'act_modul'));
        }
        return view('admin.menus.sistem.role_user.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.sistem.role_user.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama_role' => 'required|min:3|max:100',
            'keterangan' => 'required|min:3|max:100',
            'beone_moduls' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        $detail = Helper::ungroupTreeMenu($inputs->input('beone_moduls'));
//        return $detail;
        try {
            $uid = $inputs->input('id');
            $message = 'Role User Berhasil Dibuat';

            $data = new BeoneRolesUser();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneRolesUser::find($uid);
                $message = 'Role User Berhasil Diedit';
            }

            $data->nama_role = $inputs->input('nama_role');
            $data->keterangan = $inputs->input('keterangan');

            $beone_roles_moduls = Helper::ungroupTreeMenu($inputs->input('beone_moduls'));

            $data->save();
            $data->beone_roles_moduls()->delete();
            $data->beone_roles_moduls()->createMany($beone_roles_moduls);

            DB::commit();
            return Helper::redirect('role_user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Role User Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneRolesUser::find($uid)->delete();
        return Helper::redirect('role_user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['role_id', 'nama_role', 'keterangan'];
//        $model = DB::table('beone_roles_user')->select($field)->whereNull('deleted_at');
        $model = BeoneRolesUser::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('role_user.detail', ['id' => $item->role_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('role_user.delete', ['id' => $item->role_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneRolesUser::where('nama_role', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
