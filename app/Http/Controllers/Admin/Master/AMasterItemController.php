<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneItem;
use App\Models\BeoneSatuanItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterItemController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneItem::with(['beone_satuan_items','beone_item_type'])->find($uid);
            return view('admin.menus.master.item.detail', compact('detail'));
        }
        return view('admin.menus.master.item.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.item.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'item_code' => 'required', Rule::unique('beone_item')->ignore($request->input('item_code')),
            'nama' => 'required|min:3|max:100',
            'beone_satuan_items' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Item Berhasil Dibuat';

            $data = new BeoneItem();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneItem::find($uid);
                $message = 'Item Berhasil Dibuat';
            }

            $data->item_code = $inputs->input('item_code');
            $data->nama = $inputs->input('nama');
            $data->item_type_id = $inputs->input('beone_item_type')->id;
            $data->keterangan = $inputs->input('keterangan');
            $data->save();

            $beone_satuan_items = [];
            if (!$uid) {

                $detail = $inputs->input('beone_satuan_items');
                foreach ($detail as $key => $value) {
                    if ($value['satuan_code'] <> '') {
                        $beone_satuan_item = [
                            'satuan_id' => $value['satuan_id'],
                            'item_id' => $data['item_id'],
                            'satuan_code' => $value['satuan_code'],
                            'keterangan' => $value['keterangan'],
                            'rasio' => $value['rasio'],
                            'flag' => 1,
                        ];
                        $beone_satuan_items[] = $beone_satuan_item;
                    }
                }

                if (!collect($beone_satuan_items)->contains('rasio', 1)) {
                    $message2 = 'Gagal menyimpan data Rasio 1 Wajib ada!';
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message2);
                }
                $data->beone_satuan_items()->delete();
                $data->beone_satuan_items()->createMany($beone_satuan_items);
            }

            DB::commit();
            return Helper::redirect('item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneItem::find($uid)->delete();

        return Helper::redirect('item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $model = BeoneItem::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/item/detail?id=' . $item->item_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/item/del?id=' . $item->item_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function searchData(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneItem::where('item_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
//            $items = Helper::s2($items, '', 'id', 'text');
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function searchSatuan(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneSatuanItem::where('satuan_code', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
//            $items = Helper::s2($items, '', 'id', 'text');
        }

        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

//    public function searchData(Request $request)
//    {
//        $q = $request->input('q');
//        $gudang_id = $request->input('gudang_id');
//        if ($gudang_id) {
//            $dbQuery = "select *
//            from (
//                     select i.item_id, i.item_code, i.nama, iv.gudang_id, sum(iv.qty_in - iv.qty_out) as stok_akhir
//                     from beone_inventory iv
//                              join beone_item i on i.item_id = iv.item_id
//                     where iv.gudang_id = 1
//                     group by i.item_id, i.item_code, i.nama, iv.gudang_id
//                 ) x
//            where (x.item_code ilike '%$q%' or x.nama ilike '%$q%');";
//            $data = DB::select($dbQuery);
////            return BeoneInventory::with(['beone_item'])
////                ->groupBy('item_id')
////                ->select('item_id', DB::raw('SUM(qty_in-qty_out) as stok_akhir'))
////                ->whereILike(['beone_item.item_code', 'beone_item.nama'], $q)
////                ->where('gudang_id', $gudang_id)
////                ->get();
//            return Helper::s2($q, $data);
//        }
//    }
}
