<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterKategoriItemController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneItemCategory::find($uid);
            return view('admin.menus.master.kategori_item.detail', compact('detail'));
        }
        return view('admin.menus.master.kategori_item.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.kategori_item.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required',
            'keterangan' => 'required|min:3|max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Kategori Item Berhasil Dibuat';

            $data = new BeoneItemCategory();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneItemCategory::find($uid);
                $message = 'Kategori Item Berhasil Dibuat';
            }

            $data->item_category_code = $inputs->input('category_code');
            $data->nama = $inputs->input('nama');
            $data->keterangan = $inputs->input('keterangan');
            $data->flag = 1;
            $data->save();

            DB::commit();
            return Helper::redirect('kategori_item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Kategori Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneItemCategory::find($uid)->delete();

        return Helper::redirect('kategori_item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['item_type_id', 'nama', 'keterangan'];
//        $model = DB::table('beone_item_type')->select($field)->whereNull('deleted_at');
        $model = BeoneItemCategory::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/kategori_item/detail?id=' . $item->item_category_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/kategori_item/del?id=' . $item->item_category_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();

        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
