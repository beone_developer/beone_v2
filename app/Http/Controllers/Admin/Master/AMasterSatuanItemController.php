<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneSatuanItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterSatuanItemController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneSatuanItem::with(['beone_item'])->find($uid);
//            $detail = Helper::s2($detail, 'beone_item', 'item_id', 'nama', true);
            return view('admin.menus.master.satuan_item.detail', compact('detail'));
        }
        return view('admin.menus.master.satuan_item.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.satuan_item.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'satuan_code' => 'required',
            'keterangan' => 'required|min:3|max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Satuan Item Berhasil Dibuat';

            $data = new BeoneSatuanItem();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneSatuanItem::find($uid);
                $message = 'Satuan Item Berhasil Diedit';
            }
            $data->item_id = $inputs->input('item_id')->id;
            $data->satuan_code = $inputs->input('satuan_code');
            $data->rasio = $inputs->input('rasio');
            $data->keterangan = $inputs->input('keterangan');
            $data->save();

            DB::commit();
            return Helper::redirect('satuan_item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Satuan Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneSatuanItem::find($uid)->delete();

        return Helper::redirect('satuan_item.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['satuan_id', 'satuan_code', 'item_id', 'rasio'];
//        $model = DB::table('beone_satuan_item')->select($field);
        $model = BeoneSatuanItem::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/satuan_item/detail?id=' . $item->satuan_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/satuan_item/del?id=' . $item->satuan_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
