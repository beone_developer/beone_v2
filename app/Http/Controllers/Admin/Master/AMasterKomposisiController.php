<?php

namespace App\Http\Controllers\Admin\Master;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterKomposisiController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneItem::with(['beone_komposisi_to', 'beone_komposisi_from'])->find($uid);
            return view('admin.menus.master.komposisi.detail', compact('detail'));
        }
        return view('admin.menus.master.komposisi.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.komposisi.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'beone_item_jadi' => 'required_without:id',
            'beone_komposisi_from' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $status = 'Sukses ';
            $message = 'Komposisi Telah Diedit';

            $data = BeoneItem::find($uid);

            if ($inputs->has('beone_item_jadi')) {
                $status = 'Sukses ';
                $message = 'Komposisi Telah Diedit';
                $item_jadi = $inputs->input('beone_item_jadi');
                $data = BeoneItem::find($item_jadi->id);
            }

            $beone_komposisi_froms = [];
            $detail = $inputs->input('beone_komposisi_from');

            foreach ($detail as $key => $value) {
                if ($value['beone_item_baku']->id <> '') {
                    $beone_komposisi_from = [
                        'item_baku_id' => $value['beone_item_baku']->id,
                        'qty_item_baku' => $value['qty_item_baku'],
                        'flag' => 1,
                    ];
                    $beone_komposisi_froms[] = $beone_komposisi_from;
                }
            }

            $data->beone_komposisi_from()->delete();
            $data->beone_komposisi_from()->createMany($beone_komposisi_froms);

            DB::commit();
            return redirect()->route('komposisi.list')->withStatus([
                'alert' => 'alert-success',
                'status' => $status,
                'message' => $message,
            ]);
        } catch
        (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withStatus([
                'alert' => 'alert-warning',
                'status' => 'Warning! ',
                'message' => 'Gagal menyimpan data .' . $e->getMessage(),
            ]);
        }
    }

    public function deleteData(Request $request)
    {
        $status = 'Sukses ';
        $message = 'Komposisi Berhasil Dihapus';
        $item = BeoneItem::find($request->input('id'));
        $item->beone_komposisi_from()->delete();
        return redirect()->route('komposisi.list')->withStatus([
            'alert' => 'alert-success',
            'status' => $status,
            'message' => $message,
        ]);
    }

    public function dataTable(Request $request)
    {
//        $model = BeoneItem::with(['beone_komposisi_to', 'beone_komposisi_from'])->whereHas('beone_komposisi_from')->get();
        $model = DB::select('select i.item_id,i.item_code,i.nama from beone_item i where exists (select komposisi_id from beone_komposisi k where i.item_id = k.item_jadi_id limit 1)');
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('komposisi.detail', ['id' => $item->item_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('komposisi.delete', ['id' => $item->item_id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
