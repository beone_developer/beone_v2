<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneSupplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterSupplierController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneSupplier::find($uid);
            return view('admin.menus.master.supplier.detail', compact('detail'));
        }
        return view('admin.menus.master.supplier.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.supplier.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'supplier_code' => 'required', Rule::unique('beone_supplier')->ignore($request->input('supplier_code')),
            'nama' => 'required|min:3|max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Supplier Berhasil Dibuat';

            $data = new BeoneSupplier();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneSupplier::find($uid);
                $message = 'Supplier Berhasil Dibuat';
            }

            $data->supplier_code = $inputs->input('supplier_code');
            $data->nama = $inputs->input('nama');
            $data->alamat = $inputs->input('alamat');
            $data->ppn = $inputs->has('ppn');
            $data->pph21 = $inputs->has('pph21');
            $data->pph23 = $inputs->has('pph23');
            $data->save();

            DB::commit();
            return Helper::redirect('supplier.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Supplier Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        BeoneSupplier::find($uid)->delete();
        return Helper::redirect('supplier.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['supplier_id', 'supplier_code', 'nama', 'alamat', 'ppn', 'pph21', 'pph23'];
//        $model = DB::table('beone_supplier')->select($field)->whereNull('deleted_at');
        $model = BeoneSupplier::query();
        $datatable = Datatables::of($model)
//            ->editColumn('ppn', function ($item) {
//                return
//                    '<div class="text-center form-check">
//                        <input class="form-check-input position-static" type="checkbox" ' . ($item->ppn ? 'checked=checked' : '') . ' disabled>
//                    </div>';
//            })
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/supplier/detail?id=' . $item->supplier_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/supplier/del?id=' . $item->supplier_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneSupplier::where('supplier_code', 'ilike', $q)->orWhere('nama', 'ilike', $q)->get();
            $total_count = $items->count();
//            $items = Helper::s2($items, '', 'id', 'text');
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
