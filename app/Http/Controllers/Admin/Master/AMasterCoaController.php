<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneCoa;
use App\Models\BeoneTipeCoa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterCoaController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            //$detail = BeoneCoa::find($uid);
            $detail = BeoneCoa::with(['beone_tipe_coa'])->find($uid);
//            return $detail;
            return view('admin.menus.master.coa.detail', compact('detail'));
        }
        return view('admin.menus.master.coa.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.coa.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nomor' => 'required', Rule::unique('beone_coa')->ignore($request->input('nomor')),
            'nama' => 'required|min:3|max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Akun Telah Terbuat';

            $data = new BeoneCoa();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneCoa::find($uid);
                $message = 'Akun Telah Terbuat';
            }

            $data->nomor = $inputs->input('nomor');
            $data->nama = $inputs->input('nama');
            $data->tipe_akun = $inputs->input('tipe_akun')->id;
            $data->dk = $inputs->input('dk');
            $data->save();

            DB::commit();
            return Helper::redirect('coa.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Coa Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneCoa::find($uid)->delete();

        return Helper::redirect('coa.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $model = DB::table('beone_coa')
            ->join('beone_tipe_coa', 'beone_coa.tipe_akun', '=', 'beone_tipe_coa.tipe_coa_id')
            ->select('beone_coa.coa_id', 'beone_coa.nomor', 'beone_coa.nama', 'beone_coa.dk', 'beone_tipe_coa.nama as tnama')->get();

        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/coa/detail?id=' . $item->coa_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/coa/del?id=' . $item->coa_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchDataTipe(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneTipeCoa::where('nama', 'ilike', $q)->get();
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

}
