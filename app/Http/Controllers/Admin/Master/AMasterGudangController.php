<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneGudang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMasterGudangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BeoneGudang::find($uid);
            return view('admin.menus.master.gudang.detail', compact('detail'));
        }
        return view('admin.menus.master.gudang.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.gudang.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required', Rule::unique('BeoneGudang')->ignore($request->input('nama')),
            'keterangan' => 'required|min:3|max:100',
        ], $message);

        $inputs = Helper::merge($request);

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Gudang Berhasil Dibuat';

            $data = new BeoneGudang();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BeoneGudang::find($uid);
                $message = 'Gudang Berhasil Diedit';
            }

            $data->nama = $inputs->input('nama');
            $data->keterangan = $inputs->input('keterangan');
            $data->save();

            DB::commit();
            return Helper::redirect('gudang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Gudang Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BeoneGudang::find($uid)->delete();

        return Helper::redirect('gudang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
//        $field = ['gudang_id', 'nama', 'keterangan'];
//        $model = DB::table('beone_gudang')->select($field)->whereNull('deleted_at');
        $model = BeoneGudang::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . url('master/gudang/detail?id=' . $item->gudang_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . url('master/gudang/del?id=' . $item->gudang_id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();

        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function searchData(Request $request)
    {
        $q = $request->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = BeoneGudang::where('nama', 'ilike', $q)->orWhere('keterangan', 'ilike', $q)->get();
            $total_count = $items->count();
//            $items = Helper::s2($items, '', 'id', 'text');
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
