<?php

namespace App\Http\Middleware;

use App\Http\Kernel;
use App\Models\BeoneKonfigurasiPerusahaan;
use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

class TrustAuth
{
    public function handle($request, Closure $next)
    {
        $stats = "";
        try {
            $info = BeoneKonfigurasiPerusahaan::first();
            $gate = Kernel::get(config('auth.trusted.gate'));
            $client = new Client();
            $res = $client->get($gate, [
                'timeout' => config('auth.trusted.expire'),
                'query' => $info->toArray(),
            ]);
            $contents = $res->getBody()->getContents();
            $data = json_decode($contents);
            if (isset(json_decode($contents)->error)) {
                $error = $data->status;
                return response()->view('admin.menus.error.index', compact('error'), $res->getStatusCode());
            }
        } catch (ConnectException $e) {
            $stats = Kernel::get(config('auth.trusted.stats'));
        } catch (ServerException $e) {
            $stats = Kernel::get(config('auth.trusted.stats'));
        } catch (RequestException $e) {
            $stats = Kernel::get(config('auth.trusted.stats'));
        }
        view()->share('stats', $stats);
        return $next($request);
    }
}
