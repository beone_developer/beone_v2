<?php

namespace App\Providers;

use App\Helpers\Helper;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeoneUser;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        view()->composer('*', function ($view) {
            if (Auth::check()) {
                $user = BeoneUser::with(['beone_roles_users.beone_moduls'])->find(Auth::id());
                $g_settings = BeoneKonfigurasiPerusahaan::first();
                $g_moduls = $user->beone_roles_users->beone_moduls;
                $g_sidebar = Helper::sidebar(request(), collect($g_moduls)->toArray());
                $view->with('g_sidebar', $g_sidebar);
                $view->with('g_moduls', $g_moduls);
                $view->with('g_settings', $g_settings);
            }
        });
    }
}

//subkon out ada jaminan barang
//3 barang jadi 1 + jasa
//261
//262
