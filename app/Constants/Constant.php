<?php

namespace App\Constants;

class Constant
{
    const AlertWarning = 'alert-warning';
    const AlertError = 'alert-error';
    const AlertSuccess = 'alert-success';

    const TitleWarning = 'Warning! ';
    const TitleError = 'Error! ';
    const TitleSuccess = 'Success! ';


    const PrintBuktiKas = 'BuktiKas';
    const PrintSalesOrder = 'SalesOrder';
}
