<?php

namespace App\Helpers;

use App\Constants\Constant;
use App\Models\BeoneKonfigurasiPerusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Helper
{
    public static function toStd($array)
    {
        if (is_array($array)) {
            return (object)array_map(self::toStd($array), $array);
        } else {
            return $array;
        }
    }

    public static function getTransNo($inisial, $trans_date, $model, $column)
    {
        $prefix = self::getPrefixNo($inisial, $trans_date);
        $last_trans = $model::where($column, 'ilike', $prefix . '%')->orderByDesc($column)->withTrashed()->first();
        $no = 1;
        if ($last_trans) {
            $last_trans->toArray();
            $no = substr($last_trans[$column], strlen($prefix), 6) + 1;
        }
        return $prefix . sprintf('%06d', $no);
    }

    public static function getPrefixNo($inisial, $trans_date)
    {
        $cabang = BeoneKonfigurasiPerusahaan::first('cabang')->cabang;
        $dd = $trans_date->format('d');
        $mm = $trans_date->format('m');
        $yy = $trans_date->format('y');
        return $cabang . '/' . $inisial . '/' . $yy . $mm . '/' . $dd . '/';
    }

    public static function strChar(string $string)
    {
        $words = preg_split("/\s+/", $string);
        $res = '';
        foreach ($words as $word) {
            $res .= ucfirst($word[0]);
        }
        return substr($res, 0, 2);
    }

    public static function encode($data)
    {
        if (empty($data)) {
            return '';
        }
        return json_encode($data);
    }

    public static function isJson($str)
    {
        $result = false;
        if (!preg_match("/^\d+$/", trim($str))) {
            json_decode($str);
            $result = (json_last_error() == JSON_ERROR_NONE);
        }
        return $result;
    }

    public static function merge(Request $request)
    {
        $data = self::decode($request->all());
        $req = $request->duplicate();
        return $req->merge($data);
    }

    public static function decode($datas)
    {
        $result = [];
        foreach ($datas as $i => $value) {
            if (is_array($value)) {
                $result[$i] = self::decode($value);
                continue;
            }
            if (self::isJson($value)) {
                $result[$i] = json_decode($value);
                continue;
            }
            $result[$i] = $value;
        }
        return $result;
    }

    public static function s2($q, $model)
    {
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            if (is_array($model)) {
                $items = $model;
                $total_count = count($model);
            } else {
                $items = $model->get();
                $total_count = $items->count();
            }
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public static function pgError($message)
    {
        $message = trim(preg_replace('/\s+/', ' ', $message));
        $needle_start = 'ERROR: ';
        $needle_end_ctx = 'CONTEXT: ';
        $needle_end_det = 'DETAIL: ';
        $needle_abort = "transaction is aborted";
        if (strpos($message, $needle_abort) > 1) {
            return "There's an error when processing your query.";
        }
        $pos_start = strpos($message, $needle_start) + strlen($needle_start);
        $pos_end_ctx = strpos($message, $needle_end_ctx);
        $pos_end_det = strpos($message, $needle_end_det);
        if ($pos_end_ctx > 0) {
            $pos_end = $pos_end_ctx - $pos_start - 1;
        } else if ($pos_end_det > 0) {
            $pos_end = $pos_end_det - $pos_start - 1;
        } else {
            $pos_end = strlen($message);
        }
        $message = substr($message, $pos_start, $pos_end);
        return str_replace('"', "`", $message);
    }

    public static function redirect($route, $alert, $title, $message)
    {
        if (!$route) {
            $message = self::pgError($message);
            if ($alert == Constant::AlertSuccess) {
                $message = 'Terjadi Kesalahan. ' . $message;
            }
            return redirect()->back()->withInput()->withStatus([
                'alert' => $alert,
                'status' => $title,
                'message' => $message,
            ]);
        }
        return redirect()->route($route)->withStatus([
            'alert' => $alert,
            'status' => $title,
            'message' => $message,
        ]);
    }

    public static function selectMany($query)
    {
        $query = rtrim($query, ";");
        $querys = explode(';', $query);
        $res = '';
        foreach ($querys as $i => $q) {
            if (count($querys) - 1 == $i) {
                $res = DB::select(DB::raw($q));
                break;
            }
            DB::select(DB::raw($q));
        }
        return $res;
    }

    public static function hasAccess($moduls, $url)
    {
        $collection = collect($moduls);
        return $collection->contains('url', $url);
    }

    public static function genSidebar(Request $request, $trees)
    {
        $result = '';
        foreach ($trees as $i => $value) {
            if ($value['is_hidden']) continue;
            $result .= '<li class="kt-menu__item ' . $request->is($value['modul']) . '"
                        aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                        <a href="' . ($value['url'] != "#" ? route($value['url']) : "javascript:void(0);") . '" class="kt-menu__link kt-menu__toggle">';
            if ($value['icon_class']) {
                $result .= '<span class="kt-menu__link-icon">
                            <i class="' . $value['icon_class'] . '" aria-hidden="true"></i>
                        </span>';
            }
            $result .= '<span class="kt-menu__link-text">' . $value['modul'] . '</span>';
            if ($value['items']) {
                $result .= '<i class="kt-menu__ver-arrow la la-angle-right"></i>';
            }
            $result .= '</a>';
            if ($value['items']) {
                $result .= '<div class="kt-menu__submenu ">
                            <span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">';
                $result .= self::genSidebar($request, $value['items']);
                $result .= '</ul></div>';
            }
            $result .= '</li>';
        }
        return $result;
    }

    public static function ungroupTreeMenu($menus = null, $parent = null)
    {
        $result = [];
        if (!is_array($menus)) {
            $menus = func_get_args();
        }
        foreach ($menus as $i => $val) {
            $val = collect($val)->toArray();
            if (isset($val['items']) && (count($val['items']))) {
                $result = array_merge($result, self::ungroupTreeMenu($val['items'], $val['modul_id']));
            }
            unset($val['items']);
            unset($val['pivot']);
            $val['parent_id'] = $parent;
            $result = array_merge($result, [$i => $val]);
        }
        return $result;
    }

    public static function groupTreeMenu($menus)
    {
        $results = [];
        $children = [];
        foreach ($menus as $val) {
            if (isset($val['pivot'])) {
                $i_id = $val['pivot']['modul_id'];
                $p_id = $val['pivot']['parent_id'];
            } else {
                $i_id = $val['modul_id'];
                $p_id = $val['parent_id'];
            }

            if (!isset($children[$i_id])) {
                $children[$i_id] = [];
            }
            $val['items'] = &$children[$i_id];
            if (!strlen($p_id)) {
                $results[] = $val;
            } else {
                if (!isset($children[$p_id])) {
                    $children[$p_id] = [];
                }
                $children[$p_id][] = $val;
            }
        }
        return $results;
    }

    public static function sidebar(Request $request, $moduls)
    {
        $trees = self::groupTreeMenu($moduls);
        return self::genSidebar($request, $trees);
    }

    public static function strStrip($str)
    {
        return str_replace(array('\'', '"'), '', $str);
    }
}
