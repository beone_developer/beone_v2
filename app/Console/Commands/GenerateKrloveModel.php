<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GenerateKrloveModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pg:models {--table=} {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate models';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('all')) {
            $tables = DB::select("SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname='public'");
            $databaseName = config('database.connections.' . config('database.default') . '.database');
            $tables = collect($tables);
            foreach ($tables as $table) {
                if ($table === 'migrations') continue;
                $className = Str::camel(Str::singular($table->tablename));
                $className = ucfirst($className);
                Artisan::call('krlove:generate:model', [
                    'class-name' => $className,
                    '--table-name' => $table->tablename,
                    '--output-path' => app_path() . '/Models',
                    '--namespace=' => ' App\\Models'
                ]);
            }
            return;
        }
        $tableName = $this->option('table');
        $className = str_replace('_', ' ', $tableName);
        $className = ucwords($className);
        $className = str_replace(' ', '', $className);
        Artisan::call('krlove:generate:model', [
            'class-name' => $className,
            '--table-name' => $tableName,
            '--output-path' => app_path() . '/Models',
            '--namespace' => ' App\\Models'
        ]);
    }
}
