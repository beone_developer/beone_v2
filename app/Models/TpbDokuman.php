<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbDokuman
 * 
 * @property int $ID
 * @property string $FLAG_URL_DOKUMEN
 * @property string $KODE_JENIS_DOKUMEN
 * @property string $NOMOR_DOKUMEN
 * @property int $SERI_DOKUMEN
 * @property Carbon $TANGGAL_DOKUMEN
 * @property string $TIPE_DOKUMEN
 * @property string $URL_DOKUMEN
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbDokuman extends Model
{
	protected $table = 'tpb_dokumen';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'SERI_DOKUMEN' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'TANGGAL_DOKUMEN'
	];

	protected $fillable = [
		'FLAG_URL_DOKUMEN',
		'KODE_JENIS_DOKUMEN',
		'NOMOR_DOKUMEN',
		'SERI_DOKUMEN',
		'TANGGAL_DOKUMEN',
		'TIPE_DOKUMEN',
		'URL_DOKUMEN',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
