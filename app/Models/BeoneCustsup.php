<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $custsup_id
 * @property string $nama
 * @property string $alamat
 * @property int $tipe_custsup
 * @property int $negara
 * @property float $saldo_hutang_idr
 * @property float $saldo_hutang_valas
 * @property float $saldo_piutang_idr
 * @property float $saldo_piutang_valas
 * @property int $flag
 * @property float $pelunasan_hutang_idr
 * @property float $pelunasan_hutang_valas
 * @property float $pelunasan_piutang_idr
 * @property float $pelunasan_piutang_valas
 * @property int $status_lunas_piutang
 * @property int $status_lunas_hutang
 */
class BeoneCustsup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_custsup';

    /**
     * @var array
     */
    protected $fillable = ['custsup_id', 'nama', 'alamat', 'tipe_custsup', 'negara', 'saldo_hutang_idr', 'saldo_hutang_valas', 'saldo_piutang_idr', 'saldo_piutang_valas', 'flag', 'pelunasan_hutang_idr', 'pelunasan_hutang_valas', 'pelunasan_piutang_idr', 'pelunasan_piutang_valas', 'status_lunas_piutang', 'status_lunas_hutang'];

}
