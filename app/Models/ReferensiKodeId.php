<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKodeId
 * 
 * @property int $ID
 * @property string $KODE_ID
 * @property string $URAIAN_KODE_ID
 *
 * @package App\Models
 */
class ReferensiKodeId extends Model
{
	protected $table = 'referensi_kode_id';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_ID',
		'URAIAN_KODE_ID'
	];
}
