<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPpjk
 * 
 * @property int $ID
 * @property string $ALAMAT
 * @property string $KODE_ID
 * @property string $NAMA
 * @property string $NOMOR_NPPPJK
 * @property string $NPWP
 * @property Carbon $TANGGAL_NPPPJK
 *
 * @package App\Models
 */
class ReferensiPpjk extends Model
{
	protected $table = 'referensi_ppjk';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $dates = [
		'TANGGAL_NPPPJK'
	];

	protected $fillable = [
		'ALAMAT',
		'KODE_ID',
		'NAMA',
		'NOMOR_NPPPJK',
		'NPWP',
		'TANGGAL_NPPPJK'
	];
}
