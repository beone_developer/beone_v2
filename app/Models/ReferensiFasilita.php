<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiFasilita
 * 
 * @property int $ID
 * @property string $KODE_FASILITAS
 * @property string $URAIAN_FASILITAS
 *
 * @package App\Models
 */
class ReferensiFasilita extends Model
{
	protected $table = 'referensi_fasilitas';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_FASILITAS',
		'URAIAN_FASILITAS'
	];
}
