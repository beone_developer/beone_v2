<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiSatuan
 * 
 * @property int $ID
 * @property string $KODE_SATUAN
 * @property string $URAIAN_SATUAN
 *
 * @package App\Models
 */
class ReferensiSatuan extends Model
{
	protected $table = 'referensi_satuan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_SATUAN',
		'URAIAN_SATUAN'
	];
}
