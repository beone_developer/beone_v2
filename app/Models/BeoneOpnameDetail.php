<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $opname_detail_id
 * @property int $opname_header_id
 * @property int $item_id
 * @property float $qty_existing
 * @property float $qty_opname
 * @property float $qty_selisih
 * @property int $status_opname
 * @property int $flag
 */
class BeoneOpnameDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_opname_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'opname_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['opname_header_id', 'item_id', 'qty_existing', 'qty_opname', 'qty_selisih', 'status_opname', 'flag'];

}
