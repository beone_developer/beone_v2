<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $komposisi_id
 * @property int $item_jadi_id
 * @property int $item_baku_id
 * @property float $qty_item_baku
 * @property int $flag
 */
class BeoneKomposisi extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_komposisi';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'komposisi_id';

    /**
     * @var array
     */
    protected $fillable = ['item_jadi_id', 'item_baku_id', 'qty_item_baku', 'flag'];

    public function beone_item_jadi()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_jadi_id');
    }

    public function beone_item_baku()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_baku_id');
    }

    public $appends = ['item_id', 'item_nama', 'qty'];

//    function getItemIdAttribute()
//    {
//        $item = $this->beone_item_jadi;
//        return $item->item_code . " (" . $item->nama . ")";
//    }

    function getItemIdAttribute()
    {
        $item = $this->beone_item_baku;
        return $item->item_id;
    }

    function getItemNamaAttribute()
    {
        $item = $this->beone_item_baku;
        return $item->item_code . " (" . $item->nama . ")";
    }

    function getQtyAttribute()
    {
        return $this->qty_item_baku;
    }


}
