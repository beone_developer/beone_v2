<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $konversi_stok_detail_id
 * @property int $konversi_stok_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $qty_real
 * @property int $satuan_qty
 * @property int $gudang_id
 */
class BeoneKonversiStokDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_konversi_stok_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'konversi_stok_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['konversi_stok_header_id', 'item_id', 'qty', 'qty_real', 'satuan_qty', 'gudang_id', 'satuan_id', 'rasio'];

    public $appends = ['item_nama'];

    function getItemNamaAttribute()
    {
        return $this->beone_item->text;
    }

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }
}
