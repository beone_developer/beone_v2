<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nama_perusahaan
 * @property string $alamat_perusahaan
 * @property string $kota_perusahaan
 * @property string $provinsi_perusahaan
 * @property string $kode_pos_perusahaan
 * @property string $logo_perusahaan
 */
class BeoneKonfigurasiPerusahaan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_konfigurasi_perusahaan';

    /**
     * @var array
     */
    protected $fillable = ['nama_perusahaan', 'alamat_perusahaan', 'kota_perusahaan', 'provinsi_perusahaan', 'kode_pos_perusahaan', 'logo_perusahaan', 'receiving_multi_po'];

}
