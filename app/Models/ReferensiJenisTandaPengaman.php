<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisTandaPengaman
 * 
 * @property int $KODE_JENIS_TANDA_PENGAMAN
 * @property string $URAIAN_JENIS_TANDA_PENGAMAN
 *
 * @package App\Models
 */
class ReferensiJenisTandaPengaman extends Model
{
	protected $table = 'referensi_jenis_tanda_pengaman';
	protected $primaryKey = 'KODE_JENIS_TANDA_PENGAMAN';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'KODE_JENIS_TANDA_PENGAMAN' => 'int'
	];

	protected $fillable = [
		'URAIAN_JENIS_TANDA_PENGAMAN'
	];
}
