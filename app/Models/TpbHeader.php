<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbHeader
 *
 * @property int $ID
 * @property string $ALAMAT_PEMASOK
 * @property string $ALAMAT_PEMILIK
 * @property string $ALAMAT_PENERIMA_BARANG
 * @property string $ALAMAT_PENGIRIM
 * @property string $ALAMAT_PENGUSAHA
 * @property string $ALAMAT_PPJK
 * @property string $API_PEMILIK
 * @property string $API_PENERIMA
 * @property string $API_PENGUSAHA
 * @property string $ASAL_DATA
 * @property float $ASURANSI
 * @property float $BIAYA_TAMBAHAN
 * @property float $BRUTO
 * @property float $CIF
 * @property float $CIF_RUPIAH
 * @property float $DISKON
 * @property string $FLAG_PEMILIK
 * @property string $FLAG_URL_DOKUMEN_PABEAN
 * @property float $FOB
 * @property float $FREIGHT
 * @property float $HARGA_BARANG_LDP
 * @property float $HARGA_INVOICE
 * @property float $HARGA_PENYERAHAN
 * @property float $HARGA_TOTAL
 * @property string $ID_MODUL
 * @property string $ID_PEMASOK
 * @property string $ID_PEMILIK
 * @property string $ID_PENERIMA_BARANG
 * @property string $ID_PENGIRIM
 * @property string $ID_PENGUSAHA
 * @property string $ID_PPJK
 * @property string $JABATAN_TTD
 * @property int $JUMLAH_BARANG
 * @property int $JUMLAH_KEMASAN
 * @property int $JUMLAH_KONTAINER
 * @property string $KESESUAIAN_DOKUMEN
 * @property string $KETERANGAN
 * @property string $KODE_ASAL_BARANG
 * @property string $KODE_ASURANSI
 * @property string $KODE_BENDERA
 * @property string $KODE_CARA_ANGKUT
 * @property string $KODE_CARA_BAYAR
 * @property string $KODE_DAERAH_ASAL
 * @property string $KODE_DOKUMEN_PABEAN
 * @property string $KODE_FASILITAS
 * @property string $KODE_FTZ
 * @property string $KODE_HARGA
 * @property string $KODE_ID_PEMASOK
 * @property string $KODE_ID_PEMILIK
 * @property string $KODE_ID_PENERIMA_BARANG
 * @property string $KODE_ID_PENGIRIM
 * @property string $KODE_ID_PENGUSAHA
 * @property string $KODE_ID_PPJK
 * @property string $KODE_JENIS_API
 * @property string $KODE_JENIS_API_PEMILIK
 * @property string $KODE_JENIS_API_PENERIMA
 * @property string $KODE_JENIS_API_PENGUSAHA
 * @property string $KODE_JENIS_BARANG
 * @property string $KODE_JENIS_BC25
 * @property string $KODE_JENIS_NILAI
 * @property string $KODE_JENIS_PEMASUKAN01
 * @property string $KODE_JENIS_PEMASUKAN02
 * @property string $KODE_JENIS_TPB
 * @property string $KODE_KANTOR
 * @property string $KODE_KANTOR_BONGKAR
 * @property string $KODE_KANTOR_TUJUAN
 * @property string $KODE_LOKASI_BAYAR
 * @property string $KODE_NEGARA_PEMASOK
 * @property string $KODE_NEGARA_PEMILIK
 * @property string $KODE_NEGARA_PENGIRIM
 * @property string $KODE_NEGARA_TUJUAN
 * @property string $KODE_PEL_BONGKAR
 * @property string $KODE_PEL_MUAT
 * @property string $KODE_PEL_TRANSIT
 * @property string $KODE_PEMBAYAR
 * @property string $KODE_STATUS
 * @property string $KODE_STATUS_PENGUSAHA
 * @property string $KODE_STATUS_PERBAIKAN
 * @property string $KODE_TPS
 * @property string $KODE_TUJUAN_PEMASUKAN
 * @property string $KODE_TUJUAN_PENGIRIMAN
 * @property string $KODE_TUJUAN_TPB
 * @property string $KODE_TUTUP_PU
 * @property string $KODE_VALUTA
 * @property string $KOTA_TTD
 * @property string $LOKASI_ASAL
 * @property string $LOKASI_TUJUAN
 * @property string $NAMA_PEMASOK
 * @property string $NAMA_PEMILIK
 * @property string $NAMA_PENERIMA_BARANG
 * @property string $NAMA_PENGANGKUT
 * @property string $NAMA_PENGIRIM
 * @property string $NAMA_PENGUSAHA
 * @property string $NAMA_PPJK
 * @property string $NAMA_TTD
 * @property float $NDPBM
 * @property float $NETTO
 * @property float $NILAI_INCOTERM
 * @property string $NIPER_PENERIMA
 * @property string $NOMOR_AJU
 * @property string $NOMOR_API
 * @property string $NOMOR_BC11
 * @property string $NOMOR_BILLING
 * @property string $NOMOR_DAFTAR
 * @property string $NOMOR_IJIN_BPK_PEMASOK
 * @property string $NOMOR_IJIN_BPK_PENGUSAHA
 * @property string $NOMOR_IJIN_TPB
 * @property string $NOMOR_IJIN_TPB_PENERIMA
 * @property string $NOMOR_POLISI
 * @property string $NOMOR_VOY_FLIGHT
 * @property string $NPPPJK
 * @property string $NPWP_BILLING
 * @property string $POS_BC11
 * @property int $SERI
 * @property string $SUBPOS_BC11
 * @property string $SUBSUBPOS_BC11
 * @property Carbon $TANGGAL_AJU
 * @property Carbon $TANGGAL_BC11
 * @property Carbon $TANGGAL_BERANGKAT
 * @property Carbon $TANGGAL_BILLING
 * @property Carbon $TANGGAL_DAFTAR
 * @property Carbon $TANGGAL_IJIN_BPK_PEMASOK
 * @property Carbon $TANGGAL_IJIN_BPK_PENGUSAHA
 * @property Carbon $TANGGAL_IJIN_TPB
 * @property Carbon $TANGGAL_NPPPJK
 * @property Carbon $TANGGAL_TIBA
 * @property Carbon $TANGGAL_TTD
 * @property Carbon $TGL_JATUH_TEMPO_BILLING
 * @property float $TOTAL_BAYAR
 * @property float $TOTAL_BEBAS
 * @property float $TOTAL_DILUNASI
 * @property float $TOTAL_JAMIN
 * @property float $TOTAL_SUDAH_DILUNASI
 * @property float $TOTAL_TANGGUH
 * @property float $TOTAL_TANGGUNG
 * @property float $TOTAL_TIDAK_DIPUNGUT
 * @property string $URL_DOKUMEN_PABEAN
 * @property string $VERSI_MODUL
 * @property float $VOLUME
 * @property Carbon $WAKTU_BONGKAR
 * @property Carbon $WAKTU_STUFFING
 * @property Carbon $TANGGAL_MUAT
 * @property string $TEMPAT_STUFFING
 * @property string $CALL_SIGN
 * @property int $JUMLAH_TANDA_PENGAMAN
 * @property string $KODE_JENIS_TANDA_PENGAMAN
 * @property string $KODE_KANTOR_MUAT
 * @property string $KODE_PEL_TUJUAN
 * @property Carbon $TANGGAL_STUFFING
 * @property string $KODE_GUDANG_ASAL
 * @property string $KODE_GUDANG_TUJUAN
 *
 * @property Collection|TpbBahanBaku[] $tpb_bahan_bakus
 * @property Collection|TpbBahanBakuDokuman[] $tpb_bahan_baku_dokumen
 * @property Collection|TpbBahanBakuTarif[] $tpb_bahan_baku_tarifs
 * @property Collection|TpbBarang[] $tpb_barangs
 * @property Collection|TpbBarangDokuman[] $tpb_barang_dokumen
 * @property Collection|TpbBarangTarif[] $tpb_barang_tarifs
 * @property Collection|TpbDetilStatus[] $tpb_detil_statuses
 * @property Collection|TpbDokuman[] $tpb_dokumen
 * @property Collection|TpbJaminan[] $tpb_jaminans
 * @property Collection|TpbKemasan[] $tpb_kemasans
 * @property Collection|TpbKontainer[] $tpb_kontainers
 * @property Collection|TpbNpwpBilling[] $tpb_npwp_billings
 * @property Collection|TpbPungutan[] $tpb_pungutans
 * @property Collection|TpbRespon[] $tpb_respons
 *
 * @package App\Models
 */
class TpbHeader extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tpb_header';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    protected $casts = [
        'ASURANSI' => 'float',
        'BIAYA_TAMBAHAN' => 'float',
        'BRUTO' => 'float',
        'CIF' => 'float',
        'CIF_RUPIAH' => 'float',
        'DISKON' => 'float',
        'FOB' => 'float',
        'FREIGHT' => 'float',
        'HARGA_BARANG_LDP' => 'float',
        'HARGA_INVOICE' => 'float',
        'HARGA_PENYERAHAN' => 'float',
        'HARGA_TOTAL' => 'float',
        'JUMLAH_BARANG' => 'int',
        'JUMLAH_KEMASAN' => 'int',
        'JUMLAH_KONTAINER' => 'int',
        'NDPBM' => 'float',
        'NETTO' => 'float',
        'NILAI_INCOTERM' => 'float',
        'SERI' => 'int',
        'TOTAL_BAYAR' => 'float',
        'TOTAL_BEBAS' => 'float',
        'TOTAL_DILUNASI' => 'float',
        'TOTAL_JAMIN' => 'float',
        'TOTAL_SUDAH_DILUNASI' => 'float',
        'TOTAL_TANGGUH' => 'float',
        'TOTAL_TANGGUNG' => 'float',
        'TOTAL_TIDAK_DIPUNGUT' => 'float',
        'VOLUME' => 'float',
        'JUMLAH_TANDA_PENGAMAN' => 'int'
    ];

    protected $dates = [
        'TANGGAL_AJU',
        'TANGGAL_BC11',
        'TANGGAL_BERANGKAT',
        'TANGGAL_BILLING',
        'TANGGAL_DAFTAR',
        'TANGGAL_IJIN_BPK_PEMASOK',
        'TANGGAL_IJIN_BPK_PENGUSAHA',
        'TANGGAL_IJIN_TPB',
        'TANGGAL_NPPPJK',
        'TANGGAL_TIBA',
        'TANGGAL_TTD',
        'TGL_JATUH_TEMPO_BILLING',
        'WAKTU_BONGKAR',
        'WAKTU_STUFFING',
        'TANGGAL_MUAT',
        'TANGGAL_STUFFING'
    ];

    protected $fillable = [
        'ALAMAT_PEMASOK',
        'ALAMAT_PEMILIK',
        'ALAMAT_PENERIMA_BARANG',
        'ALAMAT_PENGIRIM',
        'ALAMAT_PENGUSAHA',
        'ALAMAT_PPJK',
        'API_PEMILIK',
        'API_PENERIMA',
        'API_PENGUSAHA',
        'ASAL_DATA',
        'ASURANSI',
        'BIAYA_TAMBAHAN',
        'BRUTO',
        'CIF',
        'CIF_RUPIAH',
        'DISKON',
        'FLAG_PEMILIK',
        'FLAG_URL_DOKUMEN_PABEAN',
        'FOB',
        'FREIGHT',
        'HARGA_BARANG_LDP',
        'HARGA_INVOICE',
        'HARGA_PENYERAHAN',
        'HARGA_TOTAL',
        'ID_MODUL',
        'ID_PEMASOK',
        'ID_PEMILIK',
        'ID_PENERIMA_BARANG',
        'ID_PENGIRIM',
        'ID_PENGUSAHA',
        'ID_PPJK',
        'JABATAN_TTD',
        'JUMLAH_BARANG',
        'JUMLAH_KEMASAN',
        'JUMLAH_KONTAINER',
        'KESESUAIAN_DOKUMEN',
        'KETERANGAN',
        'KODE_ASAL_BARANG',
        'KODE_ASURANSI',
        'KODE_BENDERA',
        'KODE_CARA_ANGKUT',
        'KODE_CARA_BAYAR',
        'KODE_DAERAH_ASAL',
        'KODE_DOKUMEN_PABEAN',
        'KODE_FASILITAS',
        'KODE_FTZ',
        'KODE_HARGA',
        'KODE_ID_PEMASOK',
        'KODE_ID_PEMILIK',
        'KODE_ID_PENERIMA_BARANG',
        'KODE_ID_PENGIRIM',
        'KODE_ID_PENGUSAHA',
        'KODE_ID_PPJK',
        'KODE_JENIS_API',
        'KODE_JENIS_API_PEMILIK',
        'KODE_JENIS_API_PENERIMA',
        'KODE_JENIS_API_PENGUSAHA',
        'KODE_JENIS_BARANG',
        'KODE_JENIS_BC25',
        'KODE_JENIS_NILAI',
        'KODE_JENIS_PEMASUKAN01',
        'KODE_JENIS_PEMASUKAN02',
        'KODE_JENIS_TPB',
        'KODE_KANTOR',
        'KODE_KANTOR_BONGKAR',
        'KODE_KANTOR_TUJUAN',
        'KODE_LOKASI_BAYAR',
        'KODE_NEGARA_PEMASOK',
        'KODE_NEGARA_PEMILIK',
        'KODE_NEGARA_PENGIRIM',
        'KODE_NEGARA_TUJUAN',
        'KODE_PEL_BONGKAR',
        'KODE_PEL_MUAT',
        'KODE_PEL_TRANSIT',
        'KODE_PEMBAYAR',
        'KODE_STATUS',
        'KODE_STATUS_PENGUSAHA',
        'KODE_STATUS_PERBAIKAN',
        'KODE_TPS',
        'KODE_TUJUAN_PEMASUKAN',
        'KODE_TUJUAN_PENGIRIMAN',
        'KODE_TUJUAN_TPB',
        'KODE_TUTUP_PU',
        'KODE_VALUTA',
        'KOTA_TTD',
        'LOKASI_ASAL',
        'LOKASI_TUJUAN',
        'NAMA_PEMASOK',
        'NAMA_PEMILIK',
        'NAMA_PENERIMA_BARANG',
        'NAMA_PENGANGKUT',
        'NAMA_PENGIRIM',
        'NAMA_PENGUSAHA',
        'NAMA_PPJK',
        'NAMA_TTD',
        'NDPBM',
        'NETTO',
        'NILAI_INCOTERM',
        'NIPER_PENERIMA',
        'NOMOR_AJU',
        'NOMOR_API',
        'NOMOR_BC11',
        'NOMOR_BILLING',
        'NOMOR_DAFTAR',
        'NOMOR_IJIN_BPK_PEMASOK',
        'NOMOR_IJIN_BPK_PENGUSAHA',
        'NOMOR_IJIN_TPB',
        'NOMOR_IJIN_TPB_PENERIMA',
        'NOMOR_POLISI',
        'NOMOR_VOY_FLIGHT',
        'NPPPJK',
        'NPWP_BILLING',
        'POS_BC11',
        'SERI',
        'SUBPOS_BC11',
        'SUBSUBPOS_BC11',
        'TANGGAL_AJU',
        'TANGGAL_BC11',
        'TANGGAL_BERANGKAT',
        'TANGGAL_BILLING',
        'TANGGAL_DAFTAR',
        'TANGGAL_IJIN_BPK_PEMASOK',
        'TANGGAL_IJIN_BPK_PENGUSAHA',
        'TANGGAL_IJIN_TPB',
        'TANGGAL_NPPPJK',
        'TANGGAL_TIBA',
        'TANGGAL_TTD',
        'TGL_JATUH_TEMPO_BILLING',
        'TOTAL_BAYAR',
        'TOTAL_BEBAS',
        'TOTAL_DILUNASI',
        'TOTAL_JAMIN',
        'TOTAL_SUDAH_DILUNASI',
        'TOTAL_TANGGUH',
        'TOTAL_TANGGUNG',
        'TOTAL_TIDAK_DIPUNGUT',
        'URL_DOKUMEN_PABEAN',
        'VERSI_MODUL',
        'VOLUME',
        'WAKTU_BONGKAR',
        'WAKTU_STUFFING',
        'TANGGAL_MUAT',
        'TEMPAT_STUFFING',
        'CALL_SIGN',
        'JUMLAH_TANDA_PENGAMAN',
        'KODE_JENIS_TANDA_PENGAMAN',
        'KODE_KANTOR_MUAT',
        'KODE_PEL_TUJUAN',
        'TANGGAL_STUFFING',
        'KODE_GUDANG_ASAL',
        'KODE_GUDANG_TUJUAN'
    ];

    public function tpb_bahan_bakus()
    {
        return $this->hasMany(TpbBahanBaku::class, 'ID_HEADER');
    }

    public function tpb_bahan_baku_dokumen()
    {
        return $this->hasMany(TpbBahanBakuDokuman::class, 'ID_HEADER');
    }

    public function tpb_bahan_baku_tarifs()
    {
        return $this->hasMany(TpbBahanBakuTarif::class, 'ID_HEADER');
    }

    public function tpb_barangs()
    {
        return $this->hasMany(TpbBarang::class, 'ID_HEADER');
    }

    public function tpb_barang_dokumen()
    {
        return $this->hasMany(TpbBarangDokuman::class, 'ID_HEADER');
    }

    public function tpb_barang_tarifs()
    {
        return $this->hasMany(TpbBarangTarif::class, 'ID_HEADER');
    }

    public function tpb_detil_statuses()
    {
        return $this->hasMany(TpbDetilStatus::class, 'ID_HEADER');
    }

    public function tpb_dokumen()
    {
        return $this->hasMany(TpbDokuman::class, 'ID_HEADER');
    }

    public function tpb_jaminans()
    {
        return $this->hasMany(TpbJaminan::class, 'ID_HEADER');
    }

    public function tpb_jaminan()
    {
        return $this->belongsTo(TpbJaminan::class, 'ID','ID_HEADER');
    }

    public function tpb_kemasans()
    {
        return $this->hasMany(TpbKemasan::class, 'ID_HEADER');
    }

    public function tpb_kontainers()
    {
        return $this->hasMany(TpbKontainer::class, 'ID_HEADER');
    }

    public function tpb_npwp_billings()
    {
        return $this->hasMany(TpbNpwpBilling::class, 'ID_HEADER');
    }

    public function tpb_pungutans()
    {
        return $this->hasMany(TpbPungutan::class, 'ID_HEADER');
    }

    public function tpb_respons()
    {
        return $this->hasMany(TpbRespon::class, 'ID_HEADER');
    }

    public $appends = ['CURRENCY', 'SUPPLIER', 'JENIS', 'AMOUNT'];

    function getAMOUNTAttribute()
    {
        if ($this->KODE_DOKUMEN_PABEAN == 23) {
            return $this->HARGA_INVOICE;
        }
        return $this->HARGA_PENYERAHAN;
    }

    function getJENISAttribute()
    {
        return "BC " . $this->KODE_DOKUMEN_PABEAN;
    }

    function getSUPPLIERAttribute()
    {
        if ($this->KODE_DOKUMEN_PABEAN == 23) {
            return $this->NAMA_PEMASOK;
        }
        return $this->NAMA_PEMILIK;
    }

    function getCURRENCYAttribute()
    {
        if ($this->KODE_DOKUMEN_PABEAN == 23) {
            return "USD";
        }
        return "IDR";
    }
}
