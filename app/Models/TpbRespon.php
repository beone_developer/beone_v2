<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbRespon
 * 
 * @property int $ID
 * @property string $BYTE_STREM_PDF
 * @property string $FLAG_BACA
 * @property string $KODE_RESPON
 * @property string $NOMOR_AJU
 * @property string $NOMOR_RESPON
 * @property Carbon $TANGGAL_RESPON
 * @property Carbon $WAKTU_RESPON
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbRespon extends Model
{
	protected $table = 'tpb_respon';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'TANGGAL_RESPON',
		'WAKTU_RESPON'
	];

	protected $fillable = [
		'BYTE_STREM_PDF',
		'FLAG_BACA',
		'KODE_RESPON',
		'NOMOR_AJU',
		'NOMOR_RESPON',
		'TANGGAL_RESPON',
		'WAKTU_RESPON',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
