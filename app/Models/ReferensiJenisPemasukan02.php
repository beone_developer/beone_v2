<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisPemasukan02
 * 
 * @property int $ID
 * @property string $KODE_JENIS_PEMASUKAN
 * @property string $URAIAN_JENIS_PEMASUKAN
 *
 * @package App\Models
 */
class ReferensiJenisPemasukan02 extends Model
{
	protected $table = 'referensi_jenis_pemasukan02';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_PEMASUKAN',
		'URAIAN_JENIS_PEMASUKAN'
	];
}
