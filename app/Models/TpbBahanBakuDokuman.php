<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBahanBakuDokuman
 * 
 * @property int $ID
 * @property int $KODE_ASAL_BAHAN_BAKU
 * @property int $SERI_DOKUMEN
 * @property int $ID_BAHAN_BAKU
 * @property int $ID_BARANG
 * @property int $ID_HEADER
 * 
 * @property TpbBahanBaku $tpb_bahan_baku
 * @property TpbBarang $tpb_barang
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbBahanBakuDokuman extends Model
{
	protected $table = 'tpb_bahan_baku_dokumen';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'KODE_ASAL_BAHAN_BAKU' => 'int',
		'SERI_DOKUMEN' => 'int',
		'ID_BAHAN_BAKU' => 'int',
		'ID_BARANG' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'KODE_ASAL_BAHAN_BAKU',
		'SERI_DOKUMEN',
		'ID_BAHAN_BAKU',
		'ID_BARANG',
		'ID_HEADER'
	];

	public function tpb_bahan_baku()
	{
		return $this->belongsTo(TpbBahanBaku::class, 'ID_BAHAN_BAKU');
	}

	public function tpb_barang()
	{
		return $this->belongsTo(TpbBarang::class, 'ID_BARANG');
	}

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
