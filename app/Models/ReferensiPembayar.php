<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPembayar
 * 
 * @property int $ID
 * @property string $KODE_PEMBAYAR
 * @property string $URAIAN_PEMBAYAR
 *
 * @package App\Models
 */
class ReferensiPembayar extends Model
{
	protected $table = 'referensi_pembayar';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_PEMBAYAR',
		'URAIAN_PEMBAYAR'
	];
}
