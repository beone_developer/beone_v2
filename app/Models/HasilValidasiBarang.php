<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HasilValidasiBarang
 * 
 * @property int $ID
 * @property int $ID_BARANG
 * @property string $KODE_JENIS_VALIDASI
 * @property string $URAIAN_JENIS_VALIDASI
 *
 * @package App\Models
 */
class HasilValidasiBarang extends Model
{
	protected $table = 'hasil_validasi_barang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_BARANG' => 'int'
	];

	protected $fillable = [
		'ID_BARANG',
		'KODE_JENIS_VALIDASI',
		'URAIAN_JENIS_VALIDASI'
	];
}
