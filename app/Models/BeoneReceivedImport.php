<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $received_id
 * @property string $nomor_aju
 * @property string $nomor_dokumen_pabean
 * @property string $tanggal_received
 * @property int $status_received
 * @property int $flag
 * @property int $update_by
 * @property string $update_date
 * @property string $nomor_received
 * @property int $tpb_header_id
 * @property float $kurs
 * @property int $po_import_header_id
 */
class BeoneReceivedImport extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_received_import';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'received_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nomor_aju', 'nomor_dokumen_pabean', 'tanggal_received', 'status_received', 'flag', 'update_by', 'update_date', 'nomor_received', 'tpb_header_id', 'kurs', 'po_import_header_id'];

}
