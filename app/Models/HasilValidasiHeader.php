<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HasilValidasiHeader
 * 
 * @property int $ID
 * @property int $ID_HEADER
 * @property string $KODE_JENIS_VALIDASI
 * @property string $URAIAN_JENIS_VALIDASI
 *
 * @package App\Models
 */
class HasilValidasiHeader extends Model
{
	protected $table = 'hasil_validasi_header';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'ID_HEADER',
		'KODE_JENIS_VALIDASI',
		'URAIAN_JENIS_VALIDASI'
	];
}
