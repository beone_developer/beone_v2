<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiMataUang
 * 
 * @property int $ID
 * @property string $KODE_MATA_UANG
 * @property string $URAIAN_MATA_UANG
 *
 * @package App\Models
 */
class ReferensiMataUang extends Model
{
	protected $table = 'referensi_mata_uang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_MATA_UANG',
		'URAIAN_MATA_UANG'
	];
}
