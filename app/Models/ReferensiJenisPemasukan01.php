<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisPemasukan01
 * 
 * @property int $ID
 * @property string $KODE_JENIS_PEMASUKAN
 * @property string $URAIAN_JENIS_PEMASUKAN
 *
 * @package App\Models
 */
class ReferensiJenisPemasukan01 extends Model
{
	protected $table = 'referensi_jenis_pemasukan01';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_PEMASUKAN',
		'URAIAN_JENIS_PEMASUKAN'
	];
}
