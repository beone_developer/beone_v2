<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $purchase_detail_id
 * @property int $purchase_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $price
 * @property float $amount
 * @property int $flag
 */
class BeonePurchaseDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_purchase_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'purchase_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['purchase_header_id', 'item_id', 'qty', 'price', 'amount', 'flag'];

}
