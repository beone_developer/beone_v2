<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbPenerima
 * 
 * @property int $ID
 * @property int $ID_HEADER
 * @property int $SERI
 * @property string $NAMA_PENERIMA
 * @property string $ALAMAT_PENERIMA
 *
 * @package App\Models
 */
class TpbPenerima extends Model
{
	protected $table = 'tpb_penerima';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int',
		'SERI' => 'int'
	];

	protected $fillable = [
		'ID_HEADER',
		'SERI',
		'NAMA_PENERIMA',
		'ALAMAT_PENERIMA'
	];
}
