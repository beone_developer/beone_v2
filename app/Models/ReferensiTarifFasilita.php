<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTarifFasilita
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN_PABEAN
 * @property string $KODE_FASILITAS
 * @property string $URAIAN_FASILITAS
 * @property string $URAIAN_PENDEK
 *
 * @package App\Models
 */
class ReferensiTarifFasilita extends Model
{
	protected $table = 'referensi_tarif_fasilitas';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DOKUMEN_PABEAN',
		'KODE_FASILITAS',
		'URAIAN_FASILITAS',
		'URAIAN_PENDEK'
	];
}
