<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $purchase_detail_id
 * @property int $purchase_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $price
 * @property float $amount
 * @property float $amount_sys
 * @property int $flag
 */
class BeonePoImportDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_po_import_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'purchase_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['purchase_header_id', 'item_id', 'qty', 'price', 'amount', 'flag', 'satuan_id', 'rasio', 'amount_sys'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }

    public function beone_po_import_header()
    {
        return $this->belongsTo(\App\Models\BeonePoImportHeader::class, 'purchase_header_id');
    }

    public function beone_import_details()
    {
        return $this->hasMany(\App\Models\BeoneImportDetail::class, 'purchase_detail_id');
    }

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->purchase_detail_id,
            'text' => $this->beone_po_import_header->purchase_no . " (" . $this->beone_item->item_code . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->beone_po_import_header->purchase_no . " (" . $this->beone_item->item_code . ")";
    }

}
