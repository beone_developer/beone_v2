<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBarangPenerima
 * 
 * @property int $ID
 * @property int $ID_HEADER
 * @property int $ID_BARANG
 * @property int $SERI_PENERIMA
 *
 * @package App\Models
 */
class TpbBarangPenerima extends Model
{
	protected $table = 'tpb_barang_penerima';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int',
		'ID_BARANG' => 'int',
		'SERI_PENERIMA' => 'int'
	];

	protected $fillable = [
		'ID_HEADER',
		'ID_BARANG',
		'SERI_PENERIMA'
	];
}
