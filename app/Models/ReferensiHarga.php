<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiHarga
 * 
 * @property int $ID
 * @property string $KODE_HARGA
 * @property string $URAIAN_HARGA
 *
 * @package App\Models
 */
class ReferensiHarga extends Model
{
	protected $table = 'referensi_harga';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_HARGA',
		'URAIAN_HARGA'
	];
}
