<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPengusaha
 * 
 * @property int $ID
 * @property string $ALAMAT
 * @property string $CONTACT_PERSON
 * @property string $EMAIL
 * @property string $FAX
 * @property string $ID_PENGENAL
 * @property string $JENISTPB
 * @property string $KODE_ID
 * @property string $KODE_KANTOR
 * @property string $NAMA
 * @property string $NOMOR_PENGENAL
 * @property string $NOMOR_SKEP
 * @property string $NPWP
 * @property string $STATUS_IMPORTIR
 * @property Carbon $TANGGAL_SKEP
 * @property string $TELEPON
 *
 * @package App\Models
 */
class ReferensiPengusaha extends Model
{
	protected $table = 'referensi_pengusaha';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $dates = [
		'TANGGAL_SKEP'
	];

	protected $fillable = [
		'ALAMAT',
		'CONTACT_PERSON',
		'EMAIL',
		'FAX',
		'ID_PENGENAL',
		'JENISTPB',
		'KODE_ID',
		'KODE_KANTOR',
		'NAMA',
		'NOMOR_PENGENAL',
		'NOMOR_SKEP',
		'NPWP',
		'STATUS_IMPORTIR',
		'TANGGAL_SKEP',
		'TELEPON'
	];
}
