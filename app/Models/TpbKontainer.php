<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbKontainer
 * 
 * @property int $ID
 * @property string $KESESUAIAN_DOKUMEN
 * @property string $KETERANGAN
 * @property string $KODE_STUFFING
 * @property string $KODE_TIPE_KONTAINER
 * @property string $KODE_UKURAN_KONTAINER
 * @property string $FLAG_GATE_IN
 * @property string $FLAG_GATE_OUT
 * @property string $NO_POLISI
 * @property string $NOMOR_KONTAINER
 * @property string $NOMOR_SEGEL
 * @property int $SERI_KONTAINER
 * @property Carbon $WAKTU_GATE_IN
 * @property Carbon $WAKTU_GATE_OUT
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbKontainer extends Model
{
	protected $table = 'tpb_kontainer';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'SERI_KONTAINER' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'WAKTU_GATE_IN',
		'WAKTU_GATE_OUT'
	];

	protected $fillable = [
		'KESESUAIAN_DOKUMEN',
		'KETERANGAN',
		'KODE_STUFFING',
		'KODE_TIPE_KONTAINER',
		'KODE_UKURAN_KONTAINER',
		'FLAG_GATE_IN',
		'FLAG_GATE_OUT',
		'NO_POLISI',
		'NOMOR_KONTAINER',
		'NOMOR_SEGEL',
		'SERI_KONTAINER',
		'WAKTU_GATE_IN',
		'WAKTU_GATE_OUT',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
