<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $adjustment_id
 * @property string $adjustment_no
 * @property string $adjustment_date
 * @property string $keterangan
 * @property int $item_id
 * @property integer $qty_adjustment
 * @property int $posisi_qty
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 */
class BeoneAdjustment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_adjustment';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'adjustment_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['adjustment_no', 'adjustment_date', 'keterangan', 'item_id', 'qty_adjustment', 'posisi_qty', 'update_by', 'update_date', 'flag'];

}
