<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPosTarif
 * 
 * @property int $ID
 * @property string $JENIS_TARIF_BM
 * @property string $JENIS_TARIF_CUKAI
 * @property string $KODE_SATUAN_BM
 * @property string $KODE_SATUAN_CUKAI
 * @property string $NOMOR_HS
 * @property int $SERI_HS
 * @property float $TARIF_BM
 * @property float $TARIF_CUKAI
 * @property float $TARIF_PPH
 * @property float $TARIF_PPN
 * @property float $TARIF_PPNBM
 *
 * @package App\Models
 */
class ReferensiPosTarif extends Model
{
	protected $table = 'referensi_pos_tarif';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'SERI_HS' => 'int',
		'TARIF_BM' => 'float',
		'TARIF_CUKAI' => 'float',
		'TARIF_PPH' => 'float',
		'TARIF_PPN' => 'float',
		'TARIF_PPNBM' => 'float'
	];

	protected $fillable = [
		'JENIS_TARIF_BM',
		'JENIS_TARIF_CUKAI',
		'KODE_SATUAN_BM',
		'KODE_SATUAN_CUKAI',
		'NOMOR_HS',
		'SERI_HS',
		'TARIF_BM',
		'TARIF_CUKAI',
		'TARIF_PPH',
		'TARIF_PPN',
		'TARIF_PPNBM'
	];
}
