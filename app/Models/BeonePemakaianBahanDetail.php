<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $pemakaian_detail_id
 * @property int $pemakaian_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $unit_price
 */
class BeonePemakaianBahanDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_pemakaian_bahan_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pemakaian_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['pemakaian_header_id', 'item_id', 'qty', 'unit_price'];

}
