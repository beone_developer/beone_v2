<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $mutasi_stok_detail_id
 * @property int $mutasi_stok_header_id
 * @property int $item_id
 * @property float $qty
 * @property int $gudang_id_asal
 * @property int $gudang_id_tujuan
 */
class BeoneMutasiStokDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_mutasi_stok_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'mutasi_stok_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['mutasi_stok_header_id', 'item_id', 'qty', 'gudang_id_asal', 'gudang_id_tujuan','satuan_id', 'rasio'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_gudang_asal()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id_asal');
    }

    public function beone_gudang_tujuan()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id_tujuan');
    }
    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }

}
