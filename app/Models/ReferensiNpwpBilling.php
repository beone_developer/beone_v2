<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiNpwpBilling
 * 
 * @property int $ID
 * @property string $JENIS_TARIF
 * @property string $NPWP_BILLING
 *
 * @package App\Models
 */
class ReferensiNpwpBilling extends Model
{
	protected $table = 'referensi_npwp_billing';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'JENIS_TARIF',
		'NPWP_BILLING'
	];
}
