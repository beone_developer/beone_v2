<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBarang
 *
 * @property int $ID
 * @property float $ASURANSI
 * @property float $CIF
 * @property float $CIF_RUPIAH
 * @property float $DISKON
 * @property string $FLAG_KENDARAAN
 * @property float $FOB
 * @property float $FREIGHT
 * @property float $HARGA_BARANG_LDP
 * @property float $HARGA_INVOICE
 * @property float $HARGA_PENYERAHAN
 * @property float $HARGA_SATUAN
 * @property float $JENIS_KENDARAAN
 * @property int $JUMLAH_BAHAN_BAKU
 * @property int $JUMLAH_KEMASAN
 * @property float $JUMLAH_SATUAN
 * @property float $KAPASITAS_SILINDER
 * @property string $KATEGORI_BARANG
 * @property string $KODE_ASAL_BARANG
 * @property string $KODE_BARANG
 * @property string $KODE_FASILITAS_DOKUMEN
 * @property string $KODE_GUNA
 * @property string $KODE_JENIS_NILAI
 * @property string $KODE_KEMASAN
 * @property string $KODE_LEBIH_DARI4TAHUN
 * @property string $KODE_NEGARA_ASAL
 * @property string $KODE_SATUAN
 * @property string $KODE_SKEMA_TARIF
 * @property string $KODE_STATUS
 * @property string $KONDISI_BARANG
 * @property string $MERK
 * @property float $NETTO
 * @property float $NILAI_INCOTERM
 * @property float $NILAI_PABEAN
 * @property string $NOMOR_MESIN
 * @property string $NOMOR_RANGKA
 * @property string $POS_TARIF
 * @property int $SERI_BARANG
 * @property int $SERI_IJIN
 * @property int $SERI_POS_TARIF
 * @property string $SPESIFIKASI_LAIN
 * @property Carbon $TAHUN_PEMBUATAN
 * @property string $TIPE
 * @property string $UKURAN
 * @property string $URAIAN
 * @property float $VOLUME
 * @property int $ID_HEADER
 * @property string $ID_EKSPORTIR
 * @property string $NAMA_EKSPORTIR
 * @property string $ALAMAT_EKSPORTIR
 * @property string $KODE_PERHITUNGAN
 *
 * @property TpbHeader $tpb_header
 * @property Collection|TpbBahanBaku[] $tpb_bahan_bakus
 * @property Collection|TpbBahanBakuDokuman[] $tpb_bahan_baku_dokumen
 * @property Collection|TpbBahanBakuTarif[] $tpb_bahan_baku_tarifs
 * @property Collection|TpbBarangDokuman[] $tpb_barang_dokumen
 * @property Collection|TpbBarangTarif[] $tpb_barang_tarifs
 *
 * @package App\Models
 */
class TpbBarang extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tpb_barang';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    protected $casts = [
        'ASURANSI' => 'float',
        'CIF' => 'float',
        'CIF_RUPIAH' => 'float',
        'DISKON' => 'float',
        'FOB' => 'float',
        'FREIGHT' => 'float',
        'HARGA_BARANG_LDP' => 'float',
        'HARGA_INVOICE' => 'float',
        'HARGA_PENYERAHAN' => 'float',
        'HARGA_SATUAN' => 'float',
        'JENIS_KENDARAAN' => 'float',
        'JUMLAH_BAHAN_BAKU' => 'int',
        'JUMLAH_KEMASAN' => 'int',
        'JUMLAH_SATUAN' => 'float',
        'KAPASITAS_SILINDER' => 'float',
        'NETTO' => 'float',
        'NILAI_INCOTERM' => 'float',
        'NILAI_PABEAN' => 'float',
        'SERI_BARANG' => 'int',
        'SERI_IJIN' => 'int',
        'SERI_POS_TARIF' => 'int',
        'VOLUME' => 'float',
        'ID_HEADER' => 'int'
    ];

    protected $dates = [
        'TAHUN_PEMBUATAN'
    ];

    protected $fillable = [
        'ASURANSI',
        'CIF',
        'CIF_RUPIAH',
        'DISKON',
        'FLAG_KENDARAAN',
        'FOB',
        'FREIGHT',
        'HARGA_BARANG_LDP',
        'HARGA_INVOICE',
        'HARGA_PENYERAHAN',
        'HARGA_SATUAN',
        'JENIS_KENDARAAN',
        'JUMLAH_BAHAN_BAKU',
        'JUMLAH_KEMASAN',
        'JUMLAH_SATUAN',
        'KAPASITAS_SILINDER',
        'KATEGORI_BARANG',
        'KODE_ASAL_BARANG',
        'KODE_BARANG',
        'KODE_FASILITAS_DOKUMEN',
        'KODE_GUNA',
        'KODE_JENIS_NILAI',
        'KODE_KEMASAN',
        'KODE_LEBIH_DARI4TAHUN',
        'KODE_NEGARA_ASAL',
        'KODE_SATUAN',
        'KODE_SKEMA_TARIF',
        'KODE_STATUS',
        'KONDISI_BARANG',
        'MERK',
        'NETTO',
        'NILAI_INCOTERM',
        'NILAI_PABEAN',
        'NOMOR_MESIN',
        'NOMOR_RANGKA',
        'POS_TARIF',
        'SERI_BARANG',
        'SERI_IJIN',
        'SERI_POS_TARIF',
        'SPESIFIKASI_LAIN',
        'TAHUN_PEMBUATAN',
        'TIPE',
        'UKURAN',
        'URAIAN',
        'VOLUME',
        'ID_HEADER',
        'ID_EKSPORTIR',
        'NAMA_EKSPORTIR',
        'ALAMAT_EKSPORTIR',
        'KODE_PERHITUNGAN'
    ];

    public function tpb_header()
    {
        return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
    }

    public function tpb_bahan_bakus()
    {
        return $this->hasMany(TpbBahanBaku::class, 'ID_BARANG');
    }

    public function tpb_bahan_baku_dokumen()
    {
        return $this->hasMany(TpbBahanBakuDokuman::class, 'ID_BARANG');
    }

    public function tpb_bahan_baku_tarifs()
    {
        return $this->hasMany(TpbBahanBakuTarif::class, 'ID_BARANG');
    }

    public function tpb_barang_dokumen()
    {
        return $this->hasMany(TpbBarangDokuman::class, 'ID_BARANG');
    }

    public function tpb_barang_tarifs()
    {
        return $this->hasMany(TpbBarangTarif::class, 'ID_BARANG');
    }

    public $appends = ['HARGA'];

    function getHARGAAttribute()
    {
        if (isset($this->HARGA_SATUAN)) {
            return $this->HARGA_SATUAN;
        } else {
            return $this->HARGA_PENYERAHAN / $this->JUMLAH_SATUAN;
        }
    }
}
