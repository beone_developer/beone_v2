<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $role_id
 * @property string $nama_role
 * @property string $keterangan
 * @property int $master_menu
 * @property int $pembelian_menu
 * @property int $penjualan_menu
 * @property int $inventory_menu
 * @property int $produksi_menu
 * @property int $asset_menu
 * @property int $jurnal_umum_menu
 * @property int $kasbank_menu
 * @property int $laporan_inventory
 * @property int $laporan_keuangan
 * @property int $konfigurasi
 * @property int $import_menu
 * @property int $eksport_menu
 */
class BeoneRole extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_role';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'role_id';

    /**
     * @var array
     */
    protected $fillable = ['nama_role', 'keterangan', 'master_menu', 'pembelian_menu', 'penjualan_menu', 'inventory_menu', 'produksi_menu', 'asset_menu', 'jurnal_umum_menu', 'kasbank_menu', 'laporan_inventory', 'laporan_keuangan', 'konfigurasi', 'import_menu', 'eksport_menu'];

}
