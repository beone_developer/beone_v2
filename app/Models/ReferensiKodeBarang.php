<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKodeBarang
 * 
 * @property int $ID
 * @property int $BARANG_KE
 * @property string $KODE_BARANG
 * @property string $MERK
 * @property string $NOHS
 * @property string $SERI
 * @property string $SPESIFIKASI_LAIN
 * @property string $TIPE
 * @property string $URAIAN_BARANG
 *
 * @package App\Models
 */
class ReferensiKodeBarang extends Model
{
	protected $table = 'referensi_kode_barang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'BARANG_KE' => 'int'
	];

	protected $fillable = [
		'BARANG_KE',
		'KODE_BARANG',
		'MERK',
		'NOHS',
		'SERI',
		'SPESIFIKASI_LAIN',
		'TIPE',
		'URAIAN_BARANG'
	];
}
