<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $export_detail_id
 * @property int $export_header_id
 * @property int $item_id
 * @property int $satuan_qty
 * @property int $satuan_pack
 * @property int $sales_header_id
 * @property int $gudang_id
 * @property float $qty
 * @property float $price
 * @property float $pack_qty
 * @property int $origin_country
 * @property string $doc
 * @property float $volume
 * @property float $netto
 * @property float $brutto
 * @property int $flag
 * @property int $sales_detail_id
 * @property float $price_hpp
 */
class BeoneExportDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_export_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'export_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['export_header_id', 'item_id', 'satuan_qty', 'satuan_pack', 'sales_header_id', 'gudang_id', 'qty', 'price', 'pack_qty', 'origin_country', 'doc', 'volume', 'netto', 'brutto', 'flag', 'sales_detail_id', 'price_hpp','satuan_id'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_sales_detail()
    {
        return $this->belongsTo(\App\Models\BeoneSalesDetail::class, 'sales_detail_id');
    }

    public function beone_export_header()
    {
        return $this->belongsTo(\App\Models\BeoneExportHeader::class, 'export_header_id');
    }
}
