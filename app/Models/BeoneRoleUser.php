<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $role_id
 * @property string $nama_role
 * @property string $keterangan
 * @property int $master_menu
 * @property int $user_add
 * @property int $user_edit
 * @property int $user_delete
 * @property int $role_add
 * @property int $role_edit
 * @property int $role_delete
 * @property int $customer_add
 * @property int $customer_edit
 * @property int $customer_delete
 * @property int $supplier_add
 * @property int $supplier_edit
 * @property int $supplier_delete
 * @property int $item_add
 * @property int $item_edit
 * @property int $item_delete
 * @property int $jenis_add
 * @property int $jenis_edit
 * @property int $jenis_delete
 * @property int $satuan_add
 * @property int $satuan_edit
 * @property int $satuan_delete
 * @property int $gudang_add
 * @property int $gudang_edit
 * @property int $gudang_delete
 * @property int $master_import
 * @property int $po_add
 * @property int $po_edit
 * @property int $po_delete
 * @property int $tracing
 * @property int $terima_barang
 * @property int $master_pembelian
 * @property int $pembelian_add
 * @property int $pembelian_edit
 * @property int $pembelian_delete
 * @property int $kredit_note_add
 * @property int $kredit_note_edit
 * @property int $kredit_note_delete
 * @property int $master_eksport
 * @property int $eksport_add
 * @property int $eksport_edit
 * @property int $eksport_delete
 * @property int $kirim_barang
 * @property int $menu_penjualan
 * @property int $penjualan_add
 * @property int $penjualan_edit
 * @property int $penjualan_delete
 * @property int $debit_note_add
 * @property int $debit_note_edit
 * @property int $debit_note_delete
 * @property int $menu_inventory
 * @property int $pindah_gudang
 * @property int $stockopname_add
 * @property int $stockopname_edit
 * @property int $stockopname_delete
 * @property int $stockopname_opname
 * @property int $adjustment_add
 * @property int $adjustment_edit
 * @property int $adjustment_delete
 * @property int $pemusnahan_add
 * @property int $pemusnahan_edit
 * @property int $pemusnahan_delete
 * @property int $recal_inventory
 * @property int $menu_produksi
 * @property int $produksi_add
 * @property int $produksi_edit
 * @property int $produksi_delete
 * @property int $menu_asset
 * @property int $menu_jurnal_umum
 * @property int $jurnal_umum_add
 * @property int $jurnal_umum_edit
 * @property int $jurnal_umum_delete
 * @property int $menu_kas_bank
 * @property int $kas_bank_add
 * @property int $kas_bank_edit
 * @property int $kas_bank_delete
 * @property int $menu_laporan_inventory
 * @property int $menu_laporan_keuangan
 * @property int $menu_konfigurasi
 */
class BeoneRoleUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    protected $table = 'beone_role_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'role_id';

    /**
     * @var array
     */
    protected $fillable = ['nama_role', 'keterangan', 'master_menu', 'user_add', 'user_edit', 'user_delete', 'role_add', 'role_edit', 'role_delete', 'customer_add', 'customer_edit', 'customer_delete', 'supplier_add', 'supplier_edit', 'supplier_delete', 'item_add', 'item_edit', 'item_delete', 'jenis_add', 'jenis_edit', 'jenis_delete', 'satuan_add', 'satuan_edit', 'satuan_delete', 'gudang_add', 'gudang_edit', 'gudang_delete', 'master_import', 'po_add', 'po_edit', 'po_delete', 'tracing', 'terima_barang', 'master_pembelian', 'pembelian_add', 'pembelian_edit', 'pembelian_delete', 'kredit_note_add', 'kredit_note_edit', 'kredit_note_delete', 'master_eksport', 'eksport_add', 'eksport_edit', 'eksport_delete', 'kirim_barang', 'menu_penjualan', 'penjualan_add', 'penjualan_edit', 'penjualan_delete', 'debit_note_add', 'debit_note_edit', 'debit_note_delete', 'menu_inventory', 'pindah_gudang', 'stockopname_add', 'stockopname_edit', 'stockopname_delete', 'stockopname_opname', 'adjustment_add', 'adjustment_edit', 'adjustment_delete', 'pemusnahan_add', 'pemusnahan_edit', 'pemusnahan_delete', 'recal_inventory', 'menu_produksi', 'produksi_add', 'produksi_edit', 'produksi_delete', 'menu_asset', 'menu_jurnal_umum', 'jurnal_umum_add', 'jurnal_umum_edit', 'jurnal_umum_delete', 'menu_kas_bank', 'kas_bank_add', 'kas_bank_edit', 'kas_bank_delete', 'menu_laporan_inventory', 'menu_laporan_keuangan', 'menu_konfigurasi'];

}
