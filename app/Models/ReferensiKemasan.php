<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKemasan
 * 
 * @property int $ID
 * @property string $KODE_KEMASAN
 * @property string $URAIAN_KEMASAN
 *
 * @package App\Models
 */
class ReferensiKemasan extends Model
{
	protected $table = 'referensi_kemasan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KEMASAN',
		'URAIAN_KEMASAN'
	];
}
