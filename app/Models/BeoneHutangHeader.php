<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $hutang_header_id
 * @property string $trans_no
 * @property string $trans_date
 * @property int $supplier_id
 * @property float $grandtotal
 * @property string $keterangan
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneHutangHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_hutang_header';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'hutang_header_id';

    /**
     * @var array
     */
    protected $fillable = ['trans_no', 'trans_date', 'supplier_id', 'grandtotal', 'keterangan', 'deleted_at', 'created_at', 'updated_at'];

    public function beone_supplier()
    {
        return $this->belongsTo(\App\Models\BeoneSupplier::class, 'supplier_id');
    }

    public function beone_hutang_details()
    {
        return $this->hasMany(\App\Models\BeoneHutangDetail::class, 'hutang_header_id');
    }
}
