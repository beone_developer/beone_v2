<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $modul_id
 * @property string $url
 * @property string $modul
 */
class BeoneModul extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    protected $table = 'beone_modul';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'modul_id';

    /**
     * @var array
     */
    protected $fillable = ['url', 'modul', 'icon_class'];

    public function beone_roles_users()
    {
        return $this->belongsToMany(\App\Models\BeoneRolesUser::class, 'beone_roles_modul');
    }

    public $appends = ['id', 'text', 'spriteCssClass'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->modul_id,
            'text' => $this->getTextAttribute(),
        ];
        return json_encode($ar);
    }

    function getSpriteCssClassAttribute()
    {
        return $this->icon_class;
    }

    function getTextAttribute()
    {
        return $this->modul . " (" . $this->url . ")";
    }
}
