<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKodeGuna
 * 
 * @property int $ID
 * @property string $KODE_GUNA
 * @property string $URAIAN_GUNA
 *
 * @package App\Models
 */
class ReferensiKodeGuna extends Model
{
	protected $table = 'referensi_kode_guna';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_GUNA',
		'URAIAN_GUNA'
	];
}
