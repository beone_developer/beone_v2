<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $pm_header_id
 * @property string $no_pm
 * @property string $tgl_pm
 * @property int $customer_id
 * @property float $qty
 * @property string $keterangan_artikel
 * @property int $status
 */
class BeonePmHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_pm_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pm_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['no_pm', 'tgl_pm', 'customer_id', 'qty', 'keterangan_artikel', 'status'];

}
