<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $log_id
 * @property string $log_user
 * @property int $log_tipe
 * @property string $log_desc
 * @property string $log_time
 */
class BeoneLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_log';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'log_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['log_user', 'log_tipe', 'log_desc', 'log_time'];

}
