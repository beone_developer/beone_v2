<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTujuanTpb
 * 
 * @property int $ID
 * @property string $KODE_TUJUAN_TPB
 * @property string $URAIAN_TUJUAN_TPB
 *
 * @package App\Models
 */
class ReferensiTujuanTpb extends Model
{
	protected $table = 'referensi_tujuan_tpb';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID' => 'int'
	];

	protected $fillable = [
		'KODE_TUJUAN_TPB',
		'URAIAN_TUJUAN_TPB'
	];
}
