<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $import_detail_id
 * @property int $item_id
 * @property int $satuan_qty
 * @property int $item_type_id
 * @property int $import_header_id
 * @property int $gudang_id
 * @property float $qty
 * @property float $price
 * @property float $pack_qty
 * @property int $satuan_pack
 * @property int $origin_country
 * @property float $volume
 * @property float $netto
 * @property float $brutto
 * @property string $hscode
 * @property float $tbm
 * @property float $ppnn
 * @property float $tpbm
 * @property int $cukai
 * @property int $sat_cukai
 * @property float $cukai_value
 * @property string $bea_masuk
 * @property int $sat_bea_masuk
 * @property int $flag
 * @property int $purchase_detail_id
 * @property int $isppn
 * @property int $bc_barang_id
 * @property float $amount
 * @property float $amount_sys
 * @property float $amount_ppn
 * @property float $amount_ppn_sys
 * @property string $bc_barang_uraian
 */
class BeoneImportDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_import_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'import_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'satuan_qty', 'item_type_id', 'import_header_id', 'gudang_id', 'qty', 'price', 'pack_qty', 'satuan_pack', 'origin_country', 'volume', 'netto', 'brutto', 'hscode', 'tbm', 'ppnn', 'tpbm', 'cukai', 'sat_cukai', 'cukai_value', 'bea_masuk', 'sat_bea_masuk', 'flag', 'purchase_detail_id', 'isppn', 'bc_barang_id', 'amount', 'amount_sys', 'amount_ppn', 'amount_ppn_sys', 'bc_barang_uraian', 'satuan_id', 'rasio'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_import_header()
    {
        return $this->belongsTo(\App\Models\BeoneImportHeader::class, 'import_header_id');
    }

    public function beone_purchase_order_detail()
    {
        return $this->belongsTo(\App\Models\BeonePoImportDetail::class, 'purchase_detail_id');
    }

    public function tpb_barang()
    {
        return $this->belongsTo(\App\Models\TpbHeader::class, 'bc_barang_id', 'ID');
    }

   public function beone_gudang()
   {
       return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
   }

}
