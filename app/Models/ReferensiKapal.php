<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKapal
 * 
 * @property int $ID
 * @property string $KODE_BENDERA
 * @property string $NAMA_KAPAL
 *
 * @package App\Models
 */
class ReferensiKapal extends Model
{
	protected $table = 'referensi_kapal';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_BENDERA',
		'NAMA_KAPAL'
	];
}
