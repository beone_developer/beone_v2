<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $usage_header_id
 * @property string $trans_no
 * @property string $trans_date
 * @property string $keterangan
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneUsageHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_usage_header';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'usage_header_id';

    /**
     * @var array
     */
    protected $fillable = ['trans_no', 'trans_date', 'keterangan', 'deleted_at', 'created_at', 'updated_at'];

    public function beone_usage_details()
    {
        return $this->hasMany(\App\Models\BeoneUsageDetail::class, 'usage_header_id');
    }

}
