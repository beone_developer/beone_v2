<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $item_type_id
 * @property string $nama
 * @property string $keterangan
 * @property int $flag
 */
class BeoneItemType extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_item_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_type_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'keterangan', 'flag'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->item_type_id,
            'text' => $this->getTextAttribute()
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return  $this->nama;
    }
}
