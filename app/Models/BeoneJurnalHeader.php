<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $jurnal_header_id
 * @property string $jurnal_number
 * @property string $keterangan
 * @property string $trans_date
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneJurnalHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_jurnal_header';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'jurnal_header_id';

    /**
     * @var array
     */
    protected $fillable = ['jurnal_no', 'keterangan', 'trans_date', 'deleted_at', 'created_at', 'updated_at'];


    public function beone_jurnal_details()
    {
        return $this->hasMany(\App\Models\BeoneJurnalDetail::class, 'jurnal_header_id');
    }

    public function beone_jurnal_detail_debets()
    {
        return $this->hasMany(\App\Models\BeoneJurnalDetail::class, 'jurnal_header_id')
            ->where('status', 'ilike', 'D');
    }

    public function beone_jurnal_detail_kredits()
    {
        return $this->hasMany(\App\Models\BeoneJurnalDetail::class, 'jurnal_header_id')
            ->where('status', 'ilike', 'K');
    }
}
