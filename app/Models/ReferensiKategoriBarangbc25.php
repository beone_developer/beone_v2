<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKategoriBarangbc25
 * 
 * @property int $ID
 * @property string $KODE_KATEGORI
 * @property string $URAIAN_KATEGORI
 *
 * @package App\Models
 */
class ReferensiKategoriBarangbc25 extends Model
{
	protected $table = 'referensi_kategori_barangbc25';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KATEGORI',
		'URAIAN_KATEGORI'
	];
}
