<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisNilai
 * 
 * @property int $ID
 * @property string $KODE_JENIS_NILAI
 * @property string $URAIAN_JENIS_NILAI
 *
 * @package App\Models
 */
class ReferensiJenisNilai extends Model
{
	protected $table = 'referensi_jenis_nilai';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_NILAI',
		'URAIAN_JENIS_NILAI'
	];
}
