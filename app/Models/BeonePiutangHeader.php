<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $piutang_header_id
 * @property string $trans_no
 * @property string $trans_date
 * @property int $customer_id
 * @property float $grandtotal
 * @property string $keterangan
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeonePiutangHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_piutang_header';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'piutang_header_id';

    /**
     * @var array
     */
    protected $fillable = ['trans_no', 'trans_date', 'customer_id', 'grandtotal', 'keterangan', 'deleted_at', 'created_at', 'updated_at'];

    public function beone_customer()
    {
        return $this->belongsTo(\App\Models\BeoneCustomer::class, 'customer_id');
    }

    public function beone_piutang_details()
    {
        return $this->hasMany(\App\Models\BeonePiutangDetail::class, 'piutang_header_id');
    }
}
