<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbKemasan
 * 
 * @property int $ID
 * @property int $JUMLAH_KEMASAN
 * @property string $KESESUAIAN_DOKUMEN
 * @property string $KETERANGAN
 * @property string $KODE_JENIS_KEMASAN
 * @property string $MERK_KEMASAN
 * @property string $NIP_GATE_IN
 * @property string $NIP_GATE_OUT
 * @property string $NO_POLISI
 * @property string $NOMOR_SEGEL
 * @property int $SERI_KEMASAN
 * @property Carbon $WAKTU_GATE_IN
 * @property Carbon $WAKTU_GATE_OUT
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbKemasan extends Model
{
	protected $table = 'tpb_kemasan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'JUMLAH_KEMASAN' => 'int',
		'SERI_KEMASAN' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'WAKTU_GATE_IN',
		'WAKTU_GATE_OUT'
	];

	protected $fillable = [
		'JUMLAH_KEMASAN',
		'KESESUAIAN_DOKUMEN',
		'KETERANGAN',
		'KODE_JENIS_KEMASAN',
		'MERK_KEMASAN',
		'NIP_GATE_IN',
		'NIP_GATE_OUT',
		'NO_POLISI',
		'NOMOR_SEGEL',
		'SERI_KEMASAN',
		'WAKTU_GATE_IN',
		'WAKTU_GATE_OUT',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
