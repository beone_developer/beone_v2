<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisBc25
 * 
 * @property int $ID
 * @property string $KODE_JENIS_BC25
 * @property string $URAIAN_JENIS_BC25
 *
 * @package App\Models
 */
class ReferensiJenisBc25 extends Model
{
	protected $table = 'referensi_jenis_bc25';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_BC25',
		'URAIAN_JENIS_BC25'
	];
}
