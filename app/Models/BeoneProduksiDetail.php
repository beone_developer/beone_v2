<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $produksi_detail_id
 * @property int $produksi_header_id
 * @property int $pemakaian_header_id
 */
class BeoneProduksiDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_produksi_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'produksi_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['produksi_header_id', 'pemakaian_header_id'];

}
