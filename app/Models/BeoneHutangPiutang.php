<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $hutang_piutang_id
 * @property int $custsup_id
 * @property string $trans_date
 * @property string $nomor
 * @property string $keterangan
 * @property float $valas_trans
 * @property float $idr_trans
 * @property float $valas_pelunasan
 * @property float $idr_pelunasan
 * @property int $tipe_trans
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 * @property int $status_lunas
 */
class BeoneHutangPiutang extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_hutang_piutang';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'hutang_piutang_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['custsup_id', 'trans_date', 'nomor', 'keterangan', 'valas_trans', 'idr_trans', 'valas_pelunasan', 'idr_pelunasan', 'tipe_trans', 'update_by', 'update_date', 'flag', 'status_lunas'];

}
