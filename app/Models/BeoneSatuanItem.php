<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $satuan_id
 * @property string $satuan_code
 * @property string $keterangan
 * @property int $flag
 */
class BeoneSatuanItem extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_satuan_item';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'satuan_id';

    /**
     * @var array
     */
    protected $fillable = ['satuan_code', 'keterangan', 'item_id', 'rasio', 'flag'];

    public $appends = ['id', 'text'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->satuan_id,
            'text' => $this->rasio . " (" . $this->satuan_code . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->rasio . " (" . $this->satuan_code . ")";
    }
}
