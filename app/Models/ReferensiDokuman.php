<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiDokuman
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN
 * @property string $TIPE_DOKUMEN
 * @property string $URAIAN_DOKUMEN
 *
 * @package App\Models
 */
class ReferensiDokuman extends Model
{
	protected $table = 'referensi_dokumen';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DOKUMEN',
		'TIPE_DOKUMEN',
		'URAIAN_DOKUMEN'
	];
}
