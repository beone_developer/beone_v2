<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKondisiBarang
 * 
 * @property int $ID
 * @property string $KODE_KONDISI
 * @property string $URAIAN_KONDISI
 *
 * @package App\Models
 */
class ReferensiKondisiBarang extends Model
{
	protected $table = 'referensi_kondisi_barang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KONDISI',
		'URAIAN_KONDISI'
	];
}
