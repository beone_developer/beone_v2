<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $hutang_detail_id
 * @property int $hutang_header_id
 * @property int $import_header_id
 * @property float $amount
 */
class BeoneHutangDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_hutang_detail';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'hutang_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['hutang_header_id', 'import_header_id', 'amount'];

    
    public $appends = ['invoice_no'];


    public function beone_import_header()
    {
        return $this->belongsTo(\App\Models\BeoneImportHeader::class, 'import_header_id');
    }

    function getInvoiceNoAttribute()
    {
        return $this->beone_import_header->invoice_no;
    }
}
