<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiStatus
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN
 * @property string $KODE_STATUS
 * @property string $URAIAN_STATUS
 *
 * @package App\Models
 */
class ReferensiStatus extends Model
{
	protected $table = 'referensi_status';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DOKUMEN',
		'KODE_STATUS',
		'URAIAN_STATUS'
	];
}
