<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $country_id
 * @property string $nama
 * @property string $country_code
 * @property int $flag
 */
class BeoneCountry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_country';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'country_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'country_code', 'flag'];

}
