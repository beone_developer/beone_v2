<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $sub_kontraktor_out_detail_id
 * @property int $item_id
 * @property int $gudang_id
 * @property int $sub_kontraktor_out_header_id
 * @property float $qty
 * @property float $price
 * @property int $bc_barang_id
 * @property int $isppn
 * @property float $amount
 * @property float $amount_ppn
 * @property BeoneItem $beoneItem
 * @property BeoneGudang $beoneGudang
 */
class BeoneSubKontraktorOutDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_sub_kontraktor_out_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sub_kontraktor_out_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'gudang_id', 'sub_kontraktor_out_header_id', 'qty', 'price', 'bc_barang_id', 'isppn', 'amount','amount_sys', 'amount_ppn','amount_ppn_sys', 'bc_barang_uraian', 'price_hpp','satuan_id', 'rasio'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id', 'gudang_id');
    }
    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }
}
