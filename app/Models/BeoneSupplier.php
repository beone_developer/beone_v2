<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $supplier_id
 * @property string $supplier_code
 * @property string $nama
 * @property string $alamat
 * @property int $ppn
 * @property int $pph21
 * @property int $pph23
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneSupplier extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_supplier';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'supplier_id';

    /**
     * @var array
     */
    protected $fillable = ['supplier_code', 'nama', 'alamat', 'ppn', 'pph21', 'pph23', 'deleted_at', 'created_at', 'updated_at'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->supplier_id,
            'text' => $this->supplier_code . " (" . $this->nama . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->supplier_code . " (" . $this->nama . ")";
    }
}
