<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBahanBaku
 * 
 * @property int $ID
 * @property float $CIF
 * @property float $CIF_RUPIAH
 * @property float $HARGA_PENYERAHAN
 * @property float $HARGA_PEROLEHAN
 * @property string $JENIS_SATUAN
 * @property float $JUMLAH_SATUAN
 * @property int $KODE_ASAL_BAHAN_BAKU
 * @property string $KODE_BARANG
 * @property string $KODE_FASILITAS_DOKUMEN
 * @property string $KODE_JENIS_DOK_ASAL
 * @property string $KODE_KANTOR
 * @property string $KODE_SKEMA_TARIF
 * @property string $KODE_STATUS
 * @property string $MERK
 * @property float $NDPBM
 * @property float $NETTO
 * @property string $NOMOR_AJU_DOK_ASAL
 * @property string $NOMOR_DAFTAR_DOK_ASAL
 * @property string $POS_TARIF
 * @property int $SERI_BAHAN_BAKU
 * @property int $SERI_BARANG
 * @property int $SERI_IJIN
 * @property int $SERI_BARANG_DOK_ASAL
 * @property string $SPESIFIKASI_LAIN
 * @property Carbon $TANGGAL_DAFTAR_DOK_ASAL
 * @property string $TIPE
 * @property string $UKURAN
 * @property string $URAIAN
 * @property int $ID_BARANG
 * @property int $ID_HEADER
 * 
 * @property TpbBarang $tpb_barang
 * @property TpbHeader $tpb_header
 * @property Collection|TpbBahanBakuDokuman[] $tpb_bahan_baku_dokumen
 * @property Collection|TpbBahanBakuTarif[] $tpb_bahan_baku_tarifs
 *
 * @package App\Models
 */
class TpbBahanBaku extends Model
{
	protected $table = 'tpb_bahan_baku';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'CIF' => 'float',
		'CIF_RUPIAH' => 'float',
		'HARGA_PENYERAHAN' => 'float',
		'HARGA_PEROLEHAN' => 'float',
		'JUMLAH_SATUAN' => 'float',
		'KODE_ASAL_BAHAN_BAKU' => 'int',
		'NDPBM' => 'float',
		'NETTO' => 'float',
		'SERI_BAHAN_BAKU' => 'int',
		'SERI_BARANG' => 'int',
		'SERI_IJIN' => 'int',
		'SERI_BARANG_DOK_ASAL' => 'int',
		'ID_BARANG' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'TANGGAL_DAFTAR_DOK_ASAL'
	];

	protected $fillable = [
		'CIF',
		'CIF_RUPIAH',
		'HARGA_PENYERAHAN',
		'HARGA_PEROLEHAN',
		'JENIS_SATUAN',
		'JUMLAH_SATUAN',
		'KODE_ASAL_BAHAN_BAKU',
		'KODE_BARANG',
		'KODE_FASILITAS_DOKUMEN',
		'KODE_JENIS_DOK_ASAL',
		'KODE_KANTOR',
		'KODE_SKEMA_TARIF',
		'KODE_STATUS',
		'MERK',
		'NDPBM',
		'NETTO',
		'NOMOR_AJU_DOK_ASAL',
		'NOMOR_DAFTAR_DOK_ASAL',
		'POS_TARIF',
		'SERI_BAHAN_BAKU',
		'SERI_BARANG',
		'SERI_IJIN',
		'SERI_BARANG_DOK_ASAL',
		'SPESIFIKASI_LAIN',
		'TANGGAL_DAFTAR_DOK_ASAL',
		'TIPE',
		'UKURAN',
		'URAIAN',
		'ID_BARANG',
		'ID_HEADER'
	];

	public function tpb_barang()
	{
		return $this->belongsTo(TpbBarang::class, 'ID_BARANG');
	}

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}

	public function tpb_bahan_baku_dokumen()
	{
		return $this->hasMany(TpbBahanBakuDokuman::class, 'ID_BAHAN_BAKU');
	}

	public function tpb_bahan_baku_tarifs()
	{
		return $this->hasMany(TpbBahanBakuTarif::class, 'ID_BAHAN_BAKU');
	}
}
