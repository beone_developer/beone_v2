<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKategoriBarang
 * 
 * @property int $ID
 * @property string $KODE_KATEGORI
 * @property string $KODE_TUJUAN_TPB
 * @property string $URAIAN_KATEGORI
 *
 * @package App\Models
 */
class ReferensiKategoriBarang extends Model
{
	protected $table = 'referensi_kategori_barang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KATEGORI',
		'KODE_TUJUAN_TPB',
		'URAIAN_KATEGORI'
	];
}
