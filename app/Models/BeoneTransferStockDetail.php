<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $transfer_stock_detail_id
 * @property int $transfer_stock_header_id
 * @property string $tipe_transfer_stock
 * @property int $item_id
 * @property float $qty
 * @property int $gudang_id
 * @property float $biaya
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 * @property int $persen_produksi
 */
class BeoneTransferStockDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_transfer_stock_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'transfer_stock_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['transfer_stock_header_id', 'tipe_transfer_stock', 'item_id', 'qty', 'gudang_id', 'biaya', 'update_by', 'update_date', 'flag', 'persen_produksi','satuan_id', 'rasio'];


    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }
}
