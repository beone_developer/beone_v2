<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPelabuhan
 * 
 * @property int $ID
 * @property string $KETERANGAN
 * @property string $KODE_KANTOR
 * @property string $KODE_PELABUHAN
 * @property string $URAIAN_PELABUHAN
 *
 * @package App\Models
 */
class ReferensiPelabuhan extends Model
{
	protected $table = 'referensi_pelabuhan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KETERANGAN',
		'KODE_KANTOR',
		'KODE_PELABUHAN',
		'URAIAN_PELABUHAN'
	];
}
