<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $transfer_stock_id
 * @property string $transfer_no
 * @property string $transfer_date
 * @property int $coa_kode_biaya
 * @property string $keterangan
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 */
class BeoneTransferStock extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_transfer_stock';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'transfer_stock_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['transfer_no', 'transfer_date', 'coa_kode_biaya', 'keterangan', 'update_by', 'update_date', 'flag'];

    public $appends = ['gudang_bahan', 'gudang_produksi'];

    public function beone_transfer_stock_details()
    {
        return $this->hasMany(\App\Models\BeoneTransferStockDetail::class, 'transfer_stock_header_id');
    }

    public function beone_coa()
    {
        return $this->belongsTo(\App\Models\BeoneCoa::class, 'coa_id');
    }

    public function beone_transfer_stock_details_bb()
    {
        return $this->hasMany(\App\Models\BeoneTransferStockDetail::class, 'transfer_stock_header_id')
            ->where('tipe_transfer_stock', 'ilike', 'BB');
    }

    public function beone_transfer_stock_details_wip()
    {
        return $this->hasMany(\App\Models\BeoneTransferStockDetail::class, 'transfer_stock_header_id')
            ->where('tipe_transfer_stock', 'ilike', 'WIP');
    }

    public function getGudangBahanAttribute()
    {
        return BeoneGudang::find($this->beone_transfer_stock_details_bb->first()->gudang_id);
    }

    public function getGudangProduksiAttribute()
    {
        return BeoneGudang::find($this->beone_transfer_stock_details_wip->first()->gudang_id);
    }
}
