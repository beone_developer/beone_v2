<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $print_out_id
 * @property string $nama
 * @property string $data
 */
class BeonePrintOut extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_print_out';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'print_out_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'design_def', 'design'];

}
