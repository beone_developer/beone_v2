<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $gudang_id
 * @property string $nama
 * @property string $keterangan
 * @property int $flag
 */
class BeoneGudang extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_gudang';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gudang_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'keterangan', 'flag'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->gudang_id,
            'text' => $this->nama . " (" . $this->keterangan . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->nama . " (" . $this->keterangan . ")";
    }
}
