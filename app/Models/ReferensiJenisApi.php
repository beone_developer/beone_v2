<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisApi
 * 
 * @property int $ID
 * @property string $KODE_JENIS_API
 * @property string $URAIAN_JENIS_API
 *
 * @package App\Models
 */
class ReferensiJenisApi extends Model
{
	protected $table = 'referensi_jenis_api';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_API',
		'URAIAN_JENIS_API'
	];
}
