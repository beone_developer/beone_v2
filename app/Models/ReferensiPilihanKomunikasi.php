<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPilihanKomunikasi
 * 
 * @property int $ID
 * @property string $KODE_PILIHAN
 * @property string $URAIAN_PILIHAN
 *
 * @package App\Models
 */
class ReferensiPilihanKomunikasi extends Model
{
	protected $table = 'referensi_pilihan_komunikasi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_PILIHAN',
		'URAIAN_PILIHAN'
	];
}
