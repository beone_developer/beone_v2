<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $produksi_header_id
 * @property string $nomor_produksi
 * @property string $tgl_produksi
 * @property int $tipe_produksi
 * @property int $item_produksi
 * @property float $qty_hasil_produksi
 * @property string $keterangan
 * @property int $flag
 * @property int $updated_by
 * @property string $udpated_date
 * @property int $pm_header_id
 */
class BeoneProduksiHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_produksi_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'produksi_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nomor_produksi', 'tgl_produksi', 'tipe_produksi', 'item_produksi', 'qty_hasil_produksi', 'keterangan', 'flag', 'updated_by', 'udpated_date', 'pm_header_id'];

}
