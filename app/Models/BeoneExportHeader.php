<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $export_header_id
 * @property int $jenis_bc
 * @property string $car_no
 * @property string $bc_no
 * @property string $bc_date
 * @property string $kontrak_no
 * @property string $kontrak_date
 * @property int $jenis_export
 * @property string $invoice_no
 * @property string $invoice_date
 * @property string $surat_jalan_no
 * @property string $surat_jalan_date
 * @property int $receiver_id
 * @property int $country_id
 * @property string $price_type
 * @property float $amount_value
 * @property float $valas_value
 * @property string $insurance_type
 * @property float $insurance_value
 * @property float $freight
 * @property int $flag
 * @property int $status
 * @property string $delivery_date
 * @property int $update_by
 * @property string $update_date
 * @property string $delivery_no
 * @property string $vessel
 * @property string $port_loading
 * @property string $port_destination
 * @property string $container
 * @property string $no_container
 * @property string $no_seal
 * @property int $currency_id
 */
class BeoneExportHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_export_header';

    protected $primaryKey = 'export_header_id';

    /**
     * @var array
     */
    protected $fillable = ['export_header_id', 'jenis_bc', 'car_no', 'bc_no', 'bc_date', 'kontrak_no', 'kontrak_date', 'jenis_export', 'invoice_no', 'invoice_date', 'surat_jalan_no', 'surat_jalan_date', 'receiver_id', 'country_id', 'price_type', 'amount_value', 'valas_value', 'insurance_type', 'insurance_value', 'freight', 'flag', 'status', 'delivery_date', 'update_by', 'update_date', 'delivery_no', 'vessel', 'port_loading', 'port_destination', 'container', 'no_container', 'no_seal', 'currency_id', 'kurs'];

    public function beone_export_details()
    {
        return $this->hasMany(\App\Models\BeoneExportDetail::class, 'export_header_id');
    }

    public function beone_receiver()
    {
        return $this->belongsTo(\App\Models\BeoneCustomer::class, 'receiver_id');
    }

    public function beone_currency()
    {
        return $this->belongsTo(\App\Models\BeoneCurrency::class, 'currency_id');
    }

    public function beone_custsup()
    {
        return $this->belongsTo(\App\Models\BeoneCustsup::class, 'custsup_id');
    }

    public $appends = ['gudang', 'sales_headers'];

    public function getGudangAttribute()
    {
        if ($this->beone_export_details->first()){
            return BeoneGudang::find($this->beone_export_details->first()->gudang_id);
        }
        return '';
    }

//    public function sales_he()
//    {
//        return $this->hasManyThrough(\App\Models\BeoneSalesHeader::class, \App\Models\BeoneExportDetail::class, 'sales_header_id', 'sales_header_id', 'export_header_id', 'export_header_id');
//    }

    public function getSalesHeadersAttribute()
    {
        $sales_header_ids = collect($this->beone_export_details)->pluck('sales_header_id');
        $header = BeoneSalesHeader::whereIn('sales_header_id', $sales_header_ids)->get();
        $res = [];
        foreach ($header as $i => $val) {
            $res[] = [
                'sales_header_id' => $val->sales_header_id,
                'beone_sales_header' => $val,
            ];
        }
        return $res;
    }

//    public function beone_sales_headers()
//    {
//        return $this->hasMany(\App\Models\BeoneSalesHeader::class, 'export_header_id');
//    }
}
