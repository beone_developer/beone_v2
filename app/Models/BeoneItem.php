<?php

namespace App\Models;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $item_id
 * @property string $nama
 * @property string $item_code
 * @property float $saldo_qty
 * @property float $saldo_idr
 * @property string $keterangan
 * @property int $flag
 * @property int $item_type_id
 * @property string $hscode
 * @property int $satuan_id
 * @property int $item_category_id
 */
class BeoneItem extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_item';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'item_code', 'saldo_qty', 'saldo_idr', 'keterangan', 'flag', 'item_type_id', 'hscode', 'satuan_id', 'item_category_id'];

    public $appends = ['id', 'text', 'satuan_def'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->item_id,
            'text' => $this->getTextAttribute(),
            'satuan_def' => $this->satuan_def,
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return Helper::strStrip($this->item_code . " (" . $this->nama . ")");
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }

    public function beone_item_type()
    {
        return $this->belongsTo(\App\Models\BeoneItemType::class, 'item_type_id');
    }

    public function beone_item_category()
    {
        return $this->belongsTo(\App\Models\BeoneItemCategory::class, 'item_category_id');
    }

    public function getSatuanDefAttribute()
    {
        return $this->beone_satuan_items->where('rasio', 1)->first();
    }

    public function beone_satuan_items()
    {
        return $this->hasMany(\App\Models\BeoneSatuanItem::class, 'item_id');
    }

    public function beone_inventories()
    {
        return $this->hasMany(\App\Models\BeoneInventory::class, 'item_id');
    }

    public function beone_komposisi_from()
    {
        return $this->hasMany(\App\Models\BeoneKomposisi::class, 'item_jadi_id');
    }

    public function beone_komposisi_to()
    {
        return $this->hasMany(\App\Models\BeoneKomposisi::class, 'item_baku_id');
    }
}
