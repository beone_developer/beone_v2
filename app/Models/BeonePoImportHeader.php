<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $purchase_header_id
 * @property string $purchase_no
 * @property string $trans_date
 * @property int $supplier_id
 * @property string $keterangan
 * @property float $grandtotal
 * @property int $flag
 * @property int $update_by
 * @property string $update_date
 * @property float $total_hitung
 */
class BeonePoImportHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_po_import_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'purchase_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['purchase_no', 'trans_date', 'supplier_id', 'keterangan', 'grandtotal', 'flag', 'update_by', 'update_date', 'total_hitung', 'currency_id', 'kurs'];

    public function beone_po_import_details()
    {
        return $this->hasMany(\App\Models\BeonePoImportDetail::class, 'purchase_header_id');
    }

    public function beone_import_headers()
    {
        return $this->hasMany(\App\Models\BeoneImportHeader::class, 'purchase_header_id');
    }

    public function beone_supplier()
    {
        return $this->belongsTo(\App\Models\BeoneSupplier::class, 'supplier_id');
    }

    public function beone_currency()
    {
        return $this->belongsTo(\App\Models\BeoneCurrency::class, 'currency_id');
    }

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->purchase_header_id,
            'text' => $this->purchase_no . " (" . $this->keterangan . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->purchase_no . " (" . $this->keterangan . ")";
    }
}
