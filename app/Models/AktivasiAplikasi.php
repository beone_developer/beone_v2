<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AktivasiAplikasi
 * 
 * @property int $ID
 * @property string $ALAMAT_PENGUSAHA
 * @property string $CERTIFICATE
 * @property string $ID_MODUL
 * @property string $KODE_GUDANG
 * @property string $KPPBC
 * @property string $NAMA_PENGUSAHA
 * @property string $NOMOR_SKEP
 * @property string $NPWP
 * @property string $PASSWORD
 * @property string $PASSWORD_CERTIFICATE
 * @property string $PORT
 * @property Carbon $TANGGAL_SKEP
 * @property string $URL
 * @property string $USERNAME
 *
 * @package App\Models
 */
class AktivasiAplikasi extends Model
{
	protected $table = 'aktivasi_aplikasi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $dates = [
		'TANGGAL_SKEP'
	];

	protected $fillable = [
		'ALAMAT_PENGUSAHA',
		'CERTIFICATE',
		'ID_MODUL',
		'KODE_GUDANG',
		'KPPBC',
		'NAMA_PENGUSAHA',
		'NOMOR_SKEP',
		'NPWP',
		'PASSWORD',
		'PASSWORD_CERTIFICATE',
		'PORT',
		'TANGGAL_SKEP',
		'URL',
		'USERNAME'
	];
}
