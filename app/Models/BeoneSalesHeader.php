<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $sales_header_id
 * @property string $sales_no
 * @property string $trans_date
 * @property int $customer_id
 * @property string $keterangan
 * @property float $ppn
 * @property float $subtotal
 * @property float $ppn_value
 * @property float $grandtotal
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 * @property float $biaya
 * @property int $status
 * @property int $realisasi
 */
class BeoneSalesHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_sales_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sales_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['sales_no', 'trans_date', 'customer_id', 'keterangan', 'ppn', 'subtotal', 'ppn_value', 'grandtotal', 'update_by', 'update_date', 'flag', 'biaya', 'status', 'realisasi', 'grandtotal_sys', 'currency_id', 'kurs'];

    public function beone_sales_details()
    {
        return $this->hasMany(\App\Models\BeoneSalesDetail::class, 'sales_header_id');
    }

    public function beone_export_details()
    {
        return $this->hasMany(\App\Models\BeoneExportDetail::class, 'sales_header_id');
    }

    public function beone_customer()
    {
        return $this->belongsTo(\App\Models\BeoneCustomer::class, 'customer_id');
    }

    public function beone_currency()
    {
        return $this->belongsTo(\App\Models\BeoneCurrency::class, 'currency_id');
    }

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->sales_header_id,
            'text' => $this->sales_no . " (" . $this->keterangan . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->sales_no . " (" . $this->keterangan . ")";
    }
}
