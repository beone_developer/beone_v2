<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPemasok
 * 
 * @property int $ID
 * @property string $ALAMAT
 * @property string $KODE_ID
 * @property string $KODE_NEGARA
 * @property string $NAMA
 * @property string $NPWP
 *
 * @package App\Models
 */
class ReferensiPemasok extends Model
{
	protected $table = 'referensi_pemasok';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'ALAMAT',
		'KODE_ID',
		'KODE_NEGARA',
		'NAMA',
		'NPWP'
	];
}
