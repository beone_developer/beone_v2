<?php

namespace App\Models;

use App\Helpers\Helper;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @property int $user_id
 * @property string $username
 * @property string $password
 * @property string $nama
 * @property int $role_id
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 */
class BeoneUser extends Authenticatable
{
    protected $appends = ['inisial'];
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * @var array
     */
    protected $fillable = ['username', 'password', 'nama', 'role_id', 'update_by', 'update_date', 'flag'];

    public function getInisialAttribute()
    {
        return Helper::strChar($this->nama);
    }

    public function beone_roles_users()
    {
        return $this->belongsTo(\App\Models\BeoneRolesUser::class, 'role_id');
    }
}
