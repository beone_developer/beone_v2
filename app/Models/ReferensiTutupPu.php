<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTutupPu
 * 
 * @property int $ID
 * @property string $KODE_TUTUP_PU
 * @property string $URAIAN_TUTUP_PU
 *
 * @package App\Models
 */
class ReferensiTutupPu extends Model
{
	protected $table = 'referensi_tutup_pu';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID' => 'int'
	];

	protected $fillable = [
		'KODE_TUTUP_PU',
		'URAIAN_TUTUP_PU'
	];
}
