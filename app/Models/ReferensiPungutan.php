<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiPungutan
 * 
 * @property int $ID
 * @property string $KODE_PUNGUTAN
 * @property string $URAIAN_PUNGUTAN
 *
 * @package App\Models
 */
class ReferensiPungutan extends Model
{
	protected $table = 'referensi_pungutan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_PUNGUTAN',
		'URAIAN_PUNGUTAN'
	];
}
