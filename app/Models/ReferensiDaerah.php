<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiDaerah
 * 
 * @property int $ID
 * @property string $KODE_DAERAH
 * @property string $URAIAN_DAERAH
 *
 * @package App\Models
 */
class ReferensiDaerah extends Model
{
	protected $table = 'referensi_daerah';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DAERAH',
		'URAIAN_DAERAH'
	];
}
