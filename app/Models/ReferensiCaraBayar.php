<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiCaraBayar
 * 
 * @property int $ID
 * @property string $KODE_CARA_BAYAR
 * @property string $URAIAN_CARA_BAYAR
 *
 * @package App\Models
 */
class ReferensiCaraBayar extends Model
{
	protected $table = 'referensi_cara_bayar';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_CARA_BAYAR',
		'URAIAN_CARA_BAYAR'
	];
}
