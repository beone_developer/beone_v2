<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $voucher_header_id
 * @property string $voucher_number
 * @property string $voucher_date
 * @property int $tipe
 * @property string $keterangan
 * @property int $coa_id_cash_bank
 * @property string $coa_no
 * @property int $update_by
 * @property string $update_date
 */
class BeoneVoucherHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_voucher_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'voucher_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['voucher_number', 'voucher_date', 'tipe', 'keterangan', 'coa_id_cash_bank', 'coa_no', 'update_by', 'update_date', 'total'];

    public function beone_voucher_details()
    {
        return $this->hasMany(\App\Models\BeoneVoucherDetail::class, 'voucher_header_id');
    }

    public function beone_coa()
    {
        return $this->belongsTo(\App\Models\BeoneCoa::class, 'coa_id_cash_bank');
    }

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->voucher_header_id,
            'text' => $this->voucher_number . " (" . $this->voucher_number . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->voucher_number . " (" . $this->voucher_number . ")";
    }
}
