<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiValidasiJenisNilai
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN
 * @property string $JENIS_NILAI
 * @property string $FLAG_PENGIRIM
 * @property string $FLAG_PENJUAL
 * @property string $FLAG_PENGUSAHA
 * @property string $FLAG_PEMILIK
 * @property string $FLAG_IMPORTIR
 *
 * @package App\Models
 */
class ReferensiValidasiJenisNilai extends Model
{
	protected $table = 'referensi_validasi_jenis_nilai';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DOKUMEN',
		'JENIS_NILAI',
		'FLAG_PENGIRIM',
		'FLAG_PENJUAL',
		'FLAG_PENGUSAHA',
		'FLAG_PEMILIK',
		'FLAG_IMPORTIR'
	];
}
