<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $jurnal_detail_id
 * @property int $jurnal_header_id
 * @property int $coa_id
 * @property float $amount
 * @property float $kurs
 * @property float $amount_idr
 * @property string $keterangan_detail
 * @property string $status
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneJurnalDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_jurnal_detail';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'jurnal_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['jurnal_header_id', 'coa_id', 'amount', 'kurs', 'amount_idr', 'keterangan_detail', 'status', 'deleted_at', 'created_at', 'updated_at'];


    public function beone_coa()
    {
        return $this->belongsTo(\App\Models\BeoneCoa::class, 'coa_id');
    }
}
