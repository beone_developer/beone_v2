<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserManajeman
 * 
 * @property int $ID
 * @property string $DELETE_DATA
 * @property string $DOKUMENBC23
 * @property string $DOKUMENBC25
 * @property int $ID_MODUL
 * @property string $INSERT_DATA
 * @property string $KIRIM_DATA
 * @property string $PASSWORD
 * @property string $UPDATE_DATA
 * @property string $UPDATE_PASSWORD
 * @property string $USER_NAME
 * @property string $DOKUMENBC16
 * @property string $DOKUMENBC161
 * @property string $DOKUMENBC261
 * @property string $DOKUMENBC262
 * @property string $DOKUMENBC27
 * @property string $DOKUMENBC40
 * @property string $DOKUMENBC41
 * @property string $GATE_IN
 * @property string $GATE_OUT
 * @property string $KIRIM_DOKUMEN
 * @property string $RESPON_PER_DOKUMEN
 * @property string $RESPON_SEMUA
 * @property string $TRANSFER_DATA
 * @property string $DAFTAR_BARANG
 * @property string $TARIF_HS
 * @property string $KANTOR_BC
 * @property string $ALAT_ANGKUT
 * @property string $EDIFACT
 * @property string $PEMASOK
 * @property string $PERUSAHAAN
 * @property string $TPS
 * @property string $ABOUT
 * @property string $BACKUP
 * @property string $RESTORE
 * @property string $RESTORE_DATA_LAMA
 * @property string $SETTING
 * @property string $USER_MANAGEMENT
 *
 * @package App\Models
 */
class UserManajeman extends Model
{
	protected $table = 'user_manajemen';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_MODUL' => 'int'
	];

	protected $fillable = [
		'DELETE_DATA',
		'DOKUMENBC23',
		'DOKUMENBC25',
		'ID_MODUL',
		'INSERT_DATA',
		'KIRIM_DATA',
		'PASSWORD',
		'UPDATE_DATA',
		'UPDATE_PASSWORD',
		'USER_NAME',
		'DOKUMENBC16',
		'DOKUMENBC161',
		'DOKUMENBC261',
		'DOKUMENBC262',
		'DOKUMENBC27',
		'DOKUMENBC40',
		'DOKUMENBC41',
		'GATE_IN',
		'GATE_OUT',
		'KIRIM_DOKUMEN',
		'RESPON_PER_DOKUMEN',
		'RESPON_SEMUA',
		'TRANSFER_DATA',
		'DAFTAR_BARANG',
		'TARIF_HS',
		'KANTOR_BC',
		'ALAT_ANGKUT',
		'EDIFACT',
		'PEMASOK',
		'PERUSAHAAN',
		'TPS',
		'ABOUT',
		'BACKUP',
		'RESTORE',
		'RESTORE_DATA_LAMA',
		'SETTING',
		'USER_MANAGEMENT'
	];
}
