<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbPungutan
 * 
 * @property int $ID
 * @property string $JENIS_TARIF
 * @property string $KODE_FASILITAS
 * @property float $NILAI_PUNGUTAN
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbPungutan extends Model
{
	protected $table = 'tpb_pungutan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'NILAI_PUNGUTAN' => 'float',
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'JENIS_TARIF',
		'KODE_FASILITAS',
		'NILAI_PUNGUTAN',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
