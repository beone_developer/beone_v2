<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $mutasi_stok_header_id
 * @property string $mutasi_stok_no
 * @property string $trans_date
 * @property string $keterangan
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property int $flag
 */
class BeoneMutasiStokHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_mutasi_stok_header';

    protected $casts = [
        'flag' => 'int',
    ];

    protected $dates = [
        'trans_date'
    ];
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'mutasi_stok_header_id';

    /**
     * @var array
     */
    protected $fillable = ['mutasi_stok_no', 'trans_date', 'keterangan', 'deleted_at', 'created_at', 'updated_at', 'flag'];

    public $appends = ['gudang_asal', 'gudang_tujuan'];

    public function beone_mutasi_stok_details()
    {
        return $this->hasMany(\App\Models\BeoneMutasiStokDetail::class, 'mutasi_stok_header_id');
    }

    public function getGudangAsalAttribute()
    {
        return BeoneGudang::find($this->beone_mutasi_stok_details->first()->gudang_id_asal);
    }

    public function getGudangTujuanAttribute()
    {
        return BeoneGudang::find($this->beone_mutasi_stok_details->first()->gudang_id_tujuan);
    }
}
