<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $gl_id
 * @property string $gl_date
 * @property int $coa_id
 * @property string $coa_no
 * @property int $coa_id_lawan
 * @property string $coa_no_lawan
 * @property string $keterangan
 * @property float $debet
 * @property float $kredit
 * @property string $pasangan_no
 * @property string $gl_number
 * @property int $update_by
 * @property string $update_date
 */
class BeoneGl extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_gl';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gl_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['gl_date', 'coa_id', 'coa_no', 'coa_id_lawan', 'coa_no_lawan', 'keterangan', 'debet', 'kredit', 'pasangan_no', 'gl_number', 'update_by', 'update_date'];

}
