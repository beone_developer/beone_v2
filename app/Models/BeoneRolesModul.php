<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $role_modul_id
 * @property int $role_id
 * @property int $modul_id
 * @property int $parent_id
 */
class BeoneRolesModul extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_roles_modul';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'role_modul_id';

    /**
     * @var array
     */
    protected $fillable = ['role_id', 'modul_id', 'parent_id'];

    public function beone_roles_user()
    {
        return $this->belongsTo(\App\Models\BeoneRolesUser::class);
    }

    public function beone_modul()
    {
        return $this->belongsTo(\App\Models\BeoneModul::class);
    }

//    public function children()
//    {
//        return $this->hasMany(static::class, 'parent_id')->orderBy('role_id', 'asc');
//    }
}
