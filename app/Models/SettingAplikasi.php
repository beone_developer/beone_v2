<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingAplikasi
 * 
 * @property int $ID
 * @property string $ALAMAT_PENGUSAHA
 * @property string $FLAG_APLIKASI
 * @property string $ID_PENGUSAHA
 * @property string $JABATAN_TTD
 * @property string $KODE_DOKUMEN_PABEAN
 * @property string $KODE_ID_PENGUSAHA
 * @property string $KOTA_TTD
 * @property string $NAMA_PENGUSAHA
 * @property string $NAMA_TTD
 * @property string $NOMOR_AJU
 * @property string $NOMOR_REGISTRASI
 * @property string $STATUS_PENGUSAHA
 *
 * @package App\Models
 */
class SettingAplikasi extends Model
{
	protected $table = 'setting_aplikasi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'ALAMAT_PENGUSAHA',
		'FLAG_APLIKASI',
		'ID_PENGUSAHA',
		'JABATAN_TTD',
		'KODE_DOKUMEN_PABEAN',
		'KODE_ID_PENGUSAHA',
		'KOTA_TTD',
		'NAMA_PENGUSAHA',
		'NAMA_TTD',
		'NOMOR_AJU',
		'NOMOR_REGISTRASI',
		'STATUS_PENGUSAHA'
	];
}
