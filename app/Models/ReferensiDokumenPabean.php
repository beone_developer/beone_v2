<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiDokumenPabean
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN_PABEAN
 * @property string $URAIAN_DOKUMEN_PABEAN
 *
 * @package App\Models
 */
class ReferensiDokumenPabean extends Model
{
	protected $table = 'referensi_dokumen_pabean';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_DOKUMEN_PABEAN',
		'URAIAN_DOKUMEN_PABEAN'
	];
}
