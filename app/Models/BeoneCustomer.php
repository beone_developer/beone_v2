<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $customer_id
 * @property string $customer_code
 * @property string $nama
 * @property string $alamat
 * @property int $ppn
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneCustomer extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_customer';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'customer_id';

    /**
     * @var array
     */
    protected $fillable = ['customer_code', 'nama', 'alamat', 'ppn', 'deleted_at', 'created_at', 'updated_at'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->customer_id,
            'text' => $this->customer_code . " (" . $this->nama . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->customer_code . " (" . $this->nama . ")";
    }

}
