<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $stok_opname_header_id
 * @property string $stok_opname_no
 * @property string $trans_date
 * @property string $keterangan
 * @property string $approved_by
 * @property string $approved_at
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneStokOpnameHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_stok_opname_header';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'stok_opname_header_id';

    /**
     * @var array
     */
    protected $fillable = ['stok_opname_no', 'trans_date', 'keterangan', 'approved_by', 'approved_at', 'deleted_at', 'created_at', 'updated_at'];


    public function beone_stok_opname_details()
    {
        return $this->hasMany(\App\Models\BeoneStokOpnameDetail::class, 'stok_opname_header_id');
    }
}
