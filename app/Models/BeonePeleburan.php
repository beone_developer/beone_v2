<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $peleburan_id
 * @property string $peleburan_no
 * @property string $peleburan_date
 * @property string $keterangan
 * @property int $item_id
 * @property integer $qty_peleburan
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 */
class BeonePeleburan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_peleburan';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'peleburan_id';

    /**
     * @var array
     */
    protected $fillable = ['peleburan_no', 'peleburan_date', 'keterangan', 'item_id', 'qty_peleburan', 'update_by', 'update_date', 'flag'];

}
