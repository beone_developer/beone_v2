<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $currency_id
 * @property string $currency_code
 * @property string $keterangan
 */
class BeoneCurrency extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_currency';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'currency_id';

    /**
     * @var array
     */
    protected $fillable = ['currency_code', 'keterangan'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->currency_id,
            'text' => $this->currency_code . " (" . $this->keterangan . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->currency_code . " (" . $this->keterangan . ")";
    }
}
