<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBarangDokuman
 * 
 * @property int $ID
 * @property int $SERI_DOKUMEN
 * @property int $ID_BARANG
 * @property int $ID_HEADER
 * 
 * @property TpbBarang $tpb_barang
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbBarangDokuman extends Model
{
	protected $table = 'tpb_barang_dokumen';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'SERI_DOKUMEN' => 'int',
		'ID_BARANG' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'SERI_DOKUMEN',
		'ID_BARANG',
		'ID_HEADER'
	];

	public function tpb_barang()
	{
		return $this->belongsTo(TpbBarang::class, 'ID_BARANG');
	}

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
