<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $gudang_detail_id
 * @property int $gudang_id
 * @property string $trans_date
 * @property int $item_id
 * @property float $qty_in
 * @property float $qty_out
 * @property string $nomor_transaksi
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 * @property string $keterangan
 * @property string $kode_tracing
 */
class BeoneGudangDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_gudang_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gudang_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['gudang_id', 'trans_date', 'item_id', 'qty_in', 'qty_out', 'nomor_transaksi', 'update_by', 'update_date', 'flag', 'keterangan', 'kode_tracing'];

}
