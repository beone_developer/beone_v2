<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisJaminan
 * 
 * @property int $ID
 * @property string $KODE_JENIS_JAMINAN
 * @property string $URAIAN_JENIS_JAMINAN
 *
 * @package App\Models
 */
class ReferensiJenisJaminan extends Model
{
	protected $table = 'referensi_jenis_jaminan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_JAMINAN',
		'URAIAN_JENIS_JAMINAN'
	];
}
