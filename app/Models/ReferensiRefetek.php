<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiRefetek
 * 
 * @property int $ID
 * @property string $KODE_REFETEKS
 * @property string $URAIAN_REFETEKS
 *
 * @package App\Models
 */
class ReferensiRefetek extends Model
{
	protected $table = 'referensi_refeteks';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_REFETEKS',
		'URAIAN_REFETEKS'
	];
}
