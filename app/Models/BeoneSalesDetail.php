<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $sales_detail_id
 * @property int $sales_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $price
 * @property float $amount
 * @property float $amount_sys
 * @property int $flag
 */
class BeoneSalesDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_sales_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sales_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['sales_header_id', 'item_id', 'qty', 'price', 'amount', 'flag', 'amount_sys','satuan_id', 'rasio'];

    public function beone_sales_header()
    {
        return $this->belongsTo(\App\Models\BeoneSalesHeader::class, 'sales_header_id');
    }

    public function beone_export_details()
    {
        return $this->belongsTo(\App\Models\BeoneExportDetail::class, 'sales_detail_id', 'sales_detail_id');
    }

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }
    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }

}
