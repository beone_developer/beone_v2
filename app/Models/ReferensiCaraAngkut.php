<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiCaraAngkut
 * 
 * @property int $ID
 * @property string $KODE_CARA_ANGKUT
 * @property string $URAIAN_CARA_ANGKUT
 *
 * @package App\Models
 */
class ReferensiCaraAngkut extends Model
{
	protected $table = 'referensi_cara_angkut';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_CARA_ANGKUT',
		'URAIAN_CARA_ANGKUT'
	];
}
