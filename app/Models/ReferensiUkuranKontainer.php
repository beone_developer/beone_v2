<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiUkuranKontainer
 * 
 * @property int $ID
 * @property string $KODE_UKURAN_KONTAINER
 * @property string $URAIAN_UKURAN_KONTAINER
 *
 * @package App\Models
 */
class ReferensiUkuranKontainer extends Model
{
	protected $table = 'referensi_ukuran_kontainer';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID' => 'int'
	];

	protected $fillable = [
		'KODE_UKURAN_KONTAINER',
		'URAIAN_UKURAN_KONTAINER'
	];
}
