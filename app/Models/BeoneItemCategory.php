<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $item_category_id
 * @property string $nama
 * @property string $keterangan
 * @property int $flag
 * @property int $item_category_code
 */
class BeoneItemCategory extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_item_category';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_category_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nama', 'keterangan', 'flag','item_category_code'];

}
