<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbNpwpBilling
 * 
 * @property int $ID
 * @property string $JENIS_TARIF
 * @property string $NPWP_BILLING
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbNpwpBilling extends Model
{
	protected $table = 'tpb_npwp_billing';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'JENIS_TARIF',
		'NPWP_BILLING',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
