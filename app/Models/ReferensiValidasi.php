<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiValidasi
 * 
 * @property int $ID
 * @property string $FLAG_MANDATORY
 * @property string $KODE_DOKUMEN
 * @property string $NAMA_KOLOM
 * @property string $NAMA_TABEL
 * @property string $TIPE_DATA
 * @property string $URAIAN
 *
 * @package App\Models
 */
class ReferensiValidasi extends Model
{
	protected $table = 'referensi_validasi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'FLAG_MANDATORY',
		'KODE_DOKUMEN',
		'NAMA_KOLOM',
		'NAMA_TABEL',
		'TIPE_DATA',
		'URAIAN'
	];
}
