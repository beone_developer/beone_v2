<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiAsalDatum
 * 
 * @property int $ID
 * @property string $KODE_ASAL_DATA
 * @property string $URAIAN_ASAL_DATA
 *
 * @package App\Models
 */
class ReferensiAsalDatum extends Model
{
	protected $table = 'referensi_asal_data';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_ASAL_DATA',
		'URAIAN_ASAL_DATA'
	];
}
