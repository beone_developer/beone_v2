<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbJaminan
 * 
 * @property int $ID
 * @property string $KODE_JENIS_JAMINAN
 * @property string $KODE_KANTOR
 * @property float $NILAI_JAMINAN
 * @property string $NOMOR_BPJ
 * @property string $NOMOR_JAMINAN
 * @property string $PENJAMIN
 * @property Carbon $TANGGAL_BPJ
 * @property Carbon $TANGGAL_JAMINAN
 * @property Carbon $TANGGAL_JATUH_TEMPO
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbJaminan extends Model
{
	protected $table = 'tpb_jaminan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'NILAI_JAMINAN' => 'float',
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'TANGGAL_BPJ',
		'TANGGAL_JAMINAN',
		'TANGGAL_JATUH_TEMPO'
	];

	protected $fillable = [
		'KODE_JENIS_JAMINAN',
		'KODE_KANTOR',
		'NILAI_JAMINAN',
		'NOMOR_BPJ',
		'NOMOR_JAMINAN',
		'PENJAMIN',
		'TANGGAL_BPJ',
		'TANGGAL_JAMINAN',
		'TANGGAL_JATUH_TEMPO',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
