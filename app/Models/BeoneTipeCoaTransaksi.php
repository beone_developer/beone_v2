<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tipe_coa_trans_id
 * @property string $nama
 * @property int $flag
 */
class BeoneTipeCoaTransaksi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_tipe_coa_transaksi';

    /**
     * @var array
     */
    protected $fillable = ['tipe_coa_trans_id', 'nama', 'flag'];

}
