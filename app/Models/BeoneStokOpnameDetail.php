<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $stok_opname_detail_id
 * @property int $stok_opname_header_id
 * @property int $item_id
 * @property float $qty
 * @property float $qty_opname
 * @property float $qty_selisih
 * @property int $gudang_id
 */
class BeoneStokOpnameDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_stok_opname_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'stok_opname_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['stok_opname_header_id', 'item_id', 'qty', 'qty_opname', 'qty_selisih', 'gudang_id', 'satuan_id'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }

    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
    }
}
