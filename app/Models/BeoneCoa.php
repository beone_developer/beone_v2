<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $coa_id
 * @property string $nama
 * @property string $nomor
 * @property int $tipe_akun
 * @property float $debet_valas
 * @property float $debet_idr
 * @property float $kredit_valas
 * @property float $kredit_idr
 * @property string $dk
 * @property int $tipe_transaksi
 */
class BeoneCoa extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_coa';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'coa_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'nomor', 'tipe_akun', 'debet_valas', 'debet_idr', 'kredit_valas', 'kredit_idr', 'dk', 'tipe_transaksi'];


    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->coa_id,
            'text' => $this->nomor . " (" . $this->nama . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->nomor . " (" . $this->nama . ")";
    }

    public function beone_tipe_coa()
    {
        return $this->belongsTo(\App\Models\BeoneTipeCoa::class, 'tipe_akun');
    }
}
