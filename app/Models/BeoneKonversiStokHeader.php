<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $konversi_stok_header_id
 * @property int $item_id
 * @property string $konversi_stok_no
 * @property string $konversi_stok_date
 * @property int $flag
 * @property float $qty
 * @property int $satuan_qty
 * @property int $gudang_id
 */
class BeoneKonversiStokHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_konversi_stok_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'konversi_stok_header_id';

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'konversi_stok_no', 'konversi_stok_date', 'flag', 'qty', 'satuan_qty', 'gudang_id', 'satuan_id', 'rasio'];

    public function beone_konversi_stok_details()
    {
        return $this->hasMany(\App\Models\BeoneKonversiStokDetail::class, 'konversi_stok_header_id');
    }

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
    }

    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }
}
