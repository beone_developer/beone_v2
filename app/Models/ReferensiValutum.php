<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiValutum
 * 
 * @property int $ID
 * @property string $KODE_VALUTA
 * @property string $URAIAN_VALUTA
 *
 * @package App\Models
 */
class ReferensiValutum extends Model
{
	protected $table = 'referensi_valuta';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_VALUTA',
		'URAIAN_VALUTA'
	];
}
