<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKomoditi
 * 
 * @property int $ID
 * @property string $KODE_KOMODITI
 * @property string $URAIAN_KOMODITI
 *
 * @package App\Models
 */
class ReferensiKomoditi extends Model
{
	protected $table = 'referensi_komoditi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KOMODITI',
		'URAIAN_KOMODITI'
	];
}
