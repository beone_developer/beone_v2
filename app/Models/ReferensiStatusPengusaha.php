<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiStatusPengusaha
 * 
 * @property int $ID
 * @property string $KODE_STATUS_PENGUSAHA
 * @property string $URAIAN_STATUS_PENGUSAHA
 *
 * @package App\Models
 */
class ReferensiStatusPengusaha extends Model
{
	protected $table = 'referensi_status_pengusaha';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_STATUS_PENGUSAHA',
		'URAIAN_STATUS_PENGUSAHA'
	];
}
