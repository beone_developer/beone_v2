<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $pemakaian_header_id
 * @property string $nomor_pemakaian
 * @property string $keterangan
 * @property string $tgl_pemakaian
 * @property int $update_by
 * @property string $update_date
 * @property string $nomor_pm
 * @property float $amount_pemakaian
 * @property int $status
 */
class BeonePemakaianBahanHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_pemakaian_bahan_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pemakaian_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nomor_pemakaian', 'keterangan', 'tgl_pemakaian', 'update_by', 'update_date', 'nomor_pm', 'amount_pemakaian', 'status'];

}
