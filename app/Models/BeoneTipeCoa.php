<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tipe_coa_id
 * @property string $nama
 */
class BeoneTipeCoa extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_tipe_coa';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'tipe_coa_id';

    /**
     * @var array
     */
    protected $fillable = ['nama'];

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->tipe_coa_id,
            'text' => $this->nama,
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->nama;
    }

}
