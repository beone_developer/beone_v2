<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTujuanPengiriman
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN
 * @property string $KODE_TUJUAN_PENGIRIMAN
 * @property string $URAIAN_TUJUAN_PENGIRIMAN
 *
 * @package App\Models
 */
class ReferensiTujuanPengiriman extends Model
{
	protected $table = 'referensi_tujuan_pengiriman';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID' => 'int'
	];

	protected $fillable = [
		'KODE_DOKUMEN',
		'KODE_TUJUAN_PENGIRIMAN',
		'URAIAN_TUJUAN_PENGIRIMAN'
	];
}
