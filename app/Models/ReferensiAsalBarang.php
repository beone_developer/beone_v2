<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiAsalBarang
 * 
 * @property int $ID
 * @property string $KODE_ASAL_BARANG
 * @property string $URAIAN_ASAL_BARANG
 *
 * @package App\Models
 */
class ReferensiAsalBarang extends Model
{
	protected $table = 'referensi_asal_barang';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_ASAL_BARANG',
		'URAIAN_ASAL_BARANG'
	];
}
