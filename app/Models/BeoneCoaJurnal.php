<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $coa_jurnal_id
 * @property string $nama_coa_jurnal
 * @property string $keterangan_coa_jurnal
 * @property int $coa_id
 * @property string $coa_no
 */
class BeoneCoaJurnal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_coa_jurnal';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'coa_jurnal_id';

    /**
     * @var array
     */
    protected $fillable = ['nama_coa_jurnal', 'keterangan_coa_jurnal', 'coa_id', 'coa_no'];

}
