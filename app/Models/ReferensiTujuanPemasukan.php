<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTujuanPemasukan
 * 
 * @property int $ID
 * @property string $KODE_DOKUMEN
 * @property string $KODE_TUJUAN_PEMASUKAN
 * @property string $URAIAN_TUJUAN_PEMASUKAN
 *
 * @package App\Models
 */
class ReferensiTujuanPemasukan extends Model
{
	protected $table = 'referensi_tujuan_pemasukan';
	protected $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ID' => 'int'
	];

	protected $fillable = [
		'KODE_DOKUMEN',
		'KODE_TUJUAN_PEMASUKAN',
		'URAIAN_TUJUAN_PEMASUKAN'
	];
}
