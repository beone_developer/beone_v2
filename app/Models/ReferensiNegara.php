<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiNegara
 * 
 * @property int $ID
 * @property string $KODE_NEGARA
 * @property string $URAIAN_NEGARA
 *
 * @package App\Models
 */
class ReferensiNegara extends Model
{
	protected $table = 'referensi_negara';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_NEGARA',
		'URAIAN_NEGARA'
	];
}
