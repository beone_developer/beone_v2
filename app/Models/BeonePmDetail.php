<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $pm_detail_id
 * @property int $item_id
 * @property float $qty
 * @property int $satuan_id
 * @property int $pm_header_id
 */
class BeonePmDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_pm_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pm_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'qty', 'satuan_id', 'pm_header_id'];

}
