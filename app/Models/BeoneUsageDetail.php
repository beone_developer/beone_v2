<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $usage_detail_id
 * @property int $usage_header_id
 * @property int $item_id
 * @property float $qty_in
 * @property float $qty_out
 * @property int $gudang_id
 */
class BeoneUsageDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_usage_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'usage_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['usage_header_id', 'item_id', 'qty_in','qty_out', 'gudang_id','satuan_id', 'rasio'];


    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }


    public function beone_gudang()
    {
        return $this->belongsTo(\App\Models\BeoneGudang::class, 'gudang_id');
    }
    public function beone_satuan_item()
    {
        return $this->belongsTo(\App\Models\BeoneSatuanItem::class, 'satuan_id');
    }
}
