<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTipeKontainer
 * 
 * @property int $ID
 * @property string $KODE_TIPE_KONTAINER
 * @property string $URAIAN_TIPE_KONTAINER
 *
 * @package App\Models
 */
class ReferensiTipeKontainer extends Model
{
	protected $table = 'referensi_tipe_kontainer';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_TIPE_KONTAINER',
		'URAIAN_TIPE_KONTAINER'
	];
}
