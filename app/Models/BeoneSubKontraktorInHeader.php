<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $supplier_id
 * @property int $sub_kontraktor_in_header_id
 * @property string $trans_no
 * @property string $trans_date
 * @property int $currency_id
 * @property float $dpp
 * @property float $ppn
 * @property float $grandtotal
 * @property float $sisabayar
 * @property float $terbayar
 * @property int $jenis_bc
 * @property string $bc_no
 * @property string $bc_date
 * @property string $bc_no_aju
 * @property string $bc_nama_vendor
 * @property string $bc_nama_pemilik
 * @property string $bc_nama_penerima_barang
 * @property string $bc_nama_pengangkut
 * @property string $bc_nama_pengirim
 * @property float $bc_kurs
 * @property int $bc_header_id
 * @property string $keterangan
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property BeoneSupplier $beoneSupplier
 */
class BeoneSubKontraktorInHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_sub_kontraktor_in_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sub_kontraktor_in_header_id';

    /**
     * @var array
     */
    protected $fillable = ['supplier_id', 'trans_no', 'trans_date', 'currency_id', 'dpp','dpp_sys', 'ppn','ppn_sys', 'grandtotal','grandtotal_sys', 'sisabayar', 'terbayar', 'jenis_bc', 'bc_no', 'bc_date', 'bc_no_aju', 'bc_nama_vendor', 'bc_nama_pemilik', 'bc_nama_penerima_barang', 'bc_nama_pengangkut', 'bc_nama_pengirim', 'bc_kurs', 'bc_header_id', 'sub_kontraktor_out_header_id','keterangan', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beone_supplier()
    {
        return $this->belongsTo(\App\Models\BeoneSupplier::class, 'supplier_id', 'supplier_id');
    }

    public function beone_currency()
    {
        return $this->belongsTo(\App\Models\BeoneCurrency::class, 'currency_id');
    }

    public function beone_sub_kontraktor_out_header()
    {
        return $this->belongsTo(\App\Models\BeoneSubKontraktorOutHeader::class, 'sub_kontraktor_out_header_id', 'sub_kontraktor_out_header_id');
    }

    public function beone_sub_kontraktor_in_details()
    {
        return $this->hasMany(\App\Models\BeoneSubKontraktorInDetail::class, 'sub_kontraktor_in_header_id');
    }
}
