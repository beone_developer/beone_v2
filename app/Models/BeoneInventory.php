<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $intvent_trans_id
 * @property string $intvent_trans_no
 * @property int $item_id
 * @property string $trans_date
 * @property string $keterangan
 * @property float $qty_in
 * @property float $value_in
 * @property float $qty_out
 * @property float $value_out
 * @property float $sa_qty
 * @property float $sa_unit_price
 * @property float $sa_amount
 * @property int $flag
 * @property int $update_by
 * @property string $update_date
 * @property int $gudang_id
 */
class BeoneInventory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_inventory';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'intvent_trans_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['intvent_trans_no', 'item_id', 'trans_date', 'keterangan', 'qty_in', 'value_in', 'qty_out', 'value_out', 'sa_qty', 'sa_unit_price', 'sa_amount', 'flag', 'update_by', 'update_date', 'gudang_id'];

    public function beone_item()
    {
        return $this->belongsTo(\App\Models\BeoneItem::class, 'item_id');
    }

}
