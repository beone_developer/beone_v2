<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiFilterKomunikasi
 * 
 * @property int $ID
 * @property string $KODE_FILTER
 * @property string $URAIAN_FILTER
 *
 * @package App\Models
 */
class ReferensiFilterKomunikasi extends Model
{
	protected $table = 'referensi_filter_komunikasi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_FILTER',
		'URAIAN_FILTER'
	];
}
