<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $role_id
 * @property string $nama_role
 * @property string $keterangan
 * @property int $modul_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class BeoneRolesUser extends Model
{
    public $timestamps = false;
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    protected $table = 'beone_roles_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'role_id';

    /**
     * @var array
     */
    protected $fillable = ['nama_role', 'keterangan', 'modul_id', 'deleted_at', 'created_at', 'updated_at'];

    public function beone_moduls()
    {
        return $this->belongsToMany(\App\Models\BeoneModul::class, 'beone_roles_modul', 'role_id', 'modul_id')->where('beone_modul.is_hidden', false)->withPivot('role_modul_id', 'parent_id')->orderBy('parent_id', 'desc')->orderBy('modul_id', 'asc');
    }

    public function beone_users()
    {
        return $this->hasMany(\App\Models\BeoneUser::class);
    }

    public function beone_roles_moduls()
    {
        return $this->hasMany(\App\Models\BeoneRolesModul::class, 'role_id');
    }

    public $appends = ['id', 'text'];

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->role_id,
            'text' => $this->nama_role . " (" . $this->keterangan . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->nama_role . " (" . $this->keterangan . ")";
    }
}
