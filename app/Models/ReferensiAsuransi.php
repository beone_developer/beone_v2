<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiAsuransi
 * 
 * @property int $ID
 * @property string $KODE_ASURANSI
 * @property string $URAIAN_ASURANSI
 *
 * @package App\Models
 */
class ReferensiAsuransi extends Model
{
	protected $table = 'referensi_asuransi';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_ASURANSI',
		'URAIAN_ASURANSI'
	];
}
