<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiSkemaTarif
 * 
 * @property int $ID
 * @property string $KODE_SKEMA
 * @property string $URAIAN_SKEMA
 *
 * @package App\Models
 */
class ReferensiSkemaTarif extends Model
{
	protected $table = 'referensi_skema_tarif';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_SKEMA',
		'URAIAN_SKEMA'
	];
}
