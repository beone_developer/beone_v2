<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisKendaraan
 * 
 * @property int $ID
 * @property string $KODE_JENIS_KENDARAAN
 * @property string $URAIAN_JENIS_KENDARAAN
 *
 * @package App\Models
 */
class ReferensiJenisKendaraan extends Model
{
	protected $table = 'referensi_jenis_kendaraan';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_KENDARAAN',
		'URAIAN_JENIS_KENDARAAN'
	];
}
