<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiKantorPabean
 * 
 * @property int $ID
 * @property string $KODE_KANTOR
 * @property string $URAIAN_KANTOR
 *
 * @package App\Models
 */
class ReferensiKantorPabean extends Model
{
	protected $table = 'referensi_kantor_pabean';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_KANTOR',
		'URAIAN_KANTOR'
	];
}
