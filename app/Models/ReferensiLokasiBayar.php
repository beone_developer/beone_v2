<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiLokasiBayar
 * 
 * @property int $ID
 * @property string $KODE_LOKASI_BAYAR
 * @property string $URAIAN_LOKASI_BAYAR
 *
 * @package App\Models
 */
class ReferensiLokasiBayar extends Model
{
	protected $table = 'referensi_lokasi_bayar';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_LOKASI_BAYAR',
		'URAIAN_LOKASI_BAYAR'
	];
}
