<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $opname_header_id
 * @property string $document_no
 * @property string $opname_date
 * @property int $total_item_opname
 * @property int $total_item_opname_match
 * @property int $total_item_opname_plus
 * @property int $total_item_opname_min
 * @property int $update_by
 * @property int $flag
 * @property string $keterangan
 * @property string $update_date
 */
class BeoneOpnameHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_opname_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'opname_header_id';

    /**
     * @var array
     */
    protected $fillable = ['document_no', 'opname_date', 'total_item_opname', 'total_item_opname_match', 'total_item_opname_plus', 'total_item_opname_min', 'update_by', 'flag', 'keterangan', 'update_date'];

}
