<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $import_header_id
 * @property int $jenis_bc
 * @property string $car_no
 * @property string $bc_no
 * @property string $bc_date
 * @property string $invoice_no
 * @property string $invoice_date
 * @property string $kontrak_no
 * @property string $kontrak_date
 * @property string $purpose_id
 * @property int $supplier_id
 * @property string $price_type
 * @property string $from
 * @property float $amount_value
 * @property float $valas_value
 * @property float $value_added
 * @property float $discount
 * @property string $insurance_type
 * @property float $insurace_value
 * @property float $freight
 * @property int $flag
 * @property int $update_by
 * @property string $update_date
 * @property int $status
 * @property string $receive_date
 * @property string $receive_no
 * @property string $import_no
 * @property string $trans_date
 * @property string $bc_no_aju
 * @property string $bc_nama_pamasok
 */
class BeoneImportHeader extends Model
{
    use SoftDeletes;
    protected $dateFormat = 'Y-m-d H:i:s.uO';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_import_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'import_header_id';

    /**
     * @var array
     */
    protected $fillable = ['jenis_bc', 'car_no', 'bc_no', 'bc_date', 'invoice_no', 'invoice_date', 'kontrak_no', 'kontrak_date', 'purpose_id', 'supplier_id', 'price_type', 'from', 'amount_value', 'valas_value', 'value_added', 'discount', 'insurance_type', 'insurace_value', 'freight', 'flag', 'update_by', 'update_date', 'status', 'receive_date', 'receive_no', 'import_no', 'trans_date', 'bc_no_aju', 'bc_nama_vendor', 'bc_nama_pemilik', 'bc_nama_penerima_barang', 'bc_nama_pengangkut', 'bc_nama_pengirim', 'purchase_header_id', 'bc_header_id','terbayar'];

    public function beone_import_details()
    {
        return $this->hasMany(\App\Models\BeoneImportDetail::class, 'import_header_id');
    }

    public function beone_purchase_order()
    {
        return $this->belongsTo(\App\Models\BeonePoImportHeader::class, 'purchase_header_id');
    }

    public function beone_supplier()
    {
        return $this->belongsTo(\App\Models\BeoneSupplier::class, 'supplier_id');
    }

    public function beone_currency()
    {
        return $this->belongsTo(\App\Models\BeoneCurrency::class, 'currency_id');
    }

    public function tpb_header()
    {
        return $this->belongsTo(\App\Models\TpbHeader::class, 'bc_header_id', 'ID');
    }

    public $appends = ['gudang', 'id', 'text'];

    public function getGudangAttribute()
    {
        if ($this->beone_import_details->first()){
            return BeoneGudang::find($this->beone_import_details->first()->gudang_id);
        }
        return '';
    }

    function getIdAttribute()
    {
        $ar = [
            'id' => $this->import_header_id,
            'text' => $this->import_no . " (" . $this->receive_no . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->import_no . " (" . $this->receive_no . ")";
    }
}
