<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbDetilStatus
 * 
 * @property int $ID
 * @property string $KODE_STATUS
 * @property Carbon $WAKTU_STATUS
 * @property int $ID_HEADER
 * 
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbDetilStatus extends Model
{
	protected $table = 'tpb_detil_status';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_HEADER' => 'int'
	];

	protected $dates = [
		'WAKTU_STATUS'
	];

	protected $fillable = [
		'KODE_STATUS',
		'WAKTU_STATUS',
		'ID_HEADER'
	];

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
