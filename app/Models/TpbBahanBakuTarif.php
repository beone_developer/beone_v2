<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TpbBahanBakuTarif
 * 
 * @property int $ID
 * @property string $JENIS_TARIF
 * @property float $JUMLAH_SATUAN
 * @property int $KODE_ASAL_BAHAN_BAKU
 * @property string $KODE_FASILITAS
 * @property string $KODE_KOMODITI_CUKAI
 * @property string $KODE_SATUAN
 * @property string $KODE_TARIF
 * @property float $NILAI_BAYAR
 * @property float $NILAI_FASILITAS
 * @property float $NILAI_SUDAH_DILUNASI
 * @property int $SERI_BAHAN_BAKU
 * @property int $SERI_BARANG
 * @property float $TARIF
 * @property float $TARIF_FASILITAS
 * @property int $ID_BAHAN_BAKU
 * @property int $ID_BARANG
 * @property int $ID_HEADER
 * 
 * @property TpbBahanBaku $tpb_bahan_baku
 * @property TpbBarang $tpb_barang
 * @property TpbHeader $tpb_header
 *
 * @package App\Models
 */
class TpbBahanBakuTarif extends Model
{
	protected $table = 'tpb_bahan_baku_tarif';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'JUMLAH_SATUAN' => 'float',
		'KODE_ASAL_BAHAN_BAKU' => 'int',
		'NILAI_BAYAR' => 'float',
		'NILAI_FASILITAS' => 'float',
		'NILAI_SUDAH_DILUNASI' => 'float',
		'SERI_BAHAN_BAKU' => 'int',
		'SERI_BARANG' => 'int',
		'TARIF' => 'float',
		'TARIF_FASILITAS' => 'float',
		'ID_BAHAN_BAKU' => 'int',
		'ID_BARANG' => 'int',
		'ID_HEADER' => 'int'
	];

	protected $fillable = [
		'JENIS_TARIF',
		'JUMLAH_SATUAN',
		'KODE_ASAL_BAHAN_BAKU',
		'KODE_FASILITAS',
		'KODE_KOMODITI_CUKAI',
		'KODE_SATUAN',
		'KODE_TARIF',
		'NILAI_BAYAR',
		'NILAI_FASILITAS',
		'NILAI_SUDAH_DILUNASI',
		'SERI_BAHAN_BAKU',
		'SERI_BARANG',
		'TARIF',
		'TARIF_FASILITAS',
		'ID_BAHAN_BAKU',
		'ID_BARANG',
		'ID_HEADER'
	];

	public function tpb_bahan_baku()
	{
		return $this->belongsTo(TpbBahanBaku::class, 'ID_BAHAN_BAKU');
	}

	public function tpb_barang()
	{
		return $this->belongsTo(TpbBarang::class, 'ID_BARANG');
	}

	public function tpb_header()
	{
		return $this->belongsTo(TpbHeader::class, 'ID_HEADER');
	}
}
