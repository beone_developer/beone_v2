<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $voucher_detail_id
 * @property int $voucher_header_id
 * @property int $coa_id_lawan
 * @property string $coa_no_lawan
 * @property float $jumlah_valas
 * @property float $kurs
 * @property float $jumlah_idr
 * @property string $keterangan_detail
 */
class BeoneVoucherDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_voucher_detail';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'voucher_detail_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['voucher_header_id', 'coa_id_lawan', 'coa_no_lawan', 'jumlah_valas', 'kurs', 'jumlah_idr', 'keterangan_detail'];

    public function beone_coa()
    {
        return $this->belongsTo(\App\Models\BeoneCoa::class, 'coa_id_lawan');
    }
}
