<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $piutang_detail_id
 * @property int $piutang_header_id
 * @property int $export_header_id
 * @property float $amount
 */
class BeonePiutangDetail extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'beone_piutang_detail';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'piutang_detail_id';

    /**
     * @var array
     */
    protected $fillable = ['piutang_header_id', 'export_header_id', 'amount'];
    
    public $appends = ['invoice_no'];


    public function beone_export_header()
    {
        return $this->belongsTo(\App\Models\BeoneExportHeader::class, 'export_header_id');
    }

    function getInvoiceNoAttribute()
    {
        return $this->beone_export_header->invoice_no;
    }
}
