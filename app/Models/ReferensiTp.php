<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiTp
 * 
 * @property int $ID
 * @property string $FL_AKTIF
 * @property string $JNS_GUDANG
 * @property string $KD_KANTOR
 * @property string $KODE_TPS
 * @property string $URAIAN_TPS
 *
 * @package App\Models
 */
class ReferensiTp extends Model
{
	protected $table = 'referensi_tps';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'FL_AKTIF',
		'JNS_GUDANG',
		'KD_KANTOR',
		'KODE_TPS',
		'URAIAN_TPS'
	];
}
