<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiRespon
 * 
 * @property int $ID
 * @property string $KODE_RESPON
 * @property string $URAIAN_RESPON
 *
 * @package App\Models
 */
class ReferensiRespon extends Model
{
	protected $table = 'referensi_respon';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_RESPON',
		'URAIAN_RESPON'
	];
}
