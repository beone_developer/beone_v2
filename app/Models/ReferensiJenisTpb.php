<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisTpb
 * 
 * @property int $ID
 * @property string $KODE_JENIS_TPB
 * @property string $URAIAN_JENIS_TPB
 *
 * @package App\Models
 */
class ReferensiJenisTpb extends Model
{
	protected $table = 'referensi_jenis_tpb';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_TPB',
		'URAIAN_JENIS_TPB'
	];
}
