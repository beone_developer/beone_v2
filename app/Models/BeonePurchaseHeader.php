<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $purchase_header_id
 * @property string $purchase_no
 * @property string $trans_date
 * @property int $supplier_id
 * @property string $keterangan
 * @property int $update_by
 * @property string $update_date
 * @property int $flag
 * @property int $ppn
 * @property float $subtotal
 * @property float $ppn_value
 * @property float $grandtotal
 * @property int $realisasi
 */
class BeonePurchaseHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beone_purchase_header';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'purchase_header_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['purchase_no', 'trans_date', 'supplier_id', 'keterangan', 'update_by', 'update_date', 'flag', 'ppn', 'subtotal', 'ppn_value', 'grandtotal', 'realisasi'];

}
