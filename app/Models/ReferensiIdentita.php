<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiIdentita
 * 
 * @property int $ID
 * @property string $KODE_IDENTITAS
 * @property string $URAIAN_IDENTITAS
 *
 * @package App\Models
 */
class ReferensiIdentita extends Model
{
	protected $table = 'referensi_identitas';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_IDENTITAS',
		'URAIAN_IDENTITAS'
	];
}
