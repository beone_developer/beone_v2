<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $kode_trans_id
 * @property string $nama
 * @property int $coa_id
 * @property string $kode_bank
 * @property int $in_out
 * @property int $flag
 */
class BeoneKodeTran extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'kode_trans_id';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'coa_id', 'kode_bank', 'in_out', 'flag'];

}
