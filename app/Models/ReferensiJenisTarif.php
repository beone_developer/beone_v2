<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferensiJenisTarif
 * 
 * @property int $ID
 * @property string $KODE_JENIS_TARIF
 * @property string $URAIAN_JENIS_TARIF
 *
 * @package App\Models
 */
class ReferensiJenisTarif extends Model
{
	protected $table = 'referensi_jenis_tarif';
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'KODE_JENIS_TARIF',
		'URAIAN_JENIS_TARIF'
	];
}
